<?php

    namespace app\controllers;
    use Yii;
    use app\models\LogHistory;
    use app\models\AuthAssignment;

    function pre($arr){
        echo '<pre>'.print_r($arr, true).'</pre>';
        die();
    }

    function eventUser($username, $event_time, $event_id, $description){
        $model = new LogHistory();
        $model->username = $username;
        $model->event_time = $event_time;
        $model->event_id = $event_id;
        $model->description = $description;
        $model->save();
    }

    function numberZero($number){
        $count = strlen($number);
        $for = 7 - $count;
        $null = "";
        for ($i = 0; $i < $for; $i++){
            $null .= 0;
        }
        return $null.$number;
    }

    function getRole()
    {
        $role = AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        return $role;
    }

?>