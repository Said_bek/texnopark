<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "machines".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $status
 */
class Machines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'machines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'default', 'value' => null],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Stanok nomi',
            'status' => 'Status',
        ];
    }

    public function getPrice()
    {
        return $this->hasOne(MachinesPrice::classname(), ['machine_id' => 'id'])->orderBy(['id' => SORT_DESC]);
    }
}
