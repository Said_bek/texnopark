<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "materials".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $number
 * @property int|null $status
 * @property int|null $code_number
 * @property string|null $code_name
 */
class Materials extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'materials';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'code_name'], 'default', 'value' => null],
            [['status', 'code_number'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['number'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nomi',
            'number' => 'Raqami',
            'status' => 'Holati',
            'code_number' => 'Code',
            'code_name' => 'Code Name'
        ];
    }

    public function getPara()
    {
        return $this->hasOne(MaterialParameter::classname(), ['material_id' => 'id']);
    }
}
