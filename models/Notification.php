<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property int $id
 * @property int|null $module_id
 * @property int|null $order_id
 * @property string|null $enter_date
 * @property string|null $end_date
 * @property int|null $status
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['module_id', 'order_id', 'status'], 'default', 'value' => null],
            [['module_id', 'order_id', 'status'], 'integer'],
            [['enter_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module_id' => 'Module ID',
            'order_id' => 'Order ID',
            'enter_date' => 'Enter Date',
            'end_date' => 'End Date',
            'status' => 'Status',
        ];
    }
}
