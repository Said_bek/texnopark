<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_module".
 *
 * @property int $id
 * @property int|null $order_id
 * @property int|null $module_id
 * @property string|null $enter_date
 * @property string|null $exit_date
 */
class OrderModule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_module';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'module_id'], 'default', 'value' => null],
            [['order_id', 'module_id'], 'integer'],
            [['enter_date', 'exit_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'module_id' => 'Module ID',
            'enter_date' => 'Enter Date',
            'exit_date' => 'Exit Date',
        ];
    }
}
