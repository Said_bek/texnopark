<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "material_history".
 *
 * @property int $id
 * @property int|null $material_id
 * @property float|null $quantity
 * @property int|null $nakladnoy_id
 * @property string|null $created_date
 * @property int|null $price
 * @property int|null $mark_id
 */
class MaterialHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_id', 'nakladnoy_id', 'price', 'mark_id'], 'default', 'value' => null],
            [['material_id', 'nakladnoy_id', 'price', 'mark_id'], 'integer'],
            [['quantity'], 'number'],
            [['created_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_id' => 'Material ID',
            'quantity' => 'Quantity',
            'nakladnoy_id' => 'Nakladnoy ID',
            'created_date' => 'Created Date',
            'price' => 'Price',
            'mark_id' => 'Mark ID',
        ];
    }
}
