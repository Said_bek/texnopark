<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "constructor".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $status
 * @property int|null $type
 */
class Constructor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'constructor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'type'], 'default', 'value' => null],
            [['user_id', 'status', 'type'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Ishchi',
            'status' => 'Holati',
            'type' => 'Type',
        ];
    }

    public function getName()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }
}
