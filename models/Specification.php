<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "specification".
 *
 * @property int $id
 * @property int|null $order_id
 * @property string|null $created_date
 * @property string|null $name
 * @property int|null $class
 * @property string|null $img
 * @property int|null $active
 */
class Specification extends \yii\db\ActiveRecord
{
    public $image;
//    public $active;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'specification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'img', 'active'], 'default', 'value' => null],
            [['order_id', 'class', 'active'], 'integer'],
            [['created_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'created_date' => 'Created Date',
            'name' => 'Name',
            'Class' => 'Radio',
            'img' => 'Rasm',
            'active' => 'Status'
        ];
    }

    public function getCode()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }
}
