<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "machines_price".
 *
 * @property int $id
 * @property int|null $machine_id
 * @property float|null $price
 * @property string|null $enter_date
 * @property string|null $end_date
 */
class MachinesPrice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'machines_price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['machine_id'], 'default', 'value' => null],
            [['machine_id'], 'integer'],
            [['price'], 'number'],
            [['enter_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'machine_id' => 'Machine ID',
            'price' => 'Price',
            'enter_date' => 'Enter Date',
            'end_date' => 'End Date',
        ];
    }
}
