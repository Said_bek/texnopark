<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category_docs".
 *
 * @property int $id
 * @property int|null $category_id
 * @property int|null $docs_id
 */
class CategoryDocs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_docs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'docs_id'], 'default', 'value' => null],
            [['category_id', 'docs_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'docs_id' => 'Docs ID',
        ];
    }
}
