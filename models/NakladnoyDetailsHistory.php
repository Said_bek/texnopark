<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nakladnoy_details_history".
 *
 * @property int $id
 * @property int|null $nakladnoy_details_id
 * @property float|null $quantity
 * @property float|null $price
 */
class NakladnoyDetailsHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nakladnoy_details_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nakladnoy_details_id'], 'default', 'value' => null],
            [['nakladnoy_details_id'], 'integer'],
            [['quantity', 'price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nakladnoy_details_id' => 'Nakladnoy Details ID',
            'quantity' => 'Quantity',
            'price' => 'Price',
        ];
    }
}
