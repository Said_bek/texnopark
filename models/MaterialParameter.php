<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "material_parameter".
 *
 * @property int $id
 * @property int|null $material_id
 * @property int|null $parameter_id
 */
class MaterialParameter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material_parameter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_id', 'parameter_id'], 'default', 'value' => null],
            [['material_id', 'parameter_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_id' => 'Material ID',
            'parameter_id' => 'Parameter ID',
        ];
    }

    public function getParaName()
    {
        return $this->hasOne(Parameter::classname(), ['id' => 'parameter_id']);
    }
}
