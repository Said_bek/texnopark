<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_pause".
 *
 * @property int $id
 * @property int|null $order_id
 * @property int|null $section_id
 * @property string|null $start_date
 * @property string|null $end_date
 * @property int|null $status
 * @property int|null $machine_id
 * @property float|null machine_price
 * @property int|null $detail_id
 */
class OrderPause extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_pause';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'section_id', 'status', 'machine_price', 'machine_id'], 'default', 'value' => null],
            [['order_id', 'section_id', 'status', 'machine_id'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'section_id' => 'Section ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'status' => 'Status',
        ];
    }
}
