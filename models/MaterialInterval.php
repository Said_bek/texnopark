<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "material_interval".
 *
 * @property int $id
 * @property int|null $material_id
 * @property float|null $total
 * @property string|null $enter_date
 * @property int|null $mark_id
 */
class MaterialInterval extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material_interval';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_id', 'mark_id'], 'default', 'value' => null],
            [['material_id', 'mark_id'], 'integer'],
            [['total'], 'number'],
            [['enter_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_id' => 'Material ID',
            'total' => 'Total',
            'enter_date' => 'Enter Date',
            'mark_id' => 'Mark ID',
        ];
    }
}
