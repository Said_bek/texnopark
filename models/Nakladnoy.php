<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nakladnoy".
 *
 * @property int $id
 * @property string|null $code
 * @property string|null $enter_date
 * @property string|null $created_date
 * @property string|null $end_date
 * @property int|null $supplier_id
 * @property int|null $status
 */
class Nakladnoy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nakladnoy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['enter_date', 'created_date'], 'safe'],
            [['supplier_id'], 'default', 'value' => null],
            [['supplier_id'], 'integer'],
            [['code'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Nakladnoy kodi',
            'enter_date' => 'Enter Date',
            'supplier_id' => 'Supplier ID',
            'created_date' => 'Date',
        ];
    }

    public function getName()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }
}
