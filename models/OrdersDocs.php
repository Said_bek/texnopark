<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders_docs".
 *
 * @property int $id
 * @property int|null $order_id
 * @property int|null $docs_id
 */
class OrdersDocs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_docs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'docs_id'], 'default', 'value' => null],
            [['order_id', 'docs_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'docs_id' => 'Docs ID',
        ];
    }
}
