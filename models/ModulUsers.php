<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "modul_users".
 *
 * @property int $id
 * @property int|null $modul_id
 * @property int|null $user_id
 */
class ModulUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'modul_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modul_id', 'user_id'], 'default', 'value' => null],
            [['modul_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'modul_id' => 'Modul ID',
            'user_id' => 'User ID',
        ];
    }
}
