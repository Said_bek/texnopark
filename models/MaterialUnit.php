<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "material_unit".
 *
 * @property int $id
 * @property int|null $material_id
 * @property int|null $unit_id
 */
class MaterialUnit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material_unit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_id', 'unit_id'], 'default', 'value' => null],
            [['material_id', 'unit_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_id' => 'Material ID',
            'unit_id' => 'Unit ID',
        ];
    }
}
