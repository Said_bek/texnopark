<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_section_time".
 *
 * @property int $id
 * @property int|null $section_id
 * @property int|null $order_id
 * @property string|null $enter_date
 * @property string|null $exit_date
 * @property int|null $order_step_id
 * @property int|null $machine_id
 * @property float|null machine_price
 * @property int|null $detail_id
 * @property int|null $status
 */
class OrderSectionTime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_section_time';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['section_id', 'order_id', 'order_step_id', 'machine_id', 'machine_price'], 'default', 'value' => null],
            [['section_id', 'order_id', 'order_step_id', 'machine_id'], 'integer'],
            [['enter_date', 'exit_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'section_id' => 'Section ID',
            'machine_id' => 'Machine ID',
            'order_id' => 'Order ID',
            'enter_date' => 'Enter Date',
            'exit_date' => 'Exit Date',
            'order_step_id' => 'Order Step ID',
        ];
    }

    public function getOrders()
    {
        return $this->hasOne(Orders::class, ['id' => 'order_id']);
    }
    
    public function getSection()
    {
        return $this->hasOne(Section::class, ['id' => 'section_id']);
    }

    public function getMachine()
    {
        return $this->hasOne(Machines::class, ['id' => 'machine_id']);
    }

    public function getOrderStep()
    {
        return $this->hasOne(OrderStep::class, ['id' => 'order_step_id']);
    }
}
