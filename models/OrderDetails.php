<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_details".
 *
 * @property int $id
 * @property int|null $order_id
 * @property int|null $material_id
 * @property int|null $mark_id
 * @property int|null $specification_id
 * @property float|null $width
 * @property float|null $height
 * @property float|null $weight
 * @property float|null $w_approx
 * @property float|null $h_approx
 * @property float|null $we_approx
 * @property int|null $count
 * @property string|null $terma
 * @property string|null $code
 */
class OrderDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'material_id', 'mark_id', 'code', 'specification_id', 'count'], 'default', 'value' => null],
            [['order_id', 'material_id', 'mark_id', 'specification_id', 'count'], 'integer'],
            [['width', 'height', 'weight', 'w_approx', 'h_approx', 'we_approx'], 'number'],
            [['terma'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'material_id' => 'Material ID',
            'mark_id' => 'Mark ID',
            'specification_id' => 'Specification ID',
            'width' => 'Width',
            'height' => 'Height',
            'weight' => 'Weight',
            'w_approx' => 'W Approx',
            'h_approx' => 'H Approx',
            'we_approx' => 'We Approx',
            'count' => 'Count',
            'terma' => 'Terma',
        ];
    }
}
