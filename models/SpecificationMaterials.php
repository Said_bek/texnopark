<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "specification_materials".
 *
 * @property int $id
 * @property int|null $material_id
 * @property int|null $mark_id
 * @property int|null $specification_id
 * @property int|null $count
 * @property string|null $terma
 * @property float|null $width
 * @property float|null $height
 * @property float|null $weight
 * @property float|null $w_approx
 * @property float|null $h_approx
 * @property float|null $we_approx
 * @property string|null $code
 */
class SpecificationMaterials extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'specification_materials';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_id', 'specification_id', 'code', 'count', 'mark_id', 'terma'], 'default', 'value' => null],
            [['material_id', 'specification_id', 'count', 'mark_id'], 'integer'],
            [['width', 'height', 'weight', 'w_approx', 'h_approx', 'we_approx'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_id' => 'Material ID',
            'mark_id' => 'Mark ID',
            'specification_id' => 'Specification ID',
            'count' => 'Count',
            'terma' => 'Terma',
            'width' => 'Width',
            'height' => 'Height',
            'weight' => 'Weight',
            'w_approx' => 'W Approx',
            'h_approx' => 'H Approx',
            'we_approx' => 'We Approx',
        ];
    }

    public function getMaterial()
    {
        return $this->hasOne(Materials::className(), ['id' => 'material_id']);
    }

    public function getMark()
    {
        return $this->hasOne(Marks::className(), ['id' => 'mark_id']);
    }
}
