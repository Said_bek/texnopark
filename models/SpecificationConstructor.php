<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "specification_constructor".
 *
 * @property int $id
 * @property int|null $specification_id
 * @property int|null $constructor_id
 * @property string|null $enter_date
 * @property string|null $deadline
 */
class SpecificationConstructor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'specification_constructor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['specification_id', 'constructor_id'], 'default', 'value' => null],
            [['specification_id', 'constructor_id'], 'integer'],
            [['enter_date', 'deadline'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'specification_id' => 'Specification ID',
            'constructor_id' => 'Constructor ID',
            'enter_date' => 'Enter Date',
            'deadline' => 'Deadline',
        ];
    }
}
