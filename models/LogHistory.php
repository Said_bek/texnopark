<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_history".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $event_time
 * @property int|null $event_id
 * @property string|null $description
 */
class LogHistory extends \yii\db\ActiveRecord
{
    CONST TYPE_1 = "Qo'shish";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_time'], 'safe'],
            [['event_id', 'description'], 'default', 'value' => null],
            [['event_id'], 'integer'],
            [['username'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Foydalanuvchi',
            'event_time' => 'Event Time',
            'event_id' => 'Event ID',
        ];
    }
}
