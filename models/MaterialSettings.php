<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "material_settings".
 *
 * @property int $id
 * @property int|null $material_id
 * @property float|null $width_f
 * @property float|null $width_s
 * @property float|null $height_f
 * @property float|null $height_s
 * @property float|null $weight_f
 * @property float|null $weight_s
 * @property float|null $w_approx
 * @property float|null $h_approx
 * @property float|null $we_approx
 */
class MaterialSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_id'], 'default', 'value' => null],
            [['material_id'], 'integer'],
            [['width_f', 'width_s', 'height_f', 'height_s', 'weight_f', 'weight_s', 'w_approx', 'h_approx', 'we_approx'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_id' => 'Material ID',
            'width_f' => 'Width F',
            'width_s' => 'Width S',
            'height_f' => 'Height F',
            'height_s' => 'Height S',
            'weight_f' => 'Weight F',
            'weight_s' => 'Weight S',
            'w_approx' => 'W Approx',
            'h_approx' => 'H Approx',
            'we_approx' => 'We Approx',
        ];
    }
}
