<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $password
 * @property string|null $phone_number
 * @property int|null $type
 * @property int|null $status
 */

class Users extends \yii\db\ActiveRecord
{
    const Admin = 1;
    const Manager = 2;
    const Seller = 3;
    const Master = 4;
    const Opertor = 5;
    const Programmer = 6;
    const Worker = 7;
    const Logist = 8;
    const Counter = 9;
    const OTK = 10;
    const Head_Constructor = 11;
    const Constructor = 12;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', ], 'required'],
            [['type', 'status'], 'default', 'value' => null],
            [['type', 'status'], 'integer'],
            [['username', 'password'], 'string', 'max' => 255],
            [['phone_number'], 'string', 'max' => 13],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Foydalanuchi Ismi',
            'password' => 'Parol',
            'phone_number' => 'Telefon raqami',
            'type' => 'Rol',
            'status' => 'Status',
        ];
    }

    public function getAllRole()
    {
        $data = [
            1 => 'Admin',
            2 => 'Manager',
            3 => 'Seller',
            4 => 'Master',
            5 => 'Opertor',
            6 => 'Programmer',
            7 => 'Worker',
            8 => 'Logist',
            9 => 'Counter',
            10 => 'OTK',
            11 => 'Head Constructor',
            12 => 'Constructor',
            13 => 'Supplier'
        ];

        return $data;
    }

    public function getRoles()
    {
        return $this->hasMany(AuthAssignment::class, ['user_id' => 'id']);
    }
}