<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "material_marks".
 *
 * @property int $id
 * @property int|null $material_id
 * @property int|null $mark_id
 * @property int|null $status
 */
class MaterialMarks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material_marks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_id', 'mark_id', 'status'], 'default', 'value' => null],
            [['material_id', 'mark_id', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_id' => 'Material ID',
            'mark_id' => 'Mark ID',
            'status' => 'Status',
        ];
    }

    public function getMark()
    {
        return $this->hasOne(Marks::classname(), ['id' => 'mark_id']);
    }
}
