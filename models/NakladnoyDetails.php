<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nakladnoy_details".
 *
 * @property int $id
 * @property int|null $nakladnoy_id
 * @property int|null $material_id
 * @property int|null $mark_id
 * @property float|null $quantity
 */
class NakladnoyDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nakladnoy_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nakladnoy_id', 'material_id', 'mark_id'], 'default', 'value' => null],
            [['nakladnoy_id', 'material_id', 'mark_id'], 'integer'],
            [['quantity'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nakladnoy_id' => 'Nakladnoy ID',
            'material_id' => 'Material ID',
            'mark_id' => 'Mark ID',
            'quantity' => 'Quantity',
        ];
    }
}
