<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "section_conductive".
 *
 * @property int $id
 * @property int|null $section_id
 * @property int|null $user_id
 */
class SectionConductive extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'section_conductive';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['section_id', 'user_id'], 'default', 'value' => null],
            [['section_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'section_id' => 'Section ID',
            'user_id' => 'User ID',
        ];
    }
}
