<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $created_date
 * @property string|null $exit_date
 * @property int|null $status
 * @property int|null $order_type
 * @property int|null $code
 * @property int|null $client_id
 * @property int|null $price
 * @property int|null $category_id
 * @property string|null $order_name
 * @property int|null $order_from
 * @property int|null $proirity
 * @property string|null $deadline_price
 * @property int|null $approved
 * @property int|null $specification_status
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_date', 'exit_date', 'deadline_price'], 'safe'],
            [['status', 'specification_status', 'order_type', 'client_id', 'price', 'category_id', 'order_from', 'proirity', 'approved'], 'default', 'value' => null],
            [['status', 'specification_status', 'client_id', 'price', 'category_id', 'order_from', 'proirity', 'approved', 'code'], 'integer'],
            [['order_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_date' => 'Created Date',
            'exit_date' => 'Exit Date',
            'status' => 'Status',
            'client_id' => 'Client ID',
            'price' => 'Price',
            'category_id' => 'Category ID',
            'order_name' => 'Order Name',
            'order_from' => 'Order From',
            'proirity' => 'Proirity',
            'deadline_price' => 'Deadline Price',
            'approved' => 'Approved',
            'code' => 'Code',
        ];
    }

    public function getClients()
    {
        return $this->hasOne(Clients::class, ['id' => 'client_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
