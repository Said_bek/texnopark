<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property string|null $face_name
 * @property string|null $phone_number
 * @property string|null $phone_number_2
 * @property string|null $company_name
 * @property string|null $country
 * @property string|null $email
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['face_name', 'phone_number', 'phone_number_2', 'company_name', 'country', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'face_name' => 'Face Name',
            'phone_number' => 'Phone Number',
            'phone_number_2' => 'Phone Number 2',
            'company_name' => 'Company Name',
            'country' => 'Country',
            'email' => 'Email',
        ];
    }
}
