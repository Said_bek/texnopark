<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_step".
 *
 * @property int $id
 * @property int|null $order_id
 * @property int|null $section_id
 * @property string|null $deadline
 * @property string|null $start_date
 * @property int|null $work_time
 * @property int|null $machine_id
 * @property int|null $detail_id
 * @property int|null $step
 * @property float|null machine_price
 */
class OrderStep extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_step';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'section_id', 'work_time', 'machine_id', 'machine_price'], 'default', 'value' => null],
            [['order_id', 'section_id', 'work_time', 'machine_id'], 'integer'],
            [['deadline', 'start_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'section_id' => 'Section ID',
            'deadline' => 'Deadline',
            'start_date' => 'Start Date',
            'work_time' => 'Work Time',
            'machine_id' => 'Machine ID',
        ];
    }

    public function getOrderSectionTime()
    {
        return $this->hasOne(OrderSectionTime::class, ['order_step_id' => 'id']);
    }

    public function getSection()
    {
        return $this->hasOne(Section::class, ['id' => 'section_id']);
    }

    public function getMachine()
    {
        return $this->hasOne(Machines::class, ['id' => 'machine_id']);
    }
}
