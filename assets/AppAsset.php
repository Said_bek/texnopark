<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/site.css',
         "/assets/images/apple-touch-icon.png",
         "/assets/images/favicon.ico",


        "/global/css/bootstrap.min.css",
        "/global/css/bootstrap-extend.min.css",
        "/assets/css/site.min.css",


        "/global/vendor/animsition/animsition.css",
        "/global/vendor/asscrollable/asScrollable.css",
        "/global/vendor/switchery/switchery.css",
        "/global/vendor/intro-js/introjs.css",
        "/global/vendor/slidepanel/slidePanel.css",
        "/global/vendor/flag-icon-css/flag-icon.css",
        "/global/vendor/jquery-wizard/jquery-wizard.css",
        "/global/vendor/formvalidation/formValidation.css",
//        "/global/vendor/chartist/chartist.css",
        "/global/vendor/jvectormap/jquery-jvectormap.css",
//        "/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css",
        "/assets/examples/css/dashboard/v1.css",
        "/global/vendor/clockpicker/clockpicker.css",


        "/global/fonts/weather-icons/weather-icons.css",
        "/global/fonts/web-icons/web-icons.min.css",
        "/global/fonts/brand-icons/brand-icons.min.css",
        'http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic',

        // "/global/vendor/footable/footable.core.css",
        "/assets/examples/css/tables/footable.css",
        '/global/vendor/alertify/alertify.css',
        '/global/vendor/notie/notie.css',
        '/assets/examples/css/advanced/alertify.css',

        "/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css",
        "/global/vendor/bootstrap-maxlength/bootstrap-maxlength.css",
        "/global/vendor/timepicker/jquery-timepicker.css",


        "/global/vendor/select2/select2.css",
        "/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css",
        "/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css",
        "/global/vendor/bootstrap-select/bootstrap-select.css",
        "/assets/examples/css/forms/advanced.css",

        "/global/fonts/font-awesome/font-awesome.css",
        "/global/vendor/tablesaw/tablesaw.css",

        "/global/vendor/footable/footable.core.css",
        "/assets/examples/css/tables/footable.css",

        "/global/vendor/ascolorpicker/asColorPicker.css",

        "/global/vendor/switchery/switchery.css",


    ];


    public $js = [
        /*"/global/vendor/html5shiv/html5shiv.min.js",
        "/global/vendor/media-match/media.match.min.js",
        '/global/vendor/respond/respond.min.js',
        '/global/vendor/breakpoints/breakpoints.js'*/
        "/global/vendor/babel-external-helpers/babel-external-helpers.js",
        "/global/vendor/jquery/jquery.js",
        "/global/vendor/popper-js/umd/popper.min.js",
        "/global/vendor/bootstrap/bootstrap.js",
        "/global/vendor/animsition/animsition.js",
        "/global/vendor/mousewheel/jquery.mousewheel.js",
        "/global/vendor/asscrollbar/jquery-asScrollbar.js",
        "/global/vendor/asscrollable/jquery-asScrollable.js",
        "/global/vendor/ashoverscroll/jquery-asHoverScroll.js",

        "/global/vendor/switchery/switchery.js",
        "/global/vendor/intro-js/intro.js",
        "/global/vendor/screenfull/screenfull.js",
        "/global/vendor/slidepanel/jquery-slidePanel.js",
        "/global/vendor/formvalidation/formValidation.js",
        "/global/vendor/formvalidation/framework/bootstrap.js",
        "/global/vendor/matchheight/jquery.matchHeight-min.js",
        "/global/vendor/jquery-wizard/jquery-wizard.js",
        "/global/vendor/skycons/skycons.js",


//       "/global/vendor/chartist/chartist.min.js",
//       "/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js",

        "/global/vendor/aspieprogress/jquery-asPieProgress.min.js",
        "/global/vendor/jvectormap/jquery-jvectormap.min.js",
        "/global/vendor/jvectormap/maps/jquery-jvectormap-au-mill-en.js",

        "/global/js/Component.js",
        "/global/js/Plugin.js",
        "/global/js/Base.js",
        "/global/js/Config.js",

        "/assets/js/Section/Menubar.js",
        "/assets/js/Section/GridMenu.js",
        "/assets/js/Section/Sidebar.js",
        "/assets/js/Section/PageAside.js",
        "/assets/js/Plugin/menu.js",

        "/global/js/config/colors.js",
        "/assets/js/config/tour.js",

        "/assets/js/Site.js",
        "/global/js/Plugin/asscrollable.js",
        "/global/js/Plugin/slidepanel.js",
        "/global/js/Plugin/switchery.js",
        "/global/js/Plugin/jquery-wizard.js",
        "/global/js/Plugin/matchheight.js",
        "/global/js/Plugin/jvectormap.js",

        "/assets/examples/js/forms/wizard.js",


        "/assets/examples/js/dashboard/v1.js",

        "/global/vendor/moment/moment.min.js",
        // "/global/vendor/footable/footable.min.js",
        // "/assets/examples/js/tables/footable.js",
        '/global/vendor/alertify/alertify.js',
        '/global/vendor/notie/notie.js',
        '/global/js/Plugin/alertify.js',
        '/global/js/Plugin/notie-js.js',
        '/global/js/Plugin/responsive-tabs.js',
        "/global/js/Plugin/closeable-tabs.js",
        "/global/js/Plugin/tabs.js",


        "/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js",
        "/global/vendor/timepicker/jquery.timepicker.min.js",
        "/global/vendor/datepair/datepair.min.js",
        "/global/vendor/datepair/jquery.datepair.min.js",


        "/global/js/Plugin/bootstrap-datepicker.js",
        "/global/js/Plugin/jt-timepicker.js",
        "/global/js/Plugin/datepair.js",


        "/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js",
        "/global/js/Plugin/bootstrap-tokenfield.js",
        "/global/js/Plugin/multi-select.js",

        "/global/vendor/select2/select2.full.min.js",
        "/global/js/Plugin/select2.js",
        "/global/vendor/bootstrap-select/bootstrap-select.js",
        "/global/vendor/asrange/jquery-asRange.min.js",
        "/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js",
        "/global/vendor/timepicker/jquery.timepicker.min.js",
        "/global/vendor/datepair/datepair.min.js",
        "/global/vendor/datepair/jquery.datepair.min.js",
        "/global/vendor/jquery-strength/password_strength.js",
        "/global/vendor/jquery-strength/jquery-strength.min.js",
        "/global/vendor/multi-select/jquery.multi-select.js",
        "/global/vendor/typeahead-js/bloodhound.min.js",

        "/global/js/Plugin/bootstrap-select.js",
        "/global/vendor/clockpicker/bootstrap-clockpicker.min.js",
        "/global/js/Plugin/clockpicker.js",

        "/global/vendor/tablesaw/tablesaw.jquery.js",
        "/global/vendor/tablesaw/tablesaw-init.js",
        "/global/js/Plugin/tablesaw.js",
        "/assets/examples/js/tables/responsive.js",

        "/global/vendor/moment/moment.min.js",
        "/global/vendor/footable/footable.min.js",
        "/assets/examples/js/tables/footable.js",

        "/global/vendor/ascolor/jquery-asColor.min.js",
        "/global/vendor/asgradient/jquery-asGradient.min.js",
        "/global/vendor/ascolorpicker/jquery-asColorPicker.min.js",
        "/global/js/config/colors.js",
        "/global/js/Plugin/ascolorpicker.js",

        "/global/vendor/switchery/switchery.js",
        "/global/js/Plugin/switchery.js",

    ];
    // public $depends = [
    //     'yii\web\YiiAsset',
    //     'yii\bootstrap\BootstrapAsset',
    // ];
}
