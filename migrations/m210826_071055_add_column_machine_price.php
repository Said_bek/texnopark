<?php

use yii\db\Migration;

/**
 * Class m210826_071055_add_column_machine_price
 */
class m210826_071055_add_column_machine_price extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_step', 'machine_price', $this->double());
        $this->addColumn('order_section_time', 'machine_price', $this->double());
        $this->addColumn('order_pause', 'machine_price', $this->double());
        $this->addColumn('order_pause', 'machine_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210826_071055_add_column_machine_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210826_071055_add_column_machine_price cannot be reverted.\n";

        return false;
    }
    */
}
