<?php

use yii\db\Migration;

/**
 * Class m210619_114925_order_modele
 */
class m210619_114925_order_modele extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_module', [
            'id' => $this->primaryKey(),
            'order_id' => $this->Integer(),
            'module_id' => $this->Integer(),
            'enter_date' => $this->timestamp(),
            'exit_date' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210619_114925_order_modele cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210619_114925_order_modele cannot be reverted.\n";

        return false;
    }
    */
}
