<?php

use yii\db\Migration;

/**
 * Class m210619_061534_constructor_add_column
 */
class m210619_061534_constructor_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('constructor', 'name', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210619_061534_constructor_add_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210619_061534_constructor_add_column cannot be reverted.\n";

        return false;
    }
    */
}
