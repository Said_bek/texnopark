<?php

use yii\db\Migration;

/**
 * Class m211011_063622_add_column_supplier
 */
class m211011_063622_add_column_supplier extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('supplier');
        $this->createTable('supplier', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(15),
            'status' => $this->smallInteger()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211011_063622_add_column_supplier cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211011_063622_add_column_supplier cannot be reverted.\n";

        return false;
    }
    */
}
