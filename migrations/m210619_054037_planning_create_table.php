<?php

use yii\db\Migration;

/**
 * Class m210619_054037_planning_create_table
 */
class m210619_054037_planning_create_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('section', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'status' => $this->smallInteger()
        ]);

        $this->createTable('order_step', [
            'id' => $this->primaryKey(),
            'order_id' => $this->Integer(),
            'section_id' => $this->Integer(),
            'work_time' => $this->timestamp(),
            'deadline' => $this->timestamp(),
            'start_date' => $this->timestamp(),
        ]);

        $this->createTable('order_section_time', [
            'id' => $this->primaryKey(),
            'section_id' => $this->Integer(),
            'order_id' => $this->Integer(),
            'enter_date' => $this->timestamp(),
            'exit_date' => $this->timestamp(),
        ]);

        $this->createTable('order_pause', [
            'id' => $this->primaryKey(),
            'order_id' => $this->Integer(),
            'section_id' => $this->Integer(),
            'start_date' => $this->timestamp(),
            'end_date' => $this->timestamp(),
            'status' => $this->smallInteger()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210619_054037_planning_create_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210619_054037_planning_create_table cannot be reverted.\n";

        return false;
    }
    */
}
