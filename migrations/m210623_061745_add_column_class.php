<?php

use yii\db\Migration;

/**
 * Class m210623_061745_add_column_class
 */
class m210623_061745_add_column_class extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('specification', 'class', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210623_061745_add_column_class cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210623_061745_add_column_class cannot be reverted.\n";

        return false;
    }
    */
}
