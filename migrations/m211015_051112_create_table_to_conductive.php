<?php

use yii\db\Migration;

/**
 * Class m211015_051112_create_table_to_conductive
 */
class m211015_051112_create_table_to_conductive extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('section_conductive', [
            'id' => $this->primaryKey(),
            'section_id' => $this->integer(15),
            'user_id' => $this->integer(15),
        ]);
        $this->addColumn('order_details', 'code', $this->string(50));
        $this->addColumn('specification_materials', 'code', $this->string(50));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211015_051112_create_table_to_conductive cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211015_051112_create_table_to_conductive cannot be reverted.\n";

        return false;
    }
    */
}
