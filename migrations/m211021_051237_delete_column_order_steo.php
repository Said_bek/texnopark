<?php

use yii\db\Migration;

/**
 * Class m211021_051237_delete_column_order_steo
 */
class m211021_051237_delete_column_order_steo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('order_step', 'status');
        $this->addColumn('order_section_time', 'status', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211021_051237_delete_column_order_steo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211021_051237_delete_column_order_steo cannot be reverted.\n";

        return false;
    }
    */
}
