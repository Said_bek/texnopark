<?php

use yii\db\Migration;

/**
 * Class m210614_062953_alter_name
 */
class m210614_062953_alter_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('specification_materials', 'list_number');
        $this->addColumn('specification_materials', 'count', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210614_062953_alter_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210614_062953_alter_name cannot be reverted.\n";

        return false;
    }
    */
}
