<?php

use yii\db\Migration;

/**
 * Class m210909_114731_add_column_specification
 */
class m210909_114731_add_column_specification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('specification', 'active', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210909_114731_add_column_specification cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210909_114731_add_column_specification cannot be reverted.\n";

        return false;
    }
    */
}
