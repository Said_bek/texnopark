<?php

use yii\db\Migration;

/**
 * Class m210628_060816_order_section_time_add_order_step_id
 */
class m210628_060816_order_section_time_add_order_step_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_section_time', 'order_step_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210628_060816_order_section_time_add_order_step_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210628_060816_order_section_time_add_order_step_id cannot be reverted.\n";

        return false;
    }
    */
}
