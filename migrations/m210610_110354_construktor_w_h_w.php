<?php

use yii\db\Migration;

/**
 * Class m210610_110354_construktor_w_h_w
 */
class m210610_110354_construktor_w_h_w extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('material_settings', [
            'id' => $this->primaryKey(),
            'material_id' => $this->Integer(),
            'width_f' => $this->double(),
            'width_s' => $this->double(),
            'height_f' => $this->double(),
            'height_s' => $this->double(),
            'weight_f' => $this->double(),
            'weight_s' => $this->double(),
            'w_approx' => $this->double(),
            'h_approx' => $this->double(),
            'we_approx' => $this->double(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210610_110354_construktor_w_h_w cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210610_110354_construktor_w_h_w cannot be reverted.\n";

        return false;
    }
    */
}
