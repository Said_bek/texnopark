<?php

use yii\db\Migration;

/**
 * Class m210623_072408_add_column_specification
 */
class m210623_072408_add_column_specification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('specification', 'img', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210623_072408_add_column_specification cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210623_072408_add_column_specification cannot be reverted.\n";

        return false;
    }
    */
}
