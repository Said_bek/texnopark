<?php

use yii\db\Migration;

/**
 * Class m211014_040535_order_deatails
 */
class m211014_040535_order_deatails extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_details', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(15),
            'material_id' => $this->integer(15),
            'mark_id' => $this->integer(15),
            'specification_id' => $this->integer(15),
            'width' => $this->double(),
            'height' => $this->double(),
            'weight' => $this->double(),
            'w_approx' => $this->double(),
            'h_approx' => $this->double(),
            'we_approx' => $this->double(),
            'count' => $this->integer(),
            'terma' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211014_040535_order_deatails cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211014_040535_order_deatails cannot be reverted.\n";

        return false;
    }
    */
}
