<?php

use yii\db\Migration;

/**
 * Class m210630_070134_warehouse_tables
 */
class m210630_070134_warehouse_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('unit', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'value' => $this->integer(2),
            'status' => $this->integer(2),
        ]);

        $this->createTable('parameter', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'status' => $this->integer(2),
        ]);

        $this->createTable('material_unit', [
            'id' => $this->primaryKey(),
            'material_id' => $this->integer(),
            'unit_id' => $this->integer(),
        ]);

        $this->createTable('material_parameter', [
            'id' => $this->primaryKey(),
            'material_id' => $this->integer(),
            'parameter_id' => $this->integer(),
        ]);

        $this->createTable('material_interval', [
            'id' => $this->primaryKey(),
            'material_id' => $this->integer(),
            'total' => $this->integer(),
        ]);

        $this->createTable('material_history', [
            'id' => $this->primaryKey(),
            'material_id' => $this->integer(),
            'quantity' => $this->integer(),
            'nakladnoy_id' => $this->integer(),
            'supplier_id' => $this->integer(),
            'created_date' => $this->timestamp()
        ]);

        $this->createTable('material_price', [
            'id' => $this->primaryKey(),
            'material_id' => $this->integer(),
            'price' => $this->double(),
            'enter_date' => $this->date(),
            'end_date' => $this->date()
        ]);

        $this->createTable('supplier', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(100),
            'phone_number' => $this->string()
        ]);

        $this->createTable('nakladnoy', [
            'id' => $this->primaryKey(),
            'code' => $this->string(100),
            'enter_date' => $this->timestamp(),
        ]);

        $this->addColumn('materials', 'code_number', $this->integer());
        $this->addColumn('materials', 'code_name', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210630_070134_warehouse_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210630_070134_warehouse_tables cannot be reverted.\n";

        return false;
    }
    */
}
