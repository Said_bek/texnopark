<?php

use yii\db\Migration;

/**
 * Class m211021_101707_main_bot_tables
 */
class m211021_101707_main_bot_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('main_step', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer(20),
            'step_1' => $this->integer(5),
            'step_2' => $this->integer(5)
        ]);

        $this->createTable('main_message_id', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer(20),
            'message_id' => $this->integer()
        ]);

        $this->createTable('main_last_id', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->integer(20),
            'last' => $this->integer(15)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211021_101707_main_bot_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211021_101707_main_bot_tables cannot be reverted.\n";

        return false;
    }
    */
}
