<?php

use yii\db\Migration;

/**
 * Class m210825_061223_add_column_order_section_time
 */
class m210825_061223_add_column_order_section_time extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_section_time', 'machine_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210825_061223_add_column_order_section_time cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210825_061223_add_column_order_section_time cannot be reverted.\n";

        return false;
    }
    */
}
