<?php

use yii\db\Migration;

/**
 * Class m211021_045518_rename_order_step
 */
class m211021_045518_rename_order_step extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('order_pause', 'details_id', 'detail_id');
        $this->renameColumn('order_section_time', 'details_id', 'detail_id');
        $this->renameColumn('order_step', 'details_id', 'detail_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211021_045518_rename_order_step cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211021_045518_rename_order_step cannot be reverted.\n";

        return false;
    }
    */
}
