<?php

use yii\db\Migration;

/**
 * Class m210522_035616_konstruktor_tables
 */
class m210522_035616_konstruktor_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('materials', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'number' => $this->string(50),
            'status' => $this->integer(2),
        ]);

        $this->createTable('marks', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'status' => $this->integer(2),
        ]);

        $this->createTable('material_marks', [
            'id' => $this->primaryKey(),
            'material_id' => $this->integer(10),
            'mark_id' => $this->integer(10),
            'status' => $this->smallInteger()
        ]);

        $this->createTable('specification_materials', [
            'id' => $this->primaryKey(),
            'material_id' => $this->integer(10),
            'specification_id' => $this->integer(10),
            'list_number' => $this->integer(10),
            'width' => $this->double(),
            'height' => $this->double(),
            'weight' => $this->double(),
            'w_approx' => $this->double(),
            'h_approx' => $this->double(),
            'we_approx' => $this->double()
        ]);

        $this->createTable('specification', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'created_date' => $this->timestamp(),
            'name' => $this->string(255)
        ]);

        $this->createTable('constructor', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'status' => $this->smallInteger(),
            'type' => $this->smallInteger()
        ]);

        $this->createTable('specification_constructor', [
            'id' => $this->primaryKey(),
            'specification_id' => $this->integer(),
            'constructor_id' => $this->integer(),
            'enter_date' => $this->timestamp(),
            'deadline' => $this->timestamp()
        ]);

        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'created_date' => $this->timestamp(),
            'exit_date' => $this->timestamp(),
            'status' => $this->smallInteger(),
            'type' => $this->smallInteger(),
            'client_id' => $this->integer(),
            'price' => $this->integer()
        ]);

        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'face_name' => $this->string(255),
            'phone_number' => $this->string(),
            'phone_number_2' => $this->string(),
            'company_name' => $this->string(),
            'country' => $this->string(),
            'email' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210522_035616_konstruktor_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210522_035616_konstruktor_tables cannot be reverted.\n";

        return false;
    }
    */
}
