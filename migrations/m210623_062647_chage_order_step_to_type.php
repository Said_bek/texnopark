<?php

use yii\db\Migration;

/**
 * Class m210623_062647_chage_order_step_to_type
 */
class m210623_062647_chage_order_step_to_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('order_step', 'work_time');
        $this->addColumn('order_step', 'work_time', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210623_062647_chage_order_step_to_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210623_062647_chage_order_step_to_type cannot be reverted.\n";

        return false;
    }
    */
}
