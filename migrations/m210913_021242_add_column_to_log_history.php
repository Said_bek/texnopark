<?php

use yii\db\Migration;

/**
 * Class m210913_021242_add_column_to_log_history
 */
class m210913_021242_add_column_to_log_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('log_history', 'description', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210913_021242_add_column_to_log_history cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210913_021242_add_column_to_log_history cannot be reverted.\n";

        return false;
    }
    */
}
