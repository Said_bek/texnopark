<?php

use yii\db\Migration;

/**
 * Class m210907_113402_notification
 */
class m210907_113402_notification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notification', [
            'id' => $this->primaryKey(),
            'module_id' => $this->integer(),
            'order_id' => $this->integer(),
            'enter_date' => $this->timestamp(),
            'end_date' => $this->timestamp(),
            'status' => $this->smallInteger()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210907_113402_notification cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210907_113402_notification cannot be reverted.\n";

        return false;
    }
    */
}
