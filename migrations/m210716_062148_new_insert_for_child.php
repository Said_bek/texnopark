<?php

use yii\db\Migration;

/**
 * Class m210716_062148_new_insert_for_child
 */
class m210716_062148_new_insert_for_child extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "UPDATE auth_item SET type = 1";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Nakladnoy.Index', 2, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item_child (parent,child) VALUES('Admin','Nakladnoy.Index');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Nakladnoy.View', 2, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item_child (parent,child) VALUES('Admin','Nakladnoy.View');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Nakladnoy.Create', 2, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item_child (parent,child) VALUES('Admin','Nakladnoy.Create');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Nakladnoy.Update', 2, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item_child (parent,child) VALUES('Admin','Nakladnoy.Update');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Nakladnoy.Delete', 2, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item_child (parent,child) VALUES('Admin','Nakladnoy.Delete');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Nakladnoy.ChangeMaterial', 2, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item_child (parent,child) VALUES('Admin','Nakladnoy.ChangeMaterial');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Nakladnoy.PlusInput', 2, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item_child (parent,child) VALUES('Admin','Nakladnoy.PlusInput');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Nakladnoy.SaveNakladnoy', 2, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item_child (parent,child) VALUES('Admin','Nakladnoy.SaveNakladnoy');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Nakladnoy.UpdateNak', 2, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item_child (parent,child) VALUES('Admin','Nakladnoy.UpdateNak');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Nakladnoy.UpdateNakladnoy', 2, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item_child (parent,child) VALUES('Admin','Nakladnoy.UpdateNakladnoy');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Nakladnoy.DeleteNak', 2, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item_child (parent,child) VALUES('Admin','Nakladnoy.DeleteNak');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Nakladnoy.Excel', 2, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item_child (parent,child) VALUES('Admin','Nakladnoy.Excel');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item (name, type, description) VALUES('Nakladnoy.DeleteNakladnoy', 2, 'Permission');";
        $this->execute($sql);

        $sql = "INSERT INTO auth_item_child (parent,child) VALUES('Admin','Nakladnoy.DeleteNakladnoy');";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210716_062148_new_insert_for_child cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210716_062148_new_insert_for_child cannot be reverted.\n";

        return false;
    }
    */
}
