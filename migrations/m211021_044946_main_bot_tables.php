<?php

use yii\db\Migration;

/**
 * Class m211021_044946_main_bot_tables
 */
class m211021_044946_main_bot_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_section_time', 'details_id', $this->integer());
        $this->addColumn('order_pause', 'details_id', $this->integer());
        $this->addColumn('order_step', 'details_id', $this->integer());
        $this->addColumn('order_step', 'status', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211021_044946_main_bot_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211021_044946_main_bot_tables cannot be reverted.\n";

        return false;
    }
    */
}
