<?php

use yii\db\Migration;

/**
 * Class m210524_105248_modul_tables
 */
class m210524_105248_modul_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('modul_users', [
            'id' => $this->primaryKey(),
            'modul_id' => $this->integer(),
            'user_id' => $this->integer()
        ]);

        $this->createTable('modules', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'status' => $this->smallInteger()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210524_105248_modul_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210524_105248_modul_tables cannot be reverted.\n";

        return false;
    }
    */
}
