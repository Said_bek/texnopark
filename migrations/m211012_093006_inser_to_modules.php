<?php

use yii\db\Migration;

/**
 * Class m211012_093006_inser_to_modules
 */
class m211012_093006_inser_to_modules extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "TRUNCATE TABLE modules";
        $this->execute($sql);

        $sql_2 = "CREATE SEQUENCE modules_3_house_id_seq MINVALUE 1";
        $this->execute($sql_2);

        $sql_3 = "ALTER TABLE modules ALTER id SET DEFAULT nextval('modules_3_house_id_seq')";
        $this->execute($sql_3);

        $sql_4 = "ALTER SEQUENCE modules_3_house_id_seq OWNED BY modules.id";
        $this->execute($sql_4);

        $this->insert('modules', [
            'title' => 'Foydalanuvchilar',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Konstruktor',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Sotuv',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Moliya',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Ishlab chiqarish',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Sklad',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Rejalashtirish',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Yetkazib berish',
            'status' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211012_093006_inser_to_modules cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211012_093006_inser_to_modules cannot be reverted.\n";

        return false;
    }
    */
}
