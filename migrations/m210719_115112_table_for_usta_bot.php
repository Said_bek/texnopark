<?php

use yii\db\Migration;

/**
 * Class m210719_115112_table_for_usta_bot
 */
class m210719_115112_table_for_usta_bot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210719_115112_table_for_usta_bot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210719_115112_table_for_usta_bot cannot be reverted.\n";

        return false;
    }
    */
}
