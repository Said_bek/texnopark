<?php

use yii\db\Migration;

/**
 * Class m210928_110208_create_table_order_step
 */
class m210928_110208_create_table_order_step extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sections', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
            'status' => $this->smallInteger()->defaultValue(1)
        ]);

        $this->createTable('order_section_step', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(10),
            'section_id' => $this->integer(10),
            'deadline' => $this->timestamp(),
            'order_column' => $this->integer(5),
            'status' => $this->smallInteger()->defaultValue(0)
        ]);

        $this->createTable('order_section', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(10),
            'section_id' => $this->integer(10),
            'enter_date' => $this->timestamp(),
            'exit_date' => $this->timestamp(),
            'status' => $this->smallInteger(),
            'step' => $this->smallInteger()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210928_110208_create_table_order_step cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210928_110208_create_table_order_step cannot be reverted.\n";

        return false;
    }
    */
}
