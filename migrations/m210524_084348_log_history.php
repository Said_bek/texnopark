<?php

use yii\db\Migration;

/**
 * Class m210524_084348_log_history
 */
class m210524_084348_log_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('log_history', [
            'id' => $this->primaryKey(),
            'username' => $this->string(255),
            'event_time' => $this->timestamp(),
            'event_id' => $this->integer()
        ]);

        $this->createTable('events', [
            'id' => $this->primaryKey(),
            'title' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210524_084348_log_history cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210524_084348_log_history cannot be reverted.\n";

        return false;
    }
    */
}
