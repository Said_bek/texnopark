<?php

use yii\db\Migration;

/**
 * Class m210712_111158_add_column_to_nakladnoy
 */
class m210712_111158_add_column_to_nakladnoy extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('nakladnoy', 'created_date', $this->timestamp());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210712_111158_add_column_to_nakladnoy cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210712_111158_add_column_to_nakladnoy cannot be reverted.\n";

        return false;
    }
    */
}
