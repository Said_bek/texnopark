<?php

use yii\db\Migration;

/**
 * Class m210805_043431_machine_tables
 */
class m210805_043431_machine_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('machines', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'status' => $this->smallInteger(),
        ]);

        $this->createTable('machines_price', [
            'id' => $this->primaryKey(),
            'machine_id' => $this->integer(),
            'price' => $this->double(),
            'enter_date' => $this->timestamp(),
            'end_date' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210805_043431_machine_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210805_043431_machine_tables cannot be reverted.\n";

        return false;
    }
    */
}
