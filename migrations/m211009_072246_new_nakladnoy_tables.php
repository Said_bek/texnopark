<?php

use yii\db\Migration;

/**
 * Class m211009_072246_new_nakladnoy_tables
 */
class m211009_072246_new_nakladnoy_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('nakladnoy', 'status', $this->smallInteger());
        $this->addColumn('nakladnoy', 'end_date', $this->timestamp());
        $this->dropTable('sections');
        $this->dropTable('order_section_step');
        $this->dropTable('order_section');
        $this->createTable('nakladnoy_details', [
            'id' => $this->primaryKey(),
            'nakladnoy_id' => $this->integer(15),
            'material_id' => $this->integer(15),
            'mark_id' => $this->integer(15),
            'quantity' => $this->double()
        ]);
        $this->createTable('nakladnoy_details_history', [
            'id' => $this->primaryKey(),
            'nakladnoy_details_id' => $this->integer(15),
            'quantity' => $this->double(),
            'price' => $this->double()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211009_072246_new_nakladnoy_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211009_072246_new_nakladnoy_tables cannot be reverted.\n";

        return false;
    }
    */
}
