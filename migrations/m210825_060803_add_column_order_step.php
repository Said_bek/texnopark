<?php

use yii\db\Migration;

/**
 * Class m210825_060803_add_column_order_step
 */
class m210825_060803_add_column_order_step extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_step', 'machine_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210825_060803_add_column_order_step cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210825_060803_add_column_order_step cannot be reverted.\n";

        return false;
    }
    */
}
