<?php

use yii\db\Migration;

/**
 * Class m210715_105327_new_insert_to_modules
 */
class m210715_105327_new_insert_to_modules extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "TRUNCATE TABLE modules";
        $this->execute($sql);

        $sql_2 = "CREATE SEQUENCE modules_2_house_id_seq MINVALUE 1";
        $this->execute($sql_2);

        $sql_3 = "ALTER TABLE modules ALTER id SET DEFAULT nextval('modules_2_house_id_seq')";
        $this->execute($sql_3);

        $sql_4 = "ALTER SEQUENCE modules_2_house_id_seq OWNED BY modules.id";
        $this->execute($sql_4);

        $this->insert('modules', [
            'title' => 'Foydalanuvchilar',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Konstruktor',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Sotuv',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Moliya',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Ishlab chiqarish',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Sklad',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Rejalashtirish',
            'status' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210715_105327_new_insert_to_modules cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210715_105327_new_insert_to_modules cannot be reverted.\n";

        return false;
    }
    */
}
