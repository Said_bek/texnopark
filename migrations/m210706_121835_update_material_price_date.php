<?php

use yii\db\Migration;

/**
 * Class m210706_121835_update_material_price_date
 */
class m210706_121835_update_material_price_date extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('material_price', 'enter_date', 'date');
        $this->alterColumn('material_price', 'end_date', 'date');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210706_121835_update_material_price_date cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210706_121835_update_material_price_date cannot be reverted.\n";

        return false;
    }
    */
}
