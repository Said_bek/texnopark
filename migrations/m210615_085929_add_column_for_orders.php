<?php

use yii\db\Migration;

/**
 * Class m210615_085929_add_column_for_orders
 */
class m210615_085929_add_column_for_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('specification_materials', 'terma', $this->string());
        $this->addColumn('orders', 'specification_status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210615_085929_add_column_for_orders cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210615_085929_add_column_for_orders cannot be reverted.\n";

        return false;
    }
    */
}
