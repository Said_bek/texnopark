<?php

use yii\db\Migration;

/**
 * Class m210518_131634_rbac_inserts
 */
class m210518_131634_rbac_inserts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('auth_item', [
            'name' => 'Admin',
            'type' => 1
        ]);

        $this->insert('auth_item', [
            'name' => 'Manager',
            'type' => 2
        ]);

        $this->insert('auth_item', [
            'name' => 'Seller',
            'type' => 3
        ]);

        $this->insert('auth_item', [
            'name' => 'Master',
            'type' => 4
        ]);

        $this->insert('auth_item', [
            'name' => 'Opertor',
            'type' => 5
        ]);

        $this->insert('auth_item', [
            'name' => 'Programmer',
            'type' => 6
        ]);

        $this->insert('auth_item', [
            'name' => 'Worker',
            'type' => 7
        ]);

        $this->insert('auth_item', [
            'name' => 'Logist',
            'type' => 8
        ]);


        $this->insert('auth_item', [
            'name' => 'Counter',
            'type' => 9
        ]);

        $this->insert('auth_item', [
            'name' => 'OTK',
            'type' => 10
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210518_131634_rbac_inserts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210518_131634_rbac_inserts cannot be reverted.\n";

        return false;
    }
    */
}
