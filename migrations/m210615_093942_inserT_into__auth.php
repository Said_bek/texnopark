<?php

use yii\db\Migration;

/**
 * Class m210615_093942_inserT_into__auth
 */
class m210615_093942_inserT_into__auth extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('auth_item', [
            'name' => 'Head Constructor',
            'type' => 11
        ]);

        $this->insert('auth_item', [
            'name' => 'Constructor',
            'type' => 12
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210615_093942_inserT_into__auth cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210615_093942_inserT_into__auth cannot be reverted.\n";

        return false;
    }
    */
}
