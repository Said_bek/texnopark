<?php

use yii\db\Migration;

/**
 * Class m210623_101336_add_plan_module
 */
class m210623_101336_add_plan_module extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->insert('modules', [
            'id' => 1,
            'title' => 'Rejalashtirish',
            'status' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210623_101336_add_plan_module cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210623_101336_add_plan_module cannot be reverted.\n";

        return false;
    }
    */
}
