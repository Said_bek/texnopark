<?php

use yii\db\Migration;

/**
 * Class m210616_112957_add_code
 */
class m210616_112957_add_code extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'code', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210616_112957_add_code cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210616_112957_add_code cannot be reverted.\n";

        return false;
    }
    */
}
