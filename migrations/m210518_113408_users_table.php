<?php

use yii\db\Migration;

/**
 * Class m210518_113408_users_table
 */
class m210518_113408_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(255),
            'password' => $this->string(255),
            'phone_number' => $this->string(20),
            'type' => $this->integer(2),
            'status' => $this->smallInteger()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210518_113408_users_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210518_113408_users_table cannot be reverted.\n";

        return false;
    }
    */
}
