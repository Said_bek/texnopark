<?php

use yii\db\Migration;

/**
 * Class m210524_113924_selling_module_tables
 */
class m210524_113924_selling_module_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'category_id', $this->smallInteger());
        $this->addColumn('orders', 'order_name', $this->string(255));
        $this->addColumn('orders', 'order_from', $this->smallInteger());
        $this->addColumn('orders', 'proirity', $this->smallInteger());
        $this->addColumn('orders', 'deadline_price', $this->timestamp());
        $this->addColumn('orders', 'approved', $this->smallInteger());

        $this->createTable('pdf', [
            'id' => $this->primaryKey(),
            'offer_code' => $this->Integer(),
            'client_id' => $this->Integer(),
            'category_id' => $this->smallInteger(),
            'about_offer' => $this->text(),
            'project_title' => $this->string(255)
        ]);


        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'status' => $this->smallInteger(),
        ]);

        $this->createTable('docs', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'status' => $this->smallInteger(),
        ]);

        $this->createTable('category_docs', [
            'id' => $this->primaryKey(),
            'category_id' => $this->smallInteger(),
            'docs_id' => $this->smallInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210524_113924_selling_module_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210524_113924_selling_module_tables cannot be reverted.\n";

        return false;
    }
    */
}
