<?php

use yii\db\Migration;

/**
 * Class m210614_114557_add_colum_for_konstruktor
 */
class m210614_114557_add_colum_for_konstruktor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('specification_materials', 'mark_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210614_114557_add_colum_for_konstruktor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210614_114557_add_colum_for_konstruktor cannot be reverted.\n";

        return false;
    }
    */
}
