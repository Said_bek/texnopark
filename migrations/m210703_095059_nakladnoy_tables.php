<?php

use yii\db\Migration;

/**
 * Class m210703_095059_nakladnoy_tables
 */
class m210703_095059_nakladnoy_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('nakladnoy', 'supplier_id', $this->integer());
        $this->addColumn('material_interval', 'enter_date', $this->date());
        $this->addColumn('material_interval', 'mark_id', $this->integer());
        $this->addColumn('material_history', 'price', $this->integer());
        $this->addColumn('material_price', 'mark_id', $this->integer());
        $this->addColumn('material_history', 'mark_id', $this->integer());
        $this->dropColumn('material_history', 'supplier_id');
        $this->alterColumn('material_history', 'quantity', 'double');
        $this->alterColumn('material_interval', 'total', 'double');
        $this->alterColumn('material_price', 'enter_date', 'timestamp');
        $this->alterColumn('material_price', 'end_date', 'timestamp');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210703_095059_nakladnoy_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210703_095059_nakladnoy_tables cannot be reverted.\n";

        return false;
    }
    */
}
