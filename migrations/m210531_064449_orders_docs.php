<?php

use yii\db\Migration;

/**
 * Class m210531_064449_orders_docs
 */
class m210531_064449_orders_docs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders_docs', [
            'id' => $this->primaryKey(),
            'order_id' => $this->Integer(),
            'docs_id' => $this->Integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210531_064449_orders_docs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210531_064449_orders_docs cannot be reverted.\n";

        return false;
    }
    */
}
