<?php

use yii\db\Migration;

/**
 * Class m210710_055607_drop_table_material_price
 */
class m210710_055607_drop_table_material_price extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('material_price');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210710_055607_drop_table_material_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210710_055607_drop_table_material_price cannot be reverted.\n";

        return false;
    }
    */
}
