<?php

use yii\db\Migration;

/**
 * Class m211012_092810_insert_to_module
 */
class m211012_092810_insert_to_module extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "TRUNCATE TABLE modules";
        $this->execute($sql);

        $this->insert('modules', [
            'title' => 'Foydalanuvchilar',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Konstruktor',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Sotuv',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Moliya',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Ishlab chiqarish',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Sklad',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Rejalashtirish',
            'status' => 1
        ]);

        $this->insert('modules', [
            'title' => 'Yetkazib berish',
            'status' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211012_092810_insert_to_module cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211012_092810_insert_to_module cannot be reverted.\n";

        return false;
    }
    */
}
