<?php

use yii\db\Migration;

/**
 * Class m210529_091626_work_on_orders_table
 */
class m210529_091626_work_on_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('orders', 'name');
        $this->dropColumn('orders', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210529_091626_work_on_orders_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210529_091626_work_on_orders_table cannot be reverted.\n";

        return false;
    }
    */
}
