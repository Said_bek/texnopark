<?php

use yii\db\Migration;

/**
 * Class m210522_043428_auth_key_to_users
 */
class m210522_043428_auth_key_to_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'auth_key', $this->Integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210522_043428_auth_key_to_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210522_043428_auth_key_to_users cannot be reverted.\n";

        return false;
    }
    */
}
