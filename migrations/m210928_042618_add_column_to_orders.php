<?php

use yii\db\Migration;

/**
 * Class m210928_042618_add_column_to_orders
 */
class m210928_042618_add_column_to_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'order_type', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210928_042618_add_column_to_orders cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210928_042618_add_column_to_orders cannot be reverted.\n";

        return false;
    }
    */
}
