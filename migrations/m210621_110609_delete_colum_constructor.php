<?php

use yii\db\Migration;

/**
 * Class m210621_110609_delete_colum_constructor
 */
class m210621_110609_delete_colum_constructor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('constructor', 'name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210621_110609_delete_colum_constructor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210621_110609_delete_colum_constructor cannot be reverted.\n";

        return false;
    }
    */
}
