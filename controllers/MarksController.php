<?php

namespace app\controllers;

use app\models\Users;
use Yii;
use app\models\Marks;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ModulUsers;

/**
 * MarksController implements the CRUD actions for Marks model.
 */
class MarksController extends Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
        else {
            $user_id = Yii::$app->user->id;
            $checkUser = ModulUsers::find()->where(['user_id' => $user_id, 'modul_id' => 6])->one();
            if (!isset($checkUser)) {
                $this->redirect('/index.php/site/login');
            }
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST','GET'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $marks = Marks::find()->orderBy(['id' => SORT_ASC])->all();

        return $this->render('index', [
            'mark' => $marks,
        ]);
    }

    public function actionAddMark()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['m']) && !empty($_GET['m'])) {
                if ($_GET['m'] == 'true') {
                    $mark = Yii::$app->request->get('mark');
                    if (isset($mark) and !empty($mark)) {
                        $model = new Marks();
                        $model->name = $mark;
                        $model->status = 1;
                        $model->save();
                        return ['status' => 'success'];
                    } else {
                        return ['status' => 'error_mark'];
                    }
                }else {
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('_form.php'),
                    ];
                }
            }
        }
    }

    public function actionUpdateMark()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && isset($_GET['update']) && $_GET['id'] > 0) {
                if ($_GET['update'] == 'true') {
                    if (isset($_GET['mark']) and !empty($_GET['mark'])){
                        $mark = Yii::$app->request->get('mark');
                        $id = Yii::$app->request->get('id');
                        $marks = Marks::findOne($id);
                        $marks->name = $mark;
                        $marks->save();
                        return ['status' => 'success',];
                    } else {
                        return ['status' => 'error_mark',];
                    }
                }else {
                    $id = Yii::$app->request->get('id');
                    $marks = Marks::findOne($id);
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('update.php',[
                            'marks' => $marks,
                        ]),
                    ];
                }
            }
        }
    }

    public function actionDelete()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && !empty($_GET['id'])) {
                $id = Yii::$app->request->get('id');
                $selectMark = Marks::findOne($id);
                $status = ($selectMark->status == 0) ? 1 : 0;
                $selectMark->status = $status;
                $selectMark->save();
                return ['status' => 'success'];
            }
        }
    }

    /**
     * Finds the Marks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Marks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Marks::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
