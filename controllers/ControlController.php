<?php

namespace app\controllers;
use app\models\Clients;
use app\models\Notification;
use app\models\Orders;
use app\models\OrderStep;
use Yii;

class ControlController extends \yii\web\Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
    }
    public function actionIndex()
    {
        $sql = "
            select 
                   o.created_date,
                   c2.company_name,
                   c2.phone_number,
                   c2.phone_number_2,
                   o.proirity,
                   o.approved,
                   o.order_name,
                   o.status,
                   o.id as order_id,
                   o.price,
                   o.deadline_price,
                   o.order_from,
                   c.title as category,
                   m.title as module_name
            from orders as o
                     inner join category c on c.id = o.category_id
                     inner join clients c2 on o.client_id = c2.id
                     inner join notification n on o.id = n.order_id and n.end_date IS NULL
                     inner join modules m on n.module_id = m.id
            where o.status = :status
        ";

        $model = Yii::$app->db->createCommand($sql)
            ->bindValue(':status', 1)
            ->queryAll();


        $unconfirmed = Orders::find()->where('status = :status',['status' => 0])->all();
        $countOrder = Orders::find()->count();

        return $this->render('index',[
            'model' => $model,
            'unconfirmed' => $unconfirmed,
            'count' => $countOrder
        ]);
    }

    public function actionNewOrder()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['o']) && !empty($_GET['o'])) {
                if ($_GET['o'] == 'true') {
                    if (!(isset($_GET['name']) and !empty($_GET['name'])))
                        return ['status' => 'failure_name'];
                    if (!(isset($_GET['client']) and !empty($_GET['client'])))
                        return ['status' => 'failure_client'];
                    $name = Yii::$app->request->get('name');
                    $client = Yii::$app->request->get('client');

                    $newOrder = new Orders();
                    $newOrder->status = 0;
                    $newOrder->order_name = $name;
                    $newOrder->client_id = $client;
                    $newOrder->created_date = date('Y-m-d H:i:s');
                    if ($newOrder->save())
                        return ['status' => 'success'];
                }else {
                    $clients = Clients::find()->all();
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('new_order.php', [
                            'clients' => $clients
                        ])
                    ];
                }
            }
        }
    }

    public function actionUpdateOrder()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && isset($_GET['update']) && $_GET['id'] > 0) {
                if ($_GET['update'] == 'true') {
                    if (!(isset($_GET['name']) and !empty($_GET['name'])))
                        return ['status' => 'failure_name'];
                    if (!(isset($_GET['client']) and !empty($_GET['client'])))
                        return ['status' => 'failure_client'];
                    $id = Yii::$app->request->get('id');
                    $name = Yii::$app->request->get('name');
                    $client = Yii::$app->request->get('client');

                    $updateOrder = Orders::find()->where(['id' => $id])->one();
                    $updateOrder->order_name = $name;
                    $updateOrder->client_id = $client;
                    if ($updateOrder->save())
                        return ['status' => 'success'];
                }else {
                    $order = Orders::find()->where(['id' => $_GET['id']])->one();
                    $clients = Clients::find()->all();
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('update_order.php',[
                            'order' => $order,
                            'clients' => $clients
                        ]),
                    ];
                }
            }
        }
    }

    public function actionInfo($id)
    {
        $orderStep = OrderStep::find()
            ->select('order_step.deadline, order_step.work_time, order_step.start_date, s.title')
            ->innerJoin('section as s', 'order_step.section_id = s.id')
            ->where(['order_id' => $id])
            ->asArray()
            ->all();
        return $this->render('info', [
            'model' => $orderStep
        ]);
    }

    public function actionOrderDelete($id)
    {
        $deleteOrder = Orders::findOne($id);
        $deleteOrder->delete();
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUpdateStatusOrder($id)
    {
        $updateStatusOrder = Orders::find()->where(['id' => $id])->one();
        $updateStatusOrder->status = 1;
        if ($updateStatusOrder->save())
            $next_section = new Notification();
            $next_section->module_id = 3;
            $next_section->order_id = $id;
            $next_section->enter_date = date("Y-m-d H:i:s");
            $next_section->status = 1;
            $next_section->save();
            return $this->redirect(Yii::$app->request->referrer);
    }
}
