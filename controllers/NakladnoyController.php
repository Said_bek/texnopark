<?php

namespace app\controllers;

use app\models\MaterialHistory;
use app\models\MaterialInterval;
use app\models\Materials;
use app\models\NakladnoyDetails;
use app\models\NakladnoyDetailsHistory;
use app\models\Supplier;
use app\models\ModulUsers;
use Yii;
use app\models\Nakladnoy;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
require(Yii::getAlias('@vendor')."/phpexcel/PHPExcel.php");

/**
 * NakladnoyController implements the CRUD actions for Nakladnoy model.
 */
class NakladnoyController extends Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
        else {
            $user_id = Yii::$app->user->id;
            $checkUser = ModulUsers::find()->where(['user_id' => $user_id, 'modul_id' => 6])->one();
            if (!isset($checkUser)) {
                $this->redirect('/index.php/site/login');
            }
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $selectNakladnoy = "
            SELECT 
               n.id, 
               n.code, 
               n.enter_date, 
               n.created_date, 
               u.username, 
               n.status 
            FROM nakladnoy AS n 
            INNER JOIN supplier AS s ON n.supplier_id = s.id
            INNER JOIN users AS u ON s.user_id = u.id
            order by n.id";
        $model = Yii::$app->db->createCommand($selectNakladnoy)->queryAll();

        return $this->render('index', [
            'model' => $model
        ]);
    }

    public function actionCreate()
    {
        $supplier = Supplier::find()->all();
        $material = Materials::findAll(['status' => 1]);

        return $this->render('_form', [
            'supplier' => $supplier,
            'material' => $material
        ]);
    }

    public function actionChangeMaterial()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['material']) && !empty($_GET['material'])) {
                $material = Yii::$app->request->get('material');
                $selectMaterialParameter = "
                    SELECT 
                           m.id, 
                           p.name as parameter, 
                           u.name as unit 
                    FROM materials AS m
                            left JOIN material_unit AS mu ON m.id = mu.material_id
                            left JOIN unit AS u ON mu.unit_id = u.id
                            left JOIN material_parameter AS mp ON m.id = mp.material_id
                            left JOIN parameter AS p ON p.id = mp.parameter_id
                    WHERE m.id = ".$material;
                $model = Yii::$app->db->createCommand($selectMaterialParameter)->queryOne();

                $parameter =  (isset($model['parameter']) and !empty($model['parameter'])) ? $model['parameter'] : "";
                $unit = (isset($model['unit']) and !empty($model['unit'])) ? $model['unit'] : "";

                $selectMark = "
                    SELECT 
                           m.id, 
                           m.name 
                    FROM material_marks AS mm 
                            INNER JOIN marks AS m ON mm.mark_id = m.id
                    WHERE mm.material_id = ".$material;
                $mark = Yii::$app->db->createCommand($selectMark)->queryAll();
                return [
                    'status' => 'success',
                    'content' => $this->renderAjax('material_form.php',[
                        'mark' => $mark
                    ]),
                    'param' => $parameter,
                    'unit' => $unit,
                ];
            }
        }
    }

    public function actionPlusInput()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $material = Materials::findAll(['status' => 1]);
            return [
                'status' => 'success',
                'content' => $this->renderAjax('plus_input.php',[
                    'count' => $_GET['count'],
                    'material' => $material
                ]),
            ];
        }
    }

    public function actionSaveNakladnoy()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['selectForm']['a']['code']) and !empty($_GET['selectForm']['a']['code'])) {
                $checkCode = Nakladnoy::find()->where(['code' => $_GET['selectForm']['a']['code']])->all();
                if (isset($checkCode) and !empty($checkCode))
                    return ['status' => 'same_code'];
            } else {
                return ['status' => 'error_code'];
            }

            $arr = [];
            $inArray = true;
            foreach ($_GET['selectForm'] as $key => $value){
                if (isset($value['material']) and !empty($value['material']) and $inArray != false){
                    if (!in_array($value['material'], $arr)){
                        $arr[] = $value['material'];
                    } else {
                        $inArray = false;
                        break;
                    }
                }
            }
            if ($inArray){
                if (!(isset($_GET['selectForm']['a']['supplier']) and !empty($_GET['selectForm']['a']['supplier'])))
                    return ['status' => 'error_supplier'];

                if (!(isset($_GET['selectForm']['a']['datepicker']) and !empty($_GET['selectForm']['a']['datepicker'])))
                    return ['status' => 'error_date'];

                if (!(isset($_GET['selectForm']['a']['timepicker']) and !empty($_GET['selectForm']['a']['timepicker'])))
                    return ['status' => 'error_date'];

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $get_date = date('Y-m-d H:i:s',strtotime(date($_GET['selectForm']['a']['datepicker']." ".$_GET['selectForm']['a']['timepicker'].":00")));

                    $newNak = new Nakladnoy();
                    $newNak->code = $_GET['selectForm']['a']['code'];
                    $newNak->enter_date = $get_date;
                    $newNak->supplier_id = $_GET['selectForm']['a']['supplier'];
                    $newNak->created_date = date('Y-m-d H:i:s');
                    $newNak->save();

                    foreach ($_GET['selectForm'] as $key => $value){
                        if (isset($value['material']) and !empty($value['material'])){
                            $newDetails = new NakladnoyDetails();
                            $newDetails->nakladnoy_id = $newNak->id;
                            $newDetails->material_id = $value['material'];
                            $newDetails->mark_id = $value['mark'];
                            $newDetails->quantity = $value['count'];
                            $newDetails->save();
                        }
                    }
                    $transaction->commit();
                    return ['status' => 'success'];

                } catch (Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            } else {
                return [
                    'status' => 'same_material'
                ];
            }
        }
    }

    public function actionUpdateNak()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['selectForm']['a']['code']) and !empty($_GET['selectForm']['a']['code'])) {
                $checkCode = Nakladnoy::find()->where(['code' => $_GET['selectForm']['a']['code']])->andWhere(['!=','id',$_GET['id']])->all();
                if (isset($checkCode) and !empty($checkCode))
                    return ['status' => 'same_code'];
            } else {
                return ['status' => 'error_code'];
            }
            $arr = [];
            $inArray = true;
            foreach ($_GET['selectForm'] as $key => $value){
                if (isset($value['material']) and !empty($value['material']) and $inArray != false){
                    if (!in_array($value['material'], $arr)){
                        $arr[] = $value['material'];
                    } else {
                        $inArray = false;
                        break;
                    }
                }
            }
            if ($inArray){
                if (count($_GET['selectForm']) == 1 and isset($_GET['selectForm']['a'])){
                    $id = Yii::$app->request->get('id');
                    $newNak = Nakladnoy::findOne($id);
                    if (isset($newNak) and !empty($newNak)){
                        $newNak->code = $_GET['selectForm']['a']['code'];
                        $newNak->enter_date = $_GET['selectForm']['a']['datepicker']." ".$_GET['selectForm']['a']['timepicker'].":00";
                        $newNak->supplier_id = $_GET['selectForm']['a']['supplier'];
                        $newNak->save();
                    }
                    return ['status' => 'success'];
                } else {
                    if (!(isset($_GET['selectForm']['a']['supplier']) and !empty($_GET['selectForm']['a']['supplier'])))
                        return ['status' => 'error_supplier'];

                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $newNak = Nakladnoy::findOne($_GET['id']);
                        $newNak->code = $_GET['selectForm']['a']['code'];
                        $newNak->supplier_id = $_GET['selectForm']['a']['supplier'];
                        $newNak->save();
                        foreach ($_GET['selectForm'] as $key => $value){
                            if (isset($value['isset']) and $value['isset'] == 0){
                                $materialHistory = new MaterialHistory();
                                $materialHistory->material_id = $value['material'];
                                $materialHistory->quantity = $value['count'];
                                $materialHistory->nakladnoy_id = $newNak->id;
                                $materialHistory->created_date = date('Y-m-d H:i:s');
                                $materialHistory->price = $value['price'];
                                $materialHistory->mark_id = $value['mark'];
                                $materialHistory->save();

                                $validateInterval = MaterialInterval::find()->where(['enter_date' => date('Y-m-d'), 'material_id' => $value['material'], 'mark_id' => $value['mark']])->one();
                                if (isset($validateInterval) and !empty($validateInterval)){
                                    $validateInterval->total = $validateInterval->total + $value['count'];
                                    $validateInterval->save();
                                } else {
                                    $selectInterval = MaterialInterval::find()->where(['mark_id' => $value['mark'], 'material_id' => $value['material']])->orderBy(['id' => SORT_DESC])->one();
                                    $newInterval = new MaterialInterval();
                                    $newInterval->material_id = $value['material'];
                                    $newInterval->enter_date = date('Y-m-d');
                                    $newInterval->mark_id = $value['mark'];
                                    $newInterval->total = (isset($selectInterval) and !empty($selectInterval)) ? $selectInterval->total + $value['count'] : $value['count'];
                                    $newInterval->save();
                                }
                            } else {
                                $materialHistory = MaterialHistory::find()->where(['nakladnoy_id' => $_GET['id'], 'material_id' => $value['material'], 'mark_id' => $value['mark']])->one();
                                if (isset($materialHistory) and !empty($materialHistory)){
                                    $materialInterval = MaterialInterval::find()->where(['enter_date' => date('Y-m-d'), 'material_id' => $value['material'], 'mark_id' => $value['mark']])->one();
                                    if (isset($materialInterval) and !empty($materialInterval)){
                                        $materialInterval->total = $materialInterval->total - $materialHistory->quantity;
                                        $materialInterval->total = $materialInterval->total + $value['count'];
                                        $materialInterval->save();
                                    }
                                    $materialHistory->quantity = $value['count'];
                                    $materialHistory->price = $value['price'];
                                    $materialHistory->save();
                                }
                            }
                        }
                        $transaction->commit();
                        return ['status' => 'success'];
                    } catch (Exception $e) {
                        $transaction->rollBack();
                        throw $e;
                    }
                }
            } else {
                return ['status' => 'same_material'];
            }
        }
    }

    public function actionUpdateNakladnoy($id)
    {
        $supplier = Supplier::find()->all();
        $selectNak = "
            SELECT 
                   n.id, 
                   nd.id as nak_detail_id,
                   n.code, 
                   m.name AS material, 
                   m.id AS material_id, 
                   ma.id AS mark_id, 
                   ma.name AS mark, 
                   n.enter_date, 
                   us.username, 
                   nd.quantity, 
                   u.name AS unit, 
                   p.name AS parameter 
            FROM nakladnoy_details AS nd 
            INNER JOIN marks AS ma ON nd.mark_id = ma.id
            INNER JOIN material_parameter AS mp ON nd.material_id = mp.material_id
            INNER JOIN materials AS m ON mp.material_id = m.id
            INNER JOIN nakladnoy AS n ON nd.nakladnoy_id = n.id
            INNER JOIN parameter AS p ON mp.parameter_id = p.id
            INNER JOIN material_unit AS mu ON mu.material_id = m.id
            INNER JOIN unit AS u ON mu.unit_id = u.id
            INNER JOIN supplier AS s ON n.supplier_id = s.id
            INNER JOIN users AS us ON s.user_id = us.id
            WHERE nd.nakladnoy_id = ".$id;
        $model = Yii::$app->db->createCommand($selectNak)->queryAll();
        $nak = Nakladnoy::findOne($id);
        return $this->render('update_nakladnoy', [
            'model' => $model,
            'supplier' => $supplier,
            'nakladnoy' => $nak,
            'id' => $id
        ]);
    }

    public function actionDeleteUpdateNak($id)
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = Yii::$app->request->get('id');
            $detail = NakladnoyDetails::findOne($id);
            $detail->delete();
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionExcel($id)
    {
        $command = Nakladnoy::find()
            ->select('nakladnoy.enter_date, u.username, nakladnoy.code')
            ->innerJoin('supplier', 'supplier.id = nakladnoy.supplier_id')
            ->innerJoin('users as u', 'supplier.user_id = u.id')
            ->where(['nakladnoy.id' => $id])
            ->asArray()
            ->one();

        $selectExcel = '
            SELECT 
                   nd.nakladnoy_id AS id, 
                   n.enter_date, m.code_name, 
                   p.name AS parameter, 
                   m.name AS material, 
                   u.username, 
                   nd.quantity, 
                   n.created_date 
            FROM nakladnoy_details AS nd 
            INNER JOIN nakladnoy AS n ON nd.nakladnoy_id = n.id
            INNER JOIN supplier AS s ON n.supplier_id = s.id
            INNER JOIN users AS u ON s.user_id = u.id
            INNER JOIN materials AS m ON nd.material_id = m.id
            INNER JOIN material_parameter AS mp ON nd.material_id = mp.material_id
            INNER JOIN parameter AS p ON mp.parameter_id = p.id
            WHERE nd.nakladnoy_id = '.$id;
        $model = Yii::$app->db->createCommand($selectExcel)->queryAll();

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");
        $objPHPExcel->setActiveSheetIndex(0)
            ->mergeCells("A1:B1")
            ->mergeCells("A2:B2")
            ->mergeCells("C1:D1")
            ->mergeCells("C2:D2")
            ->mergeCells("E1:F1")
            ->mergeCells("E2:F2")
            ->setCellValue("A1", 'Код')
            ->setCellValue("A2", $command['code'])
            ->setCellValue("C1", 'Доставщик')
            ->setCellValue("C2", $command['username'])
            ->setCellValue("E1", 'Дата')
            ->setCellValue("E2", date('d-m-Y', strtotime($command['enter_date'])))
            ->setCellValue("A3", "№")
            ->setCellValue('B3', 'Инструмент')
            ->setCellValue("C3", "Обозначение")
            ->setCellValue("D3", "Размер")
            ->setCellValue("E3", "Кол-во")
            ->setCellValue("F3", "Дата");

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('60');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('40');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('18');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('20');

        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);

        $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'size'  => 10,
                'name'  => 'Arial',
                'text-align' => 'center',
            ),
            'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,),
        );

        $valueStyle = array(
            'font'  => array(
                'size'  => 10,
                'name'  => 'Arial',
                'text-align' => 'center',
            ),
            'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,),
        );

        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($valueStyle);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($valueStyle);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($valueStyle);
        $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('C3')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('D3')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('E3')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('F3')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('A:F')->applyFromArray($valueStyle);

        $border = array(
            'borders'=>array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                ),
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($border);
        $objPHPExcel->getActiveSheet()->getStyle('A3:F3')->applyFromArray($border);
        $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($border);

        $borderInside = array(
            'borders'=>array(
                'inside' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                ),
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A3:F3')->applyFromArray($borderInside);
        $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($borderInside);
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($borderInside);

        $background = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'b5d0e8')
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($background);
        $objPHPExcel->getActiveSheet()->getStyle('A3:F3')->applyFromArray($background);

        $i = 4;
        $a = 3;

        foreach ($model as $value)
        {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $i-3)
                ->setCellValue('B'.$i, $value['code_name'])
                ->setCellValue('C'.$i, $value['material'])
                ->setCellValue('D'.$i, $value['parameter'])
                ->setCellValue('E'.$i, $value['quantity'])
                ->setCellValue('F'.$i, date('d-m-Y'), $value['created_date']);
            $i++;
            $a++;
        }
        $objPHPExcel->getActiveSheet()->getStyle('A3:F'.$a)->applyFromArray($border);
        $objPHPExcel->getActiveSheet()->getStyle('A3:F'.$a)->applyFromArray($borderInside);



        $file_name = "Nakladnoy".strtotime(date('Y-m-d H:i:s')).'.xls';

        $objPHPExcel->getActiveSheet()->setTitle('Nakladnoy');
        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$file_name.'"');

        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }

    public function actionDeleteNakladnoy()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = Yii::$app->request->get('id');
            NakladnoyDetails::deleteAll(['nakladnoy_id' => $id]);
            $deleteNak = Nakladnoy::findOne($id);
            $deleteNak->delete();
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionSaveReceiveNak()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['n']) && !empty($_GET['n'])) {
                if ($_GET['n'] == 'true') {
                    foreach ($_GET['selectForm'] as $key => $value){
                        $materialHistory = new NakladnoyDetailsHistory();
                        $materialHistory->nakladnoy_details_id = $value['details'];
                        $materialHistory->quantity = $value['count'];
                        $materialHistory->price = $value['price'];
                        $materialHistory->save();

                        if (isset($value['material']) and !empty($value['material'])){
                            $materialHistory = new MaterialHistory();
                            $materialHistory->material_id = $value['material'];
                            $materialHistory->quantity = $value['count'];
                            $materialHistory->nakladnoy_id = $_GET['id'];
                            $materialHistory->created_date = date('Y-m-d H:i:s');
                            $materialHistory->price = $value['price'];
                            $materialHistory->mark_id = $value['mark'];
                            $materialHistory->save();

                            $validateInterval = MaterialInterval::find()->where(['enter_date' => date('Y-m-d'), 'material_id' => $value['material'], 'mark_id' => $value['mark']])->one();
                            if (isset($validateInterval) and !empty($validateInterval)){
                                $validateInterval->total = $validateInterval->total + $value['count'];
                                $validateInterval->save();
                            } else {
                                $selectInterval = MaterialInterval::find()->where(['mark_id' => $value['mark'], 'material_id' => $value['material']])->orderBy(['id' => SORT_DESC])->one();
                                $newInterval = new MaterialInterval();
                                $newInterval->material_id = $value['material'];
                                $newInterval->enter_date = date('Y-m-d');
                                $newInterval->mark_id = $value['mark'];
                                $newInterval->total = (isset($selectInterval) and !empty($selectInterval)) ? $selectInterval->total + $value['count'] : $value['count'];
                                $newInterval->save();
                            }
                        }
                        $updateNak = Nakladnoy::findOne($_GET['id']);
                        $updateNak->status = 3;
                        $updateNak->save();
                        return ['status' => 'success'];
                    }

                }else {
                    $id = Yii::$app->request->get('id');
                    $selectNak = "
                        SELECT 
                            nd.id as id, 
                            m.id as material_id, 
                            ma.id as mark_id, 
                            m.name AS material, 
                            ma.name AS mark, 
                            nd.nakladnoy_id, 
                            nd.quantity 
                        FROM nakladnoy_details AS nd 
                        INNER JOIN materials AS m ON nd.material_id = m.id
                        INNER JOIN marks AS ma ON nd.mark_id = ma.id
                        where nd.nakladnoy_id = ".$id;
                    $model = Yii::$app->db->createCommand($selectNak)->queryAll();
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('count-and-price', [
                            'model' => $model,
                            'id' => $id
                        ]),
                    ];
                }
            }
        }
    }

    /**
     * Finds the Nakladnoy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Nakladnoy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Nakladnoy::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
