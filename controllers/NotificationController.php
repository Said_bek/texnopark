<?php

namespace app\controllers;

use app\models\Constructor;
use Yii;
use app\models\Notification;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NotificationController implements the CRUD actions for Notification model.
 */
class NotificationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Notification models.
     * @return mixed
     */
    public function actionUpdateNotification()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && !empty($_GET['id']) && $_GET['id'] > 0) {
                $id = Yii::$app->request->get('id');
                $updateStatus = Notification::findOne($id);
                $updateStatus->status = 2;
                $updateStatus->save();

                $user_id = Yii::$app->user->identity->id;
                $selectOrders = '
                    SELECT 
                       n.id AS notification_id, 
                       o.id, o.proirity, 
                       o.order_name, 
                       n.enter_date,
                       n.module_id
                    FROM notification AS n 
                    INNER JOIN orders AS o ON n.order_id = o.id
                    WHERE n.module_id IN (
                         SELECT mu.modul_id FROM modul_users AS mu WHERE mu.user_id = '.$user_id.'
                    ) AND n.end_date IS NULL AND o.status = 1 AND n.status = 1;';
                $orders = Yii::$app->db->createCommand($selectOrders)->queryAll();
                $notificationCount = count($orders);
                return [
                    'status' => 'success',
                    'count' => $notificationCount,
                    'content' => $this->renderAjax('list.php',[
                        'model' => $orders,
                    ]),
                ];
            }
        }
    }


    /**
     * Finds the Notification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notification::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
