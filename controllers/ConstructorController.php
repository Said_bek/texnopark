<?php

namespace app\controllers;

use app\models\MaterialMarks;
use app\models\Materials;
use app\models\MaterialSettings;
use app\models\Modules;
use app\models\Notification;
use app\models\OrderDetails;
use app\models\Orders;
use app\models\Users;
use app\models\SpecificationMaterials;
use app\models\Specification;
use app\models\SpecificationConstructor;
use Yii;
use app\models\Constructor;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ModulUsers;
use yii\data\ActiveDataProvider;
use function GuzzleHttp\Psr7\str;

require(Yii::getAlias('@vendor') . "/phpexcel/PHPExcel.php");

/**
 * ConstructorController implements the CRUD actions for Constructor model.
 */
class ConstructorController extends Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
        else {
            $user_id = Yii::$app->user->id;
            $checkUser = ModulUsers::find()->where(['user_id' => $user_id, 'modul_id' => 2])->one();
            if (!isset($checkUser)) {
                $this->redirect('/index.php/site/login');
            }
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->user->can('Admin') or Yii::$app->user->can('Head Constructor')) {
            $selectOrders = '
                SELECT 
                    o.id,
                    o.created_date,
                    o.exit_date,
                    c.title AS category_name,
                    cl.company_name,
                    o.proirity,
                    o.approved,
                    o.status,
                    o.specification_status,
                    o.order_from
                FROM orders AS o
                INNER JOIN notification AS n ON o.id = n.order_id AND n.module_id = 2
                INNER JOIN category AS c ON o.category_id = c.id
                INNER JOIN clients as cl ON o.client_id = cl.id 
                WHERE n.end_date IS NULL';
            $orders = Yii::$app->db->createCommand($selectOrders)->queryAll();
        } else {
            $selectOrders = '
            SELECT 
                o.id,
                o.created_date,
                o.exit_date,
                c.title AS category_name,
                cl.company_name,
                o.proirity,
                o.approved,
                o.status,
                o.specification_status,
                o.order_from
            FROM specification_constructor AS sc
            INNER JOIN specification AS s ON sc.specification_id = s.id
            INNER JOIN orders AS o ON o.id = s.order_id
            INNER JOIN category AS c ON o.category_id = c.id
            INNER JOIN notification AS n ON o.id = n.order_id
            INNER JOIN clients as cl ON o.client_id = cl.id
            WHERE sc.constructor_id = 3 AND n.end_date IS NULL AND n.module_id = 2
            GROUP BY 
                o.id,
                o.created_date,
                o.exit_date,
                c.title,
                cl.company_name,
                o.proirity,
                o.approved,
                o.status,
                o.specification_status,
                o.order_from';
            $orders = Yii::$app->db->createCommand($selectOrders)->queryAll();
        }
        return $this->render('index', [
            'orders' => $orders,
        ]);
    }

    public function actionView($id)
    {
        if (Yii::$app->user->can('Admin') or Yii::$app->user->can('Head Constructor')) {
            $selectOrders = '
                SELECT 
                       s.id, 
                       s.class, 
                       s.name, 
                       s.created_date, 
                       sc.deadline, 
                       u.username,
                       s.active,
                       s.order_id
                FROM specification AS s 
                INNER JOIN specification_constructor AS sc ON s.id = sc.specification_id
                INNER JOIN constructor AS c ON sc.constructor_id = c.id
                INNER JOIN users AS u ON c.user_id = u.id
                WHERE s.order_id = ' . $id;
            $constructor = Yii::$app->db->createCommand($selectOrders)->queryAll();
        } else {
            $user_id = Yii::$app->user->id;
            if (isset($user_id) and !empty($user_id)){
                $selectOrders = '
                SELECT 
                       s.id, 
                       s.class, 
                       s.name as name, 
                       s.created_date, 
                       sc.deadline, 
                       u.username,
                       s.active,
                       s.order_id
                FROM specification AS s 
                INNER JOIN specification_constructor AS sc ON s.id = sc.specification_id
                INNER JOIN constructor AS c ON sc.constructor_id = c.id
                INNER JOIN users AS u ON c.user_id = u.id
                WHERE u.id = ' . $user_id . ' AND s.order_id = ' . $id;
                $constructor = Yii::$app->db->createCommand($selectOrders)->queryAll();
            } else {
                $this->redirect('/index.php/site/login');
            }
        }

        return $this->render('view_constructor', [
            'model' => $constructor,
            'id' => $id
        ]);
    }

    public function actionConstructors()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Constructor::find()->orderBy(['id' => SORT_ASC]),
        ]);

        return $this->render('constructors', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewSpecification($id)
    {
        $codeAndCategory = Specification::find()->where('id = :id', [':id' => $id])->with(['code', 'code.category'])->one();
        $selectOrders = SpecificationMaterials::find()->where('specification_id = :specification_id',[':specification_id' => $id])->all();
        $materials = Materials::find()->where('status = :one', [':one' => 1])->all();
        return $this->render('view', [
            'model' => $selectOrders,
            'id' => $id,
            'code' => $codeAndCategory,
            'materials' => $materials
        ]);
    }

    public function actionAddConstructor()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['s']) && !empty($_GET['s'])) {
                if ($_GET['s'] == 'true') {
                    $name = $_GET['name'];
                    $constructor = $_GET['constructor'];
                    $date = $_GET['date'];
                    $time = $_GET['time'];
                    $radio = $_GET['radio'];
                    $id = $_GET['id'];
                    $error = false;

                    if (!(isset($name) and !empty($name))) {
                        $error = true;
                        return [
                            'status' => 'error_name',
                        ];
                    }

                    if (!(isset($constructor) and !empty($constructor))) {
                        $error = true;
                        return [
                            'status' => 'error_constructor',
                        ];
                    }

                    if (!(isset($date) and !empty($date))) {
                        $error = true;
                        return [
                            'status' => 'error_date',
                        ];
                    }

                    if (!(isset($time) and !empty($time))) {
                        $error = true;
                        return [
                            'status' => 'error_time',
                        ];
                    }

                    if (!(isset($id) and !empty($id)) and $id > 0) {
                        $error = true;
                        return [
                            'status' => 'error_id',
                        ];
                    }

                    if (!$error) {
                        if (date('Y-m-d H:i') < date('Y-m-d', strtotime($date)) . " " . $time) {
                            $transaction = Yii::$app->db->beginTransaction();
                            try {
                                $model = new Specification();
                                $model->order_id = $id;
                                $model->created_date = date('Y-m-d H:i:s');
                                $model->name = $name;
                                $model->active = 0;
                                $model->class = $radio;
                                if ($model->save(false)) {
                                    $modelConstructor = new SpecificationConstructor();
                                    $modelConstructor->specification_id = $model->id;
                                    $modelConstructor->constructor_id = $constructor;
                                    $modelConstructor->enter_date = $model->created_date;
                                    $modelConstructor->deadline = date('Y-m-d H:i', strtotime($date . " " . $time));
                                    $modelConstructor->save();
                                }
                                $transaction->commit();

                                $user_id = Yii::$app->user->id;
                                $selectUsers = Users::find()->where(['id' => $user_id])->one();
                                if (isset($selectUsers) and !empty($selectUsers)){
                                    $user_name = $selectUsers->username;
                                    eventUser($user_name, date('Y-m-d H:i:s'), 21, "Nakladnoy qo'shildi");
                                }

                                return ['status' => 'success'];
                            } catch (Exception $e) {
                                $transaction->rollBack();
                                throw $e;
                            }
                        } else {
                            return ['status' => 'error_date_time'];
                        }
                    }
                } else {
                    $selectConstructors = '
                        SELECT 
                               c.id, 
                               c.status, 
                               c.type, 
                               u.username 
                        FROM constructor AS c 
                        INNER JOIN users AS u ON c.user_id = u.id
                        WHERE c.status = 1';
                    $constructors = Yii::$app->db->createCommand($selectConstructors)->queryAll();
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('specification_form.php', [
                            'model' => $constructors
                        ]),
                    ];
                }
            }
        }
    }

    public function actionUpdateConstructor()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && isset($_GET['update']) && $_GET['id'] > 0) {
                if ($_GET['update'] == 'true') {
                    $name = $_GET['name'];
                    $date = $_GET['date'];
                    $time = $_GET['time'];
                    $radio = $_GET['radio'];
                    $id = $_GET['id'];
                    $error = false;

                    if (!(isset($name) and !empty($name))) {
                        $error = true;
                        return [
                            'status' => 'error_name',
                        ];
                    }

                    if (!(isset($date) and !empty($date))) {
                        $error = true;
                        return [
                            'status' => 'error_date',
                        ];
                    }

                    if (!(isset($time) and !empty($time))) {
                        $error = true;
                        return [
                            'status' => 'error_time',
                        ];
                    }

                    if (!(isset($id) and !empty($id)) and $id > 0) {
                        $error = true;
                        return [
                            'status' => 'error_id',
                        ];
                    }

                    if (!$error) {
                        $selectDate = SpecificationConstructor::find()->where(['specification_id' => $id])->one();
                        if (date('Y-m-d H:i') < date('Y-m-d', strtotime($date)) . " " . $time or $selectDate->deadline == date('Y-m-d', strtotime($date)) . " " . $time) {
                            $transaction = Yii::$app->db->beginTransaction();
                            try {
                                $select = Specification::find()->where(['id' => $id])->one();
                                $select->name = $name;
                                $select->class = $radio;
                                if ($select->save(false)) {
                                    $model = SpecificationConstructor::find()->where(['specification_id' => $select->id])->one();
                                    $model->deadline = $date . " " . $time;
                                    $model->save();
                                } else {
                                    pre($select->errors);
                                }

                                $transaction->commit();

                                $user_id = Yii::$app->user->id;
                                $selectUsers = Users::find()->where(['id' => $user_id])->one();
                                if (isset($selectUsers) and !empty($selectUsers)){
                                    $user_name = $selectUsers->username;
                                    eventUser($user_name, date('Y-m-d H:i:s'), 21, "Nakladnoy o'zgartirildi");
                                }

                                return [
                                    'status' => 'success',
                                ];
                            } catch (Exception $e) {
                                $transaction->rollBack();
                                throw $e;
                            }
                        } else {
                            return [
                                'status' => 'error_date_time',
                            ];
                        }
                    }
                } else {
                    $select = '
                        SELECT 
                            s.order_id, 
                            s.class, 
                            s.id, 
                            s.name, 
                            sc.deadline, 
                            sc.constructor_id,
                            u.username
                        FROM specification AS s 
                        INNER JOIN specification_constructor AS sc ON s.id = sc.specification_id
                        INNER JOIN constructor AS c ON sc.constructor_id = c.id
                        INNER JOIN users AS u ON u.id = c.user_id
                        WHERE s.id = ' . $_GET['id'];
                    $constructor = Yii::$app->db->createCommand($select)->queryOne();
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('update_specification_form', [
                            'constructor' => $constructor,
                        ]),
                    ];
                }
            }
        }
    }

    public function actionUpdateSettings($id, $specification_id)
    {
        $select = SpecificationMaterials::find()->where(['id' => $id])->one();
        return $this->render('settings_form', [
            'model' => $select,
            'id' => $id,
            'specification_id' => $specification_id
        ]);
    }

    public function actionUpdateSpecification()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && $_GET['id'] > 0) {
                $material = $_GET['material'];
                $mark = $_GET['mark'];
                $count = $_GET['count'];
                $terma = $_GET['terma'];
                $width = $_GET['width'];
                $height = $_GET['height'];
                $weight = $_GET['weight'];
                $id = $_GET['id'];
                $error = false;

                if (!(isset($material) and !empty($material))) {
                    $error = true;
                    return [
                        'status' => 'error_material',
                    ];
                }

                if (!(isset($mark) and !empty($mark))) {
                    $error = true;
                    return [
                        'status' => 'error_mark',
                    ];
                }

                if (!(isset($count) and !empty($count))) {
                    $error = true;
                    return [
                        'status' => 'error_count',
                    ];
                }

                if (!(isset($width) and !empty($width))) {
                    $error = true;
                    return [
                        'status' => 'error_width',
                    ];
                }

                if (!(isset($height) and !empty($height))) {
                    $error = true;
                    return [
                        'status' => 'error_height',
                    ];
                }

                if (!(isset($weight) and !empty($weight))) {
                    $error = true;
                    return [
                        'status' => 'error_weight',
                    ];
                }

                if (!$error) {
                    $intervalW = MaterialSettings::find()->where(['material_id' => $material])->andWhere(['<=', 'width_f', $width])->andWhere(['>=', 'width_s', $width])->one();
                    if (isset($intervalW) and !empty($intervalW)) {
                        $intervalH = MaterialSettings::find()->where(['material_id' => $material])->andWhere(['<=', 'height_f', $height])->andWhere(['>=', 'height_s', $height])->one();
                        if (isset($intervalH) and !empty($intervalH)) {
                            $intervalWe = MaterialSettings::find()->where(['material_id' => $material])->andWhere(['<=', 'weight_f', $weight])->andWhere(['>=', 'weight_s', $weight])->one();
                            if (isset($intervalWe) and !empty($intervalWe)) {
                                $model = SpecificationMaterials::find()->where(['id' => $id])->one();
                                if (isset($model) and !empty($model)) {
                                    $model->material_id = $material;
                                    $model->mark_id = $mark;
                                    $model->width = $width;
                                    $model->height = $height;
                                    $model->weight = $weight;
                                    $model->w_approx = $intervalW->w_approx;
                                    $model->h_approx = $intervalH->h_approx;
                                    $model->we_approx = $intervalWe->we_approx;
                                    $model->count = $count;
                                    $model->terma = (isset($terma) and !empty($terma)) ? $terma : 1;
                                    if ($model->save()) {
                                        $user_id = Yii::$app->user->id;
                                        $selectUsers = Users::find()->where(['id' => $user_id])->one();
                                        if (isset($selectUsers) and !empty($selectUsers)){
                                            $user_name = $selectUsers->username;
                                            eventUser($user_name, date('Y-m-d H:i:s'), 21, "Specification o'zgartirildi");
                                        }
                                        return ['status' => 'success'];
                                    }
                                }
                            } else {
                                return [
                                    'status' => 'error_weight',
                                ];
                            }
                        } else {
                            return [
                                'status' => 'error_height',
                            ];
                        }
                    } else {
                        return [
                            'status' => 'error_width',
                        ];
                    }
                }
            }
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $user_id = Yii::$app->user->id;
        $selectUsers = Users::find()->where(['id' => $user_id])->one();
        if (isset($selectUsers) and !empty($selectUsers)) {
            $user_name = $selectUsers->username;
            eventUser($user_name, date('Y-m-d H:i:s'), 3);
            return $this->redirect(['index']);
        }
    }

    public function actionPlusInput()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $count =  Yii::$app->request->get('count');
            $materials = Materials::find()->where('status = :one', [':one' => 1])->all();
            return [
                'status' => 'success',
                'content' => $this->renderAjax('plus_input.php', [
                    'count' => $count,
                    'materials' => $materials
                ]),
            ];
        }
    }

    public function actionSaveCount()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            foreach ($_POST['selectForm'] as $key => $value) {
                $error = false;
                if (!($value['count'] != 0)) {
                    $error = true;
                    return [
                        'status' => 'count_null',
                    ];
                }

                if (!(isset($value['count']) and !empty($value['count']))) {
                    $error = true;
                    return [
                        'status' => 'error_count',
                    ];
                }

                if (!(isset($value['countWidth']) and !empty($value['countWidth']))) {
                    $error = true;
                    return [
                        'status' => 'error_width',
                    ];
                }

                if (!(isset($value['countWidth']) and !empty($value['countWidth']))) {
                    $error = true;
                    return [
                        'status' => 'error_width',
                    ];
                }

                if (!(isset($value['countHeight']) and !empty($value['countHeight']))) {
                    $error = true;
                    return [
                        'status' => 'error_height',
                    ];
                }

                if (!(isset($value['countWeight']) and !empty($value['countWeight']))) {
                    $error = true;
                    return [
                        'status' => 'error_weight',
                    ];
                }

                if (!$error){
                    $intervalW = MaterialSettings::find()->where(['material_id' => $value['select4']])->andWhere(['<=', 'width_f', $value['countWidth']])->andWhere(['>=', 'width_s', $value['countWidth']])->one();
                    if (isset($intervalW) and !empty($intervalW)) {
                        $intervalH = MaterialSettings::find()->where(['material_id' => $value['select4']])->andWhere(['<=', 'height_f', $value['countHeight']])->andWhere(['>=', 'height_s', $value['countHeight']])->one();
                        if (isset($intervalH) and !empty($intervalH)) {
                            $intervalWe = MaterialSettings::find()->where(['material_id' => $value['select4']])->andWhere(['<=', 'weight_f', $value['countWeight']])->andWhere(['>=', 'weight_s', $value['countWeight']])->one();
                            if (isset($intervalWe) and !empty($intervalWe)) {
                                $model = new SpecificationMaterials();
                                $model->code = $value['code'];
                                $model->material_id = $value['select4'];
                                $model->mark_id = $value['select5'];
                                $model->specification_id = $_POST['id'];
                                $model->width = $value['countWidth'];
                                $model->height = $value['countHeight'];
                                $model->weight = $value['countWeight'];
                                $model->w_approx = $intervalW->w_approx;
                                $model->h_approx = $intervalH->h_approx;
                                $model->we_approx = $intervalWe->we_approx;
                                $model->count = $value['count'];
                                $model->terma = (isset($value['terma']) and !empty($value['terma'])) ? $value['terma'] : 1;
                                $model->save();
                            } else {
                                return ['status' => 'fail_weight'];
                            }
                        } else {
                            return ['status' => 'fail_height'];
                        }
                    } else {
                        return ['status' => 'fail_width'];
                    }
                }
            }
            return [
                'status' => 'success',
            ];
        }
    }

    public function actionInterval()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['type']) and !empty($_GET['type'])) {
                if (isset($_GET['width']) && !empty($_GET['width'])) {
                    if (isset($_GET['material']) and !empty($_GET['material'])) {
                        $material = $_GET['material'];
                        $width = $_GET['width'];
                        if ($_GET['type'] == 'w') {
                            $interval = MaterialSettings::find()->where(['material_id' => $material])->andWhere(['<=', 'width_f', $width])->andWhere(['>=', 'width_s', $width])->one();
                            if (isset($interval) and !empty($interval)) {
                                return [
                                    'status' => 'success',
                                    'content' => $this->renderAjax('input_count.php', [
                                        'interval' => $interval,
                                        'type' => 'width'
                                    ]),
                                ];
                            } else {
                                return [
                                    'status' => 'success',
                                    'content' => $this->renderAjax('input_count', [
                                        'interval' => $interval
                                    ]),
                                ];
                            }
                        }
                        if ($_GET['type'] == 'h') {
                            $interval = MaterialSettings::find()->where(['material_id' => $material])->andWhere(['<=', 'height_f', $width])->andWhere(['>=', 'height_s', $width])->one();
                            if (isset($interval) and !empty($interval)) {
                                return [
                                    'status' => 'success',
                                    'content' => $this->renderAjax('input_count.php', [
                                        'interval' => $interval,
                                        'type' => 'height'
                                    ]),
                                ];
                            } else {
                                return [
                                    'status' => 'success',
                                    'content' => $this->renderAjax('input_count.php', [
                                        'interval' => $interval
                                    ]),
                                ];
                            }
                        }
                        if ($_GET['type'] == 'g') {
                            $interval = MaterialSettings::find()->where(['material_id' => $material])->andWhere(['<=', 'weight_f', $width])->andWhere(['>=', 'weight_s', $width])->one();
                            if (isset($interval) and !empty($interval)) {
                                return [
                                    'status' => 'success',
                                    'content' => $this->renderAjax('input_count.php', [
                                        'interval' => $interval,
                                        'type' => 'weight'
                                    ]),
                                ];
                            } else {
                                return [
                                    'status' => 'success',
                                    'content' => $this->renderAjax('input_count.php', [
                                        'interval' => $interval
                                    ]),
                                ];
                            }
                        }
                    }
                }
            }
        }
    }

    public function actionChangeMaterial()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['material']) && !empty($_GET['material'])) {
                $material = $_GET['material'];
                $selectMarks = MaterialMarks::find()->where(['material_id' => $material])->all();
                return [
                    'status' => 'success',
                    'content' => $this->renderAjax('change_form.php', [
                        'selectMarks' => $selectMarks,
                    ]),
                ];
            }
        }
    }

    public function actionDeleteSpecification()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && !empty($_GET['id'])) {
                $id = Yii::$app->request->get('id');
                $materialSettings = SpecificationMaterials::find()->where(['id' => $id])->one();
                $materialSettings->delete();
                return [
                    'status' => 'success',
                ];
            }
        }
    }

    public function actionDeleteConstructor($id)
    {
        $id = Yii::$app->request->get('id');
        $deleteConstructor = Constructor::findOne($id);
        $deleteConstructor->status = $deleteConstructor->status == 1 ? 0 : 1;
        $deleteConstructor->save();
        $this->redirect(['constructors']);
    }

    public function actionImage($id)
    {
        $model = Specification::find()->where(['id' => $id])->one();
        if ($model->load(Yii::$app->request->post())) {
            $imageName = strtotime(date("Y-m-d H:i:s")) . rand(1000, 9999);
            $model->img = UploadedFile::getInstance($model, 'image');
            $model->img->saveAs('../web/uploads/specification/' . $imageName . "." . $model->img->extension);
            $model->img = $imageName . "." . $model->img->extension;
            if ($model->save(false)) {
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
    }

    public function actionExcel($id)
    {
        $selectExcel = '
            SELECT 
                   sm.id, 
                   sm.width, 
                   sm.height, 
                   sm.weight, 
                   sm.w_approx, 
                   sm.h_approx, 
                   sm.we_approx, 
                   sm.count, 
                   sm.terma, 
                   ma.name AS mark, 
                   m.name AS material 
            FROM specification_materials AS sm 
            INNER JOIN materials AS m ON sm.material_id = m.id 
            INNER JOIN marks AS ma ON sm.mark_id = ma.id
            WHERE sm.specification_id = ' . $id;
        $model = Yii::$app->db->createCommand($selectExcel)->queryAll();

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");
        $objPHPExcel->setActiveSheetIndex(0)
            ->mergeCells("A1:A2")
            ->setCellValue("A1", "#")
            ->mergeCells("B1:B2")
            ->setCellValue('B1', 'Material nomi')
            ->mergeCells("C1:C2")
            ->setCellValue('C1', "Marka nomi")
            ->mergeCells("D1:I1")
            ->setCellValue('D1', "Detal parametrlari")
            ->setCellValue('D2', "Shirina")
            ->setCellValue('E2', "Dopusk")
            ->setCellValue('F2', "Dlina")
            ->setCellValue('G2', "Dopusk")
            ->setCellValue('H2', "Tolshina")
            ->setCellValue('I2', "Dopusk")
            ->mergeCells("J1:O1")
            ->setCellValue('J1', "Kesish o'lchamlari")
            ->setCellValue('J2', "Shirina")
            ->setCellValue('K2', "Dopusk")
            ->setCellValue('L2', "Dlina")
            ->setCellValue('M2', "Dopusk")
            ->setCellValue('N2', "Tolshina")
            ->setCellValue('O2', "Dopusk")
            ->mergeCells("P1:P2")
            ->setCellValue('P1', "Soni")
            ->mergeCells("Q1:Q2")
            ->setCellValue('Q1', "Terma")
            ->mergeCells("R1:R2")
            ->setCellValue('R1', "Ves");

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('5');
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('30');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('25');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D:O')->setWidth('15');
        $objPHPExcel->getActiveSheet()->getColumnDimension('P:R')->setWidth('12');

        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);

        $styleArray = array(
            'font' => array(
                'bold' => true,
                'size' => 10,
                'name' => 'Arial',
                'text-align' => 'center',
            ),
            'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,),
        );

        $valueStyle = array(
            'font' => array(
                'size' => 10,
                'name' => 'Arial',
                'text-align' => 'center',
            ),
            'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,),
        );


        $objPHPExcel->getActiveSheet()->getStyle('A1:R2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle('A:R')->applyFromArray($valueStyle);

        $border = array(
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                ),
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A1:R2')->applyFromArray($border);

        $borderInside = array(
            'borders' => array(
                'inside' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                ),
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A1:R2')->applyFromArray($borderInside);

        $background = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'c7efff')
            )
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:R2')->applyFromArray($background);

        $i = 3;
        $a = 2;

        foreach ($model as $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $i - 2)
                ->setCellValue('B' . $i, $value['material'])
                ->setCellValue('C' . $i, $value['mark'])
                ->setCellValue('D' . $i, $value['width'])
                ->setCellValue('E' . $i, "±" . $value['w_approx'])
                ->setCellValue('F' . $i, $value['height'])
                ->setCellValue('G' . $i, "±" . $value['h_approx'])
                ->setCellValue('H' . $i, $value['weight'])
                ->setCellValue('I' . $i, "±" . $value['we_approx'])
                ->setCellValue('J' . $i, $value['width'] + 1)
                ->setCellValue('K' . $i, "± 10")
                ->setCellValue('L' . $i, $value['height'] + 1)
                ->setCellValue('M' . $i, "± 10")
                ->setCellValue('N' . $i, $value['weight'])
                ->setCellValue('O' . $i, "± 10")
                ->setCellValue('P' . $i, $value['count'])
                ->setCellValue('Q' . $i, $value['terma'])
                ->setCellValue('R' . $i, round((((($value['width'] + 1 + 10) * ($value['height'] + 1 + 10) * ($value['weight'] + 10)) * 0.785) / 100000) * $value['count'], 2));

            $i++;
            $a++;
        }
        $objPHPExcel->getActiveSheet()->getStyle('A3:R' . $a)->applyFromArray($border);
        $objPHPExcel->getActiveSheet()->getStyle('A3:R' . $a)->applyFromArray($borderInside);


        $file_name = "Konstruktor" . strtotime(date('Y-m-d H:i:s')) . '.xls';

        $objPHPExcel->getActiveSheet()->setTitle('Konstruktor');
        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $file_name . '"');

        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }

    public function actionUpdateSection()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['s']) && !empty($_GET['s'])) {
                if ($_GET['s'] == 'true') {
                    $order_id = Yii::$app->request->get('id');
                    $module_id = Yii::$app->request->get('module_id');

                    if (!(isset($order_id) and !empty($order_id)))
                        return ['status' => 'error_id'];

                    if (!(isset($module_id) and !empty($module_id)))
                        return ['status' => 'error_module'];

                    $model = Notification::find()->where(['order_id' => $order_id, 'end_date' => NULL])->one();
                    if (isset($model) and !empty($model)){
                        $model->end_date = date('Y-m-d H:i:s');
                        $model->status = 3;
                        if ($model->save()){
                            $next_section = new Notification();
                            $next_section->module_id = $module_id;
                            $next_section->order_id = $order_id;
                            $next_section->enter_date = date("Y-m-d H:i:s", strtotime('+1 second', strtotime(date('Y-m-d H:i:s'))));
                            $next_section->status = 1;
                            $next_section->save();

                            return [
                                'status' => 'success',
                            ];
                        } else {
                            pre($model->errors);
                        }
                    }
                }else {
                    $model = Modules::find()->where(['id' => [3,7]])->all();
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('section.php',[
                            'model' => $model
                        ]),
                    ];
                }
            }
        }
    }

    public function actionUpdateSpecificationStatus()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && !empty($_GET['id']) and $_GET['id'] > 0) {
                $id = Yii::$app->request->get('id');
                $order_id = Yii::$app->request->get('order_id');
                $updateStatus = Specification::findOne($id);
                $active = ($updateStatus->active == 1) ? 0 : 1;
                $updateStatus->active = $active;
                $updateStatus->save();

                $updateSpecificationStatus = Orders::findOne($order_id);
                $updateSpecificationStatus->specification_status = $active;
                $updateSpecificationStatus->save();

                OrderDetails::deleteAll(['order_id' => $order_id]);
                $selectMaterials = SpecificationMaterials::findAll(['specification_id' => $id]);
                if (!empty($selectMaterials) and $active == 1){
                    foreach ($selectMaterials as $value) {
                        $insertDetails = new OrderDetails();
                        $insertDetails->order_id = $order_id;
                        $insertDetails->material_id = $value->material_id;
                        $insertDetails->mark_id = $value->mark_id;
                        $insertDetails->specification_id = $id;
                        $insertDetails->width = $value->width;
                        $insertDetails->height = $value->height;
                        $insertDetails->weight = $value->weight;
                        $insertDetails->w_approx = $value->w_approx;
                        $insertDetails->h_approx = $value->h_approx;
                        $insertDetails->we_approx = $value->we_approx;
                        $insertDetails->count = $value->count;
                        $insertDetails->terma = $value->terma;
                        $insertDetails->code = $value->code;
                        $insertDetails->save();
                    }
                }
                return['status' => 'success'];
            }
        }
    }

    protected function findModel($id)
    {
        if (($model = Constructor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
