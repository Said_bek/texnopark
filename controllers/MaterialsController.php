<?php

namespace app\controllers;

use app\models\Marks;
use app\models\MaterialMarks;
use app\models\MaterialParameter;
use app\models\MaterialSettings;
use Yii;
use app\models\Materials;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Parameter;
use app\models\ModulUsers;

/**
 * MaterialsController implements the CRUD actions for Materials model.
 */
class MaterialsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
        else {
            $user_id = Yii::$app->user->id;
            $checkUser = ModulUsers::find()->where(['user_id' => $user_id, 'modul_id' => 6])->one();
            if (!isset($checkUser)) {
                $this->redirect('/index.php/site/login');
            }
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Materials models.
     * @return mixed
     */
    public function actionIndex()
    {
        $materials = Materials::find()->Where(['status' => 1])->all();
        $selectParameter = "
            SELECT 
                   mp.id, 
                   mp.material_id, 
                   mp.parameter_id, 
                   p.name 
            FROM material_parameter AS mp 
            INNER JOIN parameter AS p ON mp.parameter_id = p.id
            WHERE p.status = 1";
        $parameter = Yii::$app->db->createCommand($selectParameter)->queryAll();
        return $this->render('index', [
            'materials' => $materials,
            'parameter' => $parameter
        ]);
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Displays a single Materials model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $selectMaterial = MaterialSettings::find()->where(['material_id' => $id])->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'settings' => $selectMaterial
        ]);
    }

    /**
     * Creates a new Materials model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Materials();

        $selectParameter = Parameter::find()->where(['status' => 1])->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('_form', [
            'model' => $model,
            'parameter' => $selectParameter
        ]);
    }

    /**
     * Updates an existing Materials model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $arr = [];
        $selectMarkName = Marks::find()->Where(['status' => 1])->all();
        $selectParameter = Parameter::find()->where(['status' => 1])->all();
        $selectMark = MaterialMarks::find()->where(['material_id' => $id])->all();
        foreach ($selectMark as $key => $value){
            $arr[] = $value->mark_id;
        }
        $selectMaterial = "
            SELECT * FROM materials AS m
            LEFT JOIN material_parameter AS mp ON mp.material_id = m.id
            where m.id = ".$id;
        $material = Yii::$app->db->createCommand($selectMaterial)->queryOne();

        return $this->render('update_material_form', [
            'model' => $material,
            'arr' => $arr,
            'mark' => $selectMarkName,
            'parameter' => $selectParameter,
            'id' => $id
        ]);
    }

    /**
     * Deletes an existing Materials model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCode()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $selectCode = Materials::find()->orderBy(['id' => SORT_DESC])->one();
            return [
                'status' => 'success',
                'content' => $this->renderAjax('material_form.php',[
                    'code' => $selectCode,
                ]),
            ];
        }
    }

    public function actionAddMaterial()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $material = $_GET['material'];
            $mark = $_GET['mark'];
            $code = $_GET['code'];
            $parameter = $_GET['parameter'];
            $error = false;
            if (!(isset($material) and !empty($material))) {
                $error = true;
                return [
                    'status' => 'error_material',
                ];
            }

            if (!(isset($mark) and !empty($mark))) {
                $error = true;
                return [
                    'status' => 'error_mark',
                ];
            }

            if (!(isset($code) and !empty($code))) {
                $error = true;
                return [
                    'status' => 'error_code',
                ];
            }

            if (!(isset($parameter) and !empty($parameter))) {
                $error = true;
                return [
                    'status' => 'error_parameter',
                ];
            }
            if (!$error){
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $explodeParameter = explode("_", $parameter);
                    if (isset($explodeParameter[1]) && $explodeParameter[1] == "issetParameter"){
                        $selectParameter = Parameter::find()->where(['id' => $explodeParameter[0]])->one();
                        $parameterId = $selectParameter->id;
                    } else {
                        $selectParameter = Parameter::find()->where(['name' => $parameter])->one();
                        if (isset($selectParameter) and !empty($selectParameter)){
                            return [
                                'status' => 'isset_parameter',
                            ];
                        } else {
                            $newParameter = new Parameter();
                            $newParameter->name = $parameter;
                            $newParameter->status = 1;
                            if($newParameter->save()){

                            } else {
                                pre($newParameter->errors);
                            }
                            $parameterId = $newParameter->id;
                        }
                    }
                    $selectCode = Materials::find()->orderBy(['id' => SORT_DESC])->one();
                    if (isset($selectCode) and !empty($selectCode)){
                        $materialCode = $selectCode->code_number + 1;
                    } else {
                        $materialCode = 1;
                    }
                    $model = new Materials();
                    $model->name = $material;
                    $model->status = 1;
                    $model->code_number = $materialCode;
                    $model->code_name = $code;
                    $model->save();

                    foreach ($mark as $key => $value){
                        $materialMark = new MaterialMarks();
                        $materialMark->material_id = $model->id;
                        $materialMark->mark_id = $value;
                        $materialMark->save();
                    }

                    $materialParameter = new MaterialParameter();
                    $materialParameter->material_id = $model->id;
                    $materialParameter->parameter_id = $parameterId;
                    $materialParameter->save();

                    $transaction->commit();
                    return [
                        'status' => 'success',
                    ];
                } catch (Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
    }

    public function actionUpdateMaterial()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $material = $_GET['material'];
            $mark = $_GET['mark'];
            $code = $_GET['code'];
            $parameter = $_GET['parameter'];
            $id = $_GET['id'];
            $error = false;
            if (!(isset($id) and !empty($id))) {
                $error = true;
                return [
                    'status' => 'error_id',
                ];
            }

            if (!(isset($material) and !empty($material))) {
                $error = true;
                return [
                    'status' => 'error_material',
                ];
            }

            if (!(isset($mark) and !empty($mark))) {
                $error = true;
                return [
                    'status' => 'error_mark',
                ];
            }

            if (!(isset($code) and !empty($code))) {
                $error = true;
                return [
                    'status' => 'error_code',
                ];
            }

            if (!(isset($parameter) and !empty($parameter))) {
                $error = true;
                return [
                    'status' => 'error_parameter',
                ];
            }
            if (!$error){
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $explodeParameter = explode("_", $parameter);
                    if ($explodeParameter[1] == "issetParameter"){
                        $selectParameter = Parameter::find()->where(['id' => $explodeParameter[0]])->one();
                        $parameterId = $selectParameter->id;
                    } else {
                        $selectParameter = Parameter::find()->where(['name' => $parameter])->one();
                        if (isset($selectParameter) and !empty($selectParameter)){
                            return [
                                'status' => 'isset_parameter',
                            ];
                        } else {
                            $newParameter = new Parameter();
                            $newParameter->name = $parameter;
                            $newParameter->status = 1;
                            if($newParameter->save()){

                            } else {
                                pre($newParameter->errors);
                            }
                            $parameterId = $newParameter->id;
                        }
                    }
                    $model = Materials::find()->where(['id' => $id])->one();
                    $model->name = $material;
                    $model->code_name = $code;
                    $model->save();

                    $selectMark = MaterialMarks::find()->where(['material_id' => $id])->all();
                    if (isset($selectMark) and !empty($selectMark)){
                        foreach ($selectMark as $key => $value){
                            $value->delete();
                        }
                    }

                    foreach ($mark as $key => $value){
                        $materialMark = new MaterialMarks();
                        $materialMark->material_id = $model->id;
                        $materialMark->mark_id = $value;
                        $materialMark->save();
                    }

                    $deleteParameter = MaterialParameter::find()->where(['material_id' => $id])->one();
                    if (isset($deleteParameter) and !empty($deleteParameter)){
                        $deleteParameter->delete();
                    }

                    $materialParameter = new MaterialParameter();
                    $materialParameter->material_id = $id;
                    $materialParameter->parameter_id = $parameterId;
                    $materialParameter->save();

                    $transaction->commit();
                    return [
                        'status' => 'success',
                    ];
                } catch (Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                }
            }
        }
    }

    public function actionMaterialSettings()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $error = false;
            if (!(isset($_GET['width_1']) and !empty($_GET['width_1']))) {
                $error = true;
                return [
                    'status' => 'error_width_1',
                ];
            }

            if (!(isset($_GET['width_2']) and !empty($_GET['width_2']))){
                $error = true;
                return [
                    'status' => 'error_width_2',
                ];
            }

            if (!(isset($_GET['width_all']) and !empty($_GET['width_all']))){
                $error = true;
                return [
                    'status' => 'error_width_all',
                ];
            }

            if (!(isset($_GET['height_1']) and !empty($_GET['height_1']))){
                $error = true;
                return [
                    'status' => 'error_height_1',
                ];
            }

            if (!(isset($_GET['height_2']) and !empty($_GET['height_2']))){
                $error = true;
                return [
                    'status' => 'error_height_2',
                ];
            }

            if (!(isset($_GET['height_all']) and !empty($_GET['height_all']))){
                $error = true;
                return [
                    'status' => 'error_height_all',
                ];
            }

            if (!(isset($_GET['weight_1']) and !empty($_GET['weight_1']))){
                $error = true;
                return [
                    'status' => 'error_weight_1',
                ];
            }

            if (!(isset($_GET['weight_2']) and !empty($_GET['weight_2']))){
                $error = true;
                return [
                    'status' => 'error_weight_2',
                ];
            }

            if (!(isset($_GET['weight_all']) and !empty($_GET['weight_all']))){
                $error = true;
                return [
                    'status' => 'error_weight_all',
                ];
            }

            if (!(isset($_GET['id']) and !empty($_GET['id']))){
                $error = true;
                return [
                    'status' => 'error_id',
                ];
            }

            if (!($_GET['width_1'] < $_GET['width_2'])){
                $error = true;
                return [
                    'status' => 'width_sort',
                ];
            }

            if (!($_GET['height_1'] < $_GET['height_2'])){
                $error = true;
                return [
                    'status' => 'height_sort',
                ];
            }

            if (!($_GET['weight_1'] < $_GET['weight_2'])){
                $error = true;
                return [
                    'status' => 'weight_sort',
                ];
            }

            if ($error == false){
                $selectMaterial = MaterialSettings::find()->where(['material_id' => $_GET['id']])->one();
                if (isset($selectMaterial) and !empty($selectMaterial)){
                    if ($selectMaterial->width_s < $_GET['width_1'] and $selectMaterial->width_s < $_GET['width_2']){
                        if ($selectMaterial->height_s < $_GET['height_1'] and $selectMaterial->height_s < $_GET['height_2']){
                            if ($selectMaterial->weight_s < $_GET['weight_1'] and $selectMaterial->weight_s < $_GET['weight_2']){
                                $model = new MaterialSettings();
                                $model->material_id = $_GET['id'];
                                $model->width_f = $_GET['width_1'];
                                $model->width_s = $_GET['width_2'];
                                $model->w_approx = $_GET['width_all'];
                                $model->height_f = $_GET['height_1'];
                                $model->height_s = $_GET['height_2'];
                                $model->h_approx = $_GET['height_all'];
                                $model->weight_f = $_GET['weight_1'];
                                $model->weight_s = $_GET['weight_2'];
                                $model->we_approx = $_GET['weight_all'];
                                $model->save();
                                return [
                                    'status' => 'success',
                                ];
                            } else {
                                return [
                                    'status' => 'weight_sort',
                                ];
                            }
                        } else {
                            return [
                                'status' => 'height_sort',
                            ];
                        }
                    } else {
                        return [
                            'status' => 'width_sort',
                        ];
                    }
                } else {
                    $model = new MaterialSettings();
                    $model->material_id = $_GET['id'];
                    $model->width_f = $_GET['width_1'];
                    $model->width_s = $_GET['width_2'];
                    $model->w_approx = $_GET['width_all'];
                    $model->height_f = $_GET['height_1'];
                    $model->height_s = $_GET['height_2'];
                    $model->h_approx = $_GET['height_all'];
                    $model->weight_f = $_GET['weight_1'];
                    $model->weight_s = $_GET['weight_2'];
                    $model->we_approx = $_GET['weight_all'];
                    $model->save();
                    return [
                        'status' => 'success',
                    ];
                }
            }
        }
    }

    public function actionDeleteMaterial()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && !empty($_GET['id'])) {
                $id = $_GET['id'];
                $materialSettings = MaterialSettings::find()->where(['id' => $id])->one();
                $materialSettings->delete();
                return [
                    'status' => 'success',
                ];
            }
        }
    }

    public function actionUpdateStatus()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && !empty($_GET['id'])) {
                $id = $_GET['id'];
                $materialDelete = Materials::find()->where(['id' => $id])->one();
                $materialDelete->status = 0;
                $materialDelete->save();
                return [
                    'status' => 'success',
                ];
            }
        }
    }

    /**
     * Finds the Materials model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Materials the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Materials::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
