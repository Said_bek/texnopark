<?php

namespace app\controllers;

use app\models\Machines;
use app\models\Modules;
use app\models\Notification;
use app\models\ModulUsers;
use Yii;
use app\models\MachinesPrice;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * FinanceController implements the CRUD actions for MachinesPrice model.
 */
class FinanceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
        else {
            $user_id = Yii::$app->user->id;
            $checkUser = ModulUsers::find()->where(['user_id' => $user_id, 'modul_id' => 4])->one();
            if (!isset($checkUser)) {
                $this->redirect('/index.php/site/login');
            }
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MachinesPrice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Machines::find()->orderBy(['id' => SORT_ASC])
        ]);


        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOrders()
    {
        $selectOrders = '
            SELECT 
                o.id,
                o.created_date,
                o.status,
                o.category_id,
                cl.company_name,
                o.proirity,
                o.order_from,
                c.title AS category_name
            FROM orders AS o
            INNER JOIN notification AS n ON o.id = n.order_id AND n.module_id = 4
            INNER JOIN category AS c ON o.category_id = c.id
            INNER JOIN clients as cl ON o.client_id = cl.id
            WHERE n.end_date IS NULL';
        $orders = Yii::$app->db->createCommand($selectOrders)->queryAll();

        return $this->render('orders', [
            'model' => $orders,
        ]);
    }

    public function actionUpdateSection()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['s']) && !empty($_GET['s'])) {
                if ($_GET['s'] == 'true') {
                    $order_id = Yii::$app->request->get('id');
                    $module_id = Yii::$app->request->get('module_id');

                    if (!(isset($order_id) and !empty($order_id)))
                        return ['status' => 'error_id'];

                    if (!(isset($module_id) and !empty($module_id)))
                        return ['status' => 'error_module'];

                    $model = Notification::find()->where(['order_id' => $order_id, 'end_date' => NULL])->one();
                    if (isset($model) and !empty($model)){
                        $model->end_date = date('Y-m-d H:i:s');
                        $model->status = 3;
                        if ($model->save()){
                            $next_section = new Notification();
                            $next_section->module_id = $module_id;
                            $next_section->order_id = $order_id;
                            $next_section->enter_date = date("Y-m-d H:i:s", strtotime('+1 second', strtotime(date('Y-m-d H:i:s'))));
                            $next_section->status = 1;
                            $next_section->save();
                            return ['status' => 'success'];
                        } else {
                            pre($model->errors);
                        }
                    }
                }else {
                    $model = Modules::findOne(2);
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('section.php',[
                            'model' => $model
                        ]),
                    ];
                }
            }
        }
    }

    /**
     * Displays a single MachinesPrice model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionAddMachine()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['m']) && !empty($_GET['m'])) {
                if ($_GET['m'] == 'true') {
                    if (!(isset($_GET['name']) and !empty($_GET['name'])))
                        return ['status' => 'error_name'];

                    if (!(isset($_GET['price']) and !empty($_GET['price'])))
                        return ['status' => 'error_price'];

                    $name = Yii::$app->request->get('name');
                    $price = Yii::$app->request->get('price');

                    $model = new Machines();
                    $model->name = $name;
                    $model->status = 1;
                    if ($model->save()){
                        $machine_price = new MachinesPrice();
                        $machine_price->machine_id = $model->id;
                        $machine_price->price = $price;
                        $machine_price->enter_date = date('Y-m-d H:i:s');
                        $machine_price->end_date = '9999-12-31 23:59:59';
                        $machine_price->save();

                        return ['status' => 'success'];
                    } else {
                        pre($model->errors);
                    }
                } else {
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('_form.php'),
                    ];
                }
            }
        }
    }

    public function actionUpdateMachine()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && isset($_GET['update']) && $_GET['id'] > 0) {
                if ($_GET['update'] == 'true') {
                    if (!(isset($_GET['name']) and !empty($_GET['name'])))
                        return ['status' => 'error_name'];

                    if (!(isset($_GET['price']) and !empty($_GET['price'])))
                        return ['status' => 'error_price'];

                    $name = Yii::$app->request->get('name');
                    $price = Yii::$app->request->get('price');
                    $id = Yii::$app->request->get('id');

                    $model = Machines::findOne($id);
                    $model->name = $name;
                    if ($model->save()){
                        $machine_price = MachinesPrice::find()
                            ->where(['machine_id' => $id])
                            ->orderBy(['id' => SORT_DESC])
                            ->one();
                        if ($machine_price->price != $price){
                            $machine_price->end_date = date('Y-m-d H:i:s');
                            if ($machine_price->save()){
                                $new_price = new MachinesPrice();
                                $new_price->machine_id = $id;
                                $new_price->price = $price;
                                $new_price->enter_date = date("Y-m-d H:i:s", strtotime('+1 second', strtotime(date('Y-m-d H:i:s'))));
                                $new_price->end_date = '9999-12-31 23:59:59';
                                if ($new_price->save())
                                    return ['status' => 'success'];
                            }
                        } else {
                            return ['status' => 'success'];
                        }
                    } else {
                        pre($model->errors);
                    }
                }else {
                    $id = intval($_GET['id']);
                    $model = Machines::findOne($id);
                    return [
                        'status' => 'success',
                        'header' => "<h3>Stanok nomini o'zgartirish</h3>",
                        'content' => $this->renderAjax('form-update.php',[
                            'model' => $model
                        ]),
                    ];
                }
            }
        }
    }

    public function actionDeleteMachine($id)
    {
        $model = Machines::findOne($id);
        $status = ($model->status == 1) ? 0 : 1;
        $model->status = $status;
        if ($model->save())
            return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the MachinesPrice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MachinesPrice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MachinesPrice::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
