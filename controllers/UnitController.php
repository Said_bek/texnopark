<?php

namespace app\controllers;

use app\models\Materials;
use app\models\MaterialUnit;
use Yii;
use app\models\Unit;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UnitController implements the CRUD actions for Unit model.
 */
class UnitController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Unit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $unit = Unit::find()->where(['status' => 1])->all();

        $selectUnit = '
            SELECT 
                   mu.id, 
                   m.name as material, 
                   u.name as unit 
            FROM material_unit AS mu 
            INNER JOIN materials AS m ON m.id = mu.material_id
            INNER JOIN unit AS u ON u.id = mu.unit_id';
        $model = Yii::$app->db->createCommand($selectUnit)->queryAll();

        return $this->render('index', [
            'model' => $unit,
            'material_unit' => $model
        ]);
    }

    /**
     * Displays a single Unit model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Unit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Unit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Unit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Unit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionAddUnit()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['u']) && !empty($_GET['u'])) {
                if ($_GET['u'] == 'true') {
                    $unit_name = Yii::$app->request->get('unit_name');
                    if (!(isset($unit_name) and !empty($unit_name)))
                        return ['status' => 'error_name'];

                    $model = new Unit();
                    $model->name = $unit_name;
                    $model->value = 1;
                    $model->status = 1;
                    if($model->save())
                        return ['status' => 'success'];
                }else {
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('unit_form.php')
                    ];
                }
            }
        }
    }

    public function actionUpdateUnit()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && isset($_GET['update']) && $_GET['id'] > 0) {
                if ($_GET['update'] == 'true') {
                    if (isset($_GET['unit_name']) and !empty($_GET['unit_name'])){
                        $id = Yii::$app->request->get('id');
                        $model = Unit::findOne($id);
                        if (isset($model) and !empty($model)){
                            $model->name = Yii::$app->request->get('unit_name');
                            if ($model->save())
                                return ['status' => 'success'];
                        }
                    } else {
                        return ['status' => 'error_name'];
                    }
                }else {
                    $model = Unit::findOne($_GET['id']);
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('update_unit.php',[
                            'model' => $model,
                        ]),
                    ];
                }
            }
        }
    }

    public function actionDeleteUnit()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && !empty($_GET['id'])) {
                $id = Yii::$app->request->get('id');
                $deleteUnit = Unit::findOne($id);
                $deleteUnit->delete();
                return ['status' => 'success'];
            }
        }
    }

    public function actionUpdateUnitMaterial()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && isset($_GET['update']) && $_GET['id'] > 0) {
                if ($_GET['update'] == 'true') {
                    $unit = Yii::$app->request->get('unit');
                    $material = Yii::$app->request->get('material');
                    if (!(isset($unit) and !empty($unit)))
                        return ['status' => 'error_unit'];

                    if (!(isset($material) and !empty($material)))
                        return ['status' => 'error_material'];

                    MaterialUnit::deleteAll('unit_id = '.$unit);
                    foreach ($material as $value){
                        $materialUnit = new MaterialUnit();
                        $materialUnit->unit_id = $unit;
                        $materialUnit->material_id = $value;
                        $materialUnit->save();
                    }
                    return ['status' => 'success'];
                }else {
                    $selectUnit = '
                        SELECT 
                               material_id 
                        FROM material_unit 
                        WHERE unit_id = '.$_GET['id'];
                    $unit = Yii::$app->db->createCommand($selectUnit)->queryAll();
                    $arr = [];
                    if (isset($unit) and !empty($unit)){
                        foreach ($unit as $value){
                            $arr[] = $value['material_id'];
                        }
                    }
                    $selectAllUnit = Unit::find()->where(['id' => $_GET['id']])->one();

                    $selectMaterial = "
                        SELECT 
                            m.id, 
                            m.name 
                        FROM materials AS m 
                        WHERE m.status = 1 and m.id 
                        NOT IN (
                            SELECT 
                                   mu.material_id 
                            FROM material_unit AS mu 
                            WHERE mu.unit_id != ".$_GET['id']."
                        );";
                    $material = Yii::$app->db->createCommand($selectMaterial)->queryAll();

                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('update_modal_form.php',[
                            'unit' => $selectAllUnit,
                            'material' => $material,
                            'arr' => $arr
                        ]),
                    ];
                }
            }
        }
    }

    /**
     * Finds the Unit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Unit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Unit::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
