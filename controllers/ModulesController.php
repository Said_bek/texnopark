<?php

namespace app\controllers;

use Yii;
use app\models\Modules;
use app\models\Users;
use app\models\ModulUsers;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ModulesController implements the CRUD actions for Modules model.
 */
class ModulesController extends Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $selectModule = '
            select
                   m.id,
                   m.title,
                   mu.user_id,
                   u.username
            from modules as m
                     left join modul_users as mu on m.id = mu.modul_id
                     left join users as u on mu.user_id = u.id
            order by m.id asc';
        $modules = Yii::$app->db->createCommand($selectModule)->queryAll();
        $arr = [];
        foreach ($modules as $value){
            $arr[$value['id']]['id'] = $value['id'];
            $arr[$value['id']]['title'] = $value['title'];
            if (isset($value['username']) and !empty($value['username']))
                $arr[$value['id']]['list'][] = $value['username'];
        }
        return $this->render('index', [
            'model' => $arr,
        ]);
    }

    public function actionUpdateUser()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['id']) && isset($_GET['update']) && $_GET['id'] > 0) {
                if ($_GET['update'] == 'true') {
                    $id = Yii::$app->request->get('id');
                    $users = Yii::$app->request->get('users');
                    ModulUsers::deleteAll('modul_id = '.$id);
                    if (isset($users) and !empty($users))
                        foreach ($users as $value){
                            $model = new ModulUsers();
                            $model->modul_id = $id;
                            $model->user_id = $value;
                            $model->save();
                        }

                    $user_id = Yii::$app->user->id;
                    $selectUsers = Users::find()->where(['id' => $user_id])->one();
                    if (isset($selectUsers) and !empty($selectUsers)){
                        $user_name = $selectUsers->username;
                        eventUser($user_name, date('Y-m-d H:i:s'), 21, "Bo'limdagi foydalanuvchi o'zgartirildi");
                    }
                    return ['status' => 'success'];
                } else {
                    $selectUsers = 'SELECT user_id FROM modul_users WHERE modul_id = '.$_GET['id'];
                    $moduleUsers = Yii::$app->db->createCommand($selectUsers)->queryAll();
                    $arr = [];
                    if (isset($moduleUsers) and !empty($moduleUsers)){
                        foreach ($moduleUsers as $moduleValue){
                            $arr[] = $moduleValue['user_id'];
                        }
                    }

                    $section = Modules::find()->where(['id' => $_GET['id']])->one();
                    $users = Users::find()->where(['status' => 1])->all();
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('_form.php',[
                            'section' => $section,
                            'users' => $users,
                            'arr' => $arr
                        ]),
                    ];
                }
            }
        }
    }

    /**
     * Finds the Modules model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Modules the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Modules::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
