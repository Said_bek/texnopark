<?php

namespace app\controllers;

use app\models\Modules;
use app\models\Notification;
use Yii;
use app\models\Orders;
use app\models\Clients;
use app\models\Docs;
use app\models\Category;
use app\models\CategoryDocs;
use app\models\OrdersDocs;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ModulUsers;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
        else {
            $user_id = Yii::$app->user->id;
            $checkUser = ModulUsers::find()->where(['user_id' => $user_id, 'modul_id' => 3])->one();
            if (!isset($checkUser)) {
                $this->redirect('/index.php/site/login');
            }
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $sql = "
            SELECT 
               c.title as category,
               o.created_date as enter_date,
               ct.phone_number as phone_number,
               ct.phone_number_2 as phone_number_2,
               ct.company_name as company_name,
               o.proirity as priority,
               o.approved as approved,
               o.order_name as order_name,
               o.status as status,
               o.id as order_id,
               o.price as price,
               o.deadline_price as deadline_price,
               o.order_from as order_from,
               m.title AS module_name,
               n.module_id AS module_id
           FROM orders as o
           INNER JOIN notification AS n ON o.id = n.order_id
           INNER JOIN modules AS m ON n.module_id = m.id
           lEFT JOIN category AS c ON c.id = o.category_id AND c.status = :status
           INNER JOIN clients AS ct ON o.client_id = ct.id 
           WHERE n.end_date IS NULL
           GROUP BY 
               o.id,
               c.title,
                o.created_date,
                ct.phone_number,
                ct.phone_number_2,
                ct.company_name,
                o.proirity,
                o.approved,
                o.order_name,
                o.status,
                o.price,
                o.deadline_price,
                o.order_from,
                m.title,
                n.module_id 
        ";

        $model = Yii::$app->db->createCommand($sql)
        ->bindValue(':status', 1)
        ->queryAll();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionCategory()
    {
        $sql = "
            SELECT 
                c.id AS category_id,
                d.id AS docs_id,
                c.title AS category,
                d.title AS docs
            FROM category_docs AS cd
            LEFT JOIN category AS c ON c.id = cd.category_id AND c.status = :status
            LEFT JOIN docs AS d ON d.id = cd.docs_id AND d.status = :status
            ORDER BY cd.id ASC
        ";

        $model = Yii::$app->db->createCommand($sql)
        ->bindValue(':status', 1)
        ->queryAll();

        $arr = [];
        if (isset($model) && !empty($model)) {
            $n = 0;
            foreach ($model as $key => $value) {
                $arr[$value['category_id']]['category_id'] = $value['category_id'];
                $arr[$value['category_id']]['category'] = $value['category'];
                $arr[$value['category_id']]['list'][$n]['id'] = $value['docs_id'];
                $arr[$value['category_id']]['list'][$n]['title'] = $value['docs'];
                $n++;
            }
        }

        return $this->render('category', [
            'arr' => $arr,
        ]);
    }

    public function actionSave()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if(isset($_GET['cat']) && !empty($_GET['cat'])){
                $cat = intval($_GET['cat']);
                if(isset($_GET['docs']) && !empty($_GET['docs'])){
                    $docs_ex = explode(',', $_GET['docs']);
                    $arr = [];
                    $arr2 = [];
                    $get_docs = Docs::find()->where('status = :status', [':status' => 1])->All(); 
                    foreach ($docs_ex as $key => $value) {
                        if(is_numeric($value) == true){
                            foreach ($get_docs as $key => $value2) {
                                $docs_id = $value2->id;
                                if($value == $docs_id){
                                    $arr[] = $value;
                                }
                            }
                        }
                        elseif(is_string($value) == true){
                            $arr2[] = $value;
                        }
                    }
                if(isset($_GET['phone_number']) && !empty($_GET['phone_number'])){
                    $phone_number = intval($_GET['phone_number']);
                    if(isset($_GET['phone_number_2']) && !empty($_GET['phone_number_2'])){
                        $phone_number_2 = intval($_GET['phone_number_2']);
                        if(isset($_GET['project_name']) && !empty($_GET['project_name'])){
                            $project_name = htmlspecialchars($_GET['project_name']);
                                if(isset($_GET['company_name']) && !empty($_GET['company_name'])){
                                    $company_name = htmlspecialchars($_GET['company_name']);
                                    if(isset($_GET['from'])){
                                        $from = htmlspecialchars($_GET['from']);
                                        if(isset($_GET['price_date']) && !empty($_GET['price_date'])){
                                            $price_date = $_GET['price_date'];
                                            if(isset($_GET['price_time']) && !empty($_GET['price_time'])){
                                                $price_time = $_GET['price_time'];
                                                if(isset($_GET['proirity'])){
                                                    $proirity = intval($_GET['proirity']);
                                                    if(isset($_GET['price']) && !empty($_GET['price'])){
                                                        $price = intval($_GET['price']);
                                                        if(isset($_GET['client_email']) && !empty($_GET['client_email'])){
                                                            $client_email = htmlspecialchars($_GET['client_email']);
                                                            if(isset($_GET['client_name']) && !empty($_GET['client_name'])){
                                                                $client_name = htmlspecialchars($_GET['client_name']);
                                                                if(isset($_GET['country']) && !empty(['country'])){
                                                                    $country =  htmlspecialchars($_GET['country']);
                                                                    $db = Yii::$app->db;
                                                                    $transaction = $db->beginTransaction();
                                                                    try {
                                                                        $clients = new Clients;
                                                                        $clients->phone_number = $phone_number;
                                                                        $clients->phone_number_2 = $phone_number_2;
                                                                        $clients->company_name = $company_name;
                                                                        $clients->face_name = $client_name;
                                                                        $clients->country = $country;
                                                                        $clients->email = $client_email;
                                                                        $clients->save(false);
                                                                        
                                                                        $orders = new Orders;
                                                                        $orders->created_date = date('Y-m-d H:i:s');
                                                                        $orders->deadline_price = date('Y-m-d H:i:s', strtotime(date($price_date." ".$price_time.":00")));
                                                                        $orders->price = $price;
                                                                        $orders->order_name = $project_name;
                                                                        $orders->order_from = $from;
                                                                        $orders->proirity = $proirity;
                                                                        $orders->approved = 0;
                                                                        $orders->category_id = $cat;
                                                                        $orders->status = 0;
                                                                        $orders->client_id = $clients->id;
                                                                        $orders->save(false);


                                                                        foreach ($arr as $key => $value) {
                                                                            $order_docs = new OrdersDocs;
                                                                            $order_docs->order_id = $orders->id;
                                                                            $order_docs->docs_id = $value;
                                                                            $order_docs->save(false);
                                                                        }

                                                                        $ids = [];

                                                                        foreach ($arr2 as $key => $value) {
                                                                            $docs = new Docs;
                                                                            $docs->title = $value;
                                                                            $docs->status = 1;
                                                                            $docs->save(false);
                                                                            $ids[] = $docs->id;
                                                                        }


                                                                        foreach ($ids as $key => $value) {
                                                                            $order_docs = new OrdersDocs;
                                                                            $order_docs->order_id = $orders->id;
                                                                            $order_docs->docs_id = $value;
                                                                            $order_docs->save(false); 
                                                                        }

                                                                        $new_notification = new Notification();
                                                                        $new_notification->order_id = $orders->id;
                                                                        $new_notification->module_id = 3;
                                                                        $new_notification->enter_date = date('Y-m-d H:i:s');
                                                                        $new_notification->status = 2;
                                                                        $new_notification->save();

                                                                        $status = 'success';
                                                                        
                                                                        $transaction->commit();
                                                                    } catch(\Exception $e) {
                                                                        $transaction->rollBack();
                                                                        throw $e;
                                                                    } catch(\Throwable $e) {
                                                                        $transaction->rollBack();
                                                                    }

                                                                }
                                                                else{
                                                                    $status = 'country_fail';
                                                                }
                                                            }
                                                            else{
                                                                $status = 'client_name_fail';
                                                            }
                                                        }
                                                        else{
                                                            $status = 'client_email_fail';
                                                        }
                                                    }
                                                    else{
                                                        $status = 'price_fail';
                                                    }
                                                }
                                                else{
                                                    $status = 'proirity_fail';
                                                }
                                            }
                                            else{
                                                $status = 'price_time_fail';
                                            }
                                        }
                                        else{
                                            $status = 'price_date_fail';
                                        }
                                    }
                                    else{
                                        $status = 'from_fail';
                                    }
                                }
                                else{
                                    $status = 'company_name_fail';
                                }

                            }
                            else{
                                $status = "project_name_fail";
                            }
                        }
                        else{
                            $status = 'phone_number_2_fail';
                        }
                    }
                    else{
                        $status = 'phone_number_fail';
                    }
                }
                else{
                    $status = 'docs_fail';
                }
            }
            else{
                $status = 'cat_fail';
            }

            return ['status' => $status];
        }
    }

    public function actionUpdate_save()
    {

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if(isset($_GET['cat']) && !empty($_GET['cat'])){
                $cat = intval($_GET['cat']);
                if(isset($_GET['docs']) && !empty($_GET['docs'])){
                    $docs_ex = $_GET['docs'];
                    $arr = [];
                    $arr2 = [];
                    $get_docs = Docs::find()->where('status = :status', [':status' => 1])->All(); 
                    foreach ($docs_ex as $key => $value) {
                        if(is_numeric($value) == true){
                            foreach ($get_docs as $key => $value2) {
                                $docs_id = $value2->id;
                                if($value == $docs_id){
                                    $arr[] = $value;
                                }
                            }
                        }
                        elseif(is_string($value) == true){
                            $arr2[] = $value;
                        }
                    }               

                // $cat = intval($_GET['cat']);
                // // if(isset($_GET['docs']) && !empty($_GET['docs'])){
                //     $docs_ex = $_GET['docs'];

                //     $arr = [];
                //     $arr2 = [];
                //     $get_docs = Docs::find()->where('status = :status', [':status' => 1])->All(); 
                //     foreach ($docs_ex as $key => $value) {
                //         if(is_numeric($value) == true){
                //             foreach ($get_docs as $key => $value2) {
                //                 $docs_id = $value2->id;
                //                 if($value == $docs_id){
                //                     $arr[] = $value;
                //                 }
                //             }
                //         }
                //         elseif(is_string($value) == true){
                //             $arr2[] = $value;
                //         }
                    

                if(isset($_GET['phone_number']) && !empty($_GET['phone_number'])){
                    $phone_number = intval($_GET['phone_number']);
                    if(isset($_GET['phone_number_2']) && !empty($_GET['phone_number_2'])){
                        $phone_number_2 = intval($_GET['phone_number_2']);
                        if(isset($_GET['project_name']) && !empty($_GET['project_name'])){
                            $project_name = htmlspecialchars($_GET['project_name']);
                                if(isset($_GET['company_name']) && !empty($_GET['company_name'])){
                                    $company_name = htmlspecialchars($_GET['company_name']);
                                    if(isset($_GET['from'])){
                                        $from = htmlspecialchars($_GET['from']);
                                        if(isset($_GET['price_date']) && !empty($_GET['price_date'])){
                                            $price_date = $_GET['price_date'];
                                            if(isset($_GET['price_time']) && !empty($_GET['price_time'])){
                                                $price_time = $_GET['price_time'];
                                                if(isset($_GET['proirity'])){
                                                    $proirity = intval($_GET['proirity']);
                                                    if(isset($_GET['price']) && !empty($_GET['price'])){
                                                        $price = intval($_GET['price']);
                                                        if(isset($_GET['client_email']) && !empty($_GET['client_email'])){
                                                            $client_email = htmlspecialchars($_GET['client_email']);
                                                            if(isset($_GET['client_name']) && !empty($_GET['client_name'])){
                                                                $client_name = htmlspecialchars($_GET['client_name']);
                                                                if(isset($_GET['country']) && !empty(['country'])){
                                                                    $country =  htmlspecialchars($_GET['country']);

                                                                    $db = Yii::$app->db;
                                                                    $transaction = $db->beginTransaction();

                                                                    try {
                                                                        OrdersDocs::deleteAll(['order_id' => $_GET['order_id']]);
                                                                        $orders = Orders::findOne($_GET['order_id']);
                                                                        // echo "<pre>";
                                                                        // print_r($orders);
                                                                        // die();
                                                                        $orders->created_date = date('Y-m-d H:i:s');
                                                                        $orders->deadline_price = date('Y-m-d H:i:s', strtotime(date($price_date." ".$price_time.":00")));
                                                                        $orders->price = $price;
                                                                        $orders->order_name = $project_name;
                                                                        $orders->order_from = $from;
                                                                        $orders->proirity = $proirity;
                                                                        $orders->approved = 0;
                                                                        $orders->category_id = $cat;
                                                                        $orders->status = 0;
                                                                        // $orders->client_id = $clients->id;
                                                                        $orders->save(false);

                                                                        $clients = Clients::findOne($orders->client_id);
                                                                        // echo "<pre>";
                                                                        // print_r($clients);
                                                                        // die();
                                                                        $clients->phone_number = $phone_number;
                                                                        $clients->phone_number_2 = $phone_number_2;
                                                                        $clients->company_name = $company_name;
                                                                        $clients->face_name = $client_name;
                                                                        $clients->country = $country;
                                                                        $clients->email = $client_email;
                                                                        $clients->save(false);


                                                                        foreach ($arr as $key => $value) {
                                                                            $order_docs = new OrdersDocs;
                                                                            $order_docs->order_id = $orders->id;
                                                                            $order_docs->docs_id = $value;
                                                                            $order_docs->save(false);
                                                                        }

                                                                        $ids = [];

                                                                        foreach ($arr2 as $key => $value) {
                                                                            $docs = new Docs;
                                                                            $docs->title = $value;
                                                                            $docs->status = 1;
                                                                            $docs->save(false);
                                                                            $ids[] = $docs->id;
                                                                        }


                                                                        foreach ($ids as $key => $value) {
                                                                            $order_docs = new OrdersDocs;
                                                                            $order_docs->order_id = $orders->id;
                                                                            $order_docs->docs_id = $value;
                                                                            $order_docs->save(false); 
                                                                        }


                                                                        $status = 'success';
                                                                        
                                                                        $transaction->commit();
                                                                    } catch(\Exception $e) {
                                                                        $transaction->rollBack();
                                                                        throw $e;
                                                                    } catch(\Throwable $e) {
                                                                        $transaction->rollBack();
                                                                    }

                                                                }
                                                                else{
                                                                    $status = 'country_fail';
                                                                }
                                                            }
                                                            else{
                                                                $status = 'client_name_fail';
                                                            }
                                                        }
                                                        else{
                                                            $status = 'client_email_fail';
                                                        }
                                                    }
                                                    else{
                                                        $status = 'price_fail';
                                                    }
                                                }
                                                else{
                                                    $status = 'proirity_fail';
                                                }
                                            }
                                            else{
                                                $status = 'price_time_fail';
                                            }
                                        }
                                        else{
                                            $status = 'price_date_fail';
                                        }
                                    }
                                    else{
                                        $status = 'from_fail';
                                    }
                                }
                                else{
                                    $status = 'company_name_fail';
                                }

                            }
                            else{
                                $status = "project_name_fail";
                            }
                        }
                        else{
                            $status = 'phone_number_2_fail';
                        }
                    }
                    else{
                        $status = 'phone_number_fail';
                    }
                }
                else{
                    $status = 'docs_fail';
                }
            }
            else{
                $status = 'cat_fail';
            }

            return ['status' => $status];
        }
    }

    public function actionDel()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['cat_id']) && !empty($_GET['cat_id']) && isset($_GET['docs_id']) && !empty($_GET['docs_id'])) {

                $cat_id = htmlspecialchars($_GET['cat_id']);
                $docs_id = htmlspecialchars($_GET['docs_id']);

                $sql = "DELETE FROM category_docs WHERE category_id = :cat_id AND docs_id = :docs_id";

                $query = Yii::$app->db->createCommand($sql)
                    ->bindValue(':cat_id', $cat_id)
                    ->bindValue(':docs_id', $docs_id)
                    ->execute();

                return ['status' => 'success'];

            } else {
                return ['status' => 'fail'];
            }
        }
    }

    public function actionView($id)
    {
        $sql = "
            SELECT 
                c.title as category,
                o.created_date as enter_date,
                ct.phone_number as phone_number,
                ct.phone_number_2 as phone_number_2,
                ct.company_name as company_name,
                ct.face_name as face_name,
                ct.email as email,
                ct.country as country,
                o.proirity as priority,
                o.approved as approved,
                o.order_name as order_name,
                o.status as status,
                o.id as order_id,
                o.price as price,
                o.deadline_price as deadline_price,
                o.order_from as order_from
            FROM orders as o
            INNER JOIN category AS c ON c.id = o.category_id AND c.status = :status
            INNER JOIN clients AS ct ON o.client_id = ct.id
            WHERE o.id = :id
        ";

        $model = Yii::$app->db->createCommand($sql)
        ->bindValue(':id', $id)
        ->bindValue(':status', 1)
        ->queryOne();

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionCategory_add()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $category = Category::find()->All();
            $docs = Docs::find()->All();

            return [
                'status' => 'success',
                'content' => $this->renderAjax('category_form.php', [
                    'category' => $category,
                    'docs' => $docs
                ]),
            ];
        }
    }

    public function actionCat_filter()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if(isset($_GET['cat']) && !empty($_GET['cat'])){
                // $docs = CategoryDocs::find()->where(['category_id' => $_GET['cat']])->All();

                $sql = "
                    SELECT
                        d.id as id, 
                        d.title as docs
                    FROM category_docs AS cd
                    INNER JOIN docs AS d ON d.id = cd.docs_id
                    WHERE cd.category_id = :category_id
                ";

                $docs = Yii::$app->db->createCommand($sql)
                ->bindValue(':category_id', $_GET['cat'])
                ->queryAll();

                return [
                    'status' => 'success',
                    'content' => $this->renderAjax('select_form.php', [
                        // 'category' => $category,
                        'docs' => $docs
                    ]),
                ];
            }
        
        }
    }

    public function actionCat_filter_update()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if(isset($_GET['cat']) && !empty($_GET['cat'])){
                // $docs = CategoryDocs::find()->where(['category_id' => $_GET['cat']])->All();

                $sql = "
                    SELECT
                        d.id as id, 
                        d.title as docs
                    FROM category_docs AS cd
                    INNER JOIN docs AS d ON d.id = cd.docs_id
                    WHERE cd.category_id = :category_id
                ";

                $docs = Yii::$app->db->createCommand($sql)
                ->bindValue(':category_id', $_GET['cat'])
                ->queryAll();

                return [
                    'status' => 'success',
                    'content' => $this->renderAjax('select_form_update.php', [
                        // 'category' => $category,
                        'docs' => $docs
                    ]),
                ];
            }
        
        }
    }

    public function actionCategory_save()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            // echo "<pre>";
            // print_r($_GET);
            // die();

            if(isset($_GET['cat']) && !empty($_GET['cat']) && isset($_GET['doc']) && !empty($_GET['doc']))
            {
                // echo "<pre>";
                // print_r($_GET);
                // die();
                $cat = htmlspecialchars($_GET['cat']);  
                $doc = $_GET['doc'];
                $integerIDs = array_map('intval', explode(',', $doc));
                $doc_ex = explode(',', $doc);
                // echo "<pre>";
                // print_r($doc_ex);
                // die();
                $count_doc = count($doc_ex);
                
                $db = Yii::$app->db;
                $transaction = $db->beginTransaction();

                try {     
                    $category = new Category;
                    $category->title = $cat;
                    $category->status = 1;
                    $category->save(false);

                    $arr = [];
                    $arr2 = [];
                    foreach ($doc_ex as $key => $value) {
                        $parse = intval($value);
                        if(is_numeric($value) == true){
                            $arr2[] = $value;
                        }
                        elseif(is_string($value) == true)
                        {
                            $docs = new Docs;
                            $docs->title = $value;
                            $docs->status = 1;
                            $docs->save(false);
                            $arr[] = $docs->id;
                            
                        }
                    }   
                    $all = array_merge($arr2, $arr);

                    foreach ($all as $key => $value2) {
                        $category_docs = new CategoryDocs;
                        $category_docs->category_id = $category->id;
                        $category_docs->docs_id =  $value2;
                        $category_docs->save(false);
                    }

                    $status = 'success';
                    
                    $transaction->commit();
                } catch(\Exception $e) {
                    $transaction->rollBack();
                    throw $e;
                } catch(\Throwable $e) {
                    $transaction->rollBack();
                }
            }
            else{
                $status = 'fail';
            }

            return ['status' => $status];
        }
    }

    public function actionPdf($id)
    {
        $current_date = date('d.m.Y', strtotime(date('Y-m-d')));

        $sql = "
            SELECT 
                c.title as category,
                o.created_date as enter_date,
                ct.phone_number as phone_number,
                ct.phone_number_2 as phone_number_2,
                ct.company_name as company_name,
                ct.face_name as face_name,
                ct.email as email,
                ct.country as country,
                o.proirity as priority,
                o.approved as approved,
                o.order_name as order_name,
                o.status as status,
                o.id as order_id,
                o.price as price,
                o.deadline_price as deadline_price,
                o.order_from as order_from
            FROM orders as o
            INNER JOIN category AS c ON c.id = o.category_id AND c.status = :status
            INNER JOIN clients AS ct ON o.client_id = ct.id
            WHERE o.id = :id
        ";

        $model = Yii::$app->db->createCommand($sql)
        ->bindValue(':id', $id)
        ->bindValue(':status', 1)
        ->queryOne();  


        return $this->render('pdf', [
            'model' => $model,
            'current_date' => $current_date,
        ]);

        // return $this->redirect(['index']);
    }

    public function actionCreate()
    {
        $model = new Orders();

        $get_docs = CategoryDocs::find()->orderBy(['id'=>SORT_ASC])->One();
        if (isset($get_docs) and !empty($get_docs)){
            $sql = "
            SELECT
                d.id as id,
                d.title as docs
            FROM docs as d
            INNER JOIN category_docs as cd ON cd.docs_id = d.id AND category_id = ".$get_docs->category_id;
        } else {
            $sql = "
            SELECT
                d.id as id,
                d.title as docs
            FROM docs as d
            INNER JOIN category_docs as cd ON cd.docs_id = d.id";
        }


        $documents = Yii::$app->db->createCommand($sql)
        ->queryAll();

         $getCategory = Category::find()->all();

        return $this->render('_form', [
            'documents' => $documents,
            'getCategory' => $getCategory
        ]);
    }

    public function actionUpdate_order($id)
    {
        $docs_sql = "
            SELECT 
                d.id as id,
                d.title as docs
            FROM orders_docs AS od
            INNER JOIN docs as d on d.id = od.docs_id
            WHERE od.order_id = $id  
        ";

        $docs_model = Yii::$app->db->createCommand($docs_sql)
        ->queryAll();



         $getCategory = Category::find()->all();
         // $getOrdersCategory = Category::find()->where(['id' => $id])->one();


        $sql = "
            SELECT
                c.id, 
                c.title as category,
                o.created_date as enter_date,
                ct.phone_number as phone_number,
                ct.phone_number_2 as phone_number_2,
                ct.company_name as company_name,
                ct.face_name as face_name,
                ct.email as email,
                ct.country as country,
                o.proirity as priority,
                o.approved as approved,
                o.order_name as order_name,
                o.status as status,
                o.id as order_id,
                o.price as price,
                o.deadline_price as deadline_price,
                o.order_from as order_from
            FROM orders as o
            INNER JOIN category AS c ON c.id = o.category_id AND c.status = :status
            INNER JOIN category_docs AS cd ON cd.category_id = o.category_id
            INNER JOIN clients AS ct ON o.client_id = ct.id
            WHERE o.id = :id
        ";

        $model = Yii::$app->db->createCommand($sql)
        ->bindValue(':id', $id) 
        ->bindValue(':status', 1) 
        ->queryOne();

        $date_ex = explode(' ', $model['deadline_price']);

        $date = $date_ex[0];
        $time = $date_ex[1];

        $model_data = date('d.m.Y', strtotime($date));
        $model_time = date('H:i', strtotime($time));


        return $this->render('edit_form', [
            'model' => $model,
            'docs_model' => $docs_model,
            'getCategory' => $getCategory,
            'model_data' => $model_data,
            'model_time' => $model_time,
        ]);
    }

    public function actionUpdateSection()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['s']) && !empty($_GET['s'])) {
                if ($_GET['s'] == 'true') {
                    $error = false;
                    $order_id = Yii::$app->request->get('id');
                    $module_id = Yii::$app->request->get('module_id');

                    if (!(isset($order_id) and !empty($order_id))) {
                        $error = true;
                        return [
                            'status' => 'error_id'
                        ];
                    }

                    if (!(isset($module_id) and !empty($module_id))) {
                        $error = true;
                        return [
                            'status' => 'error_module'
                        ];
                    }

                    if (!$error){
                        $model = Notification::find()->where(['order_id' => $order_id, 'end_date' => NULL])->one();
                        if (isset($model) and !empty($model)){
                            $model->end_date = date('Y-m-d H:i:s');
                            $model->status = 3;
                            if ($model->save()){
                                $next_section = new Notification();
                                $next_section->module_id = $module_id;
                                $next_section->order_id = $order_id;
                                $next_section->enter_date = date("Y-m-d H:i:s", strtotime('+1 second', strtotime(date('Y-m-d H:i:s'))));
                                $next_section->status = 1;
                                $next_section->save();
                            } else {
                                pre($model->errors);
                            }
                        }
                        return [
                            'status' => 'success',
                        ];
                    }
                }else {
                    $model = Modules::find()->where(['id' => [2,4]])->all();
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('section.php',[
                            'model' => $model
                        ]),
                    ];
                }
            }
        }
    }

    public function actionWarehouse()
    {
        $selectOrders = '
            SELECT 
                o.id,
                o.created_date,
                o.status,
                o.category_id,
                o.order_name,
                o.proirity,
                o.order_from,
                c.title AS category_name
            FROM orders AS o
            INNER JOIN notification AS n ON o.id = n.order_id AND n.module_id = 6
            INNER JOIN category AS c ON o.category_id = c.id
            WHERE n.end_date IS NULL ';
        $orders = Yii::$app->db->createCommand($selectOrders)->queryAll();

        return $this->render('orders', [
            'model' => $orders,
        ]);
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
