<?php

namespace app\controllers;

use yii\data\ActiveDataProvider;
use Yii;
use app\models\Users;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ModulUsers;

class UsersController extends Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest){
            return $this->redirect('/site/login');
        } else {
            $user_id = Yii::$app->user->id;
            $checkUser = ModulUsers::find()->where(['user_id' => $user_id, 'modul_id' => 1])->one();
            if (!isset($checkUser)) {
                $this->redirect('/index.php/site/login');
            }
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Users::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Users();
        $model->status = 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $auth = new AuthAssignment();
            $auth->item_name = $model->role;
            $auth->user_id = strval($model->id);
            $auth->save();
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()){
                return $this->redirect(['users/index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}