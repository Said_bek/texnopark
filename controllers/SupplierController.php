<?php

namespace app\controllers;

use app\models\Nakladnoy;
use app\models\NakladnoyDetails;
use Yii;
use app\models\Supplier;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ModulUsers;

/**
 * SupplierController implements the CRUD actions for Supplier model.
 */
class SupplierController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
        else {
            $user_id = Yii::$app->user->id;
            $checkUser = ModulUsers::find()->where(['user_id' => $user_id, 'modul_id' => 8])->one();
            if (!isset($checkUser)) {
                $this->redirect('/index.php/site/login');
            }
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Supplier models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Supplier::find()->orderBy(['id' => SORT_ASC]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSupplierOrders()
    {
        $query = Supplier::find()
            ->select('n.id, n.code, n.enter_date, n.created_date, n.status, n.end_date')
            ->innerJoin(
                'nakladnoy as n', 'supplier.id = n.supplier_id'
            )
            ->where(['supplier.user_id' => Yii::$app->user->id])
            ->asArray()
            ->orderBy(['n.status' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('nakladnoys', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStart()
    {
        $id = Yii::$app->request->get('id');
        $updateNak = Nakladnoy::findOne($id);
        $updateNak->status = 1;
        $updateNak->save();
        $this->redirect(['supplier-orders']);
    }

    public function actionDone()
    {
        $id = Yii::$app->request->get('id');
        $updateNak = Nakladnoy::findOne($id);
        $updateNak->status = 2;
        $updateNak->save();
        $this->redirect(['supplier-orders']);
    }

    public function actionStatus($id)
    {
        $id = Yii::$app->request->get('id');
        $deleteSupplier = Supplier::findOne($id);
        $deleteSupplier->status = $deleteSupplier->status == 1 ? 0 : 1;
        $deleteSupplier->save();
        $this->redirect(['index']);
    }

    public function actionAbout($id)
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $id = Yii::$app->request->get('id');
            $model = NakladnoyDetails::find()
                ->select('m.name as material, ma.name as mark, nakladnoy_details.quantity')
                ->innerJoin('materials as m',  'nakladnoy_details.material_id = m.id')
                ->innerJoin('marks as ma',  'nakladnoy_details.mark_id = ma.id')
                ->where(['nakladnoy_details.nakladnoy_id' => $id])
                ->asArray()
                ->all();
            return [
                'status' => 'success',
                'content' => $this->renderAjax('view.php',[
                    'model' => $model,
                ]),
            ];
        }
    }

    /**
     * Finds the Supplier model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Supplier the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Supplier::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
