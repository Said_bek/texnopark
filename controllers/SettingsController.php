<?php

namespace app\controllers;

use app\models\SalaryCategory;
use Yii;
use app\models\Users;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SettingsController implements the CRUD actions for Users model.
 */
class SettingsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Users::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionChangePassword()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($_GET['pas'] == 'true'){
                $user_name = Yii::$app->request->get('user_name');
                $old_password = Yii::$app->request->get('old_password');
                $new_password = Yii::$app->request->get('new_password');
                $new_password_confirm = Yii::$app->request->get('new_password_confirm');
                $user_id = Yii::$app->user->id;
                $selectUser = Users::findOne($user_id);
                if (isset($selectUser) and !empty($selectUser)){
                    $user_password = $selectUser->password;
                    if ($user_password == sha1($old_password)){
                        $selectUser->username = $user_name;
                        $selectUser->password = sha1($new_password);
                        if($selectUser->save()){
                            $user_id = Yii::$app->user->id;
                            $selectUsers = Users::find()->where(['id' => $user_id])->one();
                            if (isset($selectUsers) and !empty($selectUsers)){
                                $user_name = $selectUsers->username;
                                eventUser($user_name, date('Y-m-d H:i:s'), 21);
                                return [
                                    'status' => 'success',
                                ];
                            }
                        }
                    } else {
                        return [
                            'status' => 'not_same',
                        ];
                    }
                } else {
                    return [
                        'status' => 'error',
                    ];
                }
            } else {
                $user_name = $_GET['user_name'];
                $user_id = Yii::$app->user->id;
                $selectUser = Users::find()->where(['id' => $user_id])->one();
                if (isset($selectUser) and !empty($selectUser)){
                    $selectUser->username = $user_name;
                    if($selectUser->save()){
                        $user_id = Yii::$app->user->id;
                        $selectUsers = Users::find()->where(['id' => $user_id])->one();
                        if (isset($selectUsers) and !empty($selectUsers)){
                            $user_name = $selectUsers->username;
                            eventUser($user_name, date('Y-m-d H:i:s'), 15);
                            return [
                                'status' => 'success',
                            ];
                        }
                    }
                } else {
                    return [
                        'status' => 'error',
                    ];
                }
            }
        }
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
