<?php

namespace app\controllers;

use app\models\SectionConductive;
use app\models\Users;
use Yii;
use app\models\Section;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ModulUsers;

/**
 * SectionController implements the CRUD actions for Section model.
 */
class SectionController extends Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
        else {
            $user_id = Yii::$app->user->id;
            $checkUser = ModulUsers::find()->where(['user_id' => $user_id, 'modul_id' => 5])->one();
            if (!isset($checkUser)) {
                $this->redirect('/index.php/site/login');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Section models.
     * @return mixed
     */
    public function actionIndex()
    {
        $models = Section::find()->all();

        return $this->render('index', [
            'models' => $models,
        ]);
    }

    public function actionView($id)
    {
        $section = Section::findOne($id);
        $users = Users::findAll(['status' => 1]);

        return $this->render('view', [
            'section' => $section,
            'users' => $users
        ]);
    }

    public function actionSaveConductive()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $user_id = Yii::$app->request->get('user');
            $section_id = Yii::$app->request->get('section');

            $model = new SectionConductive();
            $model->user_id = $user_id;
            $model->section_id = $section_id;
            $model->save();

            return ['status' => 'success'];
        }
    }


    /**
     * Creates a new Section model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new Section();

            if (isset($_GET['title']) && !empty($_GET['title'])) {
                $model->title = Yii::$app->request->get('title');
                $model->status = 1;
                $model->save();
                return ['status' => 'success'];
            }

            return [
                'status' => 'success',
                'content' => $this->renderAjax('_form', [
                    'model' => $model,
                ])
            ];
        }
    }

    /**
     * Updates an existing Section model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = $this->findModel($id);

            if (isset($_GET['title']) && !empty($_GET['title'])) {
                $model->title = $_GET['title'];
                $model->save();
                return ['status' => 'success'];
            }

            return [
                'status' => 'success',
                'content' => $this->renderAjax('update', [
                    'model' => $model,
                ])
            ];
        }
    }

    /**
     * Deletes an existing Section model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionArchive($id)
    {
        $model = Section::findOne($id);
        $model->status = 0;
        $model->save();

        return $this->redirect(['index']);
    }
    public function actionActive($id)
    {
        $model = Section::findOne($id);
        $model->status = 1;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Section model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Section the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Section::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
