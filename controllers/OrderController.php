<?php

namespace app\controllers;

use app\models\Machines;
use app\models\Modules;
use app\models\Notification;
use app\models\OrderDetails;
use app\models\Specification;
use Yii;
use app\models\Section;
use app\models\Orders;
use app\models\OrderModule;
use app\models\OrderStep;
use app\models\ModulUsers;
use app\models\OrderSectionTime;
use yii\filters\VerbFilter;

class OrderController extends \yii\web\Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            $this->redirect('/index.php/site/login');
        }
        else {
            $user_id = Yii::$app->user->id;
            $checkUser = ModulUsers::find()->where(['user_id' => $user_id, 'modul_id' => 7])->one();
            if (!isset($checkUser)) {
                $this->redirect('/index.php/site/login');
            }
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $date = date('Y-m-d H:i:s');

        $sql = "SELECT 
            orm.id AS order_module_id,
            orm.order_id AS order_module_order_id ,
            orm.module_id AS order_module_module_id ,
            orm.enter_date AS order_module_enter_date ,
            orm.exit_date AS order_module_exit_date ,
            ord.id AS  orders_id ,
            ord.created_date AS  orders_created_date,
            ord.exit_date AS  orders_exit_date,
            ord.\"status\" AS  orders_status,
            ord.client_id AS  orders_client_id ,
            ord.price AS  orders_price ,
            ord.category_id AS  orders_category_id ,
            ord.order_name AS  orders_order_name ,
            ord.order_from AS  orders_order_from ,
            ord.proirity AS  orders_proirity ,
            ord.deadline_price AS  orders_deadline_price ,
            ord.approved AS  orders_approved ,
            ord.specification_status AS  orders_specification_status ,
            ord.code AS orders_code  
            FROM order_module AS orm 
            INNER JOIN orders AS ord
            ON ord.id = orm.order_id
        WHERE orm.module_id = :module_id AND orm.enter_date < :date AND orm.exit_date IS NULL";

        $models = Yii::$app->db->createCommand($sql)
            ->bindValue(':module_id', 1)
            ->bindValue(':date', $date)
            ->queryAll();

        return $this->render('index', [
            'models' => $models,
        ]);
    }

    public function actionPlaning($order_id)
    {
        $sections = Section::find()->where(['=', 'status', 1])->all();
        $machines = Machines::find()->where(['=', 'status', 1])->all();
        $details = OrderDetails::find()
            ->alias('od')
            ->select('od.code, od.id')
            ->where(['od.order_id' => $order_id])
            ->asArray()
            ->all();
        return $this->render('planing', [
            'order_id' => $order_id,
            'sections' => $sections,
            'machines' => $machines,
            'details' => $details
        ]);
    }

    public function actionUpdatePlan($order_id)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_POST['planing']) && !empty($_POST['planing'])) {
                $error = 0;
                $error1 = 0;
                $error2 = 0;
                $error3 = 0;
                $error4 = 0;
                $error5 = 0;
                $error6 = 0;
                foreach ($_POST['planing'] as $plan) {
                    if (isset($plan['section']) && !empty($plan['section']) && $plan['section'] > 0) {
                    } else {
                        $error1++;
                    }
                    if (isset($plan['machine']) && !empty($plan['machine']) && $plan['machine'] > 0) {
                    } else {
                        $error5++;
                    }
                    if (isset($plan['enter']['date']) && !empty($plan['enter']['date']) && preg_match('~^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}$~', $plan['enter']['date'])) {
                    } else {
                        $error2++;
                    }
                    if (isset($plan['enter']['time']) && !empty($plan['enter']['time']) && preg_match('~^[0-9]{1,2}\:[0-9]{1,2}$~', $plan['enter']['time'])) {
                    } else {
                        $error3++;
                    }
                    if (isset($plan['worktime']) && !empty($plan['worktime']) && $plan['worktime'] > 0) {
                    } else {
                        $error4++;
                    }
                    if (isset($plan['price']) && !empty($plan['price']) && $plan['price'] > 0) {
                    } else {
                        $error6++;
                    }
                }
                if ($error == 0) {
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        foreach ($_POST['planing'] as $plan) {
                            if (isset($plan['id']) && !empty($plan['id'])) {
                                $update_order = OrderStep::find()->where(['=','id',$plan['id']])->one();
                                if (isset($update_order) && !empty($update_order)) {
                                    $update_order->section_id = $plan['section'];
                                    $update_order->machine_id = $plan['machine'];
                                    $update_order->machine_price = $plan['price'];
                                    $update_order->work_time = $plan['worktime'];
                                    $date_exploded = explode(".", $plan['enter']['date']);
                                    $date_start_date = date('Y-m-d H:i:s', strtotime($date_exploded[2] . '-' . $date_exploded[1] . '-' . $date_exploded[0] . ' ' . $plan['enter']['time']));
                                    $update_order->start_date = $date_start_date;
                                    $date_deadline = date('Y-m-d H:i:s', strtotime($date_start_date . '+' . $plan['worktime'] . ' hours'));
                                    $update_order->deadline = $date_deadline;
                                    $update_order->save();           
                                }
                            } else {
                                $order_step_create = new OrderStep();
                                $order_step_create->order_id = $order_id;
                                $order_step_create->section_id = $plan['section'];
                                $order_step_create->machine_id = $plan['machine'];
                                $order_step_create->machine_price = $plan['price'];
                                $order_step_create->work_time = $plan['worktime'];

                                $date_exploded = explode(".", $plan['enter']['date']);
                                $date_start_date = date('Y-m-d H:i:s', strtotime($date_exploded[2] . '-' . $date_exploded[1] . '-' . $date_exploded[0] . ' ' . $plan['enter']['time']));
                                $order_step_create->start_date = $date_start_date;
                                $date_deadline = date('Y-m-d H:i:s', strtotime($date_start_date . '+' . $plan['worktime'] . ' hours'));
                                $order_step_create->deadline = $date_deadline;
                                $order_step_create->save();
                            }
                        }

                        $transaction->commit();

                        return [
                            'status' => 'success',
                        ];
                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        return [
                            'status' => 'error',
                        ];
                        throw $e;
                    }
                } else {
                    return [
                        'status' => 'error',
                    ];
                }
            } else {
                return [
                    'status' => 'error',
                ];
            }
        }
    }

    public function actionSavePlan($id)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_POST['planing']) and !empty($_POST['planing'])){
                $transaction = Yii::$app->db->beginTransaction();
                $order_id = Yii::$app->request->get('id');
                try {
                    $a = true;
                    foreach ($_POST['planing'] as $plan) {
                        $order_step_create = new OrderStep();
                        $order_step_create->order_id = $order_id;
                        $order_step_create->section_id = $plan['section'];
                        $order_step_create->machine_id = $plan['machine'];
                        $order_step_create->machine_price = $plan['price'];
                        $order_step_create->work_time = $plan['worktime'];
                        $order_step_create->detail_id = $plan['detail_id'];

                        $date_exploded = explode(".", $plan['enter']['date']);
                        $date_start_date = date('Y-m-d H:i:s', strtotime($date_exploded[2] . '-' . $date_exploded[1] . '-' . $date_exploded[0] . ' ' . $plan['enter']['time']));

                        $order_step_create->start_date = $date_start_date;
                        $date_deadline = date('Y-m-d H:i:s', strtotime($date_start_date . '+' . $plan['worktime'] . ' hours'));
                        $order_step_create->deadline = $date_deadline;
                        $order_step_create->save();

                        if ($a){
                            $one_enter = new OrderSectionTime();
                            $one_enter->section_id = $plan['section'];
                            $one_enter->machine_id = $plan['machine'];
                            $one_enter->machine_price = $plan['price'];
                            $one_enter->order_id = $order_id;
                            $one_enter->enter_date = date('Y-m-d H:i:s');
                            $one_enter->detail_id = $plan['detail_id'];
                            $one_enter->status = 0;
                            $one_enter->save();

                            $one_exit = OrderModule::find()->where(['=', 'order_id', $order_id])->andWhere(['exit_date' => null])->one();
                            $one_exit->exit_date = date('Y-m-d H:i:s');
                            $one_exit->save();

                            $a = false;
                        }
                    }

                    $transaction->commit();
                    return ['status' => 'success'];
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    return ['status' => 'error'];
                    throw $e;
                }
            } else {
                return ['status' => 'error'];
            }
        }
    }

    public function actionIndexOrder()
    {
        $models = OrderSectionTime::find()->where(['exit_date' => null])->all();
        return $this->render('index-order', [
            'models' => $models,
        ]);
    }

    public function actionViewIndexOrder($id)
    {
        $order = Orders::find()->where(['=', 'id', $id])->one();
        $order_section_time = OrderSectionTime::find()->where(['=', 'order_id', $id])->all();
        $order_step = OrderStep::find()->where(['=', 'order_id', $id])->orderBy(['start_date' => SORT_ASC])->all();
        $now_step = OrderSectionTime::find()->where(['exit_date' => null])->andWhere(['=', 'order_id', $id])->one();

        return $this->render('view-index-order', [
            'now_step' => $now_step,
            'order' => $order,
            'order_section_time' => $order_section_time,
            'order_step' => $order_step,
        ]);
    }

    public function actionUpdateIndexOrder($id)
    {
        $order = Orders::find()->where(['=', 'id', $id])->one();
        $sections = Section::find()->where(['=', 'status', 1])->all();
        $machines = Machines::find()->where(['=', 'status', 1])->all();
        $not_started_plans = Yii::$app->db
            ->createCommand('SELECT
                os.id AS step_id,
                os.section_id AS section_id,
                os.machine_id AS machine_id,
                os.machine_price AS machine_price,
                os.start_date AS step_start_date,
                os.deadline AS step_end_date,
                os.work_time AS step_work_time,
                ost.id AS order_section_time_id
            FROM order_step AS os
            LEFT JOIN order_section_time AS ost
                ON ost.order_step_id = os.id
            WHERE os.order_id = :order_id AND ost.enter_date ISNULL
            ORDER BY os.id')
            ->bindValue('order_id', $id)
            ->queryAll();
        $last_started_plans = Yii::$app->db
            ->createCommand('SELECT
                os.id AS step_id,
                os.section_id AS section_id,
                os.machine_id AS machine_id,
                os.machine_price AS machine_price,
                os.start_date AS step_start_date,
                os.deadline AS step_end_date,
                os.work_time AS step_work_time,
                ost.id AS order_section_time_id
            FROM order_step AS os
                LEFT JOIN order_section_time AS ost
            ON ost.order_step_id = os.id
            WHERE os.order_id = :order_id
            ORDER BY os.id OFFSET :count_not_started LIMIT 1')
            ->bindValue('order_id', $id)
            ->bindValue('count_not_started', count($not_started_plans))
            ->queryOne();

        return $this->render('update-index-order', [
            'sections' => $sections,
            'machines' => $machines,
            'order_id' => $id,
            'order' => $order,
            'not_started_plans' => $not_started_plans,
            'last_started_plans' => $last_started_plans,
        ]);
    }

    public function actionPrice()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (!(isset($_GET['time']) && !empty($_GET['time'])))
                return ['status' => 'time_fail'];
            if (!(isset($_GET['machine']) && !empty($_GET['machine'])))
                return ['status' => 'machine_fail'];

            $time = Yii::$app->request->get('time');
            $machine = Yii::$app->request->get('machine');

            $machinePrice = Yii::$app->db
                ->createCommand('
                    select 
                        mp.price
                    from machines as m
                    inner join machines_price mp on m.id = mp.machine_id
                    where m.id = :machine_id order by mp.id desc 
                ')
                ->bindValue('machine_id', $machine)
                ->queryOne();
            if (isset($machinePrice) and !empty($machinePrice)){
                $timePrice = round($time * $machinePrice['price'], 1);
                return [
                    'status' => 'success',
                    'price' => $timePrice
                ];
            } else {
                return [
                    'status' => 'fail_machine',
                ];
            }
        }
    }

    public function actionOrders()
    {
        $selectOrders = '
            SELECT 
                o.id,
                o.created_date,
                o.status,
                o.category_id,
                cl.company_name,
                o.proirity,
                o.order_from,
                c.title AS category_name
            FROM orders AS o
            INNER JOIN notification AS n ON o.id = n.order_id AND n.module_id = 7
            INNER JOIN category AS c ON o.category_id = c.id
            INNER JOIN clients as cl ON o.client_id = cl.id
            WHERE n.end_date IS NULL ';
        $orders = Yii::$app->db->createCommand($selectOrders)->queryAll();

        return $this->render('orders', [
            'model' => $orders,
        ]);
    }

    public function actionUpdateSection()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['s']) && !empty($_GET['s'])) {
                if ($_GET['s'] == 'true') {
                    $error = false;
                    $order_id = Yii::$app->request->get('id');
                    $module_id = Yii::$app->request->get('module_id');

                    if (!(isset($order_id) and !empty($order_id))) {
                        $error = true;
                        return [
                            'status' => 'error_id'
                        ];
                    }

                    if (!(isset($module_id) and !empty($module_id))) {
                        $error = true;
                        return [
                            'status' => 'error_module'
                        ];
                    }

                    if (!$error){
                        $model = Notification::find()->where(['order_id' => $order_id, 'end_date' => NULL])->one();
                        if (isset($model) and !empty($model)){
                            $model->end_date = date('Y-m-d H:i:s');
                            $model->status = 3;
                            if ($model->save()){
                                $next_section = new Notification();
                                $next_section->module_id = $module_id;
                                $next_section->order_id = $order_id;
                                $next_section->enter_date = date("Y-m-d H:i:s", strtotime('+1 second', strtotime(date('Y-m-d H:i:s'))));
                                $next_section->status = 1;
                                $next_section->save();

                                return [
                                    'status' => 'success',
                                ];
                            } else {
                                pre($model->errors);
                            }
                        }
                    }
                }else {
                    $model = Modules::find()->where(['id' => 6])->one();
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('section.php',[
                            'model' => $model
                        ]),
                    ];
                }
            }
        }
    }
}
