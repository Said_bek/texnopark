<div class="select2-primary">
    <label for="mySelect2" style="font-size: 18px">Bo'lim</label>
    <input type="text" value="<?= $section->title ?>" data-id="<?= $section->id ?>" disabled class="form-control">
</div>

<div class="select2-primary mt-10">
    <label for="mySelect2" style="font-size: 18px">Ishchini tanlang</label>
    <select class="form-control mySelect2" <?php if(!(isset($users) and !empty($users))) echo "disabled" ?> id="select3" multiple="multiple" data-plugin="select2">
        <?php if (isset($users) and !empty($users)): ?>
            <?php foreach ($users as $key => $value): ?>
                <?php $select = (in_array($value->id, $arr)) ? "selected" : ""; ?>
                <option value="<?= $value->id ?>" <?= $select ?>><?= $value->username ?></option>
            <?php endforeach ?>
        <?php else: ?>
            <option selected>Foydalanuvchilar mavjud emas</option>
        <?php endif ?>
    </select>
</div>

<script>
    $("#select3").select2({
        tags: true
    });
</script>
