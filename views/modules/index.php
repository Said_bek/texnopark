<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\ModulUsers;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Modullar nazorati';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modules-index">
    <div class="row">
        <div class="col-md-6">
            <h2 class="m-0"><?= Html::encode($this->title) ?></h2>
        </div>
    </div>
    <table class='table table-bordered table-striped mt-10'>
        <thead>
        <tr>
            <th>#</th>
            <th>Bo'lim</th>
            <th>Foydalanuvchilar</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
        if (isset($model) and !empty($model)){
            $i = 1;
            foreach ($model as $value) {
                echo "<tr>";
                echo "<td>".$i."</td>";
                echo "<td>".$value['title']."</td>";
                echo "<td>";
                    if (isset($value['list']) and !empty($value['list'])){
                        foreach ($value['list'] as $value1){
                            echo "<span class='badge badge-primary m-1'>".$value1."</span>";
                        }
                    } else {
                        echo "<span class='badge badge-danger m-1'>Foydalanuvchi mavjud emas</span>";
                    }
                echo "</td>";
                    $id = 0;
                    if(isset($value['id']) && !empty($value['id'])){
                        $id = $value['id'];
                    }
                echo "<td><a href='#' class='update' data-target='#examplePositionCenter' data-toggle='modal' title='Update' data-update='{$id}' aria-label='Update'><i class='fa fa-pencil' style='color: #3e8ef7'></i></a></td>";
                echo "</tr>";
                $i++;
            }
        } else {
            echo "<tr>";
            echo  "<td class='text-center' colspan='4'>Ma'lumot yo'q</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
</div>
<style>
    .modules-index{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>
<?php
$js = <<<JS
    $(document).on('click', '.update', function(){
        var id = $(this).attr('data-update')
        $.ajax({
            url: 'update-user',
            data: {
                update: false,
                id: id,
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("Ishchi o'zgartirish")
                    $('.modal-footer .btn-primary').addClass('update_users')
                    $('.modal-footer .btn-primary').attr('data-update',id)
                }
            }
        })
    })
    
    $(document).on('click', '.update_users', function(){
        var users = $('#select3').val()
        var id = $(this).attr('data-update')
        $.ajax({
            url: 'update-user',
            data: {
                update: true,
                users: users,
                id: id
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    location.reload()
                }
            }
        })
    })
JS;
$this->registerJs($js);
?>
