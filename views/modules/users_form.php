<?php
    namespace app\controllers;
    use app\models\Modules;
    use app\models\ModulUsers;
    use app\models\Users;
    use Yii;
?>

<?php
$arr = [];
foreach ($selectModuleUser as $userValue){
    $arr[] = $userValue['user_id'];
}
$getUser = Users::find()->where(['not in', 'id', $arr])->all();
?>
<?php foreach ($getUser as $key => $value): ?>
    <option value="<?php echo $value->id; ?>">
        <?php echo $value->username; ?>
    </option>
<?php endforeach ?>


<script>
    $("#select3").select2({
        tags: true
    });
</script>
