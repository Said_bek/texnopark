<div data-role="container">
    <div data-role="content">
        <?php
            if (isset($model) and !empty($model)){
                foreach ($model as $value) { ?>
                    <a class="list-group-item dropdown-item order" href="javascript:void(0)"
                       role="menuitem" data-id="<?= $value['notification_id'] ?>">
                        <div class="media">
                            <div class="pr-10">
                                <?php
                                if ($value['proirity'] == 0){
                                    echo '<i class="icon wb-order bg-primary-600 white icon-circle" aria-hidden="true"></i>';
                                }
                                else if ($value['proirity'] == 1){
                                    echo '<i class="icon wb-order bg-red-600 white icon-circle" aria-hidden="true"></i>';
                                } else {
                                    echo '<i class="icon wb-order bg-green-600 white icon-circle" aria-hidden="true"></i>';
                                }
                                $first_date = new DateTime($value['enter_date']);
                                $second_date = new DateTime(date('Y-m-d H:i:s'));
                                $interval = $first_date->diff($second_date);

                                $time = '';
                                if ($interval->format('%d') >= 1){
                                    $time = $interval->format('%d kun %h soat %i daqiqa');
                                } else if ($interval->format('%d') < 1 and $interval->format('%h') >= 1){
                                    $time = $interval->format('%h soat %i daqiqa');
                                } else if ($interval->format('%d') < 1 and $interval->format('%h') < 1 and $interval->format('%i') < 1 and $interval->format('%s') >= 1){
                                    $time = $interval->format('%s soniya');
                                } else {
                                    $time = $interval->format('%i daqiqa');
                                }
                                ?>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading"><?= $value['order_name'] ?></h6>
                                <span ><?= $time ?> avval</span>
                            </div>
                        </div>
                    </a>
                <?php }
            }
        ?>
    </div>
</div>