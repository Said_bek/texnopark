<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Foydalanuvchini o\'zgartirish';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="users-update">

    <h1 class="m-0"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<style>
    .users-update{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>
