<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="users-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'password')->textInput() ?>

        <?= $form->field($model, 'phone_number')->textInput() ?>

        <?= $form->field($model, 'type')->dropDownList($model->getAllRole(), ['multiple'=>'multiple']) ?>

        <div class="form-group">
            <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success float-right']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


<!--<div class="users-form">-->
<!--    <h2 class="mb-20 mt-0">Foydalanuvchi qo'shish</h2>-->
<!---->
<!--    <div class="row">-->
<!--        <div class="col-md-6">-->
<!--            <label for="name">Ism</label>-->
<!--            <input type="text" id="name" class="form-control">-->
<!--            <small class="name_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>-->
<!--        </div>-->
<!--        <div class="col-md-6">-->
<!--            <label for="name">Parol</label>-->
<!--            <input type="password" id="password" class="form-control">-->
<!--            <small class="password_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>-->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--    <div class="row mt-20">-->
<!--        <div class="col-md-6">-->
<!--            <label for="name">Telefon raqam</label>-->
<!--            <input type="text" id="phone_number" class="form-control">-->
<!--            <small class="phone_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>-->
<!--        </div>-->
<!--        <div class="col-md-6">-->
<!--            <label for="name">Roli</label>-->
<!--            <select class="form-control" multiple="multiple" id="role" data-plugin="select2">-->
<!--                --><?php //if (isset($role) and !empty($role)){
//                    foreach ($role as $value){ ?>
<!--                       <option value="--><?//= $value->name ?><!--">--><?//= $value->name ?><!--</option>-->
<!--                    --><?php //}
//
//                } ?>
<!--            </select>-->
<!--            <small class="role_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>-->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--    <div class="row mt-20">-->
<!--    --><?php
//        if (isset($modules) and !empty($modules)){
//            foreach ($modules as $value){ ?>
<!--                <div class="col-md-3 mt-10">-->
<!--                    <input name="roleInput[]" type="checkbox" class="js-switch switch_input" data-plugin="switchery" data-switchery="true" style="display: none;" value="--><?//= $value->id ?><!--">-->
<!--                    <span class="ml-4 badge badge-lg badge-default section">--><?//= $value->title ?><!--</span>-->
<!--                </div>-->
<!--            --><?php //}
//        }
//    ?>
<!--    </div>-->
<!--    <div class="row">-->
<!--        <div class="col-lg-12 mt-5">-->
<!--            <button type="button" class="btn btn-success float-right mt-10 save_user">Saqlash</button>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!---->
<!--<style>-->
<!--    .users-form{-->
<!--        background-color: white;-->
<!--        padding: 30px;-->
<!--        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);-->
<!--        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);-->
<!--        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);-->
<!--    }-->
<!--    .hidden {-->
<!--        display: none;-->
<!--    }-->
<!---->
<!--    .select-validate {-->
<!--        border: 1px solid #ff4c52;-->
<!--        border-radius: .215rem;-->
<!--    }-->
<!--</style>-->
<!---->
<?php
//    $js = <<<JS
//        $(document).on('change', '.js-switch', function(){
//            var _this = $(this)
//            if($(_this).is(":checked")) {
//                $(_this).siblings('.section').removeClass('badge-default')
//                $(_this).siblings('.section').addClass('badge-primary')
//            } else {
//                $(_this).siblings('.section').removeClass('badge-primary')
//                $(_this).siblings('.section').addClass('badge-default')
//            }
//        })
//
//        $(document).on('click', '.save_user', function(){
//            var values = $("input[name='roleInput[]']").map(function(){
//                if ($(this).is(":checked")){
//                    return $(this).val()
//                }
//            }).get();
//            var name = $("#name").val()
//            var password = $("#password").val()
//            var phone = $("#phone_number").val()
//            var role = $("#role").val()
//            if (name != '' && password != '' && phone != '' && role != ''){
//                $('#name').removeClass('is-invalid')
//                $('.name_error').addClass('hidden')
//                $('#password').removeClass('is-invalid')
//                $('.password_error').addClass('hidden')
//                $('#phone_number').removeClass('is-invalid')
//                $('.phone_error').addClass('hidden')
//                $('#role').removeClass('is-invalid')
//                $('.role_error').addClass('hidden')
//                $.ajax({
//                    url: 'create',
//                    data: {
//                        name: name,
//                        password: password,
//                        phone: phone,
//                        role: role
//                    },
//                    dataType: 'json',
//                    type: 'get',
//                    success: function(response) {
//                        if (response.status == 'success') {
//                            window.location.replace('/users');
//                        }
//                    }
//                })
//            } else {
//                if (name == ''){
//                    $('#name').addClass('is-invalid')
//                    $('.name_error').removeClass('hidden')
//                } else {
//                    $('#name').removeClass('is-invalid')
//                    $('.name_error').addClass('hidden')
//                }
//
//                if (password == ''){
//                    $('#password').addClass('is-invalid')
//                    $('.password_error').removeClass('hidden')
//                } else {
//                    $('#password').removeClass('is-invalid')
//                    $('.password_error').addClass('hidden')
//                }
//
//                if (phone == ''){
//                    $('#phone_number').addClass('is-invalid')
//                    $('.phone_error').removeClass('hidden')
//                } else {
//                    $('#phone_number').removeClass('is-invalid')
//                    $('.phone_error').addClass('hidden')
//                }
//
//                if (role == ''){
//                    $('.select2-container').addClass('select-validate')
//                    $('.role_error').removeClass('hidden')
//
//                } else {
//                    $('.select2-container').removeClass('select-validate')
//                    $('.role_error').addClass('hidden')
//                }
//            }
//        })
//    JS;
//    $this->registerJs($js);
//?>