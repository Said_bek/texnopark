<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Foydalanuvchilar';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <div class="row">
        <div class="text-left col-md-6">
            <h1 class="m-0"><?= Html::encode($this->title) ?></h1>
        </div>

        <div class="text-right col-md-6">
            <?= Html::a("Foydalanuvchi qo'shish", ['create'], ['class' =>  'btn btn-success']) ?>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'phone_number',
            [
                'format' => 'html',
                'label' => "Foydalanuvchi ro'llari",
                'headerOptions' => ['style' => 'color: #3e8ef7 !important;'],
                'value' => function ($data) {
                    if (isset($data->roles) and !empty($data->roles)){
                        $span = '';
                        foreach ($data->roles as $value){
                            $span.= " <span class='badge badge-dark'>".$value->item_name."</span>";
                        }
                    } else {
                        $span = "<span class='badge badge-danger'>Rol biriktirilmagan</span>";
                    }
                    return $span;
               },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>


</div>

<style>
    .users-index{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>
