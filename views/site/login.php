<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login pt-10">

        <div class="brand">
          <img class="brand-img" src="../../assets//images/logo-colored.png" alt="...">
          <h2 class="brand-text font-size-18">TexnoPark</h2>
        </div>

    <?php $form = ActiveForm::begin(); ?>

        <div class="container">
              <div class="form-group form-material floating" data-plugin="formMaterial">
                    <?= $form->field($model, 'username')->label('Foydalanuvchi')->textInput(['autofocus' => true]) ?>
              </div>

              <div class="form-group form-material floating" data-plugin="formMaterial">
                <?= $form->field($model, 'password')->label('Maxfiy so`z')->passwordInput() ?>
              </div>


                <div class="form-group">
                    <div class="col-lg-offset-1 col-lg-11">
                        <?= Html::submitButton('Tizimga kirish', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>
                </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>



