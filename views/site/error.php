<?php

use yii\bootstrap\Html;

?>
<header>
    <p style="font-size: 30px!important;"> Sahifa topilmadi </p>
</header>
<p  class="error-advise">Tizimda ishlashni davom etish uchun asosiy sahifaga o'ting</p>
<a class="btn btn-primary btn-round" href="<?= \yii\helpers\Url::to(['site/index'])?>">Asosiy sahifaga o'tish</a>