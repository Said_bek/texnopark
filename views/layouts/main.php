<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="bootstrap admin template">
        <meta name="author" content="">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>


        <!-- Scripts -->
        <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
        <script>
            Breakpoints();
        </script>
    </head>

    <body class="animsition dashboard">
    <!--modal-->
    <div class="modal fade" id="examplePositionCenter" aria-hidden="true" aria-labelledby="examplePositionCenter"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-simple modal-center">
            <div class="modal-content">
                <div class="modal-header" style="font-size: 25px">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Yopish</button>
                    <button type="button" class="btn btn-primary">Saqlash</button>
                </div>
            </div>
        </div>
    </div>
<!--    end modal-->

<!--    second modal-->
    <div class="modal fade modal-3d-sign" id="exampleNifty3dSign" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-simple">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Modal Title</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body…</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Yopish</button>
                </div>
            </div>
        </div>
    </div>
<!--    end second modal-->

    <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">

        <div class="navbar-header">
            <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
                    data-toggle="menubar">
                <span class="sr-only">Toggle navigation</span>
                <span class="hamburger-bar"></span>
            </button>
            <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
                    data-toggle="collapse">
                <i class="icon wb-more-horizontal" aria-hidden="true"></i>
            </button>
            <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
                <!--             <img class="navbar-brand-logo" src="../assets/images/logo.png" title="Remark">-->
                <span class="navbar-brand-text hidden-xs-down"> T E X N O P A R K </span>
            </div>
            <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search"
                    data-toggle="collapse">
                <span class="sr-only">Toggle Search</span>
                <i class="icon wb-search" aria-hidden="true"></i>
            </button>
        </div>

        <div class="navbar-container container-fluid">
            <!-- Navbar Collapse -->
            <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
                <!-- Navbar Toolbar -->
                <ul class="nav navbar-toolbar">
                    <li class="nav-item hidden-float" id="toggleMenubar">
                        <a class="nav-link" data-toggle="menubar" href="#" role="button">
                            <i class="icon hamburger hamburger-arrow-left">
                                <span class="sr-only">Toggle menubar</span>
                                <span class="hamburger-bar"></span>
                            </i>
                        </a>
                    </li>
                </ul>

                <?php
                $user_id = Yii::$app->user->identity->id;
                if (isset($user_id) and !empty($user_id)) {
                    $selectOrders = '
                    SELECT 
                       n.id AS notification_id, 
                       o.id, o.proirity, 
                       o.order_name, 
                       n.enter_date,
                       n.module_id
                    FROM notification AS n 
                    INNER JOIN orders AS o ON n.order_id = o.id
                    WHERE n.module_id IN (
                         SELECT mu.modul_id FROM modul_users AS mu WHERE mu.user_id = ' . $user_id . '
                    ) AND n.end_date IS NULL AND o.status = 1 AND n.status = 1;';
                    $orders = Yii::$app->db->createCommand($selectOrders)->queryAll();
                    $notificationCount = count($orders);
                } else { ?>
                    <script>
                        window.location.href = '../index.php/site/login';
                    </script>
                    <?php } ?>

                <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                    <?php if (Yii::$app->user->can('Admin')) { ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?= Url::to(['log-history/index']) ?>">
                                <i class="fa fa-history font-size-20"></i>
                            </a>
                        </li>
                    <?php } ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" title="Notifications"
                           aria-expanded="false" data-animation="scale-up" role="button">
                            <i class="icon wb-bell font-size-20" aria-hidden="true"></i>
                            <?php if ($notificationCount > 0) { ?>
                                <span class="badge badge-pill badge-danger up notification_count"><?= $notificationCount ?></span>
                            <?php } ?>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
                            <div class="dropdown-menu-header">
                                <?php
                                if ($notificationCount > 0) { ?>
                                    <h5 class="news_text">YANGILIKLAR</h5>
                                    <span class="badge badge-round badge-danger new_count">Yangi <?= $notificationCount ?></span>
                                <?php } else { ?>
                                    <h5>YANGILIKLAR MAVJUD EMAS!</h5>
                                <?php } ?>
                            </div>

                            <div class="list-group div-list">
                                <div data-role="container">
                                    <div data-role="content">
                                        <?php
                                        if (isset($orders) and !empty($orders)) {
                                            foreach ($orders as $value) { ?>
                                                <a class="list-group-item dropdown-item order" href="javascript:void(0)"
                                                   role="menuitem" data-id="<?= $value['notification_id'] ?>">
                                                    <div class="media">
                                                        <div class="pr-10">
                                                            <?php
                                                            if ($value['proirity'] == 0) {
                                                                echo '<i class="icon wb-order bg-primary-600 white icon-circle" aria-hidden="true"></i>';
                                                            } else if ($value['proirity'] == 1) {
                                                                echo '<i class="icon wb-order bg-red-600 white icon-circle" aria-hidden="true"></i>';
                                                            } else {
                                                                echo '<i class="icon wb-order bg-green-600 white icon-circle" aria-hidden="true"></i>';
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="media-heading"><?= $value['order_name'] ?></h6>
                                                            <span><?= date('H:i m-d-Y', strtotime($value['enter_date'])) ?></span>
                                                        </div>
                                                    </div>
                                                </a>
                                            <?php }
                                        } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-menu-footer">
                                <!--                                                            <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">-->
                                <!--                                                                <i class="icon wb-settings" aria-hidden="true"></i>-->
                                <!--                                                            </a>-->
                                <!--                                                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">-->
                                <!--                                                                All notifications-->
                                <!--                                                            </a>-->
                            </div>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
                           data-animation="scale-up" role="button">
                          <span class="avatar avatar-online">
                            <img src="../../global/portraits/5.jpg" alt="...">
                            <i></i>
                          </span>
                        </a>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon wb-user"
                                                                                                  aria-hidden="true"></i>
                                Profile</a>
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i
                                        class="icon wb-payment" aria-hidden="true"></i> Billing</a>
                            <a class="dropdown-item" href="<?= \yii\helpers\Url::to(['settings/index']) ?>"
                               role="menuitem"><i class="icon wb-settings" aria-hidden="true"></i>Mening
                                sozlamalarim</a>
                            <div class="dropdown-divider" role="presentation"></div>
                            <a class="dropdown-item" href="<?= \yii\helpers\Url::to(['site/logout']) ?>"
                               role="menuitem"><i class="icon wb-power" aria-hidden="true"></i> Tizimdan chiqish</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="site-menubar">
        <div class="site-menubar-body">
            <div>
                <div>
                    <ul class="site-menu" data-plugin="menu">
                        <li class="site-menu-item has-sub <?php if (Yii::$app->controller->id == 'control') echo "active"; ?>">
                            <a href="<?= \yii\helpers\Url::to(['control/index']) ?>">
                                <i class="site-menu-icon wb-order" aria-hidden="true"></i>
                                <span class="site-menu-title">Nazorat bo'limi</span>
                            </a>
                        </li>
                        <li class="site-menu-item has-sub <?php if (Yii::$app->controller->id == 'users') echo "active"; ?>">
                            <a href="<?= \yii\helpers\Url::to(['users/index']) ?>">
                                <i class="site-menu-icon wb-bookmark" aria-hidden="true"></i>
                                <span class="site-menu-title">Foydalanuvchilar bo'limi</span>
                            </a>
                        </li>

                        <li class="open site-menu-item has-sub <?php if (Yii::$app->controller->id == 'constructor') echo "open active"; ?>">
                            <a href="javascript:void(0)">
                                <i class="site-menu-icon wb-plugin" aria-hidden="true"></i>
                                <span class="site-menu-title">Konstruktor bo'limi</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item <?php if (Yii::$app->controller->id == 'constructor' and Yii::$app->controller->action->id == 'index') echo "active"; ?>">
                                    <a class="animsition-link"
                                       href="<?= \yii\helpers\Url::to(['constructor/index']) ?>">
                                        <span class="site-menu-title">Spesifikation</span>
                                    </a>
                                </li>
                                <li class="site-menu-item  <?php if (Yii::$app->controller->id == 'constructor' and Yii::$app->controller->action->id == 'constructors') echo "active"; ?>">
                                    <a class="animsition-link"
                                       href="<?= \yii\helpers\Url::to(['constructor/constructors']) ?>">
                                        <span class="site-menu-title">Konstruktorlar</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="site-menu-item has-sub <?php if (Yii::$app->controller->id == 'orders') echo "open active"; ?>">
                            <a href="javascript:void(0)">
                                <i class="site-menu-icon wb-clipboard" aria-hidden="true"></i>
                                <span class="site-menu-title">Sotuv bo'limi</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item <?php if (Yii::$app->controller->id == 'orders' and Yii::$app->controller->action->id == 'index') echo "active"; ?>">
                                    <a class="animsition-link" href="<?= \yii\helpers\Url::to(['orders/index']) ?>">
                                        <span class="site-menu-title">Buyurtmalar</span>
                                    </a>
                                </li>
                                <li class="site-menu-item <?php if (Yii::$app->controller->id == 'orders' and Yii::$app->controller->action->id == 'category') echo "active"; ?>">
                                    <a class="animsition-link" href="<?= \yii\helpers\Url::to(['orders/category']) ?>">
                                        <span class="site-menu-title">Kategoriyalar</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="site-menu-item has-sub <?php if (Yii::$app->controller->id == 'finance') echo "open active"; ?>">
                            <a href="javascript:void(0)">
                                <i class="site-menu-icon wb-extension" aria-hidden="true"></i>
                                <span class="site-menu-title">Moliya bo'limi</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item <?php if (Yii::$app->controller->id == 'finance' and Yii::$app->controller->action->id == 'orders') echo "open active"; ?>">
                                    <a class="animsition-link" href="<?= \yii\helpers\Url::to(['finance/orders']) ?>">
                                        <span class="site-menu-title">Buyurtmalar</span>
                                    </a>
                                </li>
                                <li class="site-menu-item <?php if (Yii::$app->controller->id == 'finance' and Yii::$app->controller->action->id == 'index') echo "open active"; ?>">
                                    <a class="animsition-link" href="<?= \yii\helpers\Url::to(['finance/index']) ?>">
                                        <span class="site-menu-title">Stanoklar</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="site-menu-item has-sub <?php if (Yii::$app->controller->id == 'no name') echo "active"; ?>">
                            <a href="<?= \yii\helpers\Url::to(['section/index']) ?>">
                                <i class="site-menu-icon wb-table" aria-hidden="true"></i>
                                <span class="site-menu-title">Ishlab chiqish bo'limi</span>
                            </a>
                        </li>
                        <li class="site-menu-item has-sub <?php if (Yii::$app->controller->id == 'use2rs') echo "open active"; ?>">
                            <a href="javascript:void(0)">
                                <i class="site-menu-icon wb-plugin" aria-hidden="true"></i>
                                <span class="site-menu-title">Sklad</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="<?= \yii\helpers\Url::to(['materials/index']) ?>">
                                        <span class="site-menu-title">Materiallar</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="<?= \yii\helpers\Url::to(['marks/index']) ?>">
                                        <span class="site-menu-title">Markalar</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="<?= \yii\helpers\Url::to(['unit/index']) ?>">
                                        <span class="site-menu-title">O'lchov birliklari</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="<?= \yii\helpers\Url::to(['nakladnoy/index']) ?>">
                                        <span class="site-menu-title">Nakladnoylar</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="site-menu-item has-sub <?php if (Yii::$app->controller->id == 'modules') echo "active"; ?>">
                            <a href="<?= \yii\helpers\Url::to(['modules/index']) ?>">
                                <i class="site-menu-icon fa fa-eye" aria-hidden="true"></i>
                                <span class="site-menu-title">Modullar Nazorati</span>
                            </a>
                        </li>

                        <li class="site-menu-item has-sub <?php if (Yii::$app->controller->id == 'order') echo "open"; ?>">
                            <a href="javascript:void(0)">
                                <i class="site-menu-icon wb-edit" aria-hidden="true"></i>
                                <span class="site-menu-title">Rejalashtirish bo'limi</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item <?php if (Yii::$app->controller->id == 'order' and Yii::$app->controller->action->id == 'orders') echo "active"; ?>">
                                    <a class="animsition-link" href="<?= \yii\helpers\Url::to(['order/orders']) ?>">
                                        <span class="site-menu-title">Buyurtmalar</span>
                                    </a>
                                </li>
                                <li class="site-menu-item <?php if (Yii::$app->controller->id == 'order' and Yii::$app->controller->action->id == 'index') echo "active"; ?>">
                                    <a class="animsition-link" href="<?= \yii\helpers\Url::to(['order/index']) ?>">
                                        <span class="site-menu-title">Yangi buyurtmalar</span>
                                    </a>
                                </li>
                                <li class="site-menu-item <?php if (Yii::$app->controller->id == 'order' and Yii::$app->controller->action->id == 'index-order') echo "active"; ?>"
                                ">
                                <a class="animsition-link"
                                   href="<?= \yii\helpers\Url::to(['order/index-order']) ?>">
                                    <span class="site-menu-title">Reja qilinganlar</span>
                                </a>
                                </li>
                            </ul>
                        </li>
                        <li class="site-menu-item has-sub <?php if (Yii::$app->controller->id == 'supplier') echo "open active"; ?>">
                            <a href="javascript:void(0)">
                                <i class="site-menu-icon fa fa-taxi" aria-hidden="true"></i>
                                <span class="site-menu-title">Yetkazib berish</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item <?php if (Yii::$app->controller->id == 'supplier' and Yii::$app->controller->action->id == 'supplier-nakladnoy') echo "active"; ?>">
                                    <a class="animsition-link" href="<?= \yii\helpers\Url::to(['supplier/supplier-orders']) ?>">
                                        <span class="site-menu-title">Nakladnoylar</span>
                                    </a>
                                </li>
                                <li class="site-menu-item <?php if (Yii::$app->controller->id == 'supplier' and Yii::$app->controller->action->id == 'index') echo "active"; ?>">
                                    <a class="animsition-link" href="<?= \yii\helpers\Url::to(['supplier/index']) ?>">
                                        <span class="site-menu-title">Yetkazib beruvchi</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="site-menu-item has-sub <?php if (Yii::$app->controller->id == 'test') echo "active"; ?>">
                            <a href="<?= \yii\helpers\Url::to(['test/index']) ?>">
                                <i class="site-menu-icon wb-bookmark" aria-hidden="true"></i>
                                <span class="site-menu-title">Namunalar</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <!-- Page -->
    <div class="page">
        <div class="page-content container-fluid">
            <?= $content ?>
        </div>
    </div>
    <!-- End Page -->


    <!-- Core  -->

    <!-- <script>Config.set('assets', '../assets');</script> -->

    <!-- Page -->
    <!--     <script src="../assets/js/Site.js"></script>
      <script src="../../global/js/Plugin/asscrollable.js"></script>
      <script src="../../global/js/Plugin/slidepanel.js"></script>
      <script src="../../global/js/Plugin/switchery.js"></script>
          <script src="../../global/js/Plugin/matchheight.js"></script>
          <script type="text/javascript"></script>ript src="../../global/js/Plugin/jvectormap.js"></script>

          <script src="../assets/examples/js/dashboard/v1.js"></script> -->
    <?php $this->endBody() ?>
    </body>

    <script>
        $(document).on('click', '.order', function () {
            var id = $(this).attr('data-id')
            var _this = $(this)
            alertify.confirm('Buyurtmani qabul qilasizmi?', function (e) {
                if (e) {
                    alertify.success('Buyurtma qabul qilindi')
                    $.ajax({
                        url: '../notification/update-notification',
                        data: {
                            id: id
                        },
                        dataType: 'json',
                        type: 'get',
                        success: function(response) {
                            if (response.status == 'success') {
                                $(_this).remove()
                                if (response.count > 0){
                                    $('.notification_count').html(response.count)
                                    $('.new_count').html('Yangi ' + response.count)
                                    $('.list-group').html(response.content)
                                } else {
                                    $('.notification_count').html('')
                                    $('.new_count').html('')
                                    $('.news_text').text('YANGILIKLAR MAVJUD EMAS!')
                                    $('.list-group').remove()
                                }
                            }
                        }
                    })
                }
            })
        })
    </script>

    </html>
<?php $this->endPage() ?>