
  
	<!-- modal -->	

      <button class="btn btn-primary" data-target="#examplePositionCenter" data-toggle="modal"
        type="button">Generate</button>

  <!-- Modal -->
  <div class="modal fade" id="examplePositionCenter" aria-hidden="true" aria-labelledby="examplePositionCenter"
    role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-center">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Modal Title</h4>
        </div>
        <div class="modal-body">
          <p>One fine body…</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>

	<!--   end modal -->




<!--  multiple select --> 



                <div class="example-wrap">
                  <h4 class="example-title">Multiple Selects</h4>
                  <p>You can limit the number of elements you are allowed to select
                    via the <code>data-max-option</code> attribute. It also works
                    for option groups. </p>
                  <div class="example">
                    <select multiple data-plugin="selectpicker" data-max-options="2">
                      <option>Mustard</option>
                      <option>Ketchup</option>
                      <option>Relish</option>
                    </select>
                  </div>
                  <div class="example">
                    <select multiple data-plugin="selectpicker">
                      <optgroup data-max-options="2" label="Condiments">
                        <option>Mustard</option>
                        <option>Ketchup</option>
                        <option>Relish</option>
                      </optgroup>
                      <optgroup data-max-options="2" label="Breads">
                        <option>Plain</option>
                        <option>Steamed</option>
                        <option>Toasted</option>
                      </optgroup>
                    </select>
                  </div>
                </div>

   <!-- end multiple select -->



<!--    select 2  multiple select -->

                <div class="example-wrap">
                  <h4 class="example-title">Multi-Balue</h4>
                  <p>Add <code>multiple</code> to create a multi select2.</p>
                  <div class="example">
                    <select class="form-control" multiple data-plugin="select2">
                      <optgroup label="Alaskan/Hawaiian Time Zone">
                        <option value="AK">Alaska</option>
                        <option value="HI">Hawaii</option>
                      </optgroup>
                      <optgroup label="Pacific Time Zone">
                        <option value="CA">California</option>
                        <option value="NV">Nevada</option>
                        <option value="OR">Oregon</option>
                        <option value="WA">Washington</option>
                      </optgroup>
                      <optgroup label="Mountain Time Zone">
                        <option value="AZ">Arizona</option>
                        <option value="CO">Colorado</option>
                        <option value="ID">Idaho</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NM">New Mexico</option>
                        <option value="ND">North Dakota</option>
                        <option value="UT">Utah</option>
                        <option value="WY">Wyoming</option>
                      </optgroup>
                      <optgroup label="Central Time Zone">
                        <option value="AL">Alabama</option>
                        <option value="AR">Arkansas</option>
                        <option value="IL">Illinois</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="OK">Oklahoma</option>
                        <option value="SD">South Dakota</option>
                        <option value="TX">Texas</option>
                        <option value="TN">Tennessee</option>
                        <option value="WI">Wisconsin</option>
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="IN">Indiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="OH">Ohio</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WV">West Virginia</option>
                      </optgroup>
                    </select>
                  </div>
                </div>


    <!-- end select 2 multiple select -->





    <!-- simple select 2 -->

        <div class="panel">
          <div class="panel-heading">
            <h3 class="panel-title">Select 2
              <small><a class="example-plugin-link" href="https://select2.github.io" target="_blank">official website</a></small>
            </h3>
          </div>
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-6 col-xl-4">
                <!-- Example Basic -->
                <div class="example-wrap">
                  <h4 class="example-title">Basic</h4>
                  <p>Create a basic select2 by using <code>data-plugin="select2"</code>.</p>
                  <div class="example">
                    <select class="form-control" data-plugin="select2">
                      <optgroup label="Alaskan/Hawaiian Time Zone">
                        <option value="AK">Alaska</option>
                        <option value="HI">Hawaii</option>
                      </optgroup>
                      <optgroup label="Pacific Time Zone">
                        <option value="CA">California</option>
                        <option value="NV">Nevada</option>
                        <option value="OR">Oregon</option>
                        <option value="WA">Washington</option>
                      </optgroup>
                      <optgroup label="Mountain Time Zone">
                        <option value="AZ">Arizona</option>
                        <option value="CO">Colorado</option>
                        <option value="ID">Idaho</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NM">New Mexico</option>
                        <option value="ND">North Dakota</option>
                        <option value="UT">Utah</option>
                        <option value="WY">Wyoming</option>
                      </optgroup>
                      <optgroup label="Central Time Zone">
                        <option value="AL">Alabama</option>
                        <option value="AR">Arkansas</option>
                        <option value="IL">Illinois</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="OK">Oklahoma</option>
                        <option value="SD">South Dakota</option>
                        <option value="TX">Texas</option>
                        <option value="TN">Tennessee</option>
                        <option value="WI">Wisconsin</option>
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="IN">Indiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="OH">Ohio</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WV">West Virginia</option>
                      </optgroup>
                    </select>
                  </div>
                </div>
                <!-- End Example Basic -->
              </div>
            </div>
          </div>
        </div>



     <!-- end simple select 2 -->









    <!-- table -->


            <div class="panel">
              <header class="panel-heading">
                <h3 class="panel-title">Filtering rows</h3>
              </header>
              <div class="panel-body">
                <table class="table table-bordered table-hover toggle-circle" id="exampleFootableFiltering"
                  data-paging="true" data-filtering="true" data-sorting="true">
                  <thead>
                    <tr>
                      <th data-name="id" data-type="number" data-breakpoints="xs">ID</th>
                      <th data-name="firstName">First Name</th>
                      <th data-name="lastName">Last Name</th>
                      <th data-name="something" data-visible="false" data-filterable="false">Never seen but always around</th>
                      <th data-name="jobTitle" data-breakpoints="xs sm">Job Title</th>
                      <th data-name="started" data-type="date" data-breakpoints="xs sm md" data-format-string="MMM YYYY">Started On</th>
                      <th data-name="dob" data-type="date" data-breakpoints="xs sm md" data-format-string="DD MMM YYYY">Date of Birth</th>
                      <th data-name="status">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Shona</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Airline Transport Pilot</td>
                      <td>1367700388909</td>
                      <td>122365714987</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Lizzee</td>
                      <td>Goodlow</td>
                      <td>1381105566987</td>
                      <td>Technical Services Librarian</td>
                      <td>1382739570973</td>
                      <td>183768652128</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Isidra</td>
                      <td>Boudreaux</td>
                      <td>1381105566987</td>
                      <td>Traffic Court Referee</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Granville</td>
                      <td>Leonardo</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>5</td>
                      <td>Lauri</td>
                      <td>Hyland</td>
                      <td>1381105566987</td>
                      <td>Blackjack Supervisor</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>6</td>
                      <td>Easer</td>
                      <td>Dragoo</td>
                      <td>1381105566987</td>
                      <td>Drywall Stripper</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>7</td>
                      <td>Maple</td>
                      <td>Halladay</td>
                      <td>1381105566987</td>
                      <td>Aviation Tactical Readiness Officer</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>8</td>
                      <td>Maxine</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-dark">Disabled</span>
                      </td>
                    </tr>
                    <tr>
                      <td>9</td>
                      <td>Lorraine</td>
                      <td>Mcgaughy</td>
                      <td>1381105566987</td>
                      <td>Hemodialysis Technician</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>10</td>
                      <td>Judi</td>
                      <td>Badgett</td>
                      <td>1381105566987</td>
                      <td>Electrical Lineworker</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>11</td>
                      <td>Granville</td>
                      <td>Leonardo</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>12</td>
                      <td>Lauri</td>
                      <td>Hyland</td>
                      <td>1381105566987</td>
                      <td>Blackjack Supervisor</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>13</td>
                      <td>Easer</td>
                      <td>Dragoo</td>
                      <td>1381105566987</td>
                      <td>Drywall Stripper</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>14</td>
                      <td>Maple</td>
                      <td>Halladay</td>
                      <td>1381105566987</td>
                      <td>Aviation Tactical Readiness Officer</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>15</td>
                      <td>Maxine</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-dark">Disabled</span>
                      </td>
                    </tr>
                    <tr>
                      <td>16</td>
                      <td>Lorraine</td>
                      <td>Mcgaughy</td>
                      <td>1381105566987</td>
                      <td>Hemodialysis Technician</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>17</td>
                      <td>Judi</td>
                      <td>Badgett</td>
                      <td>1381105566987</td>
                      <td>Electrical Lineworker</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>18</td>
                      <td>Granville</td>
                      <td>Leonardo</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>19</td>
                      <td>Lauri</td>
                      <td>Hyland</td>
                      <td>1381105566987</td>
                      <td>Blackjack Supervisor</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>20</td>
                      <td>Easer</td>
                      <td>Dragoo</td>
                      <td>1381105566987</td>
                      <td>Drywall Stripper</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>21</td>
                      <td>Maple</td>
                      <td>Halladay</td>
                      <td>1381105566987</td>
                      <td>Aviation Tactical Readiness Officer</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>22</td>
                      <td>Maxine</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-dark">Disabled</span>
                      </td>
                    </tr>
                    <tr>
                      <td>23</td>
                      <td>Shona</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Airline Transport Pilot</td>
                      <td>1367700388909</td>
                      <td>122365714987</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>24</td>
                      <td>Lizzee</td>
                      <td>Goodlow</td>
                      <td>1381105566987</td>
                      <td>Technical Services Librarian</td>
                      <td>1382739570973</td>
                      <td>183768652128</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>25</td>
                      <td>Isidra</td>
                      <td>Boudreaux</td>
                      <td>1381105566987</td>
                      <td>Traffic Court Referee</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>26</td>
                      <td>Granville</td>
                      <td>Leonardo</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>27</td>
                      <td>Lauri</td>
                      <td>Hyland</td>
                      <td>1381105566987</td>
                      <td>Blackjack Supervisor</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>28</td>
                      <td>Easer</td>
                      <td>Dragoo</td>
                      <td>1381105566987</td>
                      <td>Drywall Stripper</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>29</td>
                      <td>Maple</td>
                      <td>Halladay</td>
                      <td>1381105566987</td>
                      <td>Aviation Tactical Readiness Officer</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>30</td>
                      <td>Maxine</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-dark">Disabled</span>
                      </td>
                    </tr>
                    <tr>
                      <td>31</td>
                      <td>Lorraine</td>
                      <td>Mcgaughy</td>
                      <td>1381105566987</td>
                      <td>Hemodialysis Technician</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>32</td>
                      <td>Judi</td>
                      <td>Badgett</td>
                      <td>1381105566987</td>
                      <td>Electrical Lineworker</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>33</td>
                      <td>Granville</td>
                      <td>Leonardo</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>34</td>
                      <td>Lauri</td>
                      <td>Hyland</td>
                      <td>1381105566987</td>
                      <td>Blackjack Supervisor</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>35</td>
                      <td>Easer</td>
                      <td>Dragoo</td>
                      <td>1381105566987</td>
                      <td>Drywall Stripper</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>36</td>
                      <td>Maple</td>
                      <td>Halladay</td>
                      <td>1381105566987</td>
                      <td>Aviation Tactical Readiness Officer</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>37</td>
                      <td>Maxine</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-dark">Disabled</span>
                      </td>
                    </tr>
                    <tr>
                      <td>38</td>
                      <td>Lorraine</td>
                      <td>Mcgaughy</td>
                      <td>1381105566987</td>
                      <td>Hemodialysis Technician</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>39</td>
                      <td>Judi</td>
                      <td>Badgett</td>
                      <td>1381105566987</td>
                      <td>Electrical Lineworker</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>40</td>
                      <td>Granville</td>
                      <td>Leonardo</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>41</td>
                      <td>Lauri</td>
                      <td>Hyland</td>
                      <td>1381105566987</td>
                      <td>Blackjack Supervisor</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>42</td>
                      <td>Easer</td>
                      <td>Dragoo</td>
                      <td>1381105566987</td>
                      <td>Drywall Stripper</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>43</td>
                      <td>Maple</td>
                      <td>Halladay</td>
                      <td>1381105566987</td>
                      <td>Aviation Tactical Readiness Officer</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>44</td>
                      <td>Maxine</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-dark">Disabled</span>
                      </td>
                    </tr>
                    <tr>
                      <td>45</td>
                      <td>Easer</td>
                      <td>Dragoo</td>
                      <td>1381105566987</td>
                      <td>Drywall Stripper</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>46</td>
                      <td>Maple</td>
                      <td>Halladay</td>
                      <td>1381105566987</td>
                      <td>Aviation Tactical Readiness Officer</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>47</td>
                      <td>Maxine</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-dark">Disabled</span>
                      </td>
                    </tr>
                    <tr>
                      <td>48</td>
                      <td>Lorraine</td>
                      <td>Mcgaughy</td>
                      <td>1381105566987</td>
                      <td>Hemodialysis Technician</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>49</td>
                      <td>Judi</td>
                      <td>Badgett</td>
                      <td>1381105566987</td>
                      <td>Electrical Lineworker</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>50</td>
                      <td>Granville</td>
                      <td>Leonardo</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>51</td>
                      <td>Shona</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Airline Transport Pilot</td>
                      <td>1367700388909</td>
                      <td>122365714987</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>52</td>
                      <td>Lizzee</td>
                      <td>Goodlow</td>
                      <td>1381105566987</td>
                      <td>Technical Services Librarian</td>
                      <td>1382739570973</td>
                      <td>183768652128</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>53</td>
                      <td>Isidra</td>
                      <td>Boudreaux</td>
                      <td>1381105566987</td>
                      <td>Traffic Court Referee</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>54</td>
                      <td>Granville</td>
                      <td>Leonardo</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>55</td>
                      <td>Lauri</td>
                      <td>Hyland</td>
                      <td>1381105566987</td>
                      <td>Blackjack Supervisor</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>56</td>
                      <td>Easer</td>
                      <td>Dragoo</td>
                      <td>1381105566987</td>
                      <td>Drywall Stripper</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>57</td>
                      <td>Maple</td>
                      <td>Halladay</td>
                      <td>1381105566987</td>
                      <td>Aviation Tactical Readiness Officer</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>58</td>
                      <td>Maxine</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-dark">Disabled</span>
                      </td>
                    </tr>
                    <tr>
                      <td>59</td>
                      <td>Lorraine</td>
                      <td>Mcgaughy</td>
                      <td>1381105566987</td>
                      <td>Hemodialysis Technician</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>60</td>
                      <td>Judi</td>
                      <td>Badgett</td>
                      <td>1381105566987</td>
                      <td>Electrical Lineworker</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>61</td>
                      <td>Granville</td>
                      <td>Leonardo</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>62</td>
                      <td>Lauri</td>
                      <td>Hyland</td>
                      <td>1381105566987</td>
                      <td>Blackjack Supervisor</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>63</td>
                      <td>Easer</td>
                      <td>Dragoo</td>
                      <td>1381105566987</td>
                      <td>Drywall Stripper</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>64</td>
                      <td>Maple</td>
                      <td>Halladay</td>
                      <td>1381105566987</td>
                      <td>Aviation Tactical Readiness Officer</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>65</td>
                      <td>Maxine</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-dark">Disabled</span>
                      </td>
                    </tr>
                    <tr>
                      <td>66</td>
                      <td>Lorraine</td>
                      <td>Mcgaughy</td>
                      <td>1381105566987</td>
                      <td>Hemodialysis Technician</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>67</td>
                      <td>Judi</td>
                      <td>Badgett</td>
                      <td>1381105566987</td>
                      <td>Electrical Lineworker</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>68</td>
                      <td>Granville</td>
                      <td>Leonardo</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>69</td>
                      <td>Lauri</td>
                      <td>Hyland</td>
                      <td>1381105566987</td>
                      <td>Blackjack Supervisor</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>70</td>
                      <td>Easer</td>
                      <td>Dragoo</td>
                      <td>1381105566987</td>
                      <td>Drywall Stripper</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>71</td>
                      <td>Maple</td>
                      <td>Halladay</td>
                      <td>1381105566987</td>
                      <td>Aviation Tactical Readiness Officer</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>72</td>
                      <td>Maxine</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-dark">Disabled</span>
                      </td>
                    </tr>
                    <tr>
                      <td>73</td>
                      <td>Shona</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Airline Transport Pilot</td>
                      <td>1367700388909</td>
                      <td>122365714987</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>74</td>
                      <td>Lizzee</td>
                      <td>Goodlow</td>
                      <td>1381105566987</td>
                      <td>Technical Services Librarian</td>
                      <td>1382739570973</td>
                      <td>183768652128</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>75</td>
                      <td>Isidra</td>
                      <td>Boudreaux</td>
                      <td>1381105566987</td>
                      <td>Traffic Court Referee</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>76</td>
                      <td>Granville</td>
                      <td>Leonardo</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>77</td>
                      <td>Lauri</td>
                      <td>Hyland</td>
                      <td>1381105566987</td>
                      <td>Blackjack Supervisor</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>78</td>
                      <td>Easer</td>
                      <td>Dragoo</td>
                      <td>1381105566987</td>
                      <td>Drywall Stripper</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>79</td>
                      <td>Maple</td>
                      <td>Halladay</td>
                      <td>1381105566987</td>
                      <td>Aviation Tactical Readiness Officer</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>80</td>
                      <td>Maxine</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-dark"></span>
                      </td>
                    </tr>
                    <tr>
                      <td>81</td>
                      <td>Lorraine</td>
                      <td>Mcgaughy</td>
                      <td>1381105566987</td>
                      <td>Hemodialysis Technician</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>82</td>
                      <td>Judi</td>
                      <td>Badgett</td>
                      <td>1381105566987</td>
                      <td>Electrical Lineworker</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>83</td>
                      <td>Granville</td>
                      <td>Leonardo</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>84</td>
                      <td>Lauri</td>
                      <td>Hyland</td>
                      <td>1381105566987</td>
                      <td>Blackjack Supervisor</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>85</td>
                      <td>Easer</td>
                      <td>Dragoo</td>
                      <td>1381105566987</td>
                      <td>Drywall Stripper</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>86</td>
                      <td>Maple</td>
                      <td>Halladay</td>
                      <td>1381105566987</td>
                      <td>Aviation Tactical Readiness Officer</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                    <tr>
                      <td>87</td>
                      <td>Maxine</td>
                      <td>Woldt</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-dark">Disabled</span>
                      </td>
                    </tr>
                    <tr>
                      <td>88</td>
                      <td>Lorraine</td>
                      <td>Mcgaughy</td>
                      <td>1381105566987</td>
                      <td>Hemodialysis Technician</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>89</td>
                      <td>Judi</td>
                      <td>Badgett</td>
                      <td>1381105566987</td>
                      <td>Electrical Lineworker</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-success">Active</span>
                      </td>
                    </tr>
                    <tr>
                      <td>90</td>
                      <td>Granville</td>
                      <td>Leonardo</td>
                      <td>1381105566987</td>
                      <td>Business Services Sales Representative</td>
                      <td>1265199486212</td>
                      <td>414197000409</td>
                      <td>
                        <span class="badge badge-table badge-danger">Suspended</span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>




    <!-- end table -->





    <!-- datepicker -->

                  <div class="example">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="icon wb-calendar" aria-hidden="true"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control" data-plugin="datepicker">
                    </div>
                  </div>

    <!-- end datepicker -->



<!--  	datetime picker -->


            <div class="example-wrap">
              <div class="example datepair-wrap" data-plugin="datepair">
                <div class="input-daterange-wrap">
                  <div class="input-daterange">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="icon wb-calendar" aria-hidden="true"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control datepair-date datepair-start" data-plugin="datepicker">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="icon wb-time" aria-hidden="true"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control datepair-time datepair-start" data-plugin="timepicker"
                      />
                    </div>
                  </div>
                </div>
                <div class="input-daterange-to">to</div>
                <div class="input-daterange-wrap">
                  <div class="input-daterange">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="icon wb-calendar" aria-hidden="true"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control datepair-date datepair-end" name="end" data-plugin="datepicker">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="icon wb-time" aria-hidden="true"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control datepair-time datepair-end" data-plugin="timepicker"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>

 	<!-- end datetime picker -->



 	<!-- color picker -->

                <div class="example-wrap">
                  <h4 class="example-title">Gradient Mode</h4>
                  <p>Add <code>data-mode="gradient"</code> ,then,you can generate
                    CSS3 gradient by the mode.</p>
                  <div class="example">
                    <input type="text" class="asColorpicker form-control" data-plugin="asColorPicker"
                      data-mode="gradient" value="#79d1c9" />
                  </div>
                </div>

 	<!-- end color picker -->



	<!-- radio -->

	                <div class="example-wrap">
                  <h4 class="example-title">Radios</h4>
                  <p>Add class <code>.radio-custom</code>to make it.</p>

                  <div class="radio-custom radio-primary">
                    <input type="radio" id="inputRadiosUnchecked" name="inputRadios" />
                    <label for="inputRadiosUnchecked">Unchecked</label>
                  </div>
                  <div class="radio-custom radio-primary">
                    <input type="radio" id="inputRadiosChecked" name="inputRadios" checked />
                    <label for="inputRadiosChecked">Checked</label>
                  </div>
                  <div class="radio-custom radio-primary">
                    <input type="radio" id="inputRadiosDisabled" name="inputRadiosDisabled" disabled
                    />
                    <label for="inputRadiosDisabled">Disabled Unchecked</label>
                  </div>
                  <div class="radio-custom radio-primary">
                    <input type="radio" id="inputRadiosDisabledChecked" name="inputRadiosDisabledChecked"
                      disabled checked />
                    <label for="inputRadiosDisabledChecked">Checked Disabled</label>
                  </div>
                </div>
	<!-- end radio --> 




	<!-- chexbox -->


                <div class="example-wrap">
                  <h4 class="example-title">Checkboxes</h4>
                  <p>Add class <code>.checkbox-custom</code>to make it.</p>
                  <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputUnchecked" />
                    <label for="inputUnchecked">Unchecked</label>
                  </div>
                  <div class="checkbox-custom checkbox-primary">
                    <input type="checkbox" id="inputChecked" checked />
                    <label for="inputChecked">Checked</label>
                  </div>
                  <div class="checkbox-custom">
                    <input type="checkbox" disabled />
                    <label>Disabled Unchecked</label>
                  </div>
                  <div class="checkbox-custom">
                    <input type="checkbox" disabled checked />
                    <label>Checked Disabled</label>
                  </div>
                </div>

    <!-- end checkbox -->



    <!-- switch -->


                <div class="example-wrap">
                  <h4 class="example-title">Size</h4>
                  <p>Add <code>data-size="large"</code> ,<code>data-size="small"</code>                    to change size.</p>
                  <ul class="list-unstyled list-inline example">
                    <li class="list-inline-item mr-25 mb-25">
                      <input type="checkbox" class="js-switch-large" data-plugin="switchery" data-size="large"
                        checked />
                    </li>
                    <li class="list-inline-item mr-25 mb-25">
                      <input type="checkbox" class="js-switch" data-plugin="switchery" checked />
                    </li>
                    <li class="list-inline-item mr-25 mb-25">
                      <input type="checkbox" class="js-switch-small" data-plugin="switchery" data-size="small"
                        checked />
                    </li>
                  </ul>
                </div>
    <!-- end switch -->


   <!-- input -->

            <form class="row">
              <div class="col-lg-6">
                <div class="row">
                  <label class="col-lg-12 col-md-3 form-control-label">To'g'ri matn</label>
                  <div class="col-lg-12 col-md-9 form-group">
                    <input type="text" class="form-control is-valid" id="inputValidation1" placeholder="Please ..."
                      value="Success" required>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="row">
                  <label class="col-lg-12 col-md-3 form-control-label">Xato matn!</label>
                  <div class="col-lg-12 col-md-9 form-group">
                    <input type="text" class="form-control is-invalid" id="inputValidation2" placeholder="To'ldiring ..."
                      required>
                    <div class="invalid-feedback">
                      Iltimos maydonni to'ldiring
                    </div>
                  </div>
                </div>
              </div>
            </form>

   <!-- end input -->






