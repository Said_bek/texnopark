<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Category;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Buyurtmalar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="constructor-index">

    <div class="modal fade" id="examplePositionCenter" aria-hidden="true" aria-labelledby="examplePositionCenter"
         role="dialog" tabindex="-1">
        <div class="modal-dialog modal-simple modal-center">
            <div class="modal-content">
                <div class="modal-header" style="font-size: 25px">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Marka qo'shish</h4>
                </div>
                <div class="modal-body">
                    <p class="text-left">Malumot yo'q!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Yopish</button>
                    <button type="button" class="btn btn-primary">Saqlash</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="text-left col-md-6">
            <h1 class="m-0"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>

    <table class='table table-bordered table-striped mt-20'>
        <thead>
        <tr>
            <th>#</th>
            <th>Xizmat kategoriyasi</th>
            <th>Buyurtma qabul qilingan sana</th>
            <th>Kompaniya nomi</th>
            <th>Buyurtma qayerdan</th>
            <th>Prioritet</th>
            <th>Buyurtma holati</th>
            <th>Spesifikatsiya holati</th>
            <?php
                if (Yii::$app->user->can('Admin') or Yii::$app->user->can('Head Constructor')){ ?>
                    <th>Keyingi bo'limga o'tkazish</th>
                <?php }
            ?>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
        if (isset($orders) and !empty($orders)){
            $i = 1;
            foreach ($orders as $key => $value) {
                echo "<tr>";
                echo "<td>".$i."</td>";
                echo "<td>".$value['category_name']."</td>";
                echo "<td>".date('Y-m-d H:i', strtotime($value['created_date']))."</td>";
                echo "<td>".$value['company_name']."</td>";
                if ($value['order_from'] == 0) {
                    echo '<td><span class = "badge badge-primary">IN</span></td>';
                }
                else{
                    echo '<td><span class = "badge badge-dark">OUT</span></td>';
                }
                if ($value['proirity'] == 0) {
                    echo '<td><span class = "badge badge-info">Oddiy</span></td>';
                }
                else if($value['proirity'] == 1){
                    echo '<td><span class = "badge badge-warning">Tezkor</span></td>';
                } else{
                    echo '<td><span class = "badge badge-primary">Kechiktirilgan</span></td>';
                }
                if($value['status'] == 0){
                    echo '<td><span class = "badge badge-danger">Faol emas</span></td>';
                }
                else{
                    echo '<td><span class = "badge badge-success">Faol</span></td>';
                }
                if($value['specification_status'] == 1){
                    echo '<td><span class = "badge badge-success">Faol</span></td>';
                }
                else{
                    echo '<td><span class = "badge badge-danger">Faol emas</span></td>';
                }
                $id = 0;
                if(isset($value['id']) && !empty($value['id'])){
                    $id = $value['id'];
                }
                if (Yii::$app->user->can('Admin') or Yii::$app->user->can('Head Constructor')){
                    echo "<td>
                        <a class='btn btn-icon btn-success next_section' data-target='#examplePositionCenter' data-toggle='modal' data-id=" . $id . " style='margin-left: 10px; cursor: pointer;'><i class='icon wb-arrow-up' style='color: white'></i></a>
                    </td>";
                }
                echo "<td>
                        <a href='/index.php/constructor/view?id=$id' title='View' aria-label='View'><span class='fa fa-eye'></span></a>
                    </td>";
                echo "</tr>";
                $i++;
            }
        } else {
            echo "<tr>";
            echo  "<td class='text-center' colspan='10'>Ma'lumot yo'q</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
</div>

<style>
    .constructor-index{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>

<?php
$js = <<<JS
$(document).on('click', '.next_section', function(){
    $('.modal-body').html('')
    $('.modal-footer .btn-primary').removeClass().addClass('btn btn-primary save_section')
    var id = $(this).attr('data-id')
    $.ajax({
        url: 'update-section',
        data: {
            s: false
        },
        dataType: 'json',
        type: 'get',
        success: function(response) {
            if (response.status == 'success') {
                $('.modal-body').html(response.content)
                $('.modal-header').html("Keyingi bo'limga o'tkazish")
                $('.modal-footer .btn-primary').addClass('save_section')
                $('.modal-footer .btn-primary').attr('data-id', id)
            }
        }
    })
})

$(document).on('click', '.save_section', function(){
    var id = $(this).attr('data-id')
    var module_id = $('input[name="options"]:checked').val();
    $.ajax({
        url: 'update-section',
        data: {
            s: true,
            id: id,
            module_id: module_id
        },
        dataType: 'json',
        type: 'get',
        success: function(response) {
            if (response.status == 'success') {
                location.reload()
            }
            if (response.status == 'error_mark') {
                $('#mark').addClass('is-invalid');
                $('.name_error').removeClass('hidden')  
            }
        }
    })
})

JS;
$this->registerJs($js);
?>