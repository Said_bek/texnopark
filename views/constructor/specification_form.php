<?php
namespace app\controllers;
use app\models\Constructor;
use Yii;
?>

<div class="text-left">
    <label for="mySelect2" style="font-size: 15px">Buyurtma nomini kriting</label>
    <input type="text" id="select2" class="form-control" name="">
    <small class="name_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
</div>

<div class="select2-primary text-left">
    <label for="mySelect2" style="font-size: 15px">Konstruktor tanlang</label>
    <select class="form-control mySelect2" id="select3" data-plugin="select2">
        <?php
        if (isset($model) and !empty($model)){
            foreach ($model as $key => $value){ ?>
                <option value="<?php echo $value['id'] ?>">
                    <?php echo $value['username']; ?>
                </option>
            <?php }
        }
        ?>
    </select>
    <small class="constructor_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
</div>

<div class="radio-custom radio-primary row d-flex text-left">
    <div class="col-md-4">
        <input type="radio" value="0" id="inputRadiosSimple" name="inputRadios" checked />
        <label for="inputRadiosChecked" style="font-size: 15px">Oddiy</label>
    </div>
    <div class="col-md-4">
        <input type="radio" value="1" id="inputRadiosNorm" name="inputRadios" />
        <label for="inputRadiosUnchecked" style="font-size: 15px">O'rtacha</label>
    </div>
    <div class="col-md-4">
        <input type="radio" value="2" id="inputRadiosFast" name="inputRadios" />
        <label for="inputRadiosUnchecked" style="font-size: 15px">Tezkor</label>
    </div>
</div>

<div class="dead_line text-left">
    <div class="example-wrap">
        <div class="example datepair-wrap" data-plugin="datepair">
            <div class="input-daterange-wrap">
                <div class="input-daterange">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                          <i class="icon wb-calendar" aria-hidden="true"></i>
                        </span>
                        </div>
                        <input type="text" data-date-format='dd.mm.yyyy' autocomplete="off" id="datepicker" class="datepicker price_date form-control datepair-date datepair-start" data-plugin="datepicker">
                        <div>
                            <small class="date_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                            <small class="small_date hidden text-danger" style="font-size: 12px">Sana notog'ri kiritildi!</small>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="icon wb-time" aria-hidden="true"></i>
                            </span>
                        </div>
                        <input type="text" class="timepicker form-control" autocomplete="off" id="timepicker" data-plugin="clockpicker" data-autoclose="true">
                        <div>
                            <small class="time_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                            <small class="small_time hidden text-danger" style="font-size: 12px">Sana notog'ri kiritildi!</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .hidden {
        display: none;
    }
    .popover {
        z-index: 9999 !important;
    }
</style>
<script>
    $("#select3").select2({
        tags: true
    });
    $(function (){
        $("#datepicker").datepicker();
    })
    $(function (){
        $("#timepicker").clockpicker();
    })

    $('#datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        language: 'en',
        orientation: 'bottom',
    }).on('hide', function(e) {
        e.stopPropagation();
    });
</script>



