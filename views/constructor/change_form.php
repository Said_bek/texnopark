<?php
namespace app\controllers;
use app\models\Marks;
use Yii;
?>

<?php
    if (isset($selectMarks) and !empty($selectMarks)){
        foreach ($selectMarks as $key => $value){
            $selectMark = Marks::find()->where(['id' => $value->mark_id])->one();
            ?> <option value="<?php echo $selectMark->id; ?>">
                <?php echo $selectMark->name; ?>
            </option>
            <?php
        }
    } else {
        ?> <option disabled selected>Bu material uchun marka mavjud emas!</option> <?php
    }
?>


<?php

$js = <<<JS
    $("#select4").select2({
        tags: true
    });
JS;

$this->registerJs($js);
?>
