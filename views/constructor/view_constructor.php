<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
    <div class="spesifikatsiya-index">
        <div class="row">
            <div class="text-left col-md-6">
                <h2 class="m-0">Spesifikatsiya</h2>
            </div>
            <?php
            if (Yii::$app->user->can('Admin') or Yii::$app->user->can('Head Constructor')) { ?>
                <div class="text-right col-md-6">
                    <button class="btn btn-success float-right" id="addSpecification" data-target="#examplePositionCenter" data-toggle="modal" type="button" data-id="<?= $id ?>">Spesifikatsiya yasash</button>
                </div>
            <?php } ?>
        </div>

        <table class='table table-bordered table-striped mt-20'>
            <thead>
            <tr>
                <th>#</th>
                <th>Buyurtma nomi</th>
                <th>Buyurtma qabul qilingan sana</th>
                <th>Buyurtma bitish sanasi</th>
                <th>Konstruktor</th>
                <th>Holati</th>
                <th>Rasm</th>
                <th>Tasdiqlash</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (isset($model) and !empty($model)) {
                $i = 1;
                foreach ($model as $key => $value) {
                    echo "<tr>";
                    echo "<td>" . $i . "</td>";
                    echo "<td>" . $value['name'] . "</td>";
                    echo "<td>" . date('Y-m-d H:i', strtotime($value['created_date'])) . "</td>";
                    echo "<td>" . date('Y-m-d H:i', strtotime($value['deadline'])) . "</td>";
                    echo "<td><span class='badge badge-primary'>" . $value['username'] . "</span></td>";
                    if ($value['class'] == 0) {
                        echo "<td><span class='badge badge-info'>Oddiy</span></td>";
                    } else if ($value['class'] == 1) {
                        echo "<td><span class='badge badge-warning'>O'rtacha</span></td>";
                    } else if ($value['class'] == 2) {
                        echo "<td><span class='badge badge-danger'>Tezkor</span></td>";
                    }
                    $id = 0;
                    if (isset($value['id']) && !empty($value['id'])) {
                        $id = $value['id'];
                    }
                    echo "<td>";
                    ?>
                    <div class="d-flex">
                        <div>
                            <?php $img = \app\models\Specification::find()->where(['id' => $id])->one() ?>
                            <?php $form = ActiveForm::begin(['enableClientScript' => false, 'action' => '/constructor/image?id=' . $id]) ?>
                        </div>

                        <div class="form-group d-flex btn-block file-input btn btn-danger mr-10">
                            <i class="fa fa-image upload_image"></i> <?= $form->field($img, 'image')->fileInput(['maxlength' => true])->label(false) ?>
                        </div>

                        <div>
                            <?= Html::submitButton('<i class="fa fa-upload save_image"></i>', ['class' => 'btn btn-direction btn-left btn-dark', 'img-id' => $id]) ?>
                        </div>
                    </div>

                    <?php ActiveForm::end() ?>

                    <?php
                    echo "</td>";
                    if ($value['active'] == 1){
                        $checked = "checked=''";
                        $class_js = "active_js";
                    } else {
                        $checked = '';
                        $class_js = '';
                    }
                    echo '<td style="text-align: center">
                            <input type="checkbox" data-id="'.$id.'" data-order-id="'.$value["order_id"].'" class="js-switch-small radio_js '.$class_js.'" '.$checked.' id="bbb" data-plugin="switchery" data-size="small" data-switchery="true" style="display: none;">
                         </td>';
                    echo "<td>
                        <a href='/index.php/constructor/view-specification?id=$id' title='View' aria-label='View'><span class='fa fa-eye'></span></a>
                        <a href='#' class='ml-3 update' data-target='#examplePositionCenter' data-toggle='modal' title='Update' data-update='$id' aria-label='Update'><i class='fa fa-pencil' style='color: #3e8ef7'></i></a>
                        </td>";
                    echo "</tr>";
                    $i++;
                }
            } else {
                echo "<tr>";
                echo "<td class='text-center' colspan='8'>Ma'lumot yo'q</td>";
                echo "</tr>";
            }
            ?>
            </tbody>
        </table>
    </div>

    <style>
        .spesifikatsiya-index {
            background-color: white;
            padding: 30px;
            -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
            -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
            box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        }

        .file-input {
            position: relative;
            overflow: hidden;
            width: 50px;
            height: 35px;
        }

        .file-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            font-size: 999px;
            text-align: right;
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }

        .save_image {
            width: 15px;
            height: 5px;
        }
    </style>

<?php
$js = <<<JS

    $(document).on('click', '#addSpecification', function(){
        $.ajax({
            url: 'add-constructor',
            data: {
                s: false,
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("Konstruktor qo'shish")
                    $('.modal-footer .btn-primary').addClass('save_constructor')
                }
            }
        })
    })
    
    $(document).on('click', '.save_constructor', function(){
        var name = $('#select2').val()
        var constructor = $('#select3').val()
        var date = $('#datepicker').val()
        var time = $('#timepicker').val()
        var id = $("#addSpecification").attr('data-id')
        var radio = $('input[name="inputRadios"]:checked').val();
        if (name != '' && constructor != '' && date != '' && time != ''){
            $.ajax({
                url: 'add-constructor',
                data: {
                    s: true,
                    name: name,
                    constructor: constructor,
                    date: date,
                    time: time,
                    radio: radio,
                    id: id
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    } 
                    if (response.status == 'error_name') {
                        $('#select2').addClass('is-invalid');
                        $('.name_error').removeClass('hidden')
                    } 
                    if (response.status == 'error_constructor') {
                        $('#select3').addClass('is-invalid');
                        $('.constructor_error').removeClass('hidden') 
                    } 
                    if (response.status == 'error_date_time') {
                        $('#datepicker').addClass('is-invalid');
                        $('.small_date').removeClass('hidden') 
                        
                        $('#timepicker').addClass('is-invalid');
                        $('.small_time').removeClass('hidden') 
                    } 
                    if (response.status == 'error') {
                        alert("Ma'lumot saqlanmadi!")
                    } 
                }
            })
        } else {
            if (name == ''){
                $('#select2').addClass('is-invalid');
                $('.name_error').removeClass('hidden')      
            } else {
                $('#select2').removeClass('is-invalid');
                $('.name_error').addClass('hidden')      
            }
            if (constructor == ''){
                $('#select3').addClass('is-invalid');
                $('.constructor_error').removeClass('hidden')      
            } else {
                $('#select3').removeClass('is-invalid');
                $('.constructor_error').addClass('hidden')     
            }
            if (date == ''){
                $('#datepicker').addClass('is-invalid');
                $('.date_error').removeClass('hidden')    
            } else {
                $('#datepicker').removeClass('is-invalid');
                $('.date_error').addClass('hidden')   
            }
            if (time == ''){
                $('#timepicker').addClass('is-invalid');
                $('.time_error').removeClass('hidden')    
            } else {
                $('#timepicker').removeClass('is-invalid');
                $('.time_error').addClass('hidden')   
            }
        }
    })
    
    $(document).on('click', '.update', function(){
        var id = $(this).attr('data-update')
        $.ajax({
            url: 'update-constructor',
            data: {
                update: false,
                id: id,
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("Spesifikatsiya o'zgartirish")
                    $('.modal-footer .btn-primary').addClass('update_constructor')
                    $('.modal-footer .btn-primary').attr('data-update',id)
                }
            }
        })
    })
    
    $(document).on('click', '.update_constructor', function(){
        var name = $('#select2').val()
        var radio = $('input[name="inputRadios"]:checked').val();
        var date = $('#datepicker').val()
        var time = $('#timepicker').val()
        var id = $(this).attr('data-update')
        if (name != '' && date != '' && time != ''){
            $.ajax({
                url: 'update-constructor',
                data: {
                    update: true,
                    name: name,
                    date: date,
                    time: time,
                    radio: radio,
                    id: id
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    } 
                    if (response.status == 'error_name') {
                        $('#select2').addClass('is-invalid');
                        $('.name_error').removeClass('hidden')
                    } 
                    if (response.status == 'error_date_time') {
                        $('#datepicker').addClass('is-invalid');
                        $('.small_date').removeClass('hidden') 
                        
                        $('#timepicker').addClass('is-invalid');
                        $('.small_time').removeClass('hidden') 
                    } 
                    if (response.status == 'error') {
                        alert("Ma'lumot saqlanmadi!")
                    } 
                }
            })
        } else {
            if (name == ''){
                $('#select2').addClass('is-invalid');
                $('.name_error').removeClass('hidden')      
            } else {
                $('#select2').removeClass('is-invalid');
                $('.name_error').addClass('hidden')      
            }
            if (date == ''){
                $('#datepicker').addClass('is-invalid');
                $('.date_error').removeClass('hidden')    
            } else {
                $('#datepicker').removeClass('is-invalid');
                $('.date_error').addClass('hidden')   
            }
            if (time == ''){
                $('#timepicker').addClass('is-invalid');
                $('.time_error').removeClass('hidden')    
            } else {
                $('#timepicker').removeClass('is-invalid');
                $('.time_error').addClass('hidden')   
            }
        }
    })

    $(document).on('change', '.radio_js', function(){
        var _this = $(this)
        var id = $(_this).attr('data-id')
        var order_id = $(_this).attr('data-order-id')
        $.ajax({
            url: 'update-specification-status',
            data: {
                id: id,
                order_id: order_id
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    if ($(_this).is(":checked")){
                        $('.active_js').trigger('click').removeClass('active_js')
                        $(_this).addClass('active_js')
                    } else {
                        $(_this).removeClass('active_js')
                    }
                }
            }
        })
    });
    
JS;
$this->registerJs($js);
?>