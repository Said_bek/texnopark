<hr class="hr">
<div class="row mt-10 material_marks">
    <div class="col-lg-2">
        <label for="mySelect2" style="font-size: 15px">Kod kiriting</label>
        <input type="text" id="code_input" class="form-control code_material" name="selectForm[<?= $count ?>][code]">
    </div>
    <div class="col-lg-3 material">
        <div class="select2-primary">
            <label for="mySelect2" style="font-size: 15px">Material tanlang</label>
            <select <?php if(!(isset($materials) and !empty($materials))) echo"disabled"; ?> id="select4" class="form-control myMaterial" name="selectForm[<?= $count ?>][select4]" data-plugin="select2">
                <?php if (isset($materials) and !empty($materials)): ?>
                    <option value="" disabled selected>Material tanlang</option>
                    <?php foreach ($materials as $value): ?>
                        <option value="<?= $value->id ?>">
                            <?= $value->name ?>
                        </option>
                    <?php endforeach; ?>
                <?php else: ?>
                    <option selected value="">Material mavjud emas</option>
                <?php endif; ?>
            </select>
        </div>
    </div>
    <div class="col-lg-3 mark_material">
        <div class="select3-primary">
            <label for="mySelect2" style="font-size: 15px">Marka tanlang</label>
            <select disabled class="form-control mySelect5" id="select5" name="selectForm[<?= $count ?>][select5]" data-plugin="select2">
            </select>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="mySelect2" style="font-size: 15px">Sonini kiriting</label>
        <input type="number" id="count_input" class="form-control count_material" name="selectForm[<?= $count ?>][count]">
    </div>
    <div class="col-lg-2">
        <label for="mySelect2" style="font-size: 15px">Terma kiriting</label>
        <input type="text" id="terma_input" class="form-control terma_material" name="selectForm[<?= $count ?>][terma]">
    </div>
    <div class="col-lg-12">
        <div class="row count_inputs">
            <div class="col-lg-4">
                <label for="mySelect2" style="font-size: 15px; margin-top: 10px">Kenglik</label>
                <div class="width_input d-flex">
                    <input type="number" id="width_input" class="form-control" name="selectForm[<?= $count ?>][countWidth]" style="width: 500px !important">
                    <div class="disable_input">
                        <input disabled type="text" id="count_input" class="form-control" name="count" style="width: 60px !important; margin-left: 10px">
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <label for="mySelect2" style="font-size: 15px; margin-top: 10px">Uzunlik</label>
                <div class="height_input d-flex">
                    <input type="number" id="height_input" class="form-control" name="selectForm[<?= $count ?>][countHeight]" style="width: 500px !important">
                    <div class="disable_input">
                        <input disabled type="text" id="count_input" class="form-control" name="count" style="width: 60px !important; margin-left: 10px">
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <label for="mySelect2" style="font-size: 15px; margin-top: 10px">Qalinlik</label>
                <div class="weight_input d-flex">
                    <input type="number" id="weight_input" class="form-control" name="selectForm[<?= $count ?>][countWeight]" style="width: 500px !important">
                    <div class="disable_input">
                        <input disabled type="text" id="count_input" class="form-control" name="count" style="width: 60px !important; margin-left: 10px">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('select').select2()
</script>