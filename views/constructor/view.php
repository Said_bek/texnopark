<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Materials;
use app\models\Marks;
use app\models\Orders;

//require(Yii::getAlias('@vendor')."/phpexcel/PHPExcel.php");

/* @var $this yii\web\View */
/* @var $model app\models\Constructor */
?>
<div class="constructor-view">
    <div class="row">
        <div class="col-md-6 p-0">
            <?php
                if (isset($code['code']['code']) and !empty($code['code']['code'])){
                    $orderCode = "<span class='badge badge-default'>".$code['code']['code']."</span>";
                } else {
                    $orderCode = "<span class='badge badge-danger'>Kod mavjud emas</span>";
                }
                echo "<h5 class='m-0' style='font-family: Arial;'>Buyurtma kodi: ".$orderCode."</h5>";
            ?>
            <?php
                if (isset($code['code']['category']['title']) and !empty($code['code']['category']['title'])){
                    $orderCategory = "<span class='badge badge-default'>".$code['code']['category']['title']."</span>";
                } else {
                    $orderCategory = "<span class='badge badge-danger'>Kategoriya mavjud emas</span>";
                }
            echo "<h5 class='m-0' style='font-family: Arial;'>Buyurtma kategoriyasi: ".$orderCategory."</h5>";
            ?>
        </div>
        <div class="col-md-6 text-right p-0">
            <?php
                if (isset($model) and !empty($model)){ ?>
                    <a href='/constructor/excel?id=<?= $id ?>' class="btn btn-outline-success btn-sm">
                        Excel <i class="fa fa-file-excel-o"></i>
                    </a>
                <?php } else { ?>
                    <a href='#' class="btn btn-outline-success btn-sm">
                        Excel <i class="fa fa-file-excel-o"></i>
                    </a>
                <?php }
            ?>
        </div>
    </div>

    <div class="row mt-10" id="w0">
        <div class="summary"></div>
        <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th style="padding: 20px; text-align: center" rowspan="2">#</th>
                <th style="padding: 20px; text-align: center" rowspan="2">Material nomi</th>
                <th style="padding: 20px; text-align: center" rowspan="2">Marka nomi</th>
                <th colspan="6" style="text-align: center">Detal parametrlari</th>
                <th colspan="6" style="text-align: center; background: #f2f2f2">Kesish o'lchamlari</th>
                <th style="padding: 20px; text-align: center" rowspan="2">Soni</th>
                <th style="padding: 20px; text-align: center" rowspan="2">Terma</th>
                <th style="padding: 20px; text-align: center" rowspan="2">Ves</th>
                <th rowspan="2"></th>
            </tr>
            <tr>
                <th>Shirina</th>
                <th>Dopusk</th>
                <th>Dlina</th>
                <th>Dopusk</th>
                <th>Tolshina</th>
                <th>Dopusk</th>
                <th style="background: #f2f2f2">Shirina</th>
                <th style="background: #f2f2f2">Dopusk</th>
                <th style="background: #f2f2f2">Dlina</th>
                <th style="background: #f2f2f2">Dopusk</th>
                <th style="background: #f2f2f2">Tolshina</th>
                <th style="background: #f2f2f2">Dopusk</th>
            </tr>
            </thead>
            <tbody class="text-center">
            <?php
            if (isset($model) and !empty($model)){
                $i = 1;
                foreach ($model as $key => $value) {
                    echo "<tr>";
                    echo "<td>".$i."</td>";
                    if (isset($value->material) and !empty($value->material)){
                        echo "<td>".$value->material->name."</td>";
                    } else {
                        echo "<td><span class='badge badge-danger'>Material mavjud emas</span></td>";
                    }
                    if (isset($value->mark) and !empty($value->mark)){
                        echo "<td>".$value->mark->name."</td>";
                    } else {
                        echo "<td><span class='badge badge-danger'>Marka mavjud emas</span></td>";
                    }
                    echo "<td>".$value->width."</td>";
                    echo "<td><span class='badge badge-lg badge-default'>± ".$value->w_approx."</span></td>";
                    echo "<td>".$value->height."</td>";
                    echo "<td><span class='badge badge-lg badge-default'>± ".$value->h_approx."</span></td>";
                    echo "<td>".$value->weight."</td>";
                    echo "<td><span class='badge badge-lg badge-default'>± ".$value->we_approx."</span></td>";
                    $all_width = $value->width + 1;
                    $all_height = $value->height + 1;
                    echo "<td style='background: #f2f2f2'><b>".$all_width."</b></td>";
                    echo "<td style='background: #f2f2f2'><span class='badge badge-lg badge-info'>± 10</span></td>";
                    echo "<td style='background: #f2f2f2'><b>".$all_height."</b></td>";
                    echo "<td style='background: #f2f2f2'><span class='badge badge-lg badge-info'>± 10</span></td>";
                    echo "<td style='background: #f2f2f2'><b>".$value->weight."</b></td>";
                    echo "<td style='background: #f2f2f2'><span class='badge badge-lg badge-info'>± 10</span></td>";
                    echo "<td>".$value->count."</td>";
                    echo "<td>".$value->terma."</td>";
                    echo "<td>".round((((($all_width + 10)*($all_height + 10)*($value->weight + 10))*0.785)/100000) * $value->count, 2)."</td>";
                    $specification_id = 0;
                    if(isset($value->id) && !empty($value->id)){
                        $specification_id = $value->id;
                    }
                    echo "<td>
                            <a href='/index.php/constructor/update-settings?id=$specification_id&specification_id=$id' class='ml-3 update' title='Update' aria-label='Update'><i class='fa fa-pencil' style='color: #3e8ef7'></i></a>
                            <a href='#' class='delete' title='Delete' data-delete='$specification_id'><span class='fa fa-trash'></span></a>
                        </td>";
                    echo "</tr>";
                    $i++;
                }
            } else {
                echo "<tr>";
                echo  "<td class='text-center' colspan='19'>Ma'lumot yo'q</td>";
                echo "</tr>";
            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="row float-right mt-10">
        <button type="button" class="btn btn-danger minus"><i class="fa fa-minus" aria-hidden="true"></i></button>
        <button type="button" class="btn btn-success ml-5 plus"><i class="fa fa-plus" aria-hidden="true"></i></button>
    </div>
    <input type="hidden" id="count" value="1">
    <form class="div_form" action="#">
        <div class="row mt-50 material_marks">
            <div class="col-lg-2">
                <label for="mySelect2" style="font-size: 15px">Kod kiriting</label>
                <input type="text" id="code_input" class="form-control code_material" name="selectForm[0][code]">
            </div>
            <div class="col-lg-3 material">
                <div class="select2-primary">
                    <label for="mySelect2" style="font-size: 15px">Material tanlang</label>
                    <select <?php if(!(isset($materials) and !empty($materials))) echo"disabled"; ?> class="form-control myMaterial" style="width: 100%;" name="selectForm[0][select4]" id="select4" data-plugin="select2">
                        <?php if (isset($materials) and !empty($materials)){ ?>
                            <option value="" disabled selected>Material tanlang</option>
                           <?php foreach ($materials as $value){ ?>
                                <option value="<?= $value->id ?>">
                                    <?= $value->name ?>
                                </option>
                            <?php }
                        } else { ?>
                            <option selected value="">Material mavjud emas</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 mark_material">
                <div class="select3-primary">
                    <label for="mySelect2" style="font-size: 15px">Marka tanlang</label>
                    <select disabled class="form-control mySelect5" id="select5" name="selectForm[0][select5]" data-plugin="select2">
                    </select>
                </div>
            </div>
            <div class="col-lg-2">
                <label for="mySelect2" style="font-size: 15px">Sonini kiriting</label>
                <input type="number" id="count_input" class="form-control count_material" name="selectForm[0][count]">
            </div>
            <div class="col-lg-2">
                <label for="mySelect2" style="font-size: 15px">Terma kiriting</label>
                <input type="text" id="terma_input" class="form-control terma_material" name="selectForm[0][terma]">
            </div>
            <div class="col-lg-12">
                <div class="row count_inputs">
                    <div class="col-lg-4">
                        <label for="mySelect2" style="font-size: 15px; margin-top: 10px">Kenglik</label>
                        <div class="width_input d-flex">
                            <input type="number" id="width_input" class="form-control" name="selectForm[0][countWidth]" style="width: 500px !important">
                            <div class="disable_input">
                                <input disabled type="text" id="count_input" class="form-control" name="count" style="width: 60px !important; margin-left: 10px">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <label for="mySelect2" style="font-size: 15px; margin-top: 10px">Uzunlik</label>
                        <div class="height_input d-flex">
                            <input type="number" id="height_input" class="form-control" name="selectForm[0][countHeight]" style="width: 500px !important">
                            <div class="disable_input">
                                <input disabled type="text" id="count_input" class="form-control" name="count" style="width: 60px !important; margin-left: 10px">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <label for="mySelect2" style="font-size: 15px; margin-top: 10px">Qalinlik</label>
                        <div class="weight_input d-flex">
                            <input type="number" id="weight_input" class="form-control" name="selectForm[0][countWeight]" style="width: 500px !important">
                            <div class="disable_input">
                                <input disabled type="text" id="count_input" class="form-control" name="count" style="width: 60px !important; margin-left: 10px">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="row">
        <div class="col-lg-12 mt-5">
            <button type="button" class="btn btn-success float-right mt-10 save_count" data-id="<?= $id ?>">Saqlash</button>
        </div>
    </div>
</div>

<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
    .constructor-view {
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        overflow: auto;
    }
    .hidden {
        display: none;
    }
    .count_inputs i {
        position: absolute;
    }
    .icon_specification {
        padding: 10px;
        min-width: 40px;
        margin-left: 23px;
        color: red !important;
    }
    .input-field {
        width: 100%;
        padding: 10px;
        text-align: center;
    }
</style>

<?php

$js = <<<JS
    $('.myMaterial').select2()
    
    $(document).ready(function(){
        setTimeout(function(){
            $("#toggleMenubar > .nav-link").trigger("click");
        }, 500);
    })
    
    $(document).on('change', '.myMaterial', function(){
        var material = $(this).val()
        var _this = $(this)
        $.ajax({
            url: 'change-material',
            data: {
                material: material
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $(_this).parent().parent().siblings('.mark_material').find('.select3-primary').find('#select5').prop("disabled", false);
                    $(_this).parent().parent().siblings('.col-lg-3').find('.select3-primary').find('#select5').html(response.content)
                }
            }
        })
    })
    
    $(document).on('click', '.plus', function(){
        var count = $('#count').val()
        $(this).attr('disabled', true)
        var _this = $(this)
        $.ajax({
            url: 'plus-input?count='+count,
            dataType: 'json',
            type: 'get',
            success: function(response) {
                $(_this).attr('disabled', false)
                if (response.status == 'success') {
                    $('#count').val(parseInt(count)+1)
                    $('.div_form').append(response.content);
                }
            }
        })
    })
    
    $(document).on('click', '.minus', function(){
        if ($('.material_marks').length > 1){
            $('.material_marks').last().remove();
            $('.hr').last().remove();
        }
    })
    
    function validateMaterial(){
        inp = $('.myMaterial')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                    return false;
                }
            }
        return true;
    }
    
    function validateCount(){
        inp = $('.count_material')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                    return false;
                }
            }
        return true;
    }
    
    function validateMark(){
        inp = $('.mySelect5')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                    return false;
                }
            }
        return true;
    }
    
    $(document).on("click", '.save_count', function() {
        if (validateMaterial()){
            if (validateMark()){
                if (validateCount()){
                    var form = $(".div_form");
                    var id = $(this).attr('data-id');
                    var csrf = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        url: 'save-count',
                        data: form.serialize() + '&id='+id + '&_csrf=' + csrf,
                        type: 'POST',
                        dataType: 'json',
                        success: function(response) {
                            if (response.status == "success") {
                                location.reload()   
                            }
                            if (response.status == "error_count"){
                                alert('Miqdor kiritmadingiz!')
                            }
                            if (response.status == "count_null"){
                                alert("Miqdor 0 ga teng bo'lishi mumkin emas!")
                            }
                            if (response.status == "error_width" || response.status == "fail_width"){
                                alert('Kenglikda bunday oraliq mavjud emas!')
                            }
                            if (response.status == "error_height" ||  response.status == "fail_height"){
                                alert('Uzunlikda bunday oraliq mavjud emas!')
                            }
                            if (response.status == "error_weight" || response.status == "fail_weight"){
                                alert('Qalinlikda bunday oraliq mavjud emas!')
                            }
                        }
                    })
                } else {
                    alert('Miqdor kiritmadingiz!')
                }
            } else {
                alert('Marka tanlamadingiz!')   
            }
        } else {
            alert('Material tanlamadingiz!')
        }
    });

    $(document).on('keyup change', '#width_input', function(){
        var width = $(this).val()
        var material = $(this).parent().parent().parent().parent().siblings('.material').find('.select2-primary').find('#select4').val()
        var _this = $(this)
        $.ajax({
            url: 'interval',
            data: {
                type: 'w',
                width: width,
                material: material
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $(_this).siblings('.disable_input').html(response.content)
                }
            }
        })
    })
    
    $(document).on('keyup change', '#height_input', function(){
        var width = $(this).val()
        var material = $(this).parent().parent().parent().parent().siblings('.material').find('.select2-primary').find('#select4').val()
        var _this = $(this)
        $.ajax({
            url: 'interval',
            data: {
                type: 'h',
                width: width,
                material: material
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $(_this).siblings('.disable_input').html(response.content)
                }
            }
        })
    })
    
    $(document).on('keyup change', '#weight_input', function(){
        var width = $(this).val()
        var material = $(this).parent().parent().parent().parent().siblings('.material').find('.select2-primary').find('#select4').val()
        var _this = $(this)
        $.ajax({
            url: 'interval',
            data: {
                type: 'g',
                width: width,
                material: material
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $(_this).siblings('.disable_input').html(response.content)
                }
            }
        })
    })
    
    $(document).on('click', '.delete', function(){
        if (confirm("O'chirishni istaysizmi?")) {
            var id = $(this).attr('data-delete')
            $.ajax({
                url: 'delete-specification',
                data: {
                    id: id
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                }
            })
        }
    })
    
    $(document).on('click', '.update', function(){
        var id = $(this).attr('data-update')
        $.ajax({
            url: 'update-settings',
            data: {
                update: false,
                id: id
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("O'zgartirish")
                    $('.modal-footer .btn-primary').addClass('update_settings')
                    $('.modal-footer .btn-primary').attr('data-update',id)
                }
            }
        })
    })
JS;
$this->registerJs($js);
?>