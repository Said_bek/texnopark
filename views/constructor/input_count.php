<?php if ((isset($interval) and !empty($interval)) and (isset($type) and !empty($type))) : ?>
    <?php if ($type == 'width') : ?>
        <input disabled value="± <?php echo $interval->w_approx ?>" type="text" id="23" class="form-control text-center"
               name="count" style="width: 60px !important; margin-left: 10px">
    <?php endif ?>
    <?php if ($type == 'height') : ?>
        <input disabled value="± <?php echo $interval->h_approx ?>" type="text" id="ww" class="form-control text-center"
               name="count" style="width: 60px !important; margin-left: 10px">
    <?php endif ?>
    <?php if ($type == 'weight') : ?>
        <input disabled value="± <?php echo $interval->we_approx ?>" type="text" id="12"
               class="form-control text-center" name="count" style="width: 60px !important; margin-left: 10px">
    <?php endif ?>
<?php else: ?>
    <i class="fa fa-times icon_specification"></i>
    <input disabled type="text" id="count_input" class="form-control input-field"
           style="width: 60px !important; margin-left: 10px">
<?php endif ?>