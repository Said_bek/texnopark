<?php
namespace app\controllers;
use app\models\Marks;
use app\models\MaterialMarks;
use yii\helpers\Html;
use Yii;
use app\models\Materials;
?>
<div class="row mt-10 material_marks">
    <div class="col-lg-4 material">
        <div class="select2-primary">
            <label for="mySelect2" style="font-size: 15px">Material tanlang</label>
            <select class="form-control mySelect4" id="select4" data-plugin="select2">
                <?php $selectMaterials = Materials::find()->where(['status' => 1])->all(); ?>
                <?php foreach ($selectMaterials as $key => $value): ?>
                    <option <?php if($model->material_id == $value->id) echo"selected";?> value="<?php echo $value->id; ?>">
                        <?php echo $value->name; ?>
                    </option>
                <?php endforeach ?>
            </select>
            <small class="material_empty hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
        </div>
    </div>
    <div class="col-lg-4 mark_material">
        <div class="select3-primary">
            <label for="mySelect2" style="font-size: 15px">Marka tanlang</label>
            <select class="form-control mySelect5" id="select5" name="selectForm[0][select5]" data-plugin="select2">
            <?php
                $selectMark = MaterialMarks::find()->where(['material_id' => $model->material_id])->all();
                if (isset($selectMark) and !empty($selectMark)){
                    foreach ($selectMark as $key => $value){
                        $selectMark = Marks::find()->where(['id' => $value->mark_id])->one();
                        ?> <option value="<?php echo $selectMark->id; ?>">
                            <?php echo $selectMark->name; ?>
                        </option>
                        <?php
                    }
                }
            ?>
            </select>
            <small class="mark_empty hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="mySelect2" style="font-size: 15px">Sonini kiriting</label>
        <input type="text" id="count_input" class="form-control count_material" value='<?php
        echo $model->count;
        ?>'>
        <small class="count_empty hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
        <small class="count_number hidden text-danger" style="font-size: 12px">Maydon faqat raqamdan iborat bo'lishi kerak!</small>
    </div>
    <div class="col-lg-2">
        <label for="mySelect2" style="font-size: 15px">Terma kiriting</label>
        <input type="text" id="terma_input" class="form-control terma_material" value='<?php
        echo $model->terma;
        ?>'>
    </div>
    <div class="col-lg-12">
        <div class="row count_inputs">
            <div class="col-lg-4">
                <label for="mySelect2" style="font-size: 15px; margin-top: 10px">Kenglik</label>
                <div class="width_input d-flex">
                    <input type="text" id="width_input" class="form-control" style="width: 500px !important" value='<?php
                    echo $model->width;
                    ?>'>
                    <div class="disable_input">
                        <input disabled type="text" id="count_input" class="form-control" name="count" style="width: 60px !important; margin-left: 10px; text-align: center" value='<?php
                        echo $model->w_approx;
                        ?>'>
                    </div>
                </div>
                <small class="width_empty hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                <small class="width_number hidden text-danger" style="font-size: 12px">Maydon faqat raqamdan iborat bo'lishi kerak!</small>
                <small class="width_error hidden text-danger" style="font-size: 12px">Bunday oraliq mavjud emas!</small>
            </div>
            <div class="col-lg-4">
                <label for="mySelect2" style="font-size: 15px; margin-top: 10px">Uzunlik</label>
                <div class="height_input d-flex">
                    <input type="text" id="height_input" class="form-control" style="width: 500px !important" value='<?php
                    echo $model->height;
                    ?>'>
                    <div class="disable_input">
                        <input disabled type="text" id="count_input" class="form-control" name="count" style="width: 60px !important; margin-left: 10px; text-align: center" value='<?php
                        echo $model->h_approx;
                        ?>'>
                    </div>
                </div>
                <small class="height_empty hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                <small class="height_number hidden text-danger" style="font-size: 12px">Maydon faqat raqamdan iborat bo'lishi kerak!</small>
                <small class="height_error hidden text-danger" style="font-size: 12px">Bunday oraliq mavjud emas!</small>
            </div>
            <div class="col-lg-4">
                <label for="mySelect2" style="font-size: 15px; margin-top: 10px">Qalinlik</label>
                <div class="weight_input d-flex">
                    <input type="text" id="weight_input" class="form-control" style="width: 500px !important" value='<?php
                    echo $model->weight;
                    ?>'>
                    <div class="disable_input">
                        <input disabled type="text" id="count_input" class="form-control" name="count" style="width: 60px !important; margin-left: 10px; text-align: center" value='<?php
                        echo $model->we_approx;
                        ?>'>
                    </div>
                </div>
                <small class="weight_empty hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                <small class="weight_number hidden text-danger" style="font-size: 12px">Maydon faqat raqamdan iborat bo'lishi kerak!</small>
                <small class="weight_error hidden text-danger" style="font-size: 12px">Bunday oraliq mavjud emas!</small>
            </div>
        </div>
    </div>
    <div class="col-lg-12 mt-5">
        <button type="button" class="btn btn-success float-right mt-10 save" data-id="<?= $id ?>" specification-id="<?= $specification_id ?>">Saqlash</button>
    </div>
</div>
<style>
    .material_marks {
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
    .hidden {
        display: none;
    }
    .count_inputs i {
        position: absolute;
    }
    .icon {
        padding: 10px;
        min-width: 40px;
        margin-left: 23px;
        color: red !important;
    }
    .input-field {
        width: 100%;
        padding: 10px;
        text-align: center;
    }
</style>
<?php
$js = <<<JS
    $(function (){
        $(".mySelect4").select2({
            tags: true
        });
    })
    
    $(document).on('change', '#select4', function(){
        var material = $(this).val()
        var _this = $(this)
        $('#width_input').val("").removeClass('is-invalid');
        $('.width_empty').addClass('hidden');
        $('.width_number').addClass('hidden');
        $('#height_input').val("").removeClass('is-invalid');
        $('.height_empty').addClass('hidden');
        $('.height_number').addClass('hidden');
        $('#weight_input').val("").removeClass('is-invalid');
        $('.weight_empty').addClass('hidden');
        $('.weight_number').addClass('hidden');
        $('.disable_input').html( $( `<i class="fa fa-times icon"></i>
    <input disabled type="text" id="count_input" class="form-control input-field" style="width: 60px !important; margin-left: 10px">` ) );
        $.ajax({
            url: 'change-material',
            data: {
                material: material
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $(_this).parent().parent().siblings('.mark_material').find('.select3-primary').find('#select5').prop("disabled", false);
                    $(_this).parent().parent().siblings('.col-lg-4').find('.select3-primary').find('#select5').html(response.content)
                }
            }
        })
    })
    
    $(document).on("click", '.save', function() {
        $('#width_input').removeClass('is-invalid');
        $('.width_error').addClass('hidden');
        $('#height_input').removeClass('is-invalid');
        $('.height_error').addClass('hidden');
        $('#weight_input').removeClass('is-invalid');
        $('.weight_error').addClass('hidden');
        var material = $("#select4").val()
        var mark = $("#select5").val()
        var count = $("#count_input").val()
        var terma = $("#terma_input").val()
        var width = $("#width_input").val()
        var height = $("#height_input").val()
        var weight = $("#weight_input").val()
        var id = $(this).attr('data-id');
        var specification = $(this).attr('specification-id');
        if (material != '' && mark != '' && count != '' && width != '' && height != '' && weight != '' && $.isNumeric(width) && $.isNumeric(height) && $.isNumeric(weight) && $.isNumeric(count)){
            $.ajax({
                url: 'update-specification',
                data: {
                  material: material,
                  mark: mark,
                  count: count,
                  terma: terma,
                  width: width,
                  height: height,
                  weight: weight,
                  id: id  
                }, 
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    if (response.status == "success") {
                        window.location.replace('/index.php/constructor/view-specification?id=' + specification);
                    }
                    if (response.status == "error_width") {
                        $('#width_input').addClass('is-invalid');
                        $('.width_error').removeClass('hidden');
                    }
                    if (response.status == "error_height") {
                        $('#height_input').addClass('is-invalid');
                        $('.height_error').removeClass('hidden');
                    }
                    if (response.status == "error_weight") {
                        $('#weight_input').addClass('is-invalid');
                        $('.weight_error').removeClass('hidden');
                    }
                }
            })
        } else {
            if (material == ''){
                $('#select4').addClass('is-invalid');
                $('.material_empty').removeClass('hidden');        
            } else {
                $('#select4').removeClass('is-invalid');
                $('.material_empty').addClass('hidden');    
            }
            
            if (mark == ''){
                $('#select5').addClass('is-invalid');
                $('.mark_empty').removeClass('hidden');
            } else {
                $('#select5').removeClass('is-invalid');
                $('.mark_empty').addClass('hidden');
            }
            
            if (count == ''){
                $('#count_input').addClass('is-invalid');
                $('.count_empty').removeClass('hidden');
            } else {
                if (!$.isNumeric(count)){
                    $('#count_input').addClass('is-invalid');
                    $('.count_number').removeClass('hidden');    
                } else {
                    $('#count_input').removeClass('is-invalid');
                    $('.count_empty').addClass('hidden');
                }
            }
            
            if (width == ''){
                $('#width_input').addClass('is-invalid');
                $('.width_empty').removeClass('hidden');
            } else {
                if (!$.isNumeric(width)){
                    $('#width_input').addClass('is-invalid');
                    $('.width_number').removeClass('hidden');    
                } else {
                    $('#width_input').removeClass('is-invalid');
                    $('.width_empty').addClass('hidden');
                }
            }
            
            if (height == ''){
                $('#height_input').addClass('is-invalid');
                $('.height_empty').removeClass('hidden');
            } else {
                if (!$.isNumeric(height)){
                    $('#height_input').addClass('is-invalid');
                    $('.height_number').removeClass('hidden');    
                } else {
                    $('#height_input').removeClass('is-invalid');
                    $('.height_empty').addClass('hidden');
                }
            }
            
            if (weight == ''){
                $('#weight_input').addClass('is-invalid');
                $('.weight_empty').removeClass('hidden');
            } else {
                if (!$.isNumeric(weight)){
                    $('#weight_input').addClass('is-invalid');
                    $('.weight_number').removeClass('hidden');    
                } else {
                    $('#weight_input').removeClass('is-invalid');
                    $('.weight_empty').addClass('hidden');
                }
            }
        }
    });

    $(document).on('keyup change', '#width_input', function(){
        $('#width_input').removeClass('is-invalid');
        $('.width_error').addClass('hidden');
        var width = $(this).val()
        var material = $(this).parent().parent().parent().parent().siblings('.material').find('.select2-primary').find('#select4').val()
        var _this = $(this)
        if ($.isNumeric(width)){
            $('#width_input').removeClass('is-invalid');
            $('.width_number').addClass('hidden');
            $('.width_empty').addClass('hidden');
            $.ajax({
                url: 'interval',
                data: {
                    type: 'w',
                    width: width,
                    material: material
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        $(_this).siblings('.disable_input').html(response.content)
                    }
                }
            })
        } else {
            if (width != ''){
                $('#width_input').removeClass('is-invalid');
                $('.width_empty').addClass('hidden');
                
                $('#width_input').addClass('is-invalid');
                $('.width_number').removeClass('hidden'); 
                
                $(_this).siblings('.disable_input').html( $( `<i class="fa fa-times icon"></i>
    <input disabled type="text" id="count_input" class="form-control input-field" style="width: 60px !important; margin-left: 10px">` ) );
            }
            if (width == ''){
                $('#width_input').removeClass('is-invalid');
                $('.width_number').addClass('hidden'); 
                $(_this).siblings('.disable_input').html( $( `<i class="fa fa-times icon"></i>
    <input disabled type="text" id="count_input" class="form-control input-field" style="width: 60px !important; margin-left: 10px">` ) );
            }
        }
    })

    $(document).on('keyup change', '#height_input', function(){
        $('#height_input').removeClass('is-invalid');
        $('.height_error').addClass('hidden');
        var width = $(this).val()
        var material = $(this).parent().parent().parent().parent().siblings('.material').find('.select2-primary').find('#select4').val()
        var _this = $(this)
        if ($.isNumeric(width)){
            $('#height_input').removeClass('is-invalid');
            $('.height_number').addClass('hidden'); 
            $('.height_empty').addClass('hidden');
            $.ajax({
                url: 'interval',
                data: {
                    type: 'h',
                    width: width,
                    material: material
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        $(_this).siblings('.disable_input').html(response.content)
                    }
                }
            })
        } else {
            if (width != ''){
                $('#height_input').removeClass('is-invalid');
                $('.height_empty').addClass('hidden');
                
                $('#height_input').addClass('is-invalid');
                $('.height_number').removeClass('hidden'); 
                
                $('#height_input').removeClass('is-invalid');
                $('.height_error').addClass('hidden'); 
                
                $(_this).siblings('.disable_input').html( $( `<i class="fa fa-times icon"></i>
    <input disabled type="text" id="count_input" class="form-control input-field" style="width: 60px !important; margin-left: 10px">` ) );
            }
            if (width == ''){
                $('#height_input').removeClass('is-invalid');
                $('.height_number').addClass('hidden'); 
                $(_this).siblings('.disable_input').html( $( `<i class="fa fa-times icon"></i>
    <input disabled type="text" id="count_input" class="form-control input-field" style="width: 60px !important; margin-left: 10px">` ) );
            }
        }
    })

    $(document).on('keyup change', '#weight_input', function(){
        $('#weight_input').removeClass('is-invalid');
        $('.weight_error').addClass('hidden');
        var width = $(this).val()
        var material = $(this).parent().parent().parent().parent().siblings('.material').find('.select2-primary').find('#select4').val()
        var _this = $(this)
        if ($.isNumeric(width) && width != ''){
            $('#weight_input').removeClass('is-invalid');
            $('.weight_number').addClass('hidden');
            $('.weight_empty').addClass('hidden');
            $.ajax({
                url: 'interval',
                data: {
                    type: 'g',
                    width: width,
                    material: material
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        $(_this).siblings('.disable_input').html(response.content)
                    }
                }
            })
        } else {
            if (width != ''){
                $('#weight_input').removeClass('is-invalid');
                $('.weight_empty').addClass('hidden');
                
                $('#weight_input').addClass('is-invalid');
                $('.weight_number').removeClass('hidden');  
                
                $(_this).siblings('.disable_input').html( $( `<i class="fa fa-times icon"></i>
    <input disabled type="text" id="count_input" class="form-control input-field" style="width: 60px !important; margin-left: 10px">` ) );
            }
            if (width == ''){
                $('#weight_input').removeClass('is-invalid');
                $('.weight_number').addClass('hidden'); 
                $(_this).siblings('.disable_input').html( $( `<i class="fa fa-times icon"></i>
    <input disabled type="text" id="count_input" class="form-control input-field" style="width: 60px !important; margin-left: 10px">` ) );
            }
        }
    })   
JS;
$this->registerJs($js);
?>
