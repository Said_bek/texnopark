<div class="row index-conductive">
    <div class="col-md-12 mb-20">
        <h2 class="m-0">Shochik qo'shish</h2>
    </div>
    <div class="col-md-6">
        <input type="text" id="section" data-id="<?= $section->id ?>" value="<?= $section->title ?>" readonly class="form-control" name="">
    </div>
    <div class="col-md-6">
        <select id="conductive" class="form-control">
            <?php
                if (isset($users) and !empty($users)){
                    foreach ($users as $key => $value){ ?>
                        <option value="<?php echo $value->id ?>">
                            <?php echo $value->username; ?>
                        </option>
                    <?php }
                }
            ?>
        </select>
    </div>
    <div class="col-md-12">
        <div class="btn btn-success float-right mt-20 save_conductive">Saqlash</div>
    </div>
</div>

<style>
    .index-conductive {
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>

<?php
$js = <<<JS
    $(document).on('click', '.save_conductive', function(){
        var user = $("#conductive").val()
        var section = $("#section").val()
        $.ajax({
            url: 'save-conductive',
            data: {
                user: user,
                section: section
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    location.reload()  
                }
            }
        })
    })
JS;
$this->registerJs($js);
?>