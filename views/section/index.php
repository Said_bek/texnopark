<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Bo'limlar";
?>
    <style>
        .section-index {
            background-color: white;
            padding: 30px;
            -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
            -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
            box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        }
    </style>
    <div class="section-index">
        <div class="row">
            <div class="col-md-6">
                <h1 class="m-0"><?= Html::encode($this->title) ?></h1>
            </div>
            <div class="col-md-6">
                <?= Html::a("Bo'lim qo'shish", [''], ['class' => 'btn btn-success add float-right m-0', 'style' => 'margin-top: 30px;', 'data-target' => "#examplePositionCenter", 'data-toggle' => "modal"]) ?>
            </div>
        </div>

        <div class="row mt-10">
            <div class="col-md-12">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Bo'lim nomi</th>
                        <th>Holati</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (isset($models) and !empty($models)): ?>
                        <?php foreach ($models as $model): ?>
                            <tr>
                                <td><?= $model->title ?></td>
                                <td>
                                    <?php if ($model->status == 1): ?>
                                        <span class="badge badge-success">Faol</span>
                                    <?php else: ?>
                                        <span class="badge badge-danger">Faol emas</span>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <a style="margin-right:10px;color: #3c8dbc;cursor:pointer;" class='update_section'
                                       data-update="<?= $model->id ?>" data-target="#examplePositionCenter"
                                       data-toggle="modal">
                                        <svg aria-hidden="true"
                                             style="display:inline-block;font-size:inherit;height:1em;overflow:visible;vertical-align:-.125em;width:1em"
                                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                            <path fill="currentColor"
                                                  d="M498 142l-46 46c-5 5-13 5-17 0L324 77c-5-5-5-12 0-17l46-46c19-19 49-19 68 0l60 60c19 19 19 49 0 68zm-214-42L22 362 0 484c-3 16 12 30 28 28l122-22 262-262c5-5 5-13 0-17L301 100c-4-5-12-5-17 0zM124 340c-5-6-5-14 0-20l154-154c6-5 14-5 20 0s5 14 0 20L144 340c-6 5-14 5-20 0zm-36 84h48v36l-64 12-32-31 12-65h36v48z"></path>
                                        </svg>
                                    </a>
                                    <?php if ($model->status == 1): ?>
                                        <a style="margin-right:10px;"
                                           href="<?= Url::to(['section/archive', 'id' => $model->id]) ?>"><i
                                                    style="font-size: 1.2em;" class="fa fa-times"
                                                    aria-hidden="true"></i></a>
                                    <?php else: ?>
                                        <a style="margin-right:10px;"
                                           href="<?= Url::to(['section/archive', 'id' => $model->id]) ?>"><i
                                                    style="font-size: 1.2em;" class="fa fa-check"
                                                    aria-hidden="true"></i></a>
                                    <?php endif; ?>
                                    <a style="margin-right:10px;"
                                       href="<?= Url::to(['section/view', 'id' => $model->id]) ?>"><i
                                                style="font-size: 1.2em;" class="fa fa-eye" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php
$js = <<<JS

    $(document).on("click", '.add', function() {
        $(".modal-footer .btn-primary").removeClass().addClass("btn btn-primary save"); 
        $('.modal-header').text("Bo'lim qo'shish")
        $.ajax({ 
            url: '/section/create',
            type: 'get',
            dataType: 'json',
            success: function(response) {
                if (response.status == "success") {
                    $('.modal-body').html(response.content)
                }
            }
        })
    });

    $(document).on("click", '.update_section', function() {
        var update_id = $(this).attr('data-update')
        $(".modal-footer .btn-primary").removeClass().addClass("btn btn-primary update_save").attr('data-update',update_id);
        $('.modal-header').text("Bo'lim o'zgartirish")
        $.ajax({ 
            url: '/section/update?id='+update_id,
            type: 'get',
            dataType: 'json',
            success: function(response) {
                if (response.status == "success") {
                    $('.modal-body').html(response.content)
                }
            }
        })
    });

    $(document).on("click", '.save', function() {
        var title = $('#section-title').val()
        if (title.trim() != '') {
            var _this = $(this)
            $(this).attr('disabled',true)
            $(this).attr('readonly',true)
            $.ajax({
                type: 'get',
                url: '/section/create',
                data: {
                    title: title
                },  
                dataType: 'json',
                success: function(response) {
                    _this.attr('disabled',true)
                    _this.attr('readonly',true)

                    if (response.status == "success") {
                        location.reload();
                    }
                }
            })
        } else {
            $('#section-title').addClass('is-invalid')
            $('.section_error').removeClass('hidden')  
        }
    });

    $(document).on("click", '.update_save', function() {
        var title = $('#section-title').val()
        var update_id = $(this).attr('data-update')
        if (title.trim() != '') {
            var _this = $(this)
            $(this).attr('disabled',true)
            $(this).attr('readonly',true)

            $.ajax({
                type: 'get',
                url: '/section/update?id='+update_id,
                data: {
                    title: title
                },  
                dataType: 'json',
                success: function(response) {
                    _this.attr('disabled',true)
                    _this.attr('readonly',true)
                    if (response.status == "success") {
                        location.reload();
                    }
                }
            })
        }else{
            $('#section-title').addClass('is-invalid')
            $('.section_error').removeClass('hidden')  
        }
    });

JS;
$this->registerJs($js);
?>