<?php
namespace app\controllers;
use app\models\Materials;
use Yii;
?>
<div class="select2-primary">
    <label for="mySelect2" style="font-size: 18px">O'lchov birligi</label>
    <select disabled class="form-control mySelect2" id="select2" data-plugin="select2">
        <option value="<?php echo $unit['id']; ?>" selected = "selected">
            <?php echo $unit['name']; ?>
        </option>
    </select>
    <small class="error_unit hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
</div>

<div class="select2-primary mt-10">
    <label for="mySelect2" style="font-size: 18px">Material tanlang</label>
    <select class="form-control mySelect3 select5" id="select5" multiple="multiple" data-plugin="select2">
        <?php
            if (isset($arr) and !empty($arr)) {
                foreach ($material as $value){
                    $select = '';
                    if (in_array($value['id'], $arr)){
                        $select = 'selected';
                    } ?>
                    <option value="<?php echo $value['id']; ?>" <?php echo $select ?>>
                        <?php
                            echo $value['name'];
                        ?>
                    </option>
                    <?php
                }
            } else {
                foreach ($material as $value){ ?>
                    <option value="<?php echo $value['id']; ?>">
                        <?php
                            echo $value['name'];
                        ?>
                    </option>
                    <?php
                }
            }
        ?>
    </select>
    <small class="error_material hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
</div>

<style>
    .hidden {
        display: none;
    }
</style>

<script>
    $(function (){
        $(".select5").select2({
            tags: true
        });
    })
</script>
