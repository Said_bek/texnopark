<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "O'lchov birliklari";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-index">
    <div class="example-wrap">
        <div class="nav-tabs-horizontal" data-plugin="tabs">
            <ul class="nav nav-tabs nav-tabs-line tabs-line-top" role="tablist">
                <li class="nav-item" role="presentation"><a class="nav-link active show" data-toggle="tab" href="#exampleTabsLineTopOne" aria-controls="exampleTabsLineTopOne" role="tab" aria-selected="true">O'lchov birligi qo'shish <i class="fa fa-plus"></i></a></li>
                <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsLineTopTwo" aria-controls="exampleTabsLineTopTwo" role="tab" aria-selected="false">O'lchov birligini biriktirish <i class="fa fa-paperclip"></i></a></li>
            </ul>
            <div class="tab-content pt-20">
                <div class="tab-pane active show" id="exampleTabsLineTopOne" role="tabpanel">
                    <div class="row">
                        <div class="text-left col-md-6">
                            <h2 class="m-0"><?= Html::encode($this->title) ?></h2>
                        </div>

                        <div class="col-md-6">
                            <button class="btn btn-success float-right" id="addUnit" data-target="#examplePositionCenter" data-toggle="modal"
                                    type="button">O'lchov birligi qo'shish</button>

                            <div class="modal fade" id="examplePositionCenter" aria-hidden="true" aria-labelledby="examplePositionCenter"
                                 role="dialog" tabindex="-1">
                                <div class="modal-dialog modal-simple modal-center">
                                    <div class="modal-content">
                                        <div class="modal-header" style="font-size: 25px">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                            <h4 class="modal-title">O'lchov birligi qo'shish</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p class="text-left">Malumot yo'q!</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Yopish</button>
                                            <button type="button" class="btn btn-primary">Saqlash</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table class='table table-bordered table-striped mt-10'>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nomi</th>
                            <th>Qiymat</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (isset($model) and !empty($model)){
                            $i = 1;
                            foreach ($model as $key => $value) {
                                echo "<tr>";
                                echo "<td>".$i."</td>";
                                echo "<td>".$value->name."</td>";
                                echo "<td>".$value->value."</td>";
                                $id = 0;
                                if(isset($value->id) && !empty($value->id)){
                                    $id = $value->id;
                                }
                                echo "<td>
                                        <a href='#' class='update' data-target='#examplePositionCenter' data-toggle='modal' title='Update' data-update='$id' aria-label='Update'><span class='fa fa-pencil'></span></a>
                                        <a href='#' class='ml-3 delete' title='Delete' data-delete='$id'><span class='fa fa-trash'></span></a>
                                    </td>";
                                echo "</tr>";
                                $i++;
                            }
                        } else {
                            echo "<tr>";
                            echo  "<td class='text-center' colspan='4'>Ma'lumot yo'q</td>";
                            echo "</tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="exampleTabsLineTopTwo" role="tabpanel">
                    <div class="row">
                        <div class="text-left col-md-12">
                            <h2 class="m-0">O'lchov birligi biriktirish</h2>
                        </div>
                    </div>

                    <div class="modal fade" id="examplePositionCenter1" aria-hidden="true" aria-labelledby="examplePositionCenter"
                         role="dialog" tabindex="-1">
                        <div class="modal-dialog modal-simple modal-center">
                            <div class="modal-content">
                                <div class="modal-header" style="font-size: 25px">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title">O'lchov birligi biriktirish</h4>
                                </div>
                                <div class="modal-body">
                                    <p class="text-left">Malumot yo'q!</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Yopish</button>
                                    <button type="button" class="btn btn-primary">Saqlash</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table class='table table-bordered table-striped mt-10'>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>O'lchov birligi</th>
                            <th>Material nomi</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (isset($model) and !empty($model)){
                            $i = 1;
                            foreach ($model as $key => $value) {
                                echo "<tr>";
                                echo "<td>".$i."</td>";
                                echo "<td>".$value->name."</td>";
                                echo "<td>";
                                $unit = '
                                    SELECT * 
                                    FROM material_unit AS mu
                                    INNER JOIN materials AS m ON mu.material_id = m.id
                                    WHERE m.status = 1 and mu.unit_id = '.$value->id;
                                $selectMaterial = Yii::$app->db->createCommand($unit)->queryAll();
                                if (isset($selectMaterial) and !empty($selectMaterial)){
                                        foreach ($selectMaterial as $key1 => $value1){
                                            echo "<span class='badge badge-primary m-1'>".$value1['name']."</span>";
                                        }
                                    } else {
                                        echo "<span class='badge badge-danger m-1'>Material biriktirilmagan!</span>";
                                    }
                                echo "</td>";
                                $id_unit = 0;
                                if(isset($value->id) && !empty($value->id)){
                                    $id_unit = $value->id;
                                }
                                echo "<td>
                                        <a href='#' class='update_unit_material' data-target='#examplePositionCenter1' data-toggle='modal' title='Update' data-unit='$id_unit' aria-label='Update'><span class='fa fa-pencil'></span></a>
                                    </td>";
                                echo "</tr>";
                                $i++;
                            }
                        } else {
                            echo "<tr>";
                            echo  "<td class='text-center' colspan='4'>Ma'lumot yo'q</td>";
                            echo "</tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .unit-index{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>

<?php
$js = <<<JS
    $(document).on('click', '#addUnit', function(){
        $('.modal-body').html('')
        $('.modal-footer .btn-primary').removeClass().addClass('btn btn-primary save_unit')
        $.ajax({
            url: 'add-unit',
            data: {
                u: false,
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("O'lchov birligi qo'shish")
                    $('.modal-footer .btn-primary').addClass('save_unit')
                }
            }
        })
    })
    
    $(document).on('click', '.save_unit', function(){
        var unit_name = $('#unit_name').val()
        if (unit_name != ''){
            $.ajax({
                url: 'add-unit',
                data: {
                    u: true,
                    unit_name: unit_name
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                    if (response.status == 'error_name') {
                        $('#unit_name').addClass('is-invalid');
                        $('.name_error').removeClass('hidden')  
                    }
                }
            })
        } else {
            $('#unit_name').addClass('is-invalid');
            $('.name_error').removeClass('hidden')  
        }
    })
    
    $(document).on('click', '.update', function(){
        $('.modal-body').html('')
        $('.modal-footer .btn-primary').removeClass().addClass('btn btn-primary update_unit')
        var id = $(this).attr('data-update')
        $.ajax({
            url: 'update-unit',
            data: {
                update: false,
                id: id,
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("O'lchov birligini o'zgartirish")
                    $('.modal-footer .btn-primary').addClass('update_unit')
                    $('.modal-footer .btn-primary').attr('data-update',id)
                }
            }
        })
    })
    
    $(document).on('click', '.update_unit', function(){
        var unit_name = $('#unit_name').val()
        var id = $(this).attr('data-update')
        if (unit_name != ''){
            $.ajax({
                url: 'update-unit',
                data: {
                    update: true,
                    unit_name: unit_name,
                    id: id
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                    if (response.status == 'error_name') {
                        $('#unit_name').addClass('is-invalid');
                        $('.name_error').removeClass('hidden')  
                    }
                }
            })
        } else {
            $('#unit_name').addClass('is-invalid');
            $('.name_error').removeClass('hidden')  
        }
    })
    
    $(document).on('click', '.delete', function(){
        if (confirm("O'chirishni istaysizmi?")) {
            var id = $(this).attr('data-delete')
            $.ajax({
                url: 'delete-unit',
                data: {
                    id: id,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                }
            })
        }
    })
    
    $(document).on('click', '.update_unit_material', function(){
        var id = $(this).attr('data-unit')
        $.ajax({
            url: 'update-unit-material',
            data: {
                update: false,
                id: id,
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("Material biriktirish")
                    $('.modal-footer .btn-primary').addClass('save_unit_material')
                    $('.modal-footer .btn-primary').attr('data-update',id)
                }
            }
        })
    })
    
    $(document).on('click', '.save_unit_material', function(){
        var id = $(this).attr('data-update')
        var unit = $('#select2').val()
        var material = $(".select5").select2("val")
        if (unit != '' && material != ''){
            $.ajax({
                url: 'update-unit-material',
                data: {
                    update: true,
                    unit: unit,
                    material: material,
                    id: id
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                }
            })
        } else {
            if (unit == ''){
                $('#select2').addClass('is-invalid')
                $('.error_unit').removeClass('hidden')       
            } else {
                $('#select2').removeClass('is-invalid')
                $('.error_unit').addClass('hidden') 
            }
            if (material == ''){
                $('.select5').siblings().find(".select2-container--default").css({"border-color": "red", "border-weight":"5px", "border-style":"solid"});
                $('.error_material').removeClass('hidden');   
            } else {
                $('.error_material').addClass('hidden');
            }
        }
    })
JS;
$this->registerJs($js);
?>
