<?php

use app\models\Users;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mening sozlamalarim';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1 class="mt-0"><?= Html::encode($this->title) ?></h1>

    <?php
        $user_id = Yii::$app->user->id;
        $selectUsers = Users::find()->where(['id' => $user_id])->one();
        if (isset($selectUsers) and !empty($selectUsers)){
            $userId = $selectUsers->id;
            $userName = $selectUsers->username;
            $phoneNumber = $selectUsers->phone_number;
            $userPassword = $selectUsers->password;
        }
    ?>

    <form class="row password_input">
        <div class="col-lg-4">
            <div class="row">
                <label class="col-lg-12 col-md-3 form-control-label">Ism Familiya</label>
                <div class="col-lg-12 col-md-9 form-group">
                    <input id="user_name" type="text" class="form-control" id="inputValidation1" placeholder="Please ..."
                           value="<?= $userName; ?>" required>
                </div>
            </div>
            <div class="row">
                <label class="col-lg-12 col-md-3 form-control-label">Parol</label>
                <div class="col-lg-12 col-md-9 form-group">
                    <input type="password" class="form-control" id="inputValidation1" placeholder="Please ..."
                           value="<?= $userPassword; ?>" disabled>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="row">
                <label class="col-lg-12 col-md-3 form-control-label">Telefon raqam</label>
                <div class="col-lg-12 col-md-9 form-group">
                    <input type="text" class="form-control" id="inputValidation1" placeholder="Please ..."
                           value="<?= $phoneNumber; ?>" disabled>
                </div>
            </div>
            <div class="row" style="margin-top: 28px;">
                <div class="col-lg-12">
                    <div class="checkbox-custom checkbox-primary">
                        <input class="password_check" type="checkbox" id="inputUnchecked" />
                        <label for="inputUnchecked">Parolni o'zgartirish</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 password_input_2">
        </div>
    </form>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-success change_password" type="button">Saqlash</button>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .users-index{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
    .hidden{
        display: none;
    }
</style>

<?php
$js = <<<JS
    $(".password_check").change(function() {
        if(this.checked) {
            var input = $(`<div class="col-lg-4 test">
            <div class="row test">
                <label class="col-lg-12 col-md-3 form-control-label">Eski parolingiz</label>
                <div class="col-lg-12 col-md-9 form-group old_password_input">
                    <input type="password" class="form-control old_password" id="inputValidation1" placeholder="Eski parolingiz" required>
                    <small class="old_empty hidden text-danger">Eski parolingizni kiritmadingiz!</small>
                    <small class="old_wrong hidden text-danger">Eski parolingizni xato kiritdingiz!</small>
                </div>
            </div>
        </div>
        <div class="col-lg-4 test">
            <div class="row test">
                <label class="col-lg-12 col-md-3 form-control-label">Yengi parolingiz</label>
                <div class="col-lg-12 col-md-9 form-group new_password_input">
                    <input type="password" class="form-control new_password" id="inputValidation1" placeholder="Yengi parolingiz" required>
                    <small class="new_empty hidden text-danger">Yangi parolingiz kiritmadingiz!</small>
                    <small class="new_wrong hidden text-danger">Yangi parolingiz mos emas!</small>
                </div>
            </div>
        </div>
        <div class="col-lg-4 test">
            <div class="row test">
                <label class="col-lg-12 col-md-3 form-control-label">Yengi parolingiz</label>
                <div class="col-lg-12 col-md-9 form-group new_confirm_password_input">
                    <input type="password" class="form-control new_password_confirm" id="inputValidation1" placeholder="Yengi parolingiz" required>
                    <small class="new_confirm_empty hidden text-danger">Yangi parolingiz takroran kiritmadingiz!</small>
                    <small class="new_confirm_wrong hidden text-danger">Yangi parolingiz mos emas!</small>
                </div>
            </div>
        </div>`);
            $('.password_input').append(input);
        } else {
            $(".test").remove();
        }
    });

    $(document).on('click', '.change_password', function(){
        if($('.password_check').is(":checked")) {
            var user_name = $("#user_name").val();
            var old_password = $(".old_password").val();
            var new_password = $(".new_password").val();
            var new_password_confirm = $(".new_password_confirm").val();
            if (new_password.trim() != '' && new_password_confirm.trim() != '' && old_password.trim() != ''){
                $('.new_password').removeClass('is-invalid'); 
                $('.new_empty').addClass('hidden'); 
                $('.new_password_confirm').removeClass('is-invalid');  
                $('.new_confirm_empty').addClass('hidden');   
                $('.old_password').removeClass('is-invalid');
                $('.old_empty').addClass('hidden');  
                if (new_password == new_password_confirm){
                    $('.new_password').removeClass('is-invalid');
                    $('.new_wrong').addClass('hidden'); 
                    $('.new_password_confirm').removeClass('is-invalid');
                    $('.new_confirm_wrong').addClass('hidden'); 
                    $.ajax({
                         url: 'change-password',
                         data: {
                             pas: true,
                             user_name: user_name,
                             old_password: old_password,
                             new_password: new_password,
                             new_password_confirm: new_password_confirm
                         },
                         dataType: 'json',
                         type: 'get',
                         success: function(response) {
                             if (response.status == "success") {
                                 location.reload();
                             } 
                             if (response.status == "not_same") {
                                $('.old_password').addClass('is-invalid');
                                $('.old_wrong').removeClass('hidden');   
                             }
                         },
                         error: function(error){
                             console.log(error)
                         }
                    })
                } else {
                    $('.new_password').addClass('is-invalid');
                    $('.new_wrong').removeClass('hidden'); 
                    $('.new_password_confirm').addClass('is-invalid');
                    $('.new_confirm_wrong').removeClass('hidden'); 
                }
            } else {
                if (new_password.trim() == ''){
                    $('.new_password').addClass('is-invalid'); 
                    $('.new_empty').removeClass('hidden');   
                }  
                if (new_password_confirm.trim() == ''){
                    $('.new_password_confirm').addClass('is-invalid');  
                    $('.new_confirm_empty').removeClass('hidden');   
                }
                if (old_password.trim() == ''){
                    $('.old_password').addClass('is-invalid');
                    $('.old_empty').removeClass('hidden');   
                }
            }
        } else {
            var user_name = $("#user_name").val();
            $.ajax({
                 url: 'change-password',
                 data: {
                     pas: false,
                     user_name: user_name
                 },
                 dataType: 'json',
                 type: 'get',
                 success: function(response) {
                     if (response.status == "success") {
                         location.reload();
                     }            
                 },
                 error: function(error){
                     console.log(error)
                 }
             })
        }
     })
JS;
$this->registerJs($js);
?>
