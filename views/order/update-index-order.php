<?php

use yii\helpers\Html;

// echo '<pre>';
// print_r($not_started_plans);
// print_r($last_started_plans);
// die();

$count_not_plan = 1;
$have_plan = false;
if (isset($not_started_plans) && !empty($not_started_plans)) {
    $count_not_plan = count($not_started_plans);
    $have_plan = true;
}

$this->title = 'Buyurtma rejalash';

?>

    <style>
        .order-planing {
            background-color: white;
            padding: 30px;
            -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
            -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
            box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        }

        .trash-center {
            margin-top: 1.2em;
            font-size: 1.7em;
            cursor: pointer;
        }

        .trash-center i {
            cursor: pointer;
        }

        .trash-center i:hover {
            color: dimgray;
        }
    </style>
    <div class="order-planing">
        <div class="row">
            <div class="col-lg-6 text-left" style='margin-bottom:15px;'>
                <h1 class="m-0"><?= Html::encode($this->title) ?></h1>
            </div>
            <div class="col-lg-6 text-left" style='margin-bottom:15px;'>
                <button type="button" class="btn btn-primary float-right mt-10 add_plan"><i class="fa fa-plus"
                                                                                            aria-hidden="true"></i>
                </button>
            </div>
        </div>
        <input type="hidden" id="count" value="<?php echo $count_not_plan; ?>">
        <?php
        $time_last = '';
        if (isset($last_started_plans) && !empty($last_started_plans)) {
            $time_last = $last_started_plans['step_start_date'];
            $work_time_last = $last_started_plans['step_work_time'];
        }
        ?>

        <div class="line_row">
            <input type="hidden" id="last_plan_end_time" class='plan-date-picker'
                   value="<?php echo date('d.m.Y', strtotime($time_last)); ?>">
            <input type="hidden" id="last_plan_end_time" class='plan-time-picker'
                   value="<?php echo date('H:i', strtotime($time_last)); ?>">
            <input type="hidden" id="last_plan_end_time" class='worktime' value="<?php echo $work_time_last; ?>">
        </div>

        <form id='form' action="#">
            <?php
            if ($have_plan) {
                $count = 0;
                foreach ($not_started_plans as $plan) {
                    $count++;
                    ?>
                    <div class="row line_row">
                        <div class="col-lg-2">
                            <input type="hidden" name="planing[<?php echo $count; ?>][id]"
                                   value="<?php echo $plan['step_id']; ?>">
                            <div class="select2-primary">
                                <label style="font-size: 15px">Bo`lim tanlang</label>
                                <select class="form-control section_select"
                                        name="planing[<?php echo $count; ?>][section]" data-plugin="select2">
                                    <?php
                                    if (isset($sections) && !empty($sections)) {
                                        foreach ($sections as $section) {
                                            if (isset($section->id) && isset($section->title) && !empty($section->id) && !empty($section->title)) {
                                                $selected_text = '';
                                                if ($plan['section_id'] == $section->id) {
                                                    $selected_text = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected_text; ?>
                                                        value="<?php echo $section->id; ?>"><?php echo $section->title; ?></option>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2 machine">
                            <div class="select2-primary">
                                <label style="font-size: 15px">Stanok tanlang</label>
                                <select class="form-control machine_select mySelect"
                                        name="planing[<?= $count; ?>][machine]" data-plugin="select2">
                                    <?php
                                    if (isset($machines) && !empty($machines)) {
                                        foreach ($machines as $machine) {
                                            if (isset($machine->id) && isset($machine->name) && !empty($machine->id) && !empty($machine->name)) {
                                                $selected_text = '';
                                                if ($plan['machine_id'] == $machine->id) {
                                                    $selected_text = 'selected';
                                                }
                                                ?>

                                                <option <?php echo $selected_text; ?>
                                                        value="<?= $machine->id ?>"><?= $machine->name ?></option>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <label style="font-size: 15px">Kirish sana</label>
                            <div class="input-daterange-wrap">
                                <div class="input-daterange">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="icon wb-calendar" aria-hidden="true"></i>
                                        </span>
                                        </div>
                                        <input type="text" name='planing[<?php echo $count; ?>][enter][date]'
                                               value='<?php echo date('d.m.Y', strtotime($plan['step_start_date'])); ?>'
                                               class="form-control plan-date-picker sort date-picker-active">
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="icon wb-time" aria-hidden="true"></i>
                                        </span>
                                        </div>
                                        <input type="text" name='planing[<?php echo $count; ?>][enter][time]'
                                               value='<?php echo date('H:i', strtotime($plan['step_start_date'])); ?>'
                                               class="form-control plan-time-picker sort time-picker-active">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 time_div">
                            <label style="font-size: 15px">Ishlash vaqti</label>
                            <input type="number" name='planing[<?php echo $count; ?>][worktime]' min='1'
                                   value='<?php echo $plan['step_work_time']; ?>' placeholder="Soatda"
                                   class="form-control worktime sort">
                        </div>
                        <div class="col-lg-2 machine_price">
                            <label style="font-size: 15px">Stanok narxi</label>
                            <input type="text" value='<?php echo $plan['machine_price']; ?>' name='planing[<?php echo $count; ?>][price]' class="form-control input_price" readonly>
                        </div>
                        <div class="col-lg-1">
                            <div class='trash-center'>
                                <i class="fa fa-times delete-planed" data-step_id="<?php echo $plan['step_id']; ?>"
                                   aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>

        </form>
        <div class="row">
            <div class="col-lg-12 mt-5">
                <button type="button" class="btn btn-success float-right mt-10" id='save_planig'
                        data-order-id="<?= $order_id ?>">Saqlash
                </button>
            </div>
        </div>
    </div>
<?php
$js = <<<JS
    let youtime = new Date()
    $(function (){
        $(".time-picker-active").clockpicker({
            autoclose: true
        });
        $(".date-picker-active").datepicker({
            autoclose: true,
            format: 'dd.mm.yyyy',
            orientation: "bottom"
        })
    })

    function validate(attr){
        inp = $('.'+attr)
        for (var i = 0; i < inp.length; ++i ) {
            if (inp[i].value == ''){
                return false;
            }
        }
        return true;
    }

    function dateCompare(d1, d2){
        let date1 = new Date(d1);
        let date2 = new Date(d2);
        if(date1 > date2){
            return true
            console.log(date1+'>'+date2);
        } else if(date2 > date1){
            console.log(date1+'<'+date2);
            return false
        } else{
            console.log(date1+'='+date2);
            return true
        }
    }

    function ChangeDateWork(date,time,work){
        let DateTime = new Date()
        
        let arr_date = date.split('.')

        DateTime.setDate(arr_date[0])
        DateTime.setMonth(parseInt(arr_date[1])-1)
        DateTime.setFullYear(arr_date[2])

        let arr_time = time.split(':')
        DateTime.setHours(arr_time[0])
        DateTime.setMinutes(arr_time[1])

        DateTime.setHours(DateTime.getHours() + parseInt(work));

        return DateTime;
    }

    function ChangeDate(date,time){
        let DateTime = new Date()

        let arr_date = date.split('.')

        DateTime.setDate(arr_date[0])
        DateTime.setMonth(parseInt(arr_date[1])-1)
        DateTime.setFullYear(arr_date[2])
        
        let arr_time = time.split(':')
        DateTime.setHours( arr_time[0])
        DateTime.setMinutes(arr_time[1])

        return DateTime;
    }

    function CheckDate(){
        if ($('.line_row').length > 1 && validate('plan-date-picker') && validate('plan-time-picker') && validate('worktime')) {
            let date = $('.plan-date-picker')
            let time = $('.plan-time-picker')
            let work = $('.worktime')
            for (let cd = 0; cd < date.length;cd++) {
                if(cd != 0){
                    let munus_one = cd-1
                    let one = new Date(ChangeDateWork(date[munus_one].value,time[munus_one].value,work[munus_one].value))
                    let two = new Date(ChangeDate(date[cd].value,time[cd].value))
                    if (dateCompare(one,two)) {
                        let date_mdY = ''
                        if (one.getDate() < 10) {
                            date_mdY += '0'+one.getDate()
                        }else{
                            date_mdY += one.getDate()
                        }

                        let month_in_real_time = one.getMonth();
                        month_in_real_time = month_in_real_time +1;
                        if (month_in_real_time == 0) {
                            date_mdY += '.0'+month_in_real_time
                        }else if (month_in_real_time < 10) {
                            date_mdY += '.0'+month_in_real_time
                        }else{
                            date_mdY += '.'+month_in_real_time
                        }
                        date_mdY += '.'+one.getFullYear()

                        one.setMinutes(one.getMinutes()+1)
                        let time_hm = ''
                        if (one.getHours() < 10) {
                            time_hm += '0'+one.getHours()
                        }else{
                            time_hm += one.getHours()
                        }
                        if (one.getMinutes() < 10) {
                            time_hm += ':0'+one.getMinutes()
                        }else{
                            time_hm += ':'+one.getMinutes()
                        }

                        date[cd].value = date_mdY
                        time[cd].value = time_hm

                    }
                }
            }
        }
    }

    $(document).on('click', '.add_plan', function(){
        if (validate('worktime')) {
            if (validate('plan-date-picker') && validate('plan-time-picker')) {
                var count = $('#count').val()
                $(this).attr('disabled', true)
                var _this = $(this)
                $.ajax({
                    url: 'add-plan?count='+(parseInt(count)+1),
                    dataType: 'json',
                    type: 'get',
                    success: function(response) {
                        $(_this).attr('disabled', false)
                        if (response.status == 'success') {
                            $('#count').val(parseInt(count)+1)
                            let date_last = $('.plan-date-picker:last').val()
                            let time_last = $('.plan-time-picker:last').val()
                            let work_last = $('.worktime:last').val()
                            let dal = new Date(ChangeDateWork(date_last,time_last,work_last))

                            $('#form').append(response.content)
                            let mdY = '' 
                            if (dal.getDate() < 10) {
                                mdY += '0'+dal.getDate()
                            }else{
                                mdY += dal.getDate()
                            }

                            let normal_month = dal.getMonth() 
                            normal_month = normal_month + 1;
                            if (normal_month < 10) {
                                mdY += '.0'+normal_month
                            }else{
                                mdY += '.'+normal_month
                            }

                            mdY += '.'+dal.getFullYear()
                            dal.setMinutes(dal.getMinutes()+1)
                            let hm = '';
                            if (dal.getHours() < 10) {
                                hm += '0'+dal.getHours()
                            }else{
                                hm += dal.getHours()
                            }
                            if (dal.getMinutes() < 10) {
                                hm += ':0'+dal.getMinutes()
                            }else{
                                hm += ':'+dal.getMinutes()
                            }
                            $('.plan-date-picker:last').val(mdY)
                            $('.plan-time-picker:last').val(hm)
                        }
                    }
                })
            }else{
                alert('Kirish sana')
            }
        }else{
            alert('Ishlash vaqti kiriting')
        }
    })

    $(document).on('change', '.sort', function(){
        setTimeout(function(){CheckDate()}, 100 );
    })

    $(document).on('click', '#save_planig', function(){
        $(this).attr('disabled', true)
        CheckDate()
        var order = $(this).attr('data-order-id')
        var form = $('#form')
        var _this = $(this)
        $.ajax({
            dataType: 'json',
            url: 'update-plan?order_id='+order,
            data: form.serialize(),
            type: 'POST',
            success: function(response) {
                if (response.status == 'success') {
                    location.replace("/order/index-order")
                }else{
                    $(_this).attr('disabled', false)
                    alert('Rejada xato bor siz xatoni topin yoki bodhqatdan kiritin rejani');
                }
            }
        })
    })
    
    $(document).on('click', '.delete', function(){
        $(this).parents(".row").remove()
    })

    $(document).on('click','.delete-planed',function(){
        if (confirm('Ushbu malumotni ochirish istaysizmi?')) {
            let step_id = $(this).attr('data-step_id')
            $(this).parents(".row").remove()
            $.ajax({
                url: 'delete-plan?step_id='+step_id,
                dataType: 'json',
                type: 'get',
            })
        }
    })
    
    $(document).on('keyup change', '.worktime', function(){
        var work_time = $(this).val()
        var machine_id = $(this).parent().siblings('.machine').find('.select2-primary').find('.machine_select').val()
        var _this = $(this)
        $.ajax({
            url: 'price',
            data: {
                time: work_time,
                machine: machine_id
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $(_this).parent().siblings('.machine_price').find('.input_price').val(response.price)
                }
            }
        })
    })
    
    $(document).on('change', '.machine_select', function(){
        $(this).parent().parent().siblings('.machine_price').find('.input_price').val('')
        $(this).parent().parent().siblings('.time_div').find('.worktime').val('')
    })
JS;
$this->registerJs($js);
?>