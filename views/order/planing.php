<?php

use yii\helpers\Html;

$this->title = 'Buyurtma rejalash';

?>
    <style>
        .order-planing {
            background-color: white;
            padding: 30px;
            -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
            -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
            box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        }

        .trash-center {
            margin-top: 1.2em;
            font-size: 1.7em;
            cursor: pointer;
        }

        .trash-center i {
            cursor: pointer;
        }

        .trash-center i:hover {
            color: dimgray;
        }

        .alertify-logs {
            top: 10px;
        }
    </style>
    <div class="order-planing">
        <div class="row">
            <div class="col-lg-6 text-left" style='margin-bottom:15px;'>
                <h2 class="m-0"><?= Html::encode($this->title) ?></h2>
            </div>
        </div>
        <input type="hidden" id="count" value="1">
        <form id='form' action="#">
            <?php if (!empty($details)): ?>
                <?php $a = 0; foreach ($details as $detail): ?>
                    <div class="row line_row div-line-<?= $a ?>">
                        <input type="hidden" name='planing[<?= $a ?>][detail_id]' value="<?= $detail['id'] ?>">
                        <div class="col-lg-1">
                            <?php if ($a == 0) echo "<label style='font-size: 15px'>Detal kodi</label>"?>
                            <input type="text" name='planing[<?= $a ?>][price]' value="<?= $detail['code'] ?>" class="form-control input_price" readonly>
                        </div>
                        <div class="col-lg-2">
                            <div class="select2-primary">
                                <?php if ($a == 0) echo "<label style='font-size: 15px'>Bo'lim tanlang</label>"?>
                                <select <?php if ($a != 0) echo "disabled" ?> class="form-control select-line-<?= $a ?> section_select line-<?= $a ?>" data-plugin="select2">
                                    <?php if (isset($sections) && !empty($sections)): ?>
                                        <?php foreach ($sections as $section): ?>
                                            <?php if (isset($section->id) && isset($section->title) && !empty($section->id) && !empty($section->title)): ?>
                                                <option value="<?= $section->id ?>"><?= $section->title ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <input type="hidden" class="line-<?= $a ?>" name="planing[<?= $a ?>][section]">
                        </div>
                        <div class="col-lg-2 machine">
                            <div class="select2-primary">
                                <?php if ($a == 0) echo "<label style='font-size: 15px'>Stanok tanlang</label>"?>
                                <select <?php if ($a != 0) echo "disabled" ?> class="form-control select-line-<?= $a ?> machine_select line-<?= $a ?>" name="planing[<?= $a ?>][machine]" data-plugin="select2">
                                    <?php if (isset($machines) && !empty($machines)): ?>
                                        <?php foreach ($machines as $machine): ?>
                                            <?php if (isset($machine->id) && isset($machine->name) && !empty($machine->id) && !empty($machine->name)): ?>
                                                <option value="<?= $machine->id ?>"><?= $machine->name ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <input type="hidden" class="line-<?= $a ?>" name="planing[<?= $a ?>][machine]">
                        </div>
                        <div class="col-lg-3">
                            <?php if ($a == 0) echo "<label style='font-size: 15px'>Kirish sana</label>"?>
                            <div class="input-daterange-wrap">
                                <div class="input-daterange">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="icon wb-calendar" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                        <input <?php if ($a != 0) echo "readonly" ?> autocomplete="off" type="text" name='planing[<?= $a ?>][enter][date]' class="form-control plan-date-picker sort date-picker-active input-<?= $a ?> line-<?= $a ?>">
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="icon wb-time" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                        <input <?php if ($a != 0) echo "readonly" ?> autocomplete="off" type="text" name='planing[<?= $a ?>][enter][time]' class="form-control plan-time-picker sort time-picker-active input-<?= $a ?> line-<?= $a ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 time_div">
                            <?php if ($a == 0) echo "<label style='font-size: 15px'>Ishlash vaqti</label>"?>
                            <input <?php if ($a != 0) echo "readonly" ?> type="number" name='planing[<?= $a ?>][worktime]' min='1' placeholder="Soatda" class="form-control worktime sort input-<?= $a ?> line-<?= $a ?>">
                        </div>
                        <div class="col-lg-1 machine_price">
                            <?php if ($a == 0) echo "<label style='font-size: 15px'>Stanok narxi</label>"?>
                            <input type="text" name='planing[<?= $a ?>][price]' class="form-control input_price input-<?= $a ?>" readonly>
                        </div>
                        <div class="col-lg-1">
                            <?php if ($a == 0) echo "<label style='font-size: 15px;'>Tasdiqlash</label>" ?>
                            <div class="checkbox-custom checkbox-primary ml-25">
                                <input <?php if ($a != 0) echo "disabled" ?> data-id="<?= $a ?>" class="checkbox line-<?= $a ?>" type="checkbox">
                                <label for="inputUnchecked"></label>
                            </div>
                        </div>
                    </div>
                <?php $a++; endforeach; ?>
            <?php else: ?>
                <span class="badge badge-lg badge-outline badge-danger">Bu buyurtma uchun detal mavjud emas</span>
            <?php endif; ?>
        </form>
        <div class="row">
            <div class="col-lg-12 mt-5">
                <button disabled <?php if(empty($details)) echo "style='display: none'" ?> type="button" class="btn btn-success float-right mt-10" id='save_planning' data-order-id="<?= $order_id ?>">
                    Saqlash
                </button>
            </div>
        </div>
    </div>

    <!--alert-->
    <div class="alert position-fixed site-alert text-center" style="z-index: 999; top: 10% !important; left: 48%;" role="alert">

<?php
$js = <<<JS
    $(document).ready(function(){
        setTimeout(function(){
            $("#toggleMenubar > .nav-link").trigger("click");
        }, 500);
    })
    
    $(function (){
        $(".time-picker-active").clockpicker({
            autoclose: true
        });
        $(".date-picker-active").datepicker({
            autoclose: true,
            format: 'dd.mm.yyyy',
            orientation: "bottom"
        })
    })
    
    function checkAllCheckbox(){
        var inp = $('.checkbox')
        for (var b = 0; b < inp.length; ++b ) {
            if (!inp[b].checked){
                return false
            }
        }
        return true;
    }
    
    function dateCompare(d1, d2){
        let date1 = new Date(d1);
        let date2 = new Date(d2);
        if (date1 < date2){
            return true
        } else{
            return false
        }
    }
    
    function ChangeDate(date,time){
        let DateTime = new Date()
        let arr_date = date.split('.')

        DateTime.setDate(arr_date[0])
        DateTime.setMonth(parseInt(arr_date[1])-1)
        DateTime.setFullYear(arr_date[2])
        
        let arr_time = time.split(':')
        DateTime.setHours(arr_time[0])
        DateTime.setMinutes(arr_time[1])

        return DateTime;
    }
    
    $('.checkbox').change(function(){
        var _this = $(this)
        var id = $(_this).attr('data-id')
        var plusId = parseInt(id) + 1
        var minusId = parseInt(id) - 1
        if ($('.checkbox.line-' + id).is(":checked")) {
            if ($(".date-picker-active.line-" + id).val() != '') {
                if ($(".time-picker-active.line-" + id).val() != '') {
                    if ($(".worktime.line-" + id).val() != '') {
                        var sectionSelectValue = $(".section_select.line-" + id).val()
                        var sectionHiddenInput = $(".section_select.line-" + id).parent().siblings('input').val(sectionSelectValue)
                        var machineSelectValue = $(".machine_select.line-" + id).val()
                        var machineHiddenInput = $(".machine_select.line-" + id).parent().siblings('input').val(machineSelectValue)
                        
                        if(minusId >= 0){
                            var getFirstDate = $(".date-picker-active.line-" + minusId).val()
                            var getFirstTime = $(".time-picker-active.line-" + minusId).val()
                            var getSecondDate = $(".date-picker-active.line-" + id).val()
                            var getSecondTime = $(".time-picker-active.line-" + id).val()
                            
                            let one = new Date(ChangeDate(getFirstDate, getFirstTime))
                            let two = new Date(ChangeDate(getSecondDate, getSecondTime))
                            
                            if (dateCompare(one,two)) {
                                $(".line-" + id).prop("readonly", true)
                                $(".select-line-" + id).prop("disabled", true)
                                
                                $(".line-" + plusId).prop("readonly", false)
                                $(".select-line-" + plusId).prop("disabled", false)
                                $('.checkbox.line-' + plusId).prop("disabled", false)       
                            } else {
                                $(".checkbox.line-" + id).prop("checked", false)
                                $('.site-alert').addClass('alert-danger in')
                                $('.site-alert').html("Sana va vaqt notog'ri kiritildi!")
                                setTimeout(() => {
                                    $('.site-alert').removeClass('alert-danger in')
                                    $('.site-alert').html('')
                                },2000) 
                            }
                        } else {
                            $(".line-" + id).prop("readonly", true)
                            $(".select-line-" + id).prop("disabled", true)
                            
                            $(".line-" + plusId).prop("readonly", false)
                            $(".select-line-" + plusId).prop("disabled", false)
                            $('.checkbox.line-' + plusId).prop("disabled", false)   
                            
                        }
                        if (checkAllCheckbox()){
                            $("#save_planning").prop("disabled", false)
                        }
                    } else {
                        $(".checkbox.line-" + id).prop("checked", false)
                        $('.site-alert').addClass('alert-danger in')
                        $('.site-alert').html("Ish vaqti belgilanmadi")
                        setTimeout(() => {
                            $('.site-alert').removeClass('alert-danger in')
                            $('.site-alert').html('')
                        },2000) 
                    }
                } else {
                    $(".checkbox.line-" + id).prop("checked", false)
                    $('.site-alert').addClass('alert-danger in')
                    $('.site-alert').html('Vaqt kiritilmadi!')
                    setTimeout(() => {
                        $('.site-alert').removeClass('alert-danger in')
                        $('.site-alert').html('')
                    },2000)
                }
            } else {
                $(".checkbox.line-" + id).prop("checked", false)
                $('.site-alert').addClass('alert-danger in')
                $('.site-alert').html('Sana kiritilmadi!')
                setTimeout(() => {
                    $('.site-alert').removeClass('alert-danger in')
                    $('.site-alert').html('')
                },2000)
            }
        } else {
            $(".select-line-" + id).prop("disabled", false)
            $(".line-" + id).prop("readonly", false)
            $('.checkbox.line-' + id).prop("disabled", false)
            
            for (var i = plusId; i < 5; ++i ) {
                $('.input-' + i).val(''); 
                $(".select-line-" + i).prop("disabled", true)
                $(".line-" + i).prop("readonly", true)
                $(".checkbox.line-" + i).prop("checked", false).prop("disabled", true)
            }
        }
    });

    $(document).on('click', '#save_planning', function(){
        // $(this).attr('disabled', true)
        var order = $(this).attr('data-order-id')
        var form = $('#form')
        var _this = $(this)
        $.ajax({
            dataType: 'json',
            url: 'save-plan?id='+order,
            data: form.serialize(),
            type: 'POST',
            success: function(response) {
                if (response.status == 'success') {
                    location.replace("/order/index")
                }
            }
        })
    })

    function validate(attr){
        inp = $('.'+attr)
        for ( var i = 0; i < inp.length; ++i ) {
            if (inp[i].value == ''){
                return false;
            }
        }
        return true;
    }

    function ChangeDateWork(date,time,work){
        let DateTime = new Date()
        
        let arr_date = date.split('.')

        DateTime.setDate(arr_date[0])
        DateTime.setMonth(parseInt(arr_date[1])-1)
        DateTime.setFullYear(arr_date[2])

        let arr_time = time.split(':')
        DateTime.setHours(arr_time[0])
        DateTime.setMinutes(arr_time[1])

        DateTime.setHours(DateTime.getHours() + parseInt(work));

        return DateTime;
    }

    function CheckDate(){
        if ($('.line_row').length > 1 && validate('plan-date-picker') && validate('plan-time-picker') && validate('worktime')) {
            let date = $('.plan-date-picker')
            let time = $('.plan-time-picker')
            let work = $('.worktime')
            for (let cd = 0; cd < date.length;cd++) {
                if(cd != 0){
                    let munus_one = cd-1
                    let one = new Date(ChangeDateWork(date[munus_one].value,time[munus_one].value,work[munus_one].value))
                    let two = new Date(ChangeDate(date[cd].value,time[cd].value))
                    if (dateCompare(one,two)) {
                        let date_mdY = ''
                        if (one.getDate() < 10) {
                            date_mdY += '0'+one.getDate()
                        }else{
                            date_mdY += one.getDate()
                        }
                        let month_in_real_time = one.getMonth();
                        month_in_real_time = month_in_real_time +1;
                        if (month_in_real_time == 0) {
                            date_mdY += '.0'+month_in_real_time
                        }else if (month_in_real_time < 10) {
                            date_mdY += '.0'+month_in_real_time
                        }else{
                            date_mdY += '.'+month_in_real_time
                        }
                        date_mdY += '.'+one.getFullYear()
                        date_mdY += '.'+one.getFullYear()

                        one.setMinutes(one.getMinutes()+1)
                        let time_hm = ''
                        if (one.getHours() < 10) {
                            time_hm += '0'+one.getHours()
                        }else{
                            time_hm += one.getHours()
                        }
                        if (one.getMinutes() < 10) {
                            time_hm += ':0'+one.getMinutes()
                        }else{
                            time_hm += ':'+one.getMinutes()
                        }

                        date[cd].value = date_mdY
                        time[cd].value = time_hm

                    }
                }
            }
        }
    }
    

    $(document).on('change', '.sort', function(){
        setTimeout(function(){CheckDate()}, 100);
    })

    
    $(document).on('keyup change', '.worktime', function(){
        var work_time = $(this).val()
        var machine_id = $(this).parent().siblings('.machine').find('.select2-primary').find('.machine_select').val()
        var _this = $(this)
        $.ajax({
            url: 'price',
            data: {
                time: work_time,
                machine: machine_id
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $(_this).parent().siblings('.machine_price').find('.input_price').val(response.price)
                }
            }
        })
    })
    
    $(document).on('change', '.machine_select', function(){
        $(this).parent().parent().siblings('.machine_price').find('.input_price').val('')
        $(this).parent().parent().siblings('.time_div').find('.worktime').val('')
    })
JS;
$this->registerJs($js);
?>