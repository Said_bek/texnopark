<?php

use yii\helpers\Html;

$this->title = 'Yangi buyurtmalar';

?>

<style>
    .order-index {
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>
<div class="order-index">
    <div class="row">
        <div class="col-md-12 text-left">
            <h1 class="m-0"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Buyurtma raqami</th>
                        <th>Buyurtma nomi</th>
                        <th>Mijoz ismi</th>
                        <th style="width: 70px;"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($models) && !empty($models)) {
                        foreach ($models as $model) {
                            if (isset($model['orders_code']) && !empty($model['orders_code']) && isset($model['orders_order_name']) && !empty($model['orders_order_name']) && isset($model['orders_client_id']) && !empty($model['orders_client_id'])) {
                                
                            }
                            echo '<tr>';
                            echo '<td>#'.$model['orders_code'].'</td>';
                            echo '<td>'.$model['orders_order_name'].'</td>';
                            echo '<td>'.$model['orders_client_id'].'</td>';
                            echo '<td>'.Html::a('<span title="View" aria-label="View"></span>Rejalash', 'planing?order_id='.$model['orders_id'], [
                                'title' => Yii::t('app', 'Korish'),
                                'class'=>'btn btn-info btn-xs',                                  
                            ]).'</td>';
                            echo '</tr>';
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>