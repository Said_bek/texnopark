<?php

use yii\helpers\Html;

$this->title = 'Reja qilngan buyutmalar';

// \app\controllers\pre($models);

?>

<style>
    .order-index-order {
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
    .margin_buttons {
        margin-left: 0.9em;
    }
</style>
<div class="order-index-order">
    <div class="row">
        <div class="col-md-12 text-left">
            <h1 class="m-0"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Buyurtma raqami</th>
                        <th>Buyurtma nomi</th>
                        <th>Mijoz ismi</th>
                        <th>Bolim</th>
                        <th>Stanok nomi</th>
                        <th>Boshlash vaqt</th>
                        <th>Ishlash vaqt</th>
                        <th style="width: 70px;"></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if (isset($models) && !empty($models)) {
                    
                    foreach ($models as $model) {
                        echo '<tr>';
                        echo '<td>#';
                        if (isset($model->orders->code) && !empty($model->orders->code)) {
                            echo $model->orders->code;
                        }
                        echo '</td>';
                        echo '<td>';
                        if (isset($model->orders->order_name) && !empty($model->orders->order_name)) {
                            echo $model->orders->order_name;
                        }
                        echo '</td>';
                        echo '<td>';
                        if (isset($model->orders->clients->face_name) && !empty($model->orders->clients->face_name)) {
                            echo $model->orders->clients->face_name;
                        }
                        echo '</td>';
                        echo '<td>';
                        if (isset($model->section->title) && !empty($model->section->title)) {
                            echo $model->section->title;
                        }
                        echo '</td>';
                        echo '<td>';
                        if (isset($model->machine->name) && !empty($model->machine->name)) {
                            echo $model->machine->name;
                        }
                        echo '</td>';
                        echo '<td>';
                        if (isset($model->orderStep->start_date) && !empty($model->orderStep->start_date)) {
                            echo date('Y-m-d H:i',strtotime($model->orderStep->start_date));
                        }
                        echo '</td>';
                        echo '<td>';
                        if (isset($model->orderStep->deadline) && !empty($model->orderStep->deadline)) {
                            echo date('Y-m-d H:i',strtotime($model->orderStep->deadline));
                        }
                        echo '</td>';
                        echo '<td>';
                        echo '<a href="/index.php/order/view-index-order?id='.$model->order_id.'" title="View" aria-label="View"><span class="fa fa-eye"></span></a>';
                        echo '<a class="margin_buttons" href="/index.php/order/update-index-order?id='.$model->order_id.'" title="Update" aria-label="Update"><span class="fa fa-pencil"></span></a>';
                        echo '</td>';
                        echo '</tr>';
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>