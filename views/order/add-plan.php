<div class="row line_row" style="margin-top: 20px">
    <div class="col-lg-2">
        <div class="select2-primary">
            <label style="font-size: 15px">Bo`lim tanlang</label>
            <select class="form-control section_select mySelect" name="planing[<?= $count; ?>][section]" data-plugin="select2">
                <?php
                if (isset($sections) && !empty($sections)) {
                    foreach ($sections as $section) {
                        if (isset($section->id) && isset($section->title) && !empty($section->id) && !empty($section->title)) {
                ?>
                            <option value="<?= $section->id ?>"><?= $section->title ?></option>
                <?php
                        }
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div class="col-lg-2 machine">
        <div class="select2-primary">
            <label style="font-size: 15px">Stanok tanlang</label>
            <select class="form-control machine_select mySelect" name="planing[<?= $count; ?>][machine]" data-plugin="select2">
                <?php
                if (isset($machines) && !empty($machines)) {
                    foreach ($machines as $machine) {
                        if (isset($machine->id) && isset($machine->name) && !empty($machine->id) && !empty($machine->name)) {
                            ?>
                            <option value="<?= $machine->id ?>"><?= $machine->name ?></option>
                            <?php
                        }
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div class="col-lg-3">
        <label style="font-size: 15px">Kirish sana</label>
        <div class="input-daterange-wrap">
            <div class="input-daterange">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="icon wb-calendar" aria-hidden="true"></i>
                        </span>
                    </div>
                    <input type="text" name='planing[<?= $count; ?>][enter][date]' class="form-control plan-date-picker ajax_date sort">
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="icon wb-time" aria-hidden="true"></i>
                        </span>
                    </div>
                    <input type="text" name='planing[<?= $count; ?>][enter][time]' class="form-control plan-time-picker ajax_time sort" data-plugin="timepicker">
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 time_div">
        <label style="font-size: 15px">Ishlash vaqti</label>
        <input type="number" name='planing[<?= $count; ?>][worktime]' placeholder="Soatda" min='0' class="worktime form-control sort">
    </div>
    <div class="col-lg-2 machine_price">
        <label style="font-size: 15px">Stanok narxi</label>
        <input type="text" name='planing[<?= $count; ?>][price]' class="form-control input_price" readonly>
    </div>
    <div class="col-lg-1">
        <div class='trash-center'>
            <i class="fa fa-times delete" aria-hidden="true"></i>
        </div>
    </div>
</div>
<script>
    $(".mySelect").select2({
    });
    $(function() {
        $(".ajax_date").datepicker({
            autoclose: true,
            format: 'dd.mm.yyyy',
            orientation: "bottom",
        });
    })
    $(function() {
        $(".ajax_time").clockpicker({
            autoclose: true,
        });
    })
</script>