<?php

use yii\helpers\Html;

$this->title = $now_step->orders->order_name;
$date_fomart = 'd.m.Y';
$date_hours_fomart = 'd.m.Y H:i';
// \app\controllers\pre($this);
?>
<style>
    .order-view-index-order {
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }

    .mt-70 {
        margin-top: 70px
    }

    .mb-70 {
        margin-bottom: 70px
    }

    .card {
        border-width: 0;
        transition: all .2s;
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: transparent;
        border-radius: .25rem
    }

    .card-body {
        flex: 1 1 auto;
        padding: 1.25rem
    }

    .vertical-timeline {
        width: 100%;
        position: relative;
        padding: 1.5rem 0 1rem
    }

    .vertical-timeline::before {
        content: '';
        position: absolute;
        top: 0;
        left: 100px;
        height: 100%;
        width: 4px;
        background: #e9ecef;
        border-radius: .25rem
    }

    .vertical-timeline-element {
        position: relative;
        margin: 0 0 1.5rem
    }

    .vertical-timeline--animate .vertical-timeline-element-icon.bounce-in {
        visibility: visible;
        animation: cd-bounce-1 .8s
    }

    .vertical-timeline-element-icon {
        position: absolute;
        top: 0;
        left: 92px
    }

    .vertical-timeline-element-icon .badge-dot-xl {
        box-shadow: 0 0 0 5px #fff
    }

    .badge-dot-xl {
        width: 18px;
        height: 18px;
        position: relative
    }

    .badge:empty {
        display: none
    }

    .badge-dot-xl::before {
        content: '';
        width: 10px;
        height: 10px;
        border-radius: .25rem;
        position: absolute;
        left: 50%;
        top: 50%;
        margin: -5px 0 0 -5px;
        background: #fff
    }

    .vertical-timeline-element-content {
        position: relative;
        margin-left: 115px;
        font-size: .8rem;
        /* left: -90px; */
    }

    .vertical-timeline-element-date {
        left: -115px;
    }

    .vertical-timeline-element-content .timeline-title {
        font-size: .8rem;
        text-transform: uppercase;
        margin: 0 0 .5rem;
        padding: 2px 0 0;
        font-weight: bold
    }

    .vertical-timeline-element-content .vertical-timeline-element-date {
        display: block;
        position: absolute;
        top: 0;
        padding-right: 10px;
        text-align: right;
        color: #adb5bd;
        font-size: .7619rem;
        white-space: nowrap
    }

    .vertical-timeline-element-content:after {
        content: "";
        display: table;
        clear: both
    }

    .second_bg {
        background: #dadada;
    }
</style>

<!-- <div class="row d-flex justify-content-center mt-70 mb-70">
    <div class="col-md-12"> -->

<div class="order-view-index-order row">
    <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h3 class="card-title"><?= $now_step->orders->order_name ?></h3>
                <div class="vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                    <?php
                    if (isset($order_step) && !empty($order_step)) {
                        foreach ($order_step as $step) {
                            ?>
                            <div class="vertical-timeline-item vertical-timeline-element">
                                <?php
                                if (isset($step->orderSectionTime) && !empty($step->orderSectionTime)) {
                                    if (isset($step->orderSectionTime->exit_date) && !empty($step->orderSectionTime->exit_date)) {
                                        $badge_status = 'success';
                                    } else {
                                        $badge_status = 'primary';
                                    }
                                } else {
                                    $badge_status = 'secondary';
                                }
                                ?>
                                <div><span class="vertical-timeline-element-icon bounce-in"> <i
                                                class="badge badge-dot badge-dot-xl badge-<?php echo $badge_status; ?>"> </i> </span>
                                    <div class="vertical-timeline-element-content bounce-in">
                                        <p><b><?php if (isset($step->section->title) && !empty($step->section->title)) {
                                                    echo $step->section->title;
                                                } ?></b> <b class="text-secondary"><?php
                                                if (isset($step->work_time) && !empty($step->work_time)) {
                                                    echo $step->work_time;
                                                }
                                                ?> soat</b></p>
                                        <?php
                                        if (isset($step->orderSectionTime->exit_date) && !empty($step->orderSectionTime->exit_date)) {
                                            echo '<p>Boshlangan sana <span class="text-primary">' . date($date_hours_fomart, strtotime($step->orderSectionTime->enter_date)) . '</span></p>';
                                            echo '<p>Tugatilgan sana <span class="text-success">' . date($date_hours_fomart, strtotime($step->orderSectionTime->exit_date)) . '</span></p>';
                                        } elseif (isset($step->orderSectionTime->enter_date) && !empty($step->orderSectionTime->enter_date)) {
                                            echo '<p>Boshlangan sana <span class="text-primary">' . date($date_hours_fomart, strtotime($step->orderSectionTime->enter_date)) . '</span></p>';
                                        }
                                        ?>
                                        <span class="vertical-timeline-element-date"><b><?php
                                                if (isset($step->start_date) && !empty($step->start_date)) {
                                                    echo date($date_hours_fomart, strtotime($step->start_date));
                                                } ?></b>
                                            <br>
                                            <b><?php
                                                if (isset($step->deadline) && !empty($step->deadline)) {
                                                    echo date($date_hours_fomart, strtotime($step->deadline));
                                                } ?></b></span>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8" style="padding-top: 4em;">
        <table class="table table-striped table-bordered detail-view">
            <thead>
            <tr>
                <th rowspan="2"><strong>Bo`lim</strong></th>
                <th rowspan="2"><strong>Stanok</strong></th>
                <th colspan="4" class='text-center second_bg'><strong>Reja boyicha</strong></th>
                <th colspan="4" class='text-center'><strong>Realda</strong></th>
            </tr>
            <tr>
                <th class="second_bg">Boshlanish sana</th>
                <th class="second_bg">Tugatilish sana</th>
                <th class="second_bg">Ishlash vaqt</th>
                <th class="second_bg">Stanok narxi</th>
                <th>Boshlangan sana</th>
                <th>Tugatilgan sana</th>
                <th>Farq</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (isset($order_step) && !empty($order_step)) {
                foreach ($order_step as $table_step) {
                    ?>
                    <tr>
                        <td>
                            <?php
                            if (isset($table_step->section->title) && !empty($table_step->section->title)) {
                                echo '<b>' . $table_step->section->title . '</b>';
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (isset($table_step->machine->name) && !empty($table_step->machine->name)) {
                                echo '<b>' . $table_step->machine->name . '</b>';
                            }
                            ?>
                        </td>
                        <td class="second_bg">
                            <?php
                            if (isset($table_step->start_date) && !empty($table_step->start_date)) {
                                echo date($date_hours_fomart, strtotime($table_step->start_date));
                            }
                            ?>
                        </td>
                        <td class="second_bg">
                            <?php
                            if (isset($table_step->deadline) && !empty($table_step->deadline)) {
                                echo date($date_hours_fomart, strtotime($table_step->deadline));
                            }
                            ?>
                        </td>
                        <td class="second_bg">
                            <?php
                            if (isset($table_step->work_time) && !empty($table_step->work_time)) {
                                echo $table_step->work_time . ' soat';
                            }
                            ?>
                        </td>
                        <td class="second_bg">
                            <?php
                            if (isset($table_step->machine_price) && !empty($table_step->machine_price)) {
                                echo $table_step->machine_price;
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (isset($table_step->orderSectionTime->enter_date) && !empty($table_step->orderSectionTime->enter_date)) {
                                echo date($date_hours_fomart, strtotime($table_step->orderSectionTime->enter_date));
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (isset($table_step->orderSectionTime->exit_date) && !empty($table_step->orderSectionTime->exit_date)) {
                                echo date($date_hours_fomart, strtotime($table_step->orderSectionTime->exit_date));
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (isset($table_step->orderSectionTime->enter_date) && !empty($table_step->orderSectionTime->enter_date) && isset($table_step->orderSectionTime->exit_date) && !empty($table_step->orderSectionTime->exit_date)) {
                                $datetime1 = date_create($table_step->orderSectionTime->enter_date);
                                $datetime2 = date_create($table_step->orderSectionTime->exit_date);
                                $interval = date_diff($datetime1, $datetime2);
                                $hour1 = 0;
                                $hour2 = 0;
                                if ($interval->format('%a') > 0) {
                                    $hour1 = $interval->format('%a') * 24;
                                }
                                if ($interval->format('%h') > 0) {
                                    $hour2 = $interval->format('%h');
                                }

                                // echo "Difference between two dates is " . ($hour1 + $hour2) . " hours.";
                                echo ($hour1 + $hour2) . ' soat';
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<!-- </div>
</div> -->