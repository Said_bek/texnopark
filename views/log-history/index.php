<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Harakatlar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-history-index">

    <h1 class="m-0"><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            [
                'format' => 'html',
                'label' => 'Sana',
                'headerOptions' => ['style' => 'color: #3c8dbc !important;'],
                'value' => function ($data) {
                    if (isset($data->event_time)) {
                        return date('Y-m-d H:i', strtotime($data->event_time));
                    }

                },
            ],
            'event_id'
        ],
    ]); ?>

</div>

<style>
    .log-history-index{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>
