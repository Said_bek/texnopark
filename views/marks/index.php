<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Markalar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marks-index">
    <div class="row">
        <div class="text-left col-md-6">
            <h1 class="m-0"><?= Html::encode($this->title) ?></h1>
        </div>

        <div class="col-md-6">
            <button class="btn btn-success float-right" id="addMark" data-target="#examplePositionCenter" data-toggle="modal"
                    type="button">Marka qo'shish</button>
        </div>
    </div>

    <table class='table table-bordered table-striped mt-20'>
        <thead>
        <tr>
            <th>#</th>
            <th>Nomi</th>
            <th>Holati</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
            if (isset($mark) and !empty($mark)){
                $i = 1;
                foreach ($mark as $key => $value) {
                    echo "<tr>";
                    echo "<td>".$i."</td>";
                    echo "<td>".$value->name."</td>";
                    echo "<td>";
                        if ($value->status == 1) {
                            echo "<span class='badge badge-success'>Faol</span>";
                        } else {
                            echo "<span class='badge badge-danger'>Faol emas</span>";
                        }
                    echo "</td>";
                    $id = 0;
                    if(isset($value->id) && !empty($value->id)){
                        $id = $value->id;
                    }
                    $iconClass = ($value->status == 1) ? "fa fa-times" : "fa fa-check";
                    echo "<td>
                            <a href='#' class='update' data-target='#examplePositionCenter' data-toggle='modal' title='Update' data-update='$id' aria-label='Update'><span class='fa fa-pencil'></span></a>
                            <a href='#' class='ml-3 delete' title='Delete' data-delete='$id'><i class='{$iconClass}'></i></a>
                        </td>";
                    echo "</tr>";
                    $i++;
                }
            } else {
                echo "<tr>";
                echo  "<td class='text-center' colspan='4'>Ma'lumot yo'q</td>";
                echo "</tr>";
            }
        ?>
        </tbody>
    </table>
</div>

<style>
    .marks-index {
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>

<?php
$js = <<<JS
    $(document).on('click', '#addMark', function(){
        $('.modal-body').html('')
        $('.modal-footer .btn-primary').removeClass().addClass('btn btn-primary save_mark')
        $.ajax({
            url: 'add-mark',
            data: {
                m: false,
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("Marka qo'shish")
                    $('.modal-footer .btn-primary').addClass('save_mark')
                }
            }
        })
    })
    
    $(document).on('click', '.save_mark', function(){
        var mark = $('#mark').val()
        if (mark != ''){
            $.ajax({
                url: 'add-mark',
                data: {
                    m: true,
                    mark: mark
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                    if (response.status == 'error_mark') {
                        $('#mark').addClass('is-invalid');
                        $('.name_error').removeClass('hidden')  
                    }
                }
            })
        } else {
            $('#mark').addClass('is-invalid');
            $('.name_error').removeClass('hidden')  
        }
    })
    
    $(document).on('click', '.update', function(){
        var id = $(this).attr('data-update')
        $('.modal-body').html('')
        $('.modal-footer .btn-primary').removeClass().addClass('btn btn-primary update_mark')
        $.ajax({
            url: 'update-mark',
            data: {
                update: false,
                id: id,
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("Markani o'zgartirish")
                    $('.modal-footer .btn-primary').addClass('update_mark')
                    $('.modal-footer .btn-primary').attr('data-update',id)
                }
            }
        })
    })
    
    $(document).on('click', '.update_mark', function(){
        var mark = $('#mark').val()
        var id = $(this).attr('data-update')
        if (mark != ''){
            $.ajax({
                url: 'update-mark',
                data: {
                    update: true,
                    mark: mark,
                    id: id
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                    if (response.status == 'error_mark') {
                        $('#mark').addClass('is-invalid');
                        $('.name_error').removeClass('hidden')  
                    }
                }
            })
        } else {
            $('#mark').addClass('is-invalid');
            $('.name_error').removeClass('hidden')  
        }
    })
    
    $(document).on('click', '.delete', function(){
        var id = $(this).attr('data-delete')
        $.ajax({
            url: 'delete',
            data: {id: id},
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    location.reload()
                }
            }
        })
    })
JS;
$this->registerJs($js);
?>