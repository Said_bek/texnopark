<label for="mySelect2" style="font-size: 15px">Buyurtma nomi</label>
<input type="text" id="orderName" value="<?= $order->order_name ?>" class="form-control" name="">
<small class="name_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>

<div class="select2-primary text-left mt-10">
    <label for="mySelect2" style="font-size: 15px">Mijoz</label>
    <select <?php if(!(isset($clients) and !empty($clients))) echo "disabled" ?> class="form-control mySelect2" id="orderClient" data-plugin="select2">
        <?php
        if (isset($clients) and !empty($clients)){
            foreach ($clients as $key => $value){ ?>
                <option <?php if($value->id == $order->client_id) echo "selected" ?> value="<?php echo $value->id ?>">
                    <?php echo $value->face_name; ?>
                </option>
            <?php }
        } else { ?>
            <option selected>Mijoz mavjud emas</option>
        <?php }
        ?>
    </select>
    <small class="client_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
</div>

<style>
    .hidden {
        display: none;
    }
</style>

<script>
    $("#orderClient").select2({
        tags: true
    });
</script>