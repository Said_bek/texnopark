<label for="mySelect2" style="font-size: 15px">Buyurtma nomi</label>
<input type="text" id="orderName" class="form-control" name="">
<small class="name_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>

<div class="select2-primary text-left mt-10">
    <label for="mySelect2" style="font-size: 15px">Mijoz</label>
    <select <?php if(!(isset($clients) and !empty($clients))) echo "disabled" ?> class="form-control mySelect2" id="orderClient" data-plugin="select2">
        <?php
            if (isset($clients) and !empty($clients)){
                foreach ($clients as $key => $value){ ?>
                    <option value="<?php echo $value->id ?>">
                        <?php echo $value->face_name; ?>
                    </option>
                <?php }
            } else { ?>
                <option selected>Mijoz mavjud emas</option>
            <?php }
        ?>
    </select>
    <small class="client_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
</div>

<!--<div class="dead_line text-left">-->
<!--    <div class="example-wrap">-->
<!--        <div class="example datepair-wrap" data-plugin="datepair">-->
<!--            <label>Buyurtma tugash sanasi</label>-->
<!--            <div class="input-daterange-wrap">-->
<!--                <div class="input-daterange">-->
<!--                    <div class="input-group">-->
<!--                        <div class="input-group-prepend">-->
<!--                            <span class="input-group-text"><i class="icon wb-calendar" aria-hidden="true"></i></span>-->
<!--                        </div>-->
<!--                        <input type="text" date-php="--><?//= date('d-m-Y H:i:s') ?><!--" data-date-format='dd.mm.yyyy' autocomplete="off"  id="datepicker" class="datepicker price_date form-control datepair-date datepair-start" data-plugin="datepicker">-->
<!--                        <div>-->
<!--                            <small class="date_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>-->
<!--                            <small class="small_date hidden text-danger" style="font-size: 12px">Sana notog'ri kiritildi!</small>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="input-group">-->
<!--                        <div class="input-group-prepend">-->
<!--                            <span class="input-group-text">-->
<!--                                <i class="icon wb-time" aria-hidden="true"></i>-->
<!--                            </span>-->
<!--                        </div>-->
<!--                        <input type="text" class="timepicker form-control" id="timepicker" autocomplete="off" data-plugin="clockpicker" data-autoclose="true">-->
<!--                        <div>-->
<!--                            <small class="time_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>-->
<!--                            <small class="small_time hidden text-danger" style="font-size: 12px">Sana notog'ri kiritildi!</small>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<style>
    .hidden {
        display: none;
    }
    .popover {
        z-index: 9999 !important;
    }
</style>


<script>
    $("#orderClient").select2({
        tags: true
    });
    $(function (){
        $("#datepicker").datepicker();
    })
    $(function (){
        $("#timepicker").clockpicker();
    })

    $(function(){
        var date_php = $("#datepicker").attr('date-php')
        $('#datepicker').datepicker('setStartDate', date_php);
    })

    $('#datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        language: 'en',
        orientation: 'bottom',
    }).on('hide', function(e) {
        e.stopPropagation();
    });
</script>