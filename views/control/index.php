<?php
/* @var $this yii\web\View */
?>
<div class="d-flex">
    <div class="col-xl-3 col-md-6" style="height: 120px;">
        <div class="card card-shadow" id="widgetLineareaOne">
            <div class="card_shadow card-block p-20 pt-10 bg-primary">
                <div class="clearfix">
                    <div class="white float-left py-10">
                        <i class="icon fa fa-cube white font-size-24 vertical-align-bottom mr-5"></i> Buyurtmalar
                    </div>
                    <span class="float-right white font-size-30"><?= (isset($count) and !empty($count)) ? $count : 0 ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6" style="height: 120px;">
        <div class="card card-shadow" id="widgetLineareaOne">
            <div class="card_shadow card-block p-20 pt-10 bg-warning">
                <div class="clearfix">
                    <div class="white float-left py-10">
                        <i class="icon fa fa-pause white font-size-24 vertical-align-bottom mr-5"></i> To'xtab turganlar
                    </div>
                    <span class="float-right white font-size-30">356</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6" style="height: 120px;">
        <div class="card card-shadow" id="widgetLineareaOne">
            <div class="card_shadow card-block p-20 pt-10 bg-danger">
                <div class="clearfix">
                    <div class="white float-left py-10">
                        <i class="icon fa fa-times white font-size-24 vertical-align-bottom mr-5"></i> Bekor qilinganlar
                    </div>
                    <span class="float-right white font-size-30">125</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6" style="height: 120px;">
        <div class="card card-shadow" id="widgetLineareaOne">
            <div class="card_shadow card-block p-20 pt-10" style="background: #23db48">
                <div class="clearfix">
                    <div class="white float-left py-10">
                        <i class="icon fa fa-check white font-size-24 vertical-align-bottom mr-5"></i> Bitganlar
                    </div>
                    <span class="float-right white font-size-30">785</span>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>

<div class="orders-index">
    <div class="example-wrap">
        <div class="nav-tabs-horizontal" data-plugin="tabs">
            <ul class="nav nav-tabs nav-tabs-line tabs-line-top" role="tablist">
                <li class="nav-item" role="presentation"><a class="nav-link active show" data-toggle="tab" href="#exampleTabsLineTopOne" aria-controls="exampleTabsLineTopOne" role="tab" aria-selected="true">Jarayondagi buyurtmalar <i class="fa fa-cogs"></i></a></li>
                <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsLineTopTwo" aria-controls="exampleTabsLineTopTwo" role="tab" aria-selected="false">Tasdiqlanmagan buyurtmalar <i class="fa fa-check"></i></a></li>
            </ul>
            <div class="tab-content pt-20">
                <div class="tab-pane active show" id="exampleTabsLineTopOne" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="m-0">Jarayondagi buyurtmalar</h2>
                        </div>
                    </div>
                    <table class='table table-bordered table-striped mt-10'>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Xizmat Kategoriyasi</th>
                            <th>Buyurtma qabul qlingan sana</th>
                            <th>Telefon raqam 1</th>
                            <th>Telefon raqam 2</th>
                            <th>Kompaniya nomi</th>
                            <th>Buyurtma qayerdan</th>
                            <th>Narx taqdim etish sanasi</th>
                            <th>Prioritet</th>
                            <th>Buyurtma holati</th>
                            <th>Narxi</th>
                            <th>Tasdiq</th>
                            <th>PDF</th>
                            <th>Keyingi bo'limga o'tkazish</th>
                            <th>Batafsil</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (isset($model) && !empty($model)) {
                            $i = 1;
                            foreach ($model as $key => $value) { ?>
                                <tr>
                                    <td><?= $i++ ?></td>
                                    <td><?= $value['category'] ?></td>
                                    <td><?= date('d-m-Y H:i', strtotime($value['created_date'])); ?></td>
                                    <td><?= $value['phone_number'] ?></td>
                                    <td><?= $value['phone_number_2'] ?></td>
                                    <td><?= $value['company_name'] ?></td>
                                    <td><?php
                                        if ($value['order_from'] == 0) {
                                            echo '<span class = "badge badge-primary">IN</span>';
                                        } else {
                                            echo '<span class = "badge badge-default">OUT</span>';
                                        }
                                        ?></td>
                                    <td><?= date('d-m-Y H:i', strtotime($value['deadline_price'])); ?></td>
                                    <td><?php
                                        if ($value['priority'] == 0) {
                                            echo '<span class = "badge badge-default">Oddiy</span>';
                                        } elseif ($value['priority'] == 1) {
                                            echo '<span class = "badge badge-danger">Tezkor</span>';
                                        } else {
                                            echo '<span class = "badge badge-warning">Kechiktirilgan</span>';
                                        }
                                        ?></td>
                                    <td><?php
                                        if ($value['status'] == 0) {
                                            echo '<span class = "badge badge-danger">Faol emas</span>';
                                        } else {
                                            echo '<span class = "badge badge-success">Faol</span>';
                                        }
                                        ?></td>
                                    <td><?= $value['price'] ?></td>
                                    <td><?php
                                        if ($value['approved'] == 0) {
                                            echo '<span class = "badge badge-danger">Tasdiqlanmagan</span>';
                                        } else {
                                            echo '<span class = "badge badge-success">Tasdiqlangan</span>';
                                        }
                                        ?></td>
                                    <td>
                                        <?= "<span class='badge badge-primary' style='cursor: pointer;'><a style='color: white;' href='" . \yii\helpers\Url::to(['orders/pdf', 'id' => $value['order_id']]) . "'><i class='fa fa-print'></i></a></span>"; ?>
                                    </td>
                                    <td>
                                        <center>
                                            <?php
                                                if ($value['module_id'] == 3) {
                                                    echo "<a class='btn btn-icon btn-success next_section' data-target='#examplePositionCenter' data-toggle='modal' data-id=" . $value['order_id'] . " style='margin-left: 10px; cursor: pointer;'><i class='icon wb-arrow-up' style='color: white'></i></a>";
                                                } else {
                                                    echo "<span class='badge badge-dark'>" . $value['module_name'] . "</span>";
                                                }
                                            ?>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <?= "<span style='cursor: pointer;'><a href='" . \yii\helpers\Url::to(['control/info', 'id' => $value['order_id']]) . "'><i class='icon fa-eye'></i></a></span>"; ?>
                                        </center>
                                    </td>
                                </tr>
                                <?php
                            }
                        } else { ?>
                            <tr>
                                <td colspan="16">Ma'lumot topilmadi!</td>
                            </tr>
                        <?php }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="exampleTabsLineTopTwo" role="tabpanel">
                    <div class="tab-pane active show" id="exampleTabsLineTopTwo" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6">
                                <h2 class="m-0">Tasdiqlanmagan buyurtmalar</h2>
                            </div>
                            <div class="col-md-6">
                                <button type="button" data-target="#examplePositionCenter" data-toggle="modal" class="btn btn-success float-right new_order">
                                    Buyurtma qo'shish
                                </button>
                            </div>
                        </div>
                        <table class='table table-bordered table-striped mt-20'>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Buyurtma nomi</th>
                                <th>Mijoz</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                if (isset($unconfirmed) and !empty($unconfirmed)){
                                    $i = 1;
                                    foreach ($unconfirmed as $key => $value) {
                                        echo "<tr>";
                                        echo "<td>".$i."</td>";
                                        echo "<td>".$value->order_name."</td>";
                                        $client = $value->clients->face_name;
                                        echo (isset($client) and !empty($client)) ? "<td>".$client."</td>" : "<td><span class='badge badge-danger'>Mijoz ismi mavjud emas</span></td>";
                                        $id = 0;
                                        if(isset($value->id) && !empty($value->id)){
                                            $id = $value->id;
                                        }
                                        echo "<td>
                                                <a href='#' class='updateOrder' data-target='#examplePositionCenter' data-toggle='modal' title='Update' data-update='$id' aria-label='Update'><span class='fa fa-pencil'></span></a>
                                                <a href='/index.php/control/order-delete?id={$id}' class='ml-3 deleteOrder' title='Delete'><i class='fa fa-trash'></i></a>
                                                <a href='/index.php/control/update-status-order?id={$id}' class='ml-3 updateStatusOrder'><i class='fa fa-check'></i></a>
                                            </td>";
                                        echo "</tr>";
                                        $i++;
                                    }
                                } else {
                                    echo "<tr>";
                                    echo  "<td class='text-center' colspan='4'>Ma'lumot yo'q</td>";
                                    echo "</tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .card_shadow{
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.20);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.20);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.20);
    }
    .orders-index {
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>

<?php
$js = <<<JS
    $(document).ready(function(){
        setTimeout(function(){
            $("#toggleMenubar > .nav-link").trigger("click");
        }, 500);
    })
    
    $(document).on('click', '.new_order', function(){
        $('.modal-body').html('')
        $('.modal-footer .btn-primary').removeClass().addClass('btn btn-primary save_order')
        $.ajax({
            url: 'new-order',
            data: {
                o: false,
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("Buyurtma qo'shish")
                    $('.modal-footer .btn-primary').addClass('save_order')
                }
            }
        })
    })
    
    $(document).on('click', '.save_order', function(){
        var name = $('#orderName').val()
        var client = $('#orderClient').val()
        $.ajax({
            url: 'new-order',
            data: {
                o: true,
                name: name,
                client: client
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    location.reload()
                }
                if (response.status == 'failure_name') {
                    $('#orderName').addClass('is-invalid');
                    $('.name_error').removeClass('hidden')  
                }
                if (response.status == 'failure_client') {
                    $('#orderClient').addClass('is-invalid');
                    $('.client_error').removeClass('hidden')  
                }
            }
        })
    })
    
    $(document).on('click', '.updateOrder', function(){
        var id = $(this).attr('data-update')
        $('.modal-body').html('')
        $('.modal-footer .btn-primary').removeClass().addClass('btn btn-primary update_order')
        $.ajax({
            url: 'update-order',
            data: {
                update: false,
                id: id,
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("Buyurtmani o'zgartirish")
                    $('.modal-footer .btn-primary').addClass('update_order')
                    $('.modal-footer .btn-primary').attr('data-update',id)
                }
            }
        })
    })
    
    $(document).on('click', '.update_order', function(){
        var name = $('#orderName').val()
        var client = $('#orderClient').val()
        var id = $(this).attr('data-update')
        $.ajax({
            url: 'update-order',
            data: {
                update: true,
                name: name,
                client: client,
                id: id
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    location.reload()
                }
                if (response.status == 'failure_name') {
                    $('#orderName').addClass('is-invalid');
                    $('.name_error').removeClass('hidden')  
                }
                if (response.status == 'failure_client') {
                    $('#orderClient').addClass('is-invalid');
                    $('.client_error').removeClass('hidden')  
                }
            }
        })
    })
    
    $(document).on('click','.deleteOrder',function(e){
        if (!confirm("Buyurtmani o'chirmoqchimisiz?")) {
            e.preventDefault();
        }
    })
    
    $(document).on('click','.updateStatusOrder',function(e){
        if (!confirm("Buyurtmani tasdiqlamochimisiz?")) {
            e.preventDefault();
        }
    })
JS;
$this->registerJs($js);
?>

