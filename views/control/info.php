<div class="order-info">
    <?php if (isset($model) and !empty($model)) : ?>
        <ul class="timeline timeline-icon">
        <li class="timeline-period time-line-header" style="border-radius: 5px">REJA</li>
        <?php $i = 1; foreach ($model as $value) : ?>
            <li class="timeline-item <?= ($i%2 == 0) ? "timeline-reverse" : ""?>">
                <div class="timeline-dot timeline-dot-shadow bg-green-500">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                </div>
                <div class="timeline-info timeline-info-shadow <?= ($i%2 == 0) ? "animation-slide-left" : ""?>">
                    <time datetime="2017-05-15"><?= date('Y-m-d H:i', strtotime($value['start_date'])) ?></time>
                </div>
                <div class="timeline-content timeline-content-shadow <?= ($i%2 == 0) ? "animation-slide-left" : ""?>">
                    <div class="card card-article card-shadow">
                        <div class="card-block">
                            <h3 class="card-title"><?= $value['title'] ?></h3>
                            <p class="card-text">
                                <small>MAY 15, 2017</small>
                            </p>
                            <p>
                                Novum formidines congressus atomorum nam permulta alterum delectatio
                                gaudeat statim. Necessariae dicturam perspexit utrum modo amicitiae
                                malum summumque, multis ante iudicia desiderat, civitas iudicare
                                attingere, amori perpaulum mediocrium, dicere notae litteras
                                plusque appareat, remotis fama futurove quandam assentiar integre
                                poenis.
                            </p>
                        </div>
                    </div>
                </div>
            </li>
        <?php $i++; endforeach; ?>
        </ul>
    <?php else: ?>
        <div class="d-flex justify-content-center align-items-center">
            <span class="badge badge-outline badge-lg badge-danger">Bu buyurtma uchun reja mavjud emas</span>
        </div>
    <?php endif; ?>
</div>


<style>
    .timeline-content-shadow, .time-line-header {
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }

    .timeline-dot-shadow, .timeline-info-shadow {
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>