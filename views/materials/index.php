<?php

use app\models\Modules;
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Materials;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Materiallar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="materials-index">
    <div class="row">
        <div class="col-md-6" style="padding-left: 0 !important">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>

        <div class="text-right col-md-6" style="padding-right: 0 !important">
            <h2>
                <?= Html::a("Material qo'shish", ['create'], ['class' =>  'new_order btn btn-success']) ?>
            </h2>
        </div>

        <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th>Kod</th>
                <th>Nomi</th>
                <th>Parameter</th>
                <th>Markalar</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (isset($materials) and !empty($materials)){
                $i = 1;
                foreach ($materials as $key => $value) {
                    echo "<tr>";
                    echo "<td>".$value->code_name." ".\app\controllers\numberZero($value->code_number)."</td>";
                    echo "<td>".$value->name."</td>";
                    echo "<td>";
                        if (isset($value->para->paraName->name) and !empty($value->para->paraName->name)){
                            echo "<span class='badge badge-primary m-1'>".$value->para->paraName->name."</span>";
                        } else {
                            echo "<span class='badge badge-danger m-1'>Parameter biriktirilmagan!</span>";
                        }
                    echo "</td>";
                    echo "<td>";
                    $marks = '
                        SELECT * 
                        FROM material_marks AS mm 
                        INNER JOIN marks AS m ON mm.mark_id = m.id
                        WHERE mm.material_id = '.$value->id;
                    $selectMarks = Yii::$app->db->createCommand($marks)->queryAll();
                    if (isset($selectMarks) and !empty($selectMarks)){
                        foreach ($selectMarks as $key1 => $value1){
                            echo "<span class='badge badge-primary m-1'>".$value1['name']."</span>";
                        }
                    } else {
                        echo "<span class='badge badge-danger m-1'>Marka biriktirilmagan!</span>";
                    }
                    echo "</td>";
                    $id = 0;
                    if(isset($value->id) && !empty($value->id)){
                        $id = $value->id;
                    }
                    echo "<td>
                        <a href='/index.php/materials/view?id=$id' title='View' aria-label='View'><span class='fa fa-eye'></span></a>
                        <a href='/index.php/materials/update?id=$id' class='update' title='Update' data-update='$id' aria-label='Update'><i class='fa fa-pencil' style='color: #3e8ef7'></i></a>
                        <a href='#' class='delete' title='Delete' data-delete='$id'><span class='fa fa-trash'></span></a>
                        </td>";
                    echo "</tr>";
                    $i++;
                }
            } else {
                echo "<tr>";
                echo  "<td class='text-center' colspan='4'>Ma'lumot yo'q</td>";
                echo "</tr>";
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<style>
    .materials-index{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>

<?php
$js = <<<JS

    $(document).on('click', '.save_material', function(){
        var name = $('#select2').val()
        var mark = $('#select3').val()
        if (name != '' && mark != ''){
            $.ajax({
                url: 'add-material',
                data: {
                    ma: true,
                    name: name,
                    mark: mark
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    } 
                    if (response.status == 'error_name') {
                        $('#select2').addClass('is-invalid');
                        $('.name_error').removeClass('hidden')
                    } 
                    if (response.status == 'error_mark') {
                        $('#select3').addClass('is-invalid');
                        $('.mark_error').removeClass('hidden') 
                    } 
                    if (response.status == 'error') {
                        alert("Ma'lumot saqlanmadi!")
                    } 
                }
            })
        } else {
            if (name == ''){
                $('#select2').addClass('is-invalid');
                $('.name_error').removeClass('hidden')      
            } else {
                $('#select2').removeClass('is-invalid');
                $('.name_error').addClass('hidden')      
            }
            if (mark == ''){
                $('#select3').addClass('is-invalid');
                $('.mark_error').removeClass('hidden')      
            } else {
                $('#select3').removeClass('is-invalid');
                $('.mark_error').addClass('hidden')     
            }
        }
    })
    
    $(document).on('click', '.delete', function(){
        if (confirm("O'chirishni istaysizmi?")) {
            var id = $(this).attr('data-delete')
            $.ajax({
                url: 'update-status',
                data: {
                    id: id,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                }
            })
        }
    })
    
JS;
$this->registerJs($js);
?>
