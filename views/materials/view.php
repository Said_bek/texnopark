<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Materials;
use app\models\Marks;

/* @var $this yii\web\View */
/* @var $model app\models\Materials */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="material-index">
    <div class="row">
        <div class="col-md-6">
            <h1 class="m-0"><?php echo $model->name ?></h1>
        </div>
        <div class="col-md-6 text-right">
            <?php
            $marks = 'SELECT * FROM material_marks AS mm
                                        INNER JOIN marks AS m ON mm.mark_id = m.id
                                        WHERE mm.material_id = '.$model->id;
            $selectMarks = Yii::$app->db->createCommand($marks)->queryAll();
            if (isset($selectMarks) and !empty($selectMarks)){
                foreach ($selectMarks as $key1 => $value1){
                    echo "<span class='badge badge-lg badge-primary m-1'>".$value1['name']."</span>";
                }
            }
            ?>
        </div>
    </div>
    <table class='table table-bordered table-striped mt-15'>
        <thead>
        <tr>
            <th>#</th>
            <th>Shirina</th>
            <th>Dlina</th>
            <th>Tolshina</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
        if (isset($settings) and !empty($settings)){
            $i = 1;
            foreach ($settings as $key => $value) {
                echo "<tr>";
                echo "<td>".$i."</td>";
                echo "<td>".$value->width_f." - ".$value->width_s."<span class='badge badge-dark m-5'>".$value->w_approx."</span></td>";
                echo "<td>".$value->height_f." - ".$value->height_s."<span class='badge badge-dark m-5'>".$value->h_approx."</span></td>";
                echo "<td>".$value->weight_f." - ".$value->weight_s."<span class='badge badge-dark m-5'>".$value->we_approx."</span></td>";
                echo "<td><a href='#' class='delete' title='Delete' data-delete='$value->id'><span class='fa fa-trash'></span></a></td>";
                echo "</tr>";
                $i++;
            }
        } else {
            echo "<tr>";
            echo  "<td class='text-center' colspan='12'>Ma'lumot yo'q</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
    <div class="nav-tabs-horizontal mt-20" data-plugin="tabs">
        <ul class="nav nav-tabs nav-tabs-reverse" role="tablist">
            <li class="nav-item" role="presentation"><a class="nav-link active show" data-toggle="tab" href="#exampleTabsReverseOne" aria-controls="exampleTabsReverseOne" role="tab" aria-selected="true">Kengligi</a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsReverseTwo" aria-controls="exampleTabsReverseTwo" role="tab" aria-selected="false">Uzunligi</a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsReverseThree" aria-controls="exampleTabsReverseThree" role="tab" aria-selected="false">Qalinligi</a></li>
            <li class="dropdown nav-item" role="presentation" style="display: none;">
                <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false">Dropdown </a>
                <div class="dropdown-menu" role="menu">
                    <a class="dropdown-item" data-toggle="tab" href="exampleTabsReverseOne" aria-controls="exampleTabsReverseOne" role="tab">Kengligi</a>
                    <a class="dropdown-item" data-toggle="tab" href="#exampleTabsReverseTwo" aria-controls="exampleTabsReverseTwo" role="tab">Uzunligi</a>
                    <a class="dropdown-item" data-toggle="tab" href="#exampleTabsReverseThree" aria-controls="exampleTabsReverseThree" role="tab">Qalinligi</a>
                </div>
            </li>
        </ul>
        <div class="tab-content pt-20">
            <div class="tab-pane active show" id="exampleTabsReverseOne" role="tabpanel">
                <div class="row">
                    <div class="col-md-3 div_width">
                        <label>1-qiymat</label>
                        <input type="text" class="form-control width_1">
                        <small class="width_error_1 hidden text-danger">Maydon bo'sh bo'lishi mumkin emas!</small>
                        <small class="width_number_1 hidden text-danger">Maydon faqat sondan iborat bo'lishi kere!</small>
                        <small class="width_sort_1 hidden text-danger">1-maydon 2-maydondan kichkina bo'lishi kerak!</small>
                        <small class="width_false hidden text-danger">Bunday oraliq mavjud!</small>
                    </div>
                    <div class="col-md-3">
                        <label>2-qiymat</label>
                        <input type="text" class="form-control width_2">
                        <small class="width_error_2 hidden text-danger">Maydon bo'sh bo'lishi mumkin emas!</small>
                        <small class="width_number_2 hidden text-danger">Maydon faqat sondan iborat bo'lishi kere!</small>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <label>Natija</label>
                        <input type="text"  class="form-control width_all">
                        <small class="width_error_3 hidden text-danger">Maydon bo'sh bo'lishi mumkin emas!</small>
                        <small class="width_number_3 hidden text-danger">Maydon faqat sondan iborat bo'lishi kere!</small>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="exampleTabsReverseTwo" role="tabpanel">
                <div class="row">
                    <div class="col-md-3 div_height">
                        <label>1-qiymat</label>
                        <input type="text"  class="form-control height_1">
                        <small class="height_error_1 hidden text-danger">Maydon bo'sh bo'lishi mumkin emas!</small>
                        <small class="height_number_1 hidden text-danger">Maydon faqat sondan iborat bo'lishi kere!</small>
                        <small class="height_sort_1 hidden text-danger">1-maydon 2-maydondan kichkina bo'lishi kerak!</small>
                        <small class="height_false hidden text-danger">Bunday oraliq mavjud!</small>
                    </div>
                    <div class="col-md-3">
                        <label>2-qiymat</label>
                        <input type="text" class="form-control height_2">
                        <small class="height_error_2 hidden text-danger">Maydon bo'sh bo'lishi mumkin emas!</small>
                        <small class="height_number_2 hidden text-danger">Maydon faqat sondan iborat bo'lishi kere!</small>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <label>Natija_2</label>
                        <input type="text"  class="form-control height_all">
                        <small class="height_error_3 hidden text-danger">Maydon bo'sh bo'lishi mumkin emas!</small>
                        <small class="height_number_3 hidden text-danger">Maydon faqat sondan iborat bo'lishi kere!</small>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="exampleTabsReverseThree" role="tabpanel">
                <div class="row">
                    <div class="col-md-3 div_weight">
                        <label>1-qiymat</label>
                        <input type="text"  class="form-control weight_1">
                        <small class="weight_error_1 hidden text-danger">Maydon bo'sh bo'lishi mumkin emas!</small>
                        <small class="weight_number_1 hidden text-danger">Maydon faqat sondan iborat bo'lishi kere!</small>
                        <small class="weight_sort_1 hidden text-danger">1-maydon 2-maydondan kichkina bo'lishi kerak!</small>
                        <small class="weight_false hidden text-danger">Bunday oraliq mavjud!</small>
                    </div>
                    <div class="col-md-3">
                        <label>2-qiymat</label>
                        <input type="text" class="form-control weight_2">
                        <small class="weight_error_2 hidden text-danger">Maydon bo'sh bo'lishi mumkin emas!</small>
                        <small class="weight_number_2 hidden text-danger">Maydon faqat sondan iborat bo'lishi kere!</small>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-3">
                        <label>Natija_3</label>
                        <input type="text"  class="form-control weight_all">
                        <small class="weight_error_3 hidden text-danger">Maydon bo'sh bo'lishi mumkin emas!</small>
                        <small class="weight_number_3 hidden text-danger">Maydon faqat sondan iborat bo'lishi kere!</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-success save mt-20" data-id="<?php echo $model->id; ?>" type="button">Saqlash</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel mt-50" id="exampleWizardFormContainer">
    <div class="panel-heading">
        <h3 class="panel-title">Pearls Steps</h3>
    </div>
    <div class="panel-body">
        <!-- Steps -->
        <div class="pearls row">
            <div class="pearl current col-4 active" aria-expanded="true">
                <div class="pearl-icon"><i class="icon wb-user" aria-hidden="true"></i></div>
                <span class="pearl-title">Account Info</span>
            </div>
            <div class="pearl col-4 disabled" aria-expanded="false">
                <div class="pearl-icon"><i class="icon wb-payment" aria-hidden="true"></i></div>
                <span class="pearl-title">Billing Info</span>
            </div>
            <div class="pearl col-4 disabled" aria-expanded="false">
                <div class="pearl-icon"><i class="icon wb-check" aria-hidden="true"></i></div>
                <span class="pearl-title">Confirmation</span>
            </div>
        </div>
        <!-- End Steps -->

        <!-- Wizard Content -->
        <form id="exampleFormContainer" novalidate="novalidate" class="fv-form fv-form-bootstrap">
            <button type="submit" class="fv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
            <div class="wizard-content">
                <div class="wizard-pane active" id="exampleAccountOne" role="tabpanel" aria-expanded="true">
                    <div class="form-group">
                        <label class="form-control-label" for="inputUserNameOne">Username</label>
                        <input type="text" class="form-control" id="inputUserNameOne" name="username"
                               required="required" data-fv-field="username">
                        <small class="invalid-feedback" data-fv-validator="notEmpty" data-fv-for="username"
                               data-fv-result="NOT_VALIDATED" style="display: none;">The username is
                            required</small></div>
                    <div class="form-group">
                        <label class="form-control-label" for="inputPasswordOne">Password</label>
                        <input type="password" class="form-control" id="inputPasswordOne" name="password"
                               required="required" data-fv-field="password">
                        <small class="invalid-feedback" data-fv-validator="notEmpty" data-fv-for="password"
                               data-fv-result="NOT_VALIDATED" style="display: none;">The password is
                            required</small></div>
                </div>
                <div class="wizard-pane" id="exampleBillingOne" role="tabpanel" aria-expanded="false">
                    <div class="form-group">
                        <label class="form-control-label" for="inputCardNumberOne">Card Number</label>
                        <input type="text" class="form-control" id="inputCardNumberOne" name="number"
                               placeholder="Card number" data-fv-field="number">
                        <small class="invalid-feedback" data-fv-validator="notEmpty" data-fv-for="number"
                               data-fv-result="NOT_VALIDATED" style="display: none;">The credit card number is not
                            valid</small></div>
                    <div class="form-group">
                        <label class="form-control-label" for="inputCVVOne">CVV</label>
                        <input type="text" class="form-control" id="inputCVVOne" name="cvv" placeholder="CVV"
                               data-fv-field="cvv">
                        <small class="invalid-feedback" data-fv-validator="notEmpty" data-fv-for="cvv"
                               data-fv-result="NOT_VALIDATED" style="display: none;">The CVV number is
                            required</small></div>
                </div>
                <div class="wizard-pane" id="exampleGettingOne" role="tabpanel" aria-expanded="false">
                    <div class="text-center my-20">
                        <h4>Please confrim your order.</h4>
                        <div class="table-responsive">
                            <table class="table table-hover text-right">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Description</th>
                                    <th class="text-right">Quantity</th>
                                    <th class="text-right">Unit Cost</th>
                                    <th class="text-right">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-left">Server hardware purchase</td>
                                    <td>32</td>
                                    <td>$75</td>
                                    <td>$2152</td>
                                </tr>
                                <tr>
                                    <td class="text-center">2</td>
                                    <td class="text-left">Office furniture purchase</td>
                                    <td>15</td>
                                    <td>$169</td>
                                    <td>$4169</td>
                                </tr>
                                <tr>
                                    <td class="text-center">3</td>
                                    <td class="text-left">Company Anual Dinner Catering</td>
                                    <td>69</td>
                                    <td>$49</td>
                                    <td>$1260</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- Wizard Content -->
        <div class="wizard-buttons"><a class="btn btn-default btn-outline disabled"
                                       href="#exampleWizardFormContainer" data-wizard="back"
                                       role="button">Back</a><a class="btn btn-primary btn-outline float-right"
                                                                href="#exampleWizardFormContainer"
                                                                data-wizard="next" role="button">Next</a><a
                    class="btn btn-success btn-outline float-right hidden-xs-up" href="#exampleWizardFormContainer"
                    data-wizard="finish" role="button">Finish</a></div>
    </div>
</div>

<style>
    .material-index{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
    .hidden{
        display: none;
    }
</style>

    <script>
        Breakpoints();
    </script>

<?php
$js = <<<JS
    $(document).on('click', '.delete', function(){
        if (confirm("O'chirishni istaysizmi?")) {
            var id = $(this).attr('data-delete')
            $.ajax({
                url: 'delete-material',
                data: {
                    id: id,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                }
            })
        }
    })
    
    $(document).on('click', '.save', function(){
        var width_1 = parseInt($('.width_1').val())
        var width_2 = parseInt($('.width_2').val())
        var width_all = $('.width_all').val()
        var height_1 = parseInt($('.height_1').val())
        var height_2 = parseInt($('.height_2').val())
        var height_all = $('.height_all').val()
        var weight_1 = parseInt($('.weight_1').val())
        var weight_2 = parseInt($('.weight_2').val())
        var weight_all = $('.weight_all').val()
        var id = $(this).attr("data-id")
        if (width_1 != '' && width_2 != '' && width_all != '' && height_1 != '' && height_2 != '' && height_all != '' && weight_1 != '' && weight_2 != '' && weight_all != '' && $.isNumeric($(".width_1").val()) && $.isNumeric($(".width_2").val()) && $.isNumeric($(".width_all").val()) && $.isNumeric($(".height_1").val()) && $.isNumeric($(".height_2").val()) && $.isNumeric($(".height_all").val()) && $.isNumeric($(".weight_1").val()) && $.isNumeric($(".weight_2").val()) && $.isNumeric($(".weight_all").val())){
            $('.width_1').removeClass('is-invalid');
            $('.width_2').removeClass('is-invalid');
            $('.width_all').removeClass('is-invalid');
            $('.width_error_1').addClass('hidden');
            $('.width_error_2').addClass('hidden');
            $('.width_error_3').addClass('hidden');
            $('.width_number_1').addClass('hidden');
            $('.width_number_2').addClass('hidden');
            $('.width_number_3').addClass('hidden');
            $('.height_1').removeClass('is-invalid');
            $('.height_2').removeClass('is-invalid');
            $('.height_all').removeClass('is-invalid');
            $('.height_error_1').addClass('hidden')
            $('.height_error_2').addClass('hidden')
            $('.height_error_3').addClass('hidden')
            $('.weight_1').removeClass('is-invalid');
            $('.weight_2').removeClass('is-invalid');
            $('.weight_all').removeClass('is-invalid');
            $('.weight_error_1').addClass('hidden')
            $('.weight_error_2').addClass('hidden')
            $('.weight_error_3').addClass('hidden')
            if (width_1 < width_2 && height_1 < height_2 && weight_1 < weight_2){
                $('.width_1').removeClass('is-invalid');
                $('.width_false').addClass('hidden');
                $('.height_1').removeClass('is-invalid');
                $('.height_false').addClass('hidden')
                $('.weight_1').removeClass('is-invalid');
                $('.weight_false').addClass('hidden')
                $.ajax({
                    url: 'material-settings',
                    data: {
                        id: id,
                        width_1: width_1,
                        width_2: width_2,
                        width_all: width_all,
                        height_1: height_1,
                        height_2: height_2,
                        height_all: height_all,
                        weight_1: weight_1,
                        weight_2: weight_2,
                        weight_all: weight_all
                    },
                    dataType: 'json',
                    type: 'get',
                    success: function(response) {
                        if (response.status == 'success') {
                            location.reload()
                        }
                        if (response.status == 'width_sort') {
                            $('.width_1').removeClass('is-invalid');
                            $('.width_sort_1').addClass('hidden'); 
                            
                            $('.width_1').addClass('is-invalid');
                            $('.width_false').removeClass('hidden');           
                        }
                        if (response.status == 'height_sort') {
                            $('.height_1').removeClass('is-invalid');
                            $('.height_sort_1').addClass('hidden') 
                    
                            $('.height_1').addClass('is-invalid');
                            $('.height_false').removeClass('hidden');           
                        }
                        if (response.status == 'weight_sort') {
                            $('.weight_1').removeClass('is-invalid');
                            $('.weight_sort_1').addClass('hidden') 
                    
                            $('.weight_1').addClass('is-invalid');
                            $('.weight_false').removeClass('hidden');           
                        }
                    }
                })
            } else {
                if (width_1 >= width_2){
                    $('.width_1').addClass('is-invalid');
                    $('.width_sort_1').removeClass('hidden');         
                }
                if (height_1 >= height_2){
                    $('.height_1').addClass('is-invalid');
                    $('.height_sort_1').removeClass('hidden')   
                }
                if (weight_1 >= weight_2){
                    $('.weight_1').addClass('is-invalid');
                    $('.weight_sort_1').removeClass('hidden')   
                }
            }
        } else {
            if (width_1 == ''){
                $('.width_1').addClass('is-invalid');
                $('.width_error_1').removeClass('hidden')  
            } else {
                if ($.isNumeric($(".width_1").val())){
                    $('.width_1').removeClass('is-invalid')
                    $('.width_error_1').addClass('hidden')
                    $('.width_number_1').addClass('hidden') 
                } else {
                    $('.width_1').addClass('is-invalid');
                    $('.width_number_1').removeClass('hidden')
                    $('.width_error_1').addClass('hidden')    
                }
            }
            if (width_2 == ''){
                $('.width_2').addClass('is-invalid');
                $('.width_error_2').removeClass('hidden')  
            } else {
                if ($.isNumeric($(".width_2").val())){
                    $('.width_2').removeClass('is-invalid')
                    $('.width_error_2').addClass('hidden')
                    $('.width_number_2').addClass('hidden') 
                } else {
                    $('.width_2').addClass('is-invalid');
                    $('.width_number_2').removeClass('hidden')
                    $('.width_error_2').addClass('hidden')    
                }
            }
            if (width_all == ''){
                $('.width_all').addClass('is-invalid');
                $('.width_error_3').removeClass('hidden')  
            } else {
                if ($.isNumeric($(".width_all").val())){
                    $('.width_all').removeClass('is-invalid')
                    $('.width_error_3').addClass('hidden')
                    $('.width_number_3').addClass('hidden') 
                } else {
                    $('.width_all').addClass('is-invalid');
                    $('.width_number_3').removeClass('hidden')
                    $('.width_error_3').addClass('hidden')    
                }
            }
            if (weight_1 == ''){
                $('.weight_1').addClass('is-invalid');
                $('.weight_error_1').removeClass('hidden')  
            } else {
                if ($.isNumeric($(".weight_1").val())){
                    $('.weight_1').removeClass('is-invalid')
                    $('.weight_error_1').addClass('hidden')
                    $('.weight_number_1').addClass('hidden') 
                } else {
                    $('.weight_1').addClass('is-invalid');
                    $('.weight_number_1').removeClass('hidden')
                    $('.weight_error_1').addClass('hidden')    
                }
            }
            if (weight_2 == ''){
                $('.weight_2').addClass('is-invalid');
                $('.weight_error_2').removeClass('hidden')  
            } else {
                if ($.isNumeric($(".weight_2").val())){
                    $('.weight_2').removeClass('is-invalid')
                    $('.weight_error_2').addClass('hidden')
                    $('.weight_number_2').addClass('hidden') 
                } else {
                    $('.weight_2').addClass('is-invalid');
                    $('.weight_number_2').removeClass('hidden')
                    $('.weight_error_2').addClass('hidden')    
                }
            }
            if (weight_all == ''){
                $('.weight_all').addClass('is-invalid');
                $('.weight_error_3').removeClass('hidden')  
            } else {
                if ($.isNumeric($(".weight_all").val())){
                    $('.weight_all').removeClass('is-invalid')
                    $('.weight_error_3').addClass('hidden')
                    $('.weight_number_3').addClass('hidden') 
                } else {
                    $('.weight_all').addClass('is-invalid');
                    $('.weight_number_3').removeClass('hidden')
                    $('.weight_error_3').addClass('hidden')    
                }
            }
            if (height_1 == ''){
                $('.height_1').addClass('is-invalid');
                $('.height_error_1').removeClass('hidden')  
            } else {
                if ($.isNumeric($(".height_1").val())){
                    $('.height_1').removeClass('is-invalid')
                    $('.height_error_1').addClass('hidden')
                    $('.height_number_1').addClass('hidden') 
                } else {
                    $('.height_1').addClass('is-invalid');
                    $('.height_number_1').removeClass('hidden')
                    $('.height_error_1').addClass('hidden')    
                }
            }
            if (height_2 == ''){
                $('.height_2').addClass('is-invalid');
                $('.height_error_2').removeClass('hidden')  
            } else {
                if ($.isNumeric($(".height_2").val())){
                    $('.height_2').removeClass('is-invalid')
                    $('.height_error_2').addClass('hidden')
                    $('.height_number_2').addClass('hidden') 
                } else {
                    $('.height_2').addClass('is-invalid');
                    $('.height_number_2').removeClass('hidden')
                    $('.height_error_2').addClass('hidden')    
                }
            }
            if (height_all == ''){
                $('.height_all').addClass('is-invalid');
                $('.height_error_3').removeClass('hidden')  
            } else {
                if ($.isNumeric($(".height_all").val())){
                    $('.height_all').removeClass('is-invalid')
                    $('.height_error_3').addClass('hidden')
                    $('.height_number_3').addClass('hidden') 
                } else {
                    $('.height_all').addClass('is-invalid');
                    $('.height_number_3').removeClass('hidden')
                    $('.height_error_3').addClass('hidden')    
                }
            }
        }
    })
JS;
$this->registerJs($js);
?>