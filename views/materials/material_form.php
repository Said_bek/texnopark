<?php

    if (isset($code) and !empty($code)){
        ?>
            <input disabled value="<?php echo \app\controllers\numberZero($code->code_number + 1) ?>" type="text" id="code_number" class="form-control text-center" name="count">
        <?php

    } else {
        ?>
            <input disabled value="0000001" type="text" id="code_number" class="form-control input-field">
        <?php
    }

?>