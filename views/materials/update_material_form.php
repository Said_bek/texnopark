<?php
namespace app\controllers;
use app\models\Marks;
use Yii;
?>

<?php

if (isset($model) and !empty($model)){ ?>
    <div class="material_form">
        <h2 class="m-0">Material qo'shish</h2>
        <div class="row mt-20">
            <div class="col-md-6">
                <div class="text-left">
                    <label for="mySelect2" style="font-size: 15px">Material nomini kriting</label>
                    <input type="text" id="select2" class="form-control" name="" value='<?php
                    echo $model['name'];
                    ?>'>
                    <small class="material_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                </div>
            </div>
            <div class="col-md-6">
                <div class="select2-primary text-left">
                    <label for="mySelect2" style="font-size: 15px">Markalar</label>
                    <select class="form-control mySelect2" id="select3" multiple="multiple" data-plugin="select2">
                        <?php foreach ($mark as $key => $value): ?>
                            <?php
                            $select = '';
                            if (in_array($value->id, $arr)){
                                $select = 'selected';
                            }
                            ?>
                            <option value="<?php echo $value->id; ?>" <?php echo $select ?>>
                                <?php
                                    echo $value->name;
                                ?>
                            </option>
                        <?php endforeach ?>
                        ?>
                    </select>
                    <small class="mark_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                </div>
            </div>
        </div>
        <div class="row mt-10">
            <div class="col-md-4">
                <div class="text-left">
                    <label for="mySelect2" style="font-size: 15px">Material kodini kiriting</label>
                    <input type="text" id="code_name" class="form-control" name="" value='<?php
                    echo $model['code_name'];
                    ?>'>
                    <small class="code_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                </div>
            </div>
            <div class="col-md-2">
                <div class="text-left mt-30 div_code">
                    <input disabled type="text" id="code_number" class="form-control" name="" value='<?php
                    echo numberZero($model['code_number']);
                    ?>'>
                </div>
            </div>
            <div class="col-md-6">
                <div class="select2-primary text-left">
                    <label for="mySelect2" style="font-size: 15px">Material parametr kiriting</label>
                    <select class="form-control mySelect1" id="select1"  data-plugin="select2">
                        <?php
                        if (isset($parameter) and !empty($parameter)){
                            foreach ($parameter as $key => $valueParameter){ ?>
                                <option <?php if($valueParameter->id == $model['parameter_id']) echo"selected"; ?> value="<?php echo $valueParameter->id ?>_issetParameter">
                                    <?php echo $valueParameter->name; ?>
                                </option>
                            <?php }
                        }
                        ?>
                    </select>
                    <small class="parameter_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                    <small class="parameter_isset hidden text-danger" style="font-size: 12px">Bunday parameter mavjud!</small>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 mt-5">
                <button data-id="<?= $id ?>" type="button" class="btn btn-success float-right mt-10 update_material">Saqlash</button>
            </div>
        </div>
    </div>
<?php }
?>

<style>
    .hidden {
        display: none;
    }

    .material_form{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>

<?php
$js = <<<JS
    $(function (){
        $("#select1").select2({
            tags: true
        });
    })
    
    $(document).on('click', '.update_material', function(){
        var material = $("#select2").val()
        var mark = $("#select3").val()
        var code = $("#code_name").val()
        var parameter = $("#select1").val()
        var id = $(this).attr('data-id')
        if (material != '' && mark != '' && code != '' && parameter != ''){
            $.ajax({
                url: 'update-material',
                data: {
                  material: material,
                  mark: mark,
                  code: code,
                  parameter: parameter,
                  id: id
                }, 
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        window.location.replace("/materials/index");
                    }
                    if (response.status == 'error_material') {
                        $('#select2').addClass('is-invalid');
                        $('.material_error').removeClass('hidden');     
                    }
                    if (response.status == 'error_mark') {
                        $('#select3').addClass('is-invalid');
                        $('.mark_error').removeClass('hidden');   
                    }
                    if (response.status == 'error_code') {
                        $('#code_name').addClass('is-invalid');
                        $('.code_error').removeClass('hidden');  
                    }
                    if (response.status == 'error_parameter') {
                        $('#select1').addClass('is-invalid');
                        $('.parameter_error').removeClass('hidden');     
                    }
                    if (response.status == 'isset_parameter') {
                        $('#select1').addClass('is-invalid');
                        $('.parameter_isset').removeClass('hidden');     
                    }
                }
            })
        } else {
            if (material == ''){
                $('#select2').addClass('is-invalid');
                $('.material_error').removeClass('hidden');        
            } else {
                $('#select2').removeClass('is-invalid');
                $('.material_error').addClass('hidden');    
            }
            
            if (mark == ''){
                $('#select3').siblings().find(".select2-selection").css({"border-color": "red", "border-weight":"5px", "border-style":"solid"});
                $('.mark_error').removeClass('hidden');        
            } else {
                $('#select3').removeClass('is-invalid');
                $('.mark_error').addClass('hidden');    
            }
            
            if (code == ''){
                $('#code_name').addClass('is-invalid');
                $('.code_error').removeClass('hidden');        
            } else {
                $('#code_name').removeClass('is-invalid');
                $('.code_error').addClass('hidden');    
            }
            
            if (parameter == ''){
                $('#select1').siblings().find(".select2-selection").css({"border-color": "red", "border-weight":"5px", "border-style":"solid"});
                $('.parameter_error').removeClass('hidden');        
            } else {
                $('#select1').removeClass('is-invalid');
                $('.parameter_error').addClass('hidden');    
            }
        }
    })
JS;
$this->registerJs($js);
?>