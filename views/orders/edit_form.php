<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use app\models\CategoryDocs;

// use app\models\OrdersDocs;
use app\models\Docs;


/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>


<h1>Buyurtmani o'zgartirish</h1>
<br>

<!-- <form id="order_forma" action="#"> -->
<div class="row">
    <div class="col-md-6">
        <div class="select2-primary">
            <label for="mySelect2" style="font-size: 15px">Kategoriyani tanlang</label>
            <select class="form-control cat mySelect2" data-plugin="select2">
                <?php
                // $getCategory = Category::find()->all();
                if (isset($getCategory) && !empty($getCategory)) {
                    foreach ($getCategory as $key => $value) { ?>
                        <option selected="<?= $model['category'] ?>" value="<?php echo $value->id; ?>">
                            <?php echo $value->title; ?>
                        </option>
                <?php }
                }
                ?>
            </select>
        </div>

    </div>
    <div class="col-md-6 div_pr order_docs_div">
        <div class="select2-primary">
            <label for="mySelect2" style="font-size: 15px">Hujjat tanlang</label>
            <small class="error_branch hidden text-danger">( Maydon bo`sh bo'lishi mumkin emas! )</small>
            <select class="form-control order_docs mySelect2" id="select3" multiple="multiple" data-plugin="select2">
                <?php
                    if (isset($docs_model) && !empty($docs_model)) {
                        foreach ($docs_model as $key => $value) { ?>
                            <option class="documents" value="<?= $value['id'] ?>"
                                    selected="selected"><?= $value['docs'] ?></option>
                    <?php }
                    }
                ?>
            </select>
        </div>
    </div>
</div>

<br>
<div class="row">
    <div class="col-md-6 div_phone_number">
        <label>Telefon raqam</label>
        <input type="text" value="<?= $model['phone_number'] ?>" class="phone_number form-control">
    </div>
    <div class="col-md-6 div_phone_number_2">
        <label>Telefon raqam 2</label>
        <input type="text" value="<?= $model['phone_number_2'] ?>" class="form-control phone_number_2">
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-6 div_client_name">
        <label>Mijoz ismi</label>
        <input type="text" value="<?= $model['face_name'] ?>" class="client_name form-control">
    </div>
    <div class="col-md-6 div_email">
        <label>Mijoz emaili</label>
        <input type="text" value="<?= $model['email'] ?>" class="form-control client_email">
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-6 div_company_name">
        <label>Kompaniya nomi</label>
        <input type="text" value="<?= $model['company_name'] ?>" class="company_name form-control">
    </div>
    <div class="col-md-6">
        <label>Mijoz manzili</label>
        <input type="text" value="<?= $model['country'] ?>" class="form-control  country">
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-6 div_price">
        <label>Buyurtma narxi</label>
        <input type="number" value="<?= $model['price'] ?>" class="price form-control">

    </div>
    <div class="col-md-6 div_pr_name">
        <label>Loyiha nomi</label>
        <input type="text" value="<?= $model['order_name'] ?>" class="project_name form-control col-xl-12 col-md-6">
        <input type="hidden" value="<?= $model['id'] ?>" class="order_id form-control col-xl-12 col-md-6">
    </div>
</div>
<br><br>
<div class="row">
    <div class="col-md-4">
        <label>Narx taqdim etish sanasi</label>
        <div class="example-wrap">
            <div class="example datepair-wrap" data-plugin="datepair">
                <div class="input-daterange-wrap">
                    <div class="input-daterange">
                        <div class="input-group div_date">
                            <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="icon wb-calendar" aria-hidden="true"></i>
                        </span>
                            </div>
                            <input value="<?= $model_data ?>" type="text" data-date-format='dd.mm.yyyy' id="datepicker"
                                   class="datepicker price_date form-control datepair-date datepair-start"
                                   data-plugin="datepicker">
                        </div>
                        <div class="input-group div_time">
                            <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="icon wb-time" aria-hidden="true"></i>
                        </span>
                            </div>
                            <input value="<?= $model_time ?>" type="text" class="timepicker price_time form-control"
                                   data-plugin="clockpicker" data-autoclose="true">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <label>Buyurtma darajasini tanlang</label>
        <div style="margin-left: -20px;" class=" col-xl-12">
            <div class="example">
                <select class="proirity form-control" data-plugin="select2">
                    <optgroup label="Buyurtma darajasini tanlang">
                        <option <?php if ($model['priority'] == 0) echo "selected"; ?> value="0">Oddiy</option>
                        <option <?php if ($model['priority'] == 1) echo "selected"; ?> value="1">Tezkor</option>
                        <option <?php if ($model['priority'] == 2) echo "selected"; ?> value="2">Kechiktirilgan</option>
                    </optgroup>
                </select>
            </div>
        </div>

    </div>
    <div class="col-md-4">
        <label>Buyurtma qayerdan qabul qilinyapti</label>
        <div style="margin-left: -20px;" class=" col-xl-12">
            <div class="example">
                <select class="form-control from" data-plugin="select2">
                    <optgroup label="Qayerdan">
                        <option <?php if ($model['order_from'] == 0) echo "selected"; ?> value="0">IN</option>
                        <option <?php if ($model['order_from'] == 1) echo "selected"; ?> value="1">OUT</option>
                    </optgroup>
                </select>
            </div>
        </div>
    </div>
</div>
<button class="btn btn-success save float-right">Saqlash</button>

<style type="text/css">
    .docs_fail {
        border: solid red 1px;
    }

    .hidden {
        display: none;
    }
</style>

<?php
$js = <<<JS
$(function(){
    $("#select3").select2({
        tags: true
    });
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
});


$(document).on("click", '.save', function() {
    var docs = $('.order_docs').val();  
    var cat = $('.cat').val();
    var project_name = $('.project_name').val();
    var phone_number = $('.phone_number').val();
    var phone_number_2 = $('.phone_number_2').val();
    var company_name = $('.company_name').val();
    var client_name = $('.client_name').val();
    var client_email = $('.client_email').val();
    var price = $('.price').val();
    var country = $('.country').val();
    var price_date = $('.price_date').val();
    var price_time = $('.price_time').val();
    var from = $('.from').val();
    var proirity = $('.proirity').val();
    var order_id = $('.order_id').val();
    $.ajax({
        data: {
            docs: docs,
            cat: cat,
            project_name: project_name,
            price: price,
            phone_number: phone_number,
            phone_number_2: phone_number_2,
            client_name: client_name,
            client_email: client_email,
            country: country,
            company_name: company_name,
            price_date: price_date,
            price_time: price_time,
            from: from,
            proirity: proirity,
            order_id: order_id
        },  
        url: '/orders/update_save',
        type: 'get',
        dataType: 'json',
            success: function(response) {
                if (response.status == "success") {
                    window.location.href = '/orders/index';
                }
                else if(response.status == "project_name_fail") 
                {
                    $('.project_name').addClass('is-invalid');
                    var input = $(`<div class="invalid-feedback">Loyiha nomini kiritmadingiz!</div>`);
                    $('.div_pr_name').append(input);
                }
                else if(response.status == "phone_number_fail") 
                {
                    $('.phone_number').addClass('is-invalid');
                    var input = $(`<div class="invalid-feedback">Telefon raqam kiritmadingiz!</div>`);
                    $('.div_phone_number').append(input);
                }
                else if(response.status == "phone_number_2_fail") 
                {
                    $('.phone_number_2').addClass('is-invalid');
                    var input = $(`<div class="invalid-feedback">Telefon raqam kiritmadingiz!</div>`);
                    $('.div_phone_number_2').append(input);
                }
                else if(response.status == "client_name_fail") 
                {
                    $('.client_name)').addClass('is-invalid');
                    var input = $(`<div class="invalid-feedback">Ism maydonini touch(filename)ldirmadingiz!</div>`);
                    $('.div_client_name').append(input);
                }
                else if(response.status == "client_email_fail") 
                {
                    $('.client_email').addClass('is-invalid');
                    var input = $(`<div class="invalid-feedback">Email pochta kiritmadingiz!</div>`);
                    $('.div_email').append(input);
                }
                else if(response.status == "price_date_fail") 
                {
                    $('.price_date').addClass('is-invalid');
                    var input = $(`<div class="invalid-feedback">Narx taqdim etadigan sana kiritmadingiz!</div>`);
                    $('.div_date').append(input);
                }
                else if(response.status == "price_time_fail") 
                {
                    $('.price_time').addClass('is-invalid');
                    var input = $(`<div class="invalid-feedback">Narx taqdim etadigan sana kiritmadingiz!</div>`);
                    $('.div_time').append(input);
                }
                // else if(response.status == "docs_fail") 
                // {
                //     $('.documents').addClass('is-invalid');
                //     var input = $(`<div class="invalid-feedback">Hujjat tanlamadingiz!</div>`);
                //     $('.order_docs_div').append(input);
                // }
                else if(response.status == "company_name_fail") 
                {
                    $('.company_name').addClass('is-invalid');
                    var input = $(`<div class="invalid-feedback"> Kompaniya nomi maydonini toldiring!</div>`);
                    $('.div_company_name').append(input);
                }
                else if(response.status == "price_fail") 
                {
                    $('.price').addClass('is-invalid');
                    var input = $(`<div class="invalid-feedback"> Narx nomi maydonini toldiring!</div>`);
                    $('.div_price').append(input);
                }
                else if(response.status == 'docs_fail'){
                    $('.order_docs').parent().addClass('has-error')
                    $('.error_branch').removeClass('hidden')
                }

            }
    })
});

$(document).on('change', '.cat', function() {
  var cat = $('.cat').val();
  $.ajax({
        url: '/orders/cat_filter_update',
        data: {
            cat: cat
        },
        type: 'get',
        dataType: 'json',
        success:function(response){
            if(response.status == "success"){
                $('#select3').html(response.content)
            }
        }
    })
});

JS;
$this->registerJs($js);
?> 


