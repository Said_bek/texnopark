<?php
// require_once 'dompdf/autoload.inc.php';
// require 'vendor/autoload.php';

use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();


$html = '

	<!DOCTYPE html>
	<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

		<style>
			*{
				font-family: DejaVu Sans, sans-serif!important;
			}
			.little_title{
				font-size: 13px;
				font-weight: bold;
				margin-top: -10px;
			}
			.row{
				display: flex;
			}
			img {
			  	display: inline-block;
			}
			.page_break { 
				page-break-before: always; 
			}

		</style>
	</head>
	<body>

	<div>
		<div>
			<div style=" margin-left: 20px; ">
				<img style=" width: 180px; height: 130px; margin-left: -10px; margin-top: 20px;"  src="../web/uploads/top_logo_1.jpg">
				<img style="width: 140px; height: 130px; margin-top: 20px; margin-left: -8px;"  src="../web/uploads/top_logo_2.png">
				<p style="font-size: 35px; margin-top: -20px; margin-left: -5px;">ПРОИЗВОДСТВО</p>
				<p style="font-size: 18px; margin-top: -40px; margin-left: -5px; color: blue;"><b>ПРЕСС - ФОРМ И ШТАМПОВ</b></p>
			</div>
			<div style="margin-left: 650px; margin-top: -300px;">
				<p style="margin-left:53px; font-size: 18px;"><b><span style="color: blue;">OOO «TEXNOPARK»</span>  MAZE MOLD</b></p>
				<p style="font-size: 14px; margin-left: -45px; margin-top: -20px;"><b>РУз. г.Ташкент,Яшнабадский р-н, ул.Элбек, 61-дом</b></p>
				<p style="font-size: 14px; margin-left: -168px; margin-top: -15px;"><b>Юнусабадский филиал “НБУ ВЭД РУз”Р/с: 20208000605090819023</b></p>
				<p style="font-size: 14px; margin-left: -118px; margin-top: -15px;"><b>МФО: 00836   ИНН: 306 493 973 ОКЭД: 27510  ОКПО: 28783966</b></p>
				<p style="font-size: 14px; margin-left: 193px; margin-top: -15px;"><b>Тел: +998 71210-20-20</b></p>
				<div style="margin-left: 100px; margin-top: -27px;">
					<img style=" width: 100px; height: 80px; margin-left: -10px; margin-top: 20px;"  src="../web/uploads/iso_90.png">					
					<img style=" width: 100px; height: 80px; margin-left: -10px; margin-top: 20px;"  src="../web/uploads/iso_90.png">					
					<img style=" width: 100px; height: 80px; margin-left: -10px; margin-top: 20px;"  src="../web/uploads/iso_90.png">					
				<div>
			</div>
		</div>
	</div>
	<hr>
	<hr style="margin-top: -5px;">

	<div>
		<center  style="margin-top: 40px;">
			<h2><span style="color: blue;">ООО ТЕХНОПАРК</span> Коммерческое Предложение</h2>
			<h2 style="margin-top: -25px; color: rgba(128,0,128);">ПРОИЗВОДСТВО ПРЕСС - ФОРМ И ШТАМПОВ</h2>
		</center>
	</div>

	<div>
		<TABLE border="1" WIDTH="100%" CELLPADDING="4" CELLSPACING="3">
		   <TR>
		      <TH style="width:28%;"> Код предложения: </TH>
		      <TH style="width:45%;">' . $model['order_id'] . '</TH>
		      <TH style="width:12%;">Дата: </TH>
		      <TH style="width:15%;">' . $current_date . '</TH>
		   </TR>
		   <TR ALIGN="CENTER">
		      <th style="width:28%;" >Название компании :</th>
		      <th style="width:72%;" colspan="3">' . $model['company_name'] . '</th>
		   </TR>
		   <TR ALIGN="CENTER">
		      <th style="width:28%;" >Контактное лицо :</th>
		      <th style="width:72%;" colspan="3">' . $model['face_name'] . '</th>
		   </TR>
		   <TR ALIGN="CENTER">
		      <th style="width:28%;" >Страна :</th>
		      <th style="width:72%;" colspan="3">' . $model['country'] . '</th>
		   </TR>
		   <TR ALIGN="CENTER">
		      <th style="width:28%;" >Телефон :</th>
		      <th style="width:72%;" colspan="3">' . $model['phone_number'] . '</th>
		   </TR>
		   <TR ALIGN="CENTER">
		      <th style="width:28%;" >Телефон 2 :</th>
		      <th style="width:72%;" colspan="3">' . $model['phone_number_2'] . '</th>
		   </TR>
		   <TR ALIGN="CENTER">
		      <th style="width:28%;" >E-mail :</th>
		      <th style="width:72%;" colspan="3">' . $model['email'] . '</th>
		   </TR>
		   <TR ALIGN="CENTER">
		      <th style="width:28%;" >Тема предложения :</th>
		      <th style="width:72%;" colspan="3">tema</th>
		   </TR>
		   <TR ALIGN="CENTER">
		      <th style="width:28%;" >Название проекта :</th>
		      <th style="width:72%;" colspan="3">' . $model['order_name'] . '</th>
		   </TR>
		   <TR ALIGN="CENTER">
		      <th style="width:28%;" >Тип проекта :</th>
		      <th style="width:72%;" colspan="3">' . $model['category'] . '</th>
		   </TR>
		</TABLE>
	</div>


	<div style="margin-top: 25px;">
		<h4 style="margin-left: 100px;"><i><b>Уважаемый:</b></i></h4>
		<h4 style="margin-left: 200px; margin-top: 40px;"><i><b>Благодарим вас за ваш любезный интерес и доверие к <span style="color: blue;"> ТЕХНОПАРКУ.</span></b></i></h4>
		<h4 style="margin-left: 200px; margin-top: -20px;"><i><b>Ниже вы можете найти наше предложение по вашему любезному запросу.</b></i></h4>
	<div>

	<div style="margin-left: 750px; margin-top: 80px;">
		<h4><i>Ташходжаев Азизходжа Н.</i></h4>
		<h4 style="margin-top: -20px; margin-left: 10px;"><i>Менеджер по продажам</i></h4>
		<h4 style="margin-top: -20px; margin-left: 50px;"><i>+99 899 093 09 09</i></h4>
	</div>

	<div style="margin-left: 820px; margin-top: -20px;">
		<img style=" width: 150px; height: 100px; margin-left: -10px; margin-top: 20px;"  src="../web/uploads/sign.png">
	</div>

	<div class="container"  style="margin-top: -10px;">
		<div class="row"> 	
			<div class="col-md-6">
				<h4 style="margin-left: 10px; color: blue;"><i>Наши Преимущества:</i></h4>				
				<ul style="margin-top: -5px;">
					<li><b>Опытные специалисты</b></li>
					<li><b>Современные станки;</b></li>
					<li><b>Качественные материалы и комплектующие части;</b></li>
					<li><b>Высокая точность обработки металла;</b></li>
					<li><b>Высокий уровень проверки качества готовой продукции;</b></li>
					<li><b>Гарантия качества.</b></li>
				</ul>
			</div>
			<div class="col-md-6" style="margin-left: 750px; margin-top: -50;">
				<img style=" width: 150px; height: 100px;"  src="../web/uploads/jac.png">	
			</div>	
		</div>
	</div>
	<div class="page_break"></div>

	<div>
		<div>
			<div style=" margin-left: 20px; ">
				<img style=" width: 180px; height: 130px; margin-left: -10px; margin-top: 20px;"  src="../web/uploads/top_logo_1.jpg">
				<img style="width: 140px; height: 130px; margin-top: 20px; margin-left: -8px;"  src="../web/uploads/top_logo_2.png">
				<p style="font-size: 35px; margin-top: -20px; margin-left: -5px;">ПРОИЗВОДСТВО</p>
				<p style="font-size: 18px; margin-top: -40px; margin-left: -5px; color: blue;"><b>ПРЕСС - ФОРМ И ШТАМПОВ</b></p>
			</div>
			<div style="margin-left: 650px; margin-top: -300px;">
				<p style="margin-left:53px; font-size: 18px;"><b><span style="color: blue;">OOO «TEXNOPARK»</span>  MAZE MOLD</b></p>
				<p style="font-size: 14px; margin-left: -45px; margin-top: -20px;"><b>РУз. г.Ташкент,Яшнабадский р-н, ул.Элбек, 61-дом</b></p>
				<p style="font-size: 14px; margin-left: -168px; margin-top: -15px;"><b>Юнусабадский филиал “НБУ ВЭД РУз”Р/с: 20208000605090819023</b></p>
				<p style="font-size: 14px; margin-left: -118px; margin-top: -15px;"><b>МФО: 00836   ИНН: 306 493 973 ОКЭД: 27510  ОКПО: 28783966</b></p>
				<p style="font-size: 14px; margin-left: 193px; margin-top: -15px;"><b>Тел: +998 71210-20-20</b></p>
				<div style="margin-left: 100px; margin-top: -27px;">
					<img style=" width: 100px; height: 80px; margin-left: -10px; margin-top: 20px;"  src="../web/uploads/iso_90.png">					
					<img style=" width: 100px; height: 80px; margin-left: -10px; margin-top: 20px;"  src="../web/uploads/iso_90.png">					
					<img style=" width: 100px; height: 80px; margin-left: -10px; margin-top: 20px;"  src="../web/uploads/iso_90.png">					
				<div>
			</div>
		</div>
	</div>
	<hr>
	<hr style="margin-top: -5px;">

	<center style="margin-top: 50px;">
		<h2 style="color: rgba(128,0,128);"><b>С П Е Ц И Ф И К А Ц И Я :</b></h2>
	</center>

	<div style="margin-top: 1000px; margin-left: 30px;">
		<h3 style="color: rgba(128,0,128); "><b>Согласовано:</b><h3>
		<p>Зам.Директор по</p>
		<p style="margin-top: -15px;">Производству  Пресс-Форм и Штампов</p>
		<p style="margin-left: 500px;">Мирзакаримов X.X<p>
	</div>
	<div class="page_break"></div>

		<script src="bootstrap-4.6.0-dist/bootstrap-4.6.0-dist/js/bootstrap.min.js"></script>
	</body>
	</html>
';

$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('ra3', 'portrait');
// $dompdf->setPaper('a4', 'landscape');
// $pdf->setPaper(array(0,0,226.77,841.89), 'portrait');// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream("dompdf_out.pdf", array("Attachment" => false));

exit(0);


?>