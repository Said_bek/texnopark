<?php

use yii\helpers\Html;

// use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Buyurtmalar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">
    <div class="row">
        <div class="text-left col-md-6">
            <h1 class="m-0"><?= Html::encode($this->title) ?></h1>
        </div>

        <!--        <div class="text-right col-md-6">-->
        <!--            --><? //= Html::a("Buyurtma qo'shish", ['create'], ['class' => 'new_order btn btn-success']) ?>
        <!--        </div>-->
    </div>

    <style type="text/css">
        .dropdown-toggle {
            display: none;
        }
    </style>

        <table class="table table-bordered table-hover toggle-circle" id="exampleFootableFiltering"
               data-paging="true" style="text-align: center;" data-filtering="true">
            <thead>
            <tr>
                <th>#</th>
                <th>Xizmat Kategoriyasi</th>
                <th>Buyurtma qabul qlingan sana</th>
                <th>Telefon raqam 1</th>
                <th>Telefon raqam 2</th>
                <th>Kompaniya nomi</th>
                <th>Buyurtma qayerdan</th>
                <th>Narx taqdim etish sanasi</th>
                <th>Prioritet</th>
                <th>Buyurtma holati</th>
                <th>Narxi</th>
                <th>Tasdiq</th>
                <th>PDF</th>
                <th>Keyingi bo'limga o'tkazish</th>
                <th>Batafsil</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (isset($model) && !empty($model)){
                $i = 1;
                foreach ($model as $key => $value) {
                    ?>
                    <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $value['category'] ?></td>
                        <td><?= date('d.m.Y H:i', strtotime($value['enter_date'])); ?></td>
                        <td><?= $value['phone_number'] ?></td>
                        <td><?= $value['phone_number_2'] ?></td>
                        <td><?= $value['company_name'] ?></td>
                        <td><?php
                            if ($value['order_from'] == 0) {
                                echo '<span class = "badge badge-primary">IN</span>';
                            } else {
                                echo '<span class = "badge badge-default">OUT</span>';
                            }
                            ?></td>
                        <td><?= date('d.m.Y H:i', strtotime($value['deadline_price'])); ?></td>
                        <td><?php
                            if ($value['priority'] == 0) {
                                echo '<span class = "badge badge-success">Oddiy</span>';
                            } elseif ($value['priority'] == 1) {
                                echo '<span class = "badge badge-danger">Tezkor</span>';
                            } else {
                                echo '<span class = "badge badge-warning">Kechiktirilgan</span>';
                            }
                            ?></td>
                        <td><?php
                            if ($value['status'] == 0) {
                                echo '<span class = "badge badge-danger">Faol emas</span>';
                            } else {
                                echo '<span class = "badge badge-success">Faol</span>';
                            }
                            ?></td>
                        <td><?= $value['price'] ?></td>
                        <td><?php
                            if ($value['approved'] == 0) {
                                echo '<span class = "badge badge-danger">Tasdiqlanmagan</span>';
                            } else {
                                echo '<span class = "badge badge-success">Tasdiqlangan</span>';
                            }
                            ?></td>
                        <td>
                            <?php
                            echo "<span class='badge badge-primary' style='cursor: pointer;'><a style='color: white;' href='" . \yii\helpers\Url::to(['orders/pdf', 'id' => $value['order_id']]) . "'>PDF</a></span>";
                            ?>
                        </td>
                        <td>
                            <center>
                                <?php
                                if ($value['module_id'] == 3) {
                                    echo "<a class='btn btn-icon btn-success next_section' data-target='#examplePositionCenter' data-toggle='modal' data-id=" . $value['order_id'] . " style='margin-left: 10px; cursor: pointer;'><i class='icon wb-arrow-up' style='color: white'></i></a>";
                                } else {
                                    echo "<span class='badge badge-primary'>" . $value['module_name'] . "</span>";
                                }
                                ?>
                            </center>
                        </td>
                        <td>
                            <center>
                                <?php
                                echo "<span style='margin-right: 10px; cursor: pointer;'><a href='" . \yii\helpers\Url::to(['orders/view', 'id' => $value['order_id']]) . "'><i class='icon fa-eye'></i></a></span>";

                                echo "<span style='margin-left: 10px; cursor: pointer;'><a href='" . \yii\helpers\Url::to(['orders/update_order', 'id' => $value['order_id']]) . "'><i class='icon fa-pencil'></i></a></span>";
                                ?>
                            </center>
                        </td>
                    </tr>
                    <?php
                }
            }
            else{
            ?>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="16">Ma'lumot topilmadi!</td>
            </tr>
            </tfoot>
            <?php
            }
            ?>
        </table>
</div>

<style type="text/css">
    .footable-empty {
        display: none;
    }

    thead tbody {
        border: solid 1px black;
    }
</style>

<style>
    .orders-index{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        overflow: auto;
    }
</style>


<?php
$js = <<<JS

$(document).ready(function(){
    setTimeout(function(){
        $("#toggleMenubar > .nav-link").trigger("click");
    }, 500);
})

$(document).on('click', '.pdf', function() {
  var order_id = $(this).attr('order_id');
  $.ajax({
        url: '/orders/pdf',
        data: {
            order_id: order_id
        },
        type: 'get',
        dataType: 'json',
        success:function(response){
            if(response.status == "success"){
                
            }
        }
    })
});

$(document).on('click', '.new_order', function() {
  var order_id = $(this).attr('order_id');
  $.ajax({
        url: '/orders/category_docs',
        data: {
            order_id: order_id
        },
        type: 'get',
        dataType: 'json',
        success:function(response){
            if(response.status == "success"){
                
            }
        }
    })
});

$(document).on('click', '.next_section', function(){
    $('.modal-body').html('')
    $('.modal-footer .btn-primary').removeClass().addClass('btn btn-primary save_section')
    var id = $(this).attr('data-id')
    $.ajax({
        url: 'update-section',
        data: {
            s: false
        },
        dataType: 'json',
        type: 'get',
        success: function(response) {
            if (response.status == 'success') {
                $('.modal-body').html(response.content)
                $('.modal-header').html("Keyingi bo'limga o'tkazish")
                $('.modal-footer .btn-primary').addClass('save_section')
                $('.modal-footer .btn-primary').attr('data-id', id)
            }
        }
    })
})

$(document).on('click', '.save_section', function(){
    var id = $(this).attr('data-id')
    var module_id = $('input[name="options"]:checked').val();
    $.ajax({
        url: 'update-section',
        data: {
            s: true,
            id: id,
            module_id: module_id
        },
        dataType: 'json',
        type: 'get',
        success: function(response) {
            if (response.status == 'success') {
                location.reload()
            }
            if (response.status == 'error_mark') {
                $('#mark').addClass('is-invalid');
                $('.name_error').removeClass('hidden')  
            }
        }
    })
})


JS;
$this->registerJs($js);
?> 
