<label>Bo'limlar</label><br>
<div class="btn-group" data-toggle="buttons" role="group">
    <?php
        $a = 1;
        foreach ($model as $value){ ?>
            <label class="btn btn-outline btn-primary <?php if($a == 1) echo"active"; ?>">
                <input type="radio" name="options" autocomplete="off" value="<?= $value->id ?>" <?php if($a == 1) echo"checked=''"; ?>">
                <i class="icon wb-check text-active" aria-hidden="true"></i> <?= $value->title ?>
            </label>
        <?php
        $a++;
        }
    ?>
</div>
