<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = 'Buyurtma haqida batafsil';
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="orders-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <br>
    <table style="color: #76838f;" class="table table-bordered table-striped">
        <tr>
            <th>Ma'lumotlar</th>
            <th>Qiymatlar</th>
        </tr>
        <tr>
            <th>Ism</th>
            <td><?= $model['face_name'] ?></td>
        </tr>
        <tr>
            <th>Kontakt 1</th>
            <td><?= $model['phone_number'] ?></td>
        </tr>
        <tr>
            <th>Kontakt 2</th>
            <td><?= $model['phone_number_2'] ?></td>
        </tr>
        <tr>
            <th>Kompaniya nomi</th>
            <td><?= $model['company_name'] ?></td>
        </tr>
        <tr>
            <th>Email</th>
            <td><?= $model['email'] ?></td>
        </tr>
        <tr>
            <th>Manzili</th>
            <td><?= $model['country'] ?></td>
        </tr>
        <tr>
            <th>Manzili</th>
            <th><?= $model['country'] ?></th>
        </tr>
        <tr>
            <th>Loyiha nomi</th>
            <td><?= $model['order_name'] ?></td>
        </tr>
        <tr>
            <th>Kategoriya nomi</th>
            <td><?= $model['category'] ?></td>
        </tr>
        <tr>
            <th>Narx</th>
            <td><?= $model['price'] ?></td>
        </tr>
        <tr>
            <th>Buyurtma qayerdan</th>
            <td><?php
                if($model['order_from'] == 0){
                    echo '<span class = "badge badge-success">IN</span>';
                }
                else{
                    echo '<span class = "badge badge-default">OUT</span>';
                }
            ?></td>
        </tr>
        <tr>
            <th>Buyurtma darajasi</th>
            <td><?php
                if($model['priority'] == 0){
                    echo '<span class = "badge badge-default">Oddiy</span>';
                }
                elseif ($model['priority'] == 1) {
                    echo '<span class = "badge badge-success">Tezkor</span>';
                }
                else{
                    echo '<span class = "badge badge-warning">Kechiktirilgan</span>';
                }
             ?></td>
        </tr>
        <tr>
            <th>Buyurtma qabul sanasi</th>
            <td><?= date('d.m.Y H:i', strtotime($model['enter_date'])); ?></td>
        </tr>
        <tr>
            <th>Narx taqdim etish sanasi</th>
            <td><?= date('d.m.Y H:i', strtotime($model['deadline_price'])); ?></td>
        </tr>
    </table>
</div>
