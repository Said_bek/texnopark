
<?php

use yii\helpers\Html;


$this->title = 'Kategoriyalar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <div class="row">
        <div class="text-left col-md-6">
            <h1 class="m-0"><?= Html::encode($this->title) ?></h1>
        </div>

        <div class="text-right col-md-6">
            <button  class="btn btn-success float-right add" id="add_cat" data-target="#examplePositionCenter" data-toggle="modal">Kategoriya qo'shish</button>
        </div>

                <div class="modal fade" id="examplePositionCenter" aria-hidden="true" aria-labelledby="examplePositionCenter"
                     role="dialog" tabindex="-1">
                    <div class="modal-dialog modal-simple modal-center">
                        <div class="modal-content">
                            <div class="modal-header" style="font-size: 25px">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title"> Kategoryaga hujjat qo'shish</h4>
                            </div>
                            <div class="modal-body">
                                <p>Malumot yo'q!</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Yopish</button>
                                <button type="button" class="btn btn-primary">Saqlash</button>
                            </div>
                        </div>
                    </div>
                </div>

    </div>
    <hr>


    <div class="panel">
      <div class="panel-body">
        <table class="table table-bordered toggle-circle" id="exampleFootableFiltering" style="text-align: center;">
          <thead>
            <tr>
              <th>#</th>
              <th>Kategoriya nomi</th>
              <th>Hujjatlar</th>
            </tr>
          </thead>
          <tbody>
            <?php 
                if(isset($arr) && !empty($arr)){
                    $i = 1;
                    foreach ($arr as $key => $value) {
                        ?>
                            <tr style="height: 60px;">
                                <td><?= $i++ ?></td>
                                <td><?= $value['category'] ?></td>
                                <td style="padding-top: 20px;">
                                	<?php 
	                                    if(isset($value['list']) && !empty($value['list'])){
	                                      foreach ($value['list'] as $key => $list) {
	                                        if(isset($list['title']) && !empty($list['title'])){
	                                          echo "<span class='bg-blue-grey-100' style='padding: 10px; border-radius: 5px;   margin-left: 10px; '>".$list['title']." &nbsp <i class='wb-close del' cat_id='".$value['category_id']."' docs_id='".$list['id']."'  style='cursor: pointer;'></i></span>";
	                                        }
	                                        else{
	                                          echo "<span class='bg-blue-grey-100' style='padding: 10px; border-radius: 5px; margin-left: 10px; '> Hujjat mavjud emas </span>";                                        
	                                        }
	                                      }
	                                    }
                                	 ?>
                                </td>
                            </tr>
                        <?php
                    }
                }
             ?>
          </tbody>
        </table>
      </div>
    </div>
</div>




<?php
$js = <<<JS

$(document).on("click", '.del', function() {
    if(confirm('Ushbu hujjatni o`chirishni tasdiqlaysizmi') == true){
      var cat_id = $(this).attr('cat_id');
      var docs_id = $(this).attr('docs_id'); 
    }
$.ajax({
  data: {
     cat_id: cat_id,
     docs_id: docs_id
  }, 
  url: '/orders/del',
  type: 'get',
  dataType: 'json',
     success: function(response) {
         if (response.status == "success") {
             location.reload();
         }
         else{
          alert('Xatolik')
         }
       }
    })
});


$(document).on("click", '.add', function() {
    $(".modal-footer .btn-primary").removeClass().addClass("btn btn-primary save"); 
    $.ajax({ 
        url: '/orders/category_add',
        type: 'get',
        dataType: 'json',
             success: function(response) {
                 if (response.status == "success") {
                     $('.modal-body').html(response.content)
                     $('.modal-header').text('Kategoriya qo`shish')
                 }
             }
    })
});

$(document).on('change', '.mySelect2', function() {
      var sel = $(this).val()
       $('.save').attr('data-cat', sel)
})

$(document).on('change', '.mySelect3', function() {
      var sel = $(this).val()
       $('.save').attr('data-doc', sel)
})


$(document).on("click", '.save', function() {
    // var cat = $(this).attr('data-cat')
    var cat = $('#cat').val()
    var doc = $(this).attr('data-doc')
    $.ajax({
        data: {
            cat: cat,
            doc: doc,
        },  
        url: '/orders/category_save',
        type: 'get',
        dataType: 'json',
            success: function(response) {
                if (response.status == "success") {
                    location.reload();
                }
                else{
                    $("#examplePositionTop").modal('toggle');
                    alert('Xatolik');
                }
            }
    })
});

JS;
$this->registerJs($js);
?>  
