<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Yetkazib beruvchilar';
?>
<div class="supplier-index">
    <div class="row">
        <div class="text-left col-md-6">
            <h2 class="m-0"><?= Html::encode($this->title) ?></h2>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'format' => 'html',
                'label' => "Ismi",
                'headerOptions' => ['style' => 'color: #3e8ef7 !important;'],
                'value' => function ($data) {
                    $username = $data->name->username;
                    $name = (isset($username) and !empty($username)) ? $username : "Yetkazib beruvchi ismi mavjud emas!";
                    return $name;
                },
            ],
            [
                'format' => 'html',
                'label' => "Holati",
                'headerOptions' => ['style' => 'color: #3e8ef7 !important;'],
                'value' => function ($data) {
                    if ($data->status == 1)
                        $span = " <span class='badge badge-success'>Faol</span>";
                    else
                        $span = "<span class='badge badge-danger'>Faol emas</span>";
                    return $span;
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{status}',
                'buttons' => [
                    'status' => function ($url,$data) {
                        $iconClass = $data->status == 1 ? 'times' : 'check';
                        return Html::a('<i class="fa fa-'.$iconClass.'"></i>', $url);
                    }
                ],
            ],
        ],
    ]); ?>
</div>

<style>
    .supplier-index{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>
