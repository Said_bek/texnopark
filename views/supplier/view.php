<?php if (isset($model) and !empty($model)): ?>
    <?php foreach ($model as $value): ?>
        <div class="row mt-5">
            <div class="col-md-5">
                <label>Material nomi</label>
                <input type="text" value="<?= $value['material'] ?>" readonly class="form-control" name="">
            </div>
            <div class="col-md-5">
                <label>Marka nomi</label>
                <input type="text" value="<?= $value['mark'] ?>" readonly class="form-control" name="">
            </div>
            <div class="col-md-2">
                <label>Soni</label>
                <input type="text" value="<?= $value['quantity'] ?>" readonly class="form-control" name="">
            </div>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <span class="m-0 badge-lg badge badge-outline badge-danger">Ma'lumot mavjud emas</span>
<?php endif ?>
