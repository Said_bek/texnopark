<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="nak-orders-index">
    <div class="row">
        <div class="col-lg-6">
            <h2 class="m-0">Nakladnoylar</h2>
        </div>
    </div>

    <table class='table table-bordered table-striped mt-10'>
        <thead>
            <tr>
                <th>#</th>
                <th>Nakladnoy kodi</th>
                <th>Holati</th>
                <th>Batafsil</th>
            </tr>
        </thead>
        <tbody>
        <?php if (isset($model) and !empty($model)): ?>
            <?php $i = 1; foreach ($model as $key => $value): ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $value['code'] ?></td>
                    <?php if ($value['status'] == 0): ?>
                        <td>
                            <a href='<?= \yii\helpers\Url::to(['supplier/start', 'id' => $value['id']]) ?>' class='btn btn-icon btn-sm btn-primary'>
                                <i class='fa fa-hourglass' style='color: white'></i>
                            </a>
                        </td>
                    <?php elseif ($value['status'] == 1): ?>
                        <td>
                            <a href='<?= \yii\helpers\Url::to(['supplier/done', 'id' => $value['id']]) ?>' class='btn btn-icon btn-sm btn-success'>
                                <i class='fa fa-check' style='color: white'></i>
                            </a>
                        </td>
                    <?php elseif ($value['status'] == 2): ?>
                        <td><span class="badge badge-outline badge-primary">Tekshiruvda</span></td>
                    <?php elseif ($value['status'] == 3): ?>
                        <td><span class="badge badge-outline badge-success">Bajarildi</span></td>
                    <?php endif; ?>
                    <td class="w-100">
                        <a href='<?php \yii\helpers\Url::to(['nakladnoy/excel', 'id' => $value['id']]) ?>' class='btn btn-outline btn-sm btn-success'>
                            <i class='fa fa-file-excel-o'></i>
                        </a>
                        <a id="about" data-id="<?= $value['id'] ?>" data-target='#exampleNifty3dSign' data-toggle='modal' class='btn btn-sm btn-outline-dark'>
                            <i class='fa fa-info-circle'></i>
                        </a>
                    </td>
                </tr>
            <?php $i++; endforeach; ?>
        <?php else: ?>
            <tr>
                <td class='text-center' colspan='4'>Ma'lumot yo'q</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>

<style>
    .nak-orders-index {
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>

<?php
$js = <<<JS
    $(document).on('click', '#about', function(){
        $('.modal-body').html('')
        var id = $(this).attr('data-id')
        $.ajax({
            url: 'about',
            data: {
                id: id
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("Batafsil").css({fontSize: 23})
                }
            }
        })
    })
JS;
$this->registerJs($js);
?>