<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Nakladnoy */
/* @var $form yii\widgets\ActiveForm */
?>
    <div class="nakladnoy-form">
        <h2 class="m-0">Nakladnoy qo'shish</h2>
        <input type="hidden" id="count" value="1">
        <form class="div_form" action="#">
            <div class="row">
                <div class="col-lg-4 mt-20">
                    <label for="mySelect2" style="font-size: 15px">Kod kiriting</label>
                    <input type="text" id="code" class="form-control nakladnoy_code" name="selectForm[a][code]">
                    <small class="code_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                    <small class="code_same hidden text-danger" style="font-size: 12px">Bunday kod majvud!</small>
                </div>
                <div class="col-lg-4 mt-20">
                    <div class="select2-primary">
                        <label for="mySelect2" style="font-size: 15px">Yetkazib beruvchi tanlang</label>
                        <select <?php if (!isset($supplier) and empty($supplier)) echo "disabled"; ?>
                                class="form-control mySelect4" name="selectForm[a][supplier]" id="supplier"
                                data-plugin="select2">
                            <?php if (isset($supplier) and !empty($supplier)): ?>
                                <?php foreach ($supplier as $value): ?>
                                    <option value="<?= $value->id; ?>">
                                        <?= $value->name->username; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <option value="" selected>
                                    Yetkazib beruvchi mavjud emas!
                                </option>
                            <?php endif; ?>
                        </select>
                        <small class="supplier_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="example-wrap">
                        <div class="example datepair-wrap" data-plugin="datepair">
                            <div class="input-daterange-wrap">
                                <label for="">Sana</label>
                                <div class="input-daterange">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="icon wb-calendar" aria-hidden="true"></i>
                                    </span>
                                        </div>
                                        <input autocomplete="off" type="text" data-date-format='dd.mm.yyyy'
                                               id="datepicker" name="selectForm[a][datepicker]"
                                               class="datepicker price_date form-control datepair-date datepair-start"
                                               data-plugin="datepicker">
                                        <div>
                                            <small class="date_error hidden text-danger" style="font-size: 12px">Maydon
                                                bo'sh bo'lishi mumkin emas!</small>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="icon wb-time" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="timepicker form-control" id="timepicker"
                                               autocomplete="off" name="selectForm[a][timepicker]"
                                               data-plugin="clockpicker" data-autoclose="true">
                                        <div>
                                            <small class="time_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row float-right mr-1" style="margin-top: -40px !important">
                <button type="button" style="background: #f2353c" class="btn btn-danger minus">
                    <i class="fa fa-minus" aria-hidden="true"></i>
                </button>
                <button type="button" class="btn btn-success ml-5 plus">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
            </div>
            <div class="row mt-30 material_marks">
                <div class="col-lg-4 material">
                    <div class="select2-primary">
                        <label for="mySelect2" style="font-size: 15px">Material tanlang</label>
                        <select <?php if (!isset($material) and empty($material)) echo "disabled"; ?>
                                class="form-control select_material" name="selectForm[0][material]" id="material"
                                data-plugin="select2">
                            <?php if (isset($material) and !empty($material)): ?>
                                <option value="" disabled selected>Material tanlang</option>
                                <?php foreach ($material as $value): ?>
                                    <option value="<?= $value->id; ?>">
                                        <?= $value->name; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <option value="" selected>Material mavjud emas!</option>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4 mark_material">
                    <div class="select3-primary">
                        <label for="mySelect2" style="font-size: 15px">Marka tanlang</label>
                        <select disabled class="form-control mySelect5" id="mark" name="selectForm[0][mark]"
                                data-plugin="select2">
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 icon_div parameter">
                    <label for="mySelect2" style="font-size: 15px">Parameter</label>
                    <div class="parameter_div">
                        <i class="fa fa-times icon hidden"></i>
                        <input readonly type="text" id="parameter_input" class="form-control parameter_material"
                               name="selectForm[0][parameter_input]">
                    </div>
                </div>
                <div class="col-lg-2 icon_div unit">
                    <label for="mySelect2" style="font-size: 15px">O'lchov birligi</label>
                    <div class="unit_div">
                        <i class="fa fa-times icon hidden"></i>
                        <input readonly type="text" id="unit_input" class="form-control unit_material"
                               name="selectForm[0][unit_input]">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row price_and_count">
                        <div class="col-lg-3">
                            <label for="mySelect2" style="font-size: 15px; margin-top: 10px">Soni</label>
                            <div class="count_input d-flex">
                                <input type="number" id="count" class="form-control count_material"
                                       name="selectForm[0][count]" style="width: 500px !important">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-lg-12 mt-5">
                <button type="button" class="btn btn-success float-right mt-10 save_count">Saqlash</button>
            </div>
        </div>
    </div>

    <style>
        .nakladnoy-form {
            background-color: white;
            padding: 30px;
            -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
            -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
            box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        }

        .hidden {
            display: none;
        }

        .icon_div i {
            position: absolute;
        }

        .icon_div .icon {
            padding: 10px;
            min-width: 40px;
            margin-left: 23px;
            color: red !important;
        }

        .input-field {
            width: 100%;
            padding: 10px;
            text-align: center;
        }
    </style>

<?php
$js = <<<JS
    $(document).on('change', '#material', function(){
        var material = $(this).val()
        var _this = $(this)
        $.ajax({
            url: 'change-material',
            data: {
                material: material
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    _this.parent().parent().siblings('.mark_material').find('.select3-primary').find('#mark').prop("disabled", false);
                    _this.parent().parent().siblings('.col-lg-4').find('.select3-primary').find('#mark').html(response.content)
                    if (response.param == ''){
                        _this.parent().parent().siblings('.parameter').find('#parameter_input').val('')
                        _this.parent().parent().siblings('.parameter').find('.icon').removeClass('hidden')
                    } else {
                        _this.parent().parent().siblings('.parameter').find('.icon').addClass('hidden')
                        _this.parent().parent().siblings('.parameter').find('#parameter_input').val(response.param)
                    }
                    if (response.unit == ''){
                        _this.parent().parent().siblings('.unit').find('#unit_input').val('')
                        _this.parent().parent().siblings('.unit').find('.icon').removeClass('hidden')
                    } else {
                        _this.parent().parent().siblings('.unit').find('.icon').addClass('hidden')
                        _this.parent().parent().siblings('.unit').find('#unit_input').val(response.unit)
                    }
                }
            }
        })
    })
    
    $(document).on('click', '.plus', function(){
        var count = $('#count').val()
        $(this).attr('disabled', true)
        var _this = $(this)
        $.ajax({
            url: 'plus-input?count='+count,
            dataType: 'json',
            type: 'get',
            success: function(response) {
                $(_this).attr('disabled', false)
                if (response.status == 'success') {
                    $('#count').val(parseInt(count)+1)
                    $('.div_form').append(response.content);
                }
            }
        })
    })
    
    $(document).on('click', '.minus', function(){
        if ($('.material_marks').length > 1){
            $('.material_marks').last().remove();
            $('.hr').last().remove();
        }
    })
    
    function validateMaterial(){
        inp = $('.select_material')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                return false;
            }
        }
        return true;
    }
    
    function validateMark(){
        inp = $('.mySelect5')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                return false;
            }
        }
        return true;
    }
    
    function validateParameter(){
        inp = $('.parameter_material')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                    return false;
                }
            }
        return true;
    }
    
    function validateUnit(){
        inp = $('.unit_material')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                    return false;
                }
            }
        return true;
    }
    
    function validateCount(){
        inp = $('.count_material')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                    return false;
                }
            }
        return true;
    }
    
    $(document).on("click", '.save_count', function() {
        var form = $(".div_form");
        var code = $("#code").val();
        var supplier = $("#supplier").val();
        var date = $("#datepicker").val();
        var time = $("#timepicker").val();
        var d = new Date();
        if (code != '' && supplier != '' && date != '' && time != ''){
            $('#code').removeClass('is-invalid')
            $('.code_error').addClass('hidden')
            $('.code_same').addClass('hidden')
            $('#supplier').removeClass('is-invalid')
            $('.supplier_error').addClass('hidden')
            $('#datepicker').removeClass('is-invalid');
            $('.date_error').addClass('hidden')
            $('#timepicker').removeClass('is-invalid');
            $('.time_error').addClass('hidden')
            if (validateMaterial()){
                if (validateMark()){
                    if (validateParameter()){
                        if (validateUnit()){
                            if (validateCount()){
                                $.ajax({
                                    url: 'save-nakladnoy',
                                    data: form.serialize(),
                                    type: 'get',
                                    dataType: 'json',
                                    success: function(response) {
                                        if (response.status == "success") {
                                            window.location.replace("/nakladnoy/index");
                                        }
                                        if (response.status == "same_code") {
                                            $('#code').addClass('is-invalid');
                                            $('.code_same').removeClass('hidden')
                                        }
                                        if (response.status == "error_code") {
                                            $('#code').addClass('is-invalid')
                                            $('.code_error').removeClass('hidden')   
                                        }
                                        if (response.status == "same_material") {
                                            alert('Bir xil material kiritish mumkin emas!')
                                        }
                                        if (response.status == "error_supplier") {
                                            $('#supplier').addClass('is-invalid')
                                            $('.supplier_error').removeClass('hidden')  
                                        }
                                        if (response.status == "error_date") {
                                            $('#datepicker').addClass('is-invalid')
                                            $('.date_error').removeClass('hidden')
                                        }
                                        if (response.status == "error_time") {
                                            $('#timepicker').addClass('is-invalid')
                                            $('.time_error').removeClass('hidden')  
                                        }
                                        if (response.status == "time_small") {
                                            $('#timepicker').addClass('is-invalid')
                                            $('.time_small').removeClass('hidden')  
                                        }
                                        if (response.status == "date_small") {
                                            $('#datepicker').addClass('is-invalid')
                                            $('.date_small').removeClass('hidden')
                                        }
                                    }
                                })
                            } else {
                                alert('Material sonini kiritmadingiz!')   
                            }
                        } else {
                            alert("Bu material uchun o'lchov birligi mavjud emas!")        
                        }
                    } else {
                        alert('Bu material uchun parameter mavjud emas!')     
                    }
                } else {
                    alert('Marka tanlamadingiz!')
                }
            } else {
                alert('Material tanlamadingiz!')
            }
        } else{
            if (code == ''){
                $('#code').addClass('is-invalid');
                $('.code_error').removeClass('hidden')     
            } else {
                $('#code').removeClass('is-invalid');
                $('.code_error').addClass('hidden') 
                $('.code_same').addClass('hidden') 
            }
            if (supplier == ''){
                $('#supplier').addClass('is-invalid');
                $('.supplier_error').removeClass('hidden')  
            } else {
                $('#supplier').removeClass('is-invalid');
                $('.supplier_error').addClass('hidden')  
            }
            if (date == ''){
                $('#datepicker').addClass('is-invalid');
                $('.date_error').removeClass('hidden')  
            } else {
                $('#datepicker').removeClass('is-invalid');
                $('.date_error').addClass('hidden')  
            }
            if (time == ''){
                $('#timepicker').addClass('is-invalid');
                $('.time_error').removeClass('hidden')  
            } else {
                $('#timepicker').removeClass('is-invalid');
                $('.time_error').addClass('hidden')  
            }
        }
    });
JS;
$this->registerJs($js);
?>