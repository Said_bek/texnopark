<hr class="hr mt-40">
<div class="row mt-30 material_marks">
    <div class="col-lg-4 material">
        <div class="select2-primary">
            <label for="mySelect2" style="font-size: 15px">Material tanlang</label>
            <select <?php if(!isset($material) and empty($material)) echo"disabled"; ?> class="form-control select_material" name="selectForm[<?= $count ?>][material]" id="material" data-plugin="select2">
                <?php
                if (isset($material) and !empty($material)){ ?>
                    <option value="" disabled selected>Material tanlang</option>
                    <?php foreach ($material as $value){ ?>
                        <option value="<?php echo $value->id; ?>">
                            <?php echo $value->name; ?>
                        </option>

                    <?php }
                } else { ?>
                    <option value="" selected>
                        Material mavjud emas!
                    </option>
                <?php }
                ?>
            </select>
        </div>
    </div>
    <div class="col-lg-4 mark_material">
        <div class="select3-primary">
            <label for="mySelect2" style="font-size: 15px">Marka tanlang</label>
            <select disabled class="form-control mySelect5" id="mark" name="selectForm[<?= $count ?>][mark]" data-plugin="select2">
            </select>
        </div>
    </div>
    <div class="col-lg-2 icon_div parameter">
        <label for="mySelect2" style="font-size: 15px">Parameter</label>
        <div class="parameter_div">
            <i class="fa fa-times icon hidden"></i>
            <input readonly type="text" id="parameter_input" class="form-control parameter_material" name="selectForm[<?= $count ?>][parameter_input]">
        </div>
    </div>
    <div class="col-lg-2 icon_div unit">
        <label for="mySelect2" style="font-size: 15px">O'lchov birligi</label>
        <div class="unit_div">
            <i class="fa fa-times icon hidden"></i>
            <input readonly type="text" id="unit_input" class="form-control unit_material" name="selectForm[<?= $count ?>][unit_input]">
        </div>
    </div>
    <div class="col-lg-12">
        <div class="row price_and_count">
            <div class="col-lg-3">
                <label for="mySelect2" style="font-size: 15px; margin-top: 10px">Soni</label>
                <div class="count_input d-flex">
                    <input type="number" id="count" class="form-control count_material" name="selectForm[<?= $count ?>][count]" style="width: 500px !important">
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="issetInArr" name="selectForm[<?= $count ?>][isset]" value="0">
</div>