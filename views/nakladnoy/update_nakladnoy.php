<div class="update-nak">
    <form class="div_form" action="#">
        <div class="row">
            <div class="col-lg-4 mt-20">
                <label for="mySelect2" style="font-size: 15px">Kod kiriting</label>
                <input type="text" id="code" class="form-control nakladnoy_code" name="selectForm[a][code]" value="<?= $nakladnoy->code ?>">
                <small class="code_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                <small class="code_same hidden text-danger" style="font-size: 12px">Bunday kod majvud!</small>
            </div>
            <div class="col-lg-4 mt-20">
                <div class="select2-primary">
                    <label for="mySelect2" style="font-size: 15px">Yetkazib beruvchi tanlang</label>
                    <select <?php if(!isset($supplier) and empty($supplier)) echo"disabled"; ?> class="form-control mySelect4" name="selectForm[a][supplier]" id="supplier" data-plugin="select2">
                        <?php if (isset($supplier) and !empty($supplier)): ?>
                            <?php foreach ($supplier as $value): ?>
                                <option <?php if($nakladnoy->supplier_id == $value->id) echo"selected"; ?> value="<?= $value->id; ?>">
                                    <?= $value->name->username; ?>
                                </option>

                            <?php endforeach; ?>
                        <?php else: ?>
                            <option value="" selected>
                                Yetkazib beruvchi mavjud emas!
                            </option>
                        <?php endif; ?>
                    </select>
                    <small class="supplier_error hidden text-danger" style="font-size: 12px">Maydon bo'sh bo'lishi mumkin emas!</small>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="example-wrap">
                    <div class="example datepair-wrap" data-plugin="datepair">
                        <div class="input-daterange-wrap">
                            <label for="">Sana</label>
                            <div class="input-daterange">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="icon wb-calendar" aria-hidden="true"></i>
                                    </span>
                                    </div>
                                    <input value="<?= date('d.m.Y', strtotime($nakladnoy->enter_date))?>" disabled type="text" data-date-format='yyyy-mm-dd'  id="datepicker" name="selectForm[a][datepicker]" class="datepicker price_date form-control datepair-date datepair-start" data-plugin="datepicker">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="icon wb-time" aria-hidden="true"></i>
                            </span>
                                    </div>
                                    <input value="<?= date('H:i', strtotime($nakladnoy->enter_date))?>" disabled type="text" class="timepicker form-control" id="timepicker" name="selectForm[a][timepicker]" data-plugin="clockpicker" data-autoclose="true">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row float-right mr-1" style="margin-top: -35px !important">
            <button type="button" style="background: #f2353c" class="btn btn-danger minus"><i class="fa fa-minus" aria-hidden="true"></i></button>
            <button type="button" class="btn btn-success ml-5 plus"><i class="fa fa-plus" aria-hidden="true"></i></button>
        </div>
        <?php if (isset($model) and !empty($model)): ?>
            <?php $a = 0; foreach ($model as $valueNak): ?>
                <div class="row mt-30 material_marks_old">
                    <div class="col-lg-3 material">
                        <label for="mySelect2" style="font-size: 15px">Material tanlang</label>
                        <input readonly class="form-control select_material" value="<?= $valueNak['material'] ?>" >
                        <input type="hidden" class="form-control" name="selectForm[<?= $a ?>][material]" id="material" value="<?= $valueNak['material_id'] ?>">
                    </div>
                    <div class="col-lg-2 mark_material">
                        <label for="mySelect2" style="font-size: 15px">Marka tanlang</label>
                        <input readonly class="form-control mySelect5" value="<?= $valueNak['mark'] ?>" >
                        <input type="hidden" readonly class="form-control mySelect4" name="selectForm[<?= $a ?>][mark]" id="mark" value="<?= $valueNak['mark_id'] ?>">
                    </div>
                    <div class="col-lg-3 icon_div parameter">
                        <label for="mySelect2" style="font-size: 15px">Parameter</label>
                        <div class="parameter_div">
                            <input readonly type="text" id="parameter_input" class="form-control parameter_material" name="selectForm[<?= $a ?>][parameter_input]" value="<?= $valueNak['parameter'] ?>">
                        </div>
                    </div>
                    <div class="col-lg-2 icon_div unit">
                        <label for="mySelect2" style="font-size: 15px">O'lchov birligi</label>
                        <div class="unit_div">
                            <input readonly type="text" id="unit_input" class="form-control unit_material" name="selectForm[<?= $a ?>][unit_input]" value="<?= $valueNak['unit'] ?>">
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <label for="count_label" style="font-size: 15px">Soni</label>
                        <div class="count_input">
                            <input type="number" id="count" class="form-control count_material" name="selectForm[<?= $a ?>][count]" value="<?= $valueNak['quantity'] ?>">
                        </div>
                    </div>
                    <input type="hidden" id="issetInArr" name="selectForm[<?= $a ?>][isset]" value="1">
                    <div class="col-lg-1 mt-30">
                        <a style="background: #f2353c !important;" href="<?= \yii\helpers\Url::to(['nakladnoy/delete-update-nak', 'id' => $valueNak['nak_detail_id']]) ?>" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </div>
                </div>
            <?php $a++; endforeach; ?>
        <?php endif; ?>
        <input type="hidden" id="countInput" value="<?= $a ?>">
    </form>
    <div class="row">
        <div class="col-lg-12 mt-30">
            <button type="button" class="btn btn-success float-right mt-10 save_count" data-id="<?= $id ?>">Saqlash</button>
        </div>
    </div>
</div>

<style>
    .update-nak{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }

    .hidden {
        display: none;
    }

    .icon_div i {
        position: absolute;
    }
    .icon_div .icon {
        padding: 10px;
        min-width: 40px;
        margin-left: 23px;
        color: red !important;
    }
    .input-field {
        width: 100%;
        padding: 10px;
        text-align: center;
    }
</style>

<?php
$js = <<<JS
    function validateMaterial(){
        inp = $('.select_material')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                return false;
            }
        }
        return true;
    }

    function validateMark(){
        inp = $('.mySelect5')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                    return false;
                }
            }
        return true;
    }
    
    function validateParameter(){
        inp = $('.parameter_material')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                    return false;
                }
            }
        return true;
    }
    
    function validateUnit(){
        inp = $('.unit_material')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                    return false;
                }
            }
        return true;
    }
    
    function validatePrice(){
        inp = $('.price_material')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                    return false;
                }
            }
        return true;
    }
    
    function validateCount(){
        inp = $('.count_material')
        for ( var i = 0; i < inp.length; ++i )    {
            if (inp[i].value==''){
                    return false;
                }
            }
        return true;
    }
    
    $(document).on("click", '.save_count', function() {
        var form = $(".div_form")
        var code = $("#code").val()
        var supplier = $("#supplier").val()
        var id = $(this).attr('data-id')
        if (code != '' && supplier != ''){
            $('#code').removeClass('is-invalid')
            $('.code_error').addClass('hidden')
            $('.code_same').addClass('hidden')
            $('#supplier').removeClass('is-invalid')
            $('.supplier_error').addClass('hidden')
            if (validateMaterial()){
                if (validateMark()){
                    if (validateParameter()){
                        if (validateUnit()){
                            if (validatePrice()){
                                if (validateCount()){
                                    $.ajax({
                                        url: 'update-nak',
                                        data: form.serialize() + '&id='+id,
                                        type: 'get',
                                        dataType: 'json',
                                        success: function(response) {
                                            if (response.status == "success") {
                                                window.location.replace("/nakladnoy/index");
                                            }
                                            if (response.status == "same_code") {
                                                $('#code').addClass('is-invalid');
                                                $('.code_same').removeClass('hidden')
                                            }
                                            if (response.status == "error_code") {
                                                $('#code').addClass('is-invalid')
                                                $('.code_error').removeClass('hidden')   
                                            }
                                            if (response.status == "same_material") {
                                                alert('Bir xil material kiritish mumkin emas!')
                                            }
                                            if (response.status == "error_supplier") {
                                                $('#supplier').addClass('is-invalid')
                                                $('.supplier_error').removeClass('hidden')  
                                            }
                                            if (response.status == "error_date") {
                                                $('#datepicker').addClass('is-invalid')
                                                $('.date_error').removeClass('hidden')
                                            }
                                            if (response.status == "error_time") {
                                                $('#timepicker').addClass('is-invalid')
                                                $('.time_error').removeClass('hidden')  
                                            }
                                            if (response.status == "time_small") {
                                                $('#timepicker').addClass('is-invalid')
                                                $('.time_small').removeClass('hidden')  
                                            }
                                            if (response.status == "date_small") {
                                                $('#datepicker').addClass('is-invalid')
                                                $('.date_small').removeClass('hidden')
                                            }
                                        }
                                    })
                                } else {
                                    alert('Material sonini kiritmadingiz!')   
                                }
                            } else {
                               alert('Summa kiritmadingiz!') 
                            }
                        } else {
                            alert("Bu material uchun o'lchov birligi mavjud emas!")        
                        }
                    } else {
                        alert('Bu material uchun parameter mavjud emas!')     
                    }
                } else {
                    alert('Marka tanlamadingiz!')
                }
            } else {
                alert('Material tanlamadingiz!')
            }
        } else{
            if (code == ''){
                $('#code').addClass('is-invalid');
                $('.code_error').removeClass('hidden')     
            } else {
                $('#code').removeClass('is-invalid');
                $('.code_error').addClass('hidden') 
                $('.code_same').addClass('hidden') 
            }
            if (supplier == ''){
                $('#supplier').addClass('is-invalid');
                $('.supplier_error').removeClass('hidden')  
            } else {
                $('#supplier').removeClass('is-invalid');
                $('.supplier_error').addClass('hidden')  
            }
        }
    });

    $(document).on('change', '#material', function(){
        var material = $(this).val()
        var _this = $(this)
        $.ajax({
            url: 'change-material',
            data: {
                material: material
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    _this.parent().parent().siblings('.mark_material').find('.select3-primary').find('#mark').prop("disabled", false);
                    _this.parent().parent().siblings('.col-lg-4').find('.select3-primary').find('#mark').html(response.content)
                    if (response.param == ''){
                        _this.parent().parent().siblings('.parameter').find('#parameter_input').val('')
                        _this.parent().parent().siblings('.parameter').find('.icon').removeClass('hidden')
                    } else {
                        _this.parent().parent().siblings('.parameter').find('.icon').addClass('hidden')
                        _this.parent().parent().siblings('.parameter').find('#parameter_input').val(response.param)
                    }
                    if (response.unit == ''){
                        _this.parent().parent().siblings('.unit').find('#unit_input').val('')
                        _this.parent().parent().siblings('.unit').find('.icon').removeClass('hidden')
                    } else {
                        _this.parent().parent().siblings('.unit').find('.icon').addClass('hidden')
                        _this.parent().parent().siblings('.unit').find('#unit_input').val(response.unit)
                    }
                }
            }
        })
    })

    $(document).on('click', '.plus', function(){
        var count = $('#countInput').val()
        $(this).attr('disabled', true)
        var _this = $(this)
        $.ajax({
            url: 'plus-input?count='+count,
            dataType: 'json',
            type: 'get',
            success: function(response) {
                $(_this).attr('disabled', false)
                if (response.status == 'success') {
                    $('#countInput').val(parseInt(count)+1)
                    $('.div_form').append(response.content);
                }
            }
        })
    })
    
    $(document).on('click', '.minus', function(){
        $('.material_marks').last().remove();
        $('.hr').last().remove();
    })
    
JS;
$this->registerJs($js);
?>