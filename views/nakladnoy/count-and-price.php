<form action="#" class="div_form">
    <input type="hidden" id="nak_id" value="<?= $id ?>">
    <?php $i = 0; foreach ($model as $value) : ?>
        <div class="row mt-5">
            <input type="hidden" name="selectForm[<?= $i ?>][details]" value="<?= $value['id'] ?>">
            <div class="col-md-3">
                <input type="hidden" value="<?= $value['material_id'] ?>" name="selectForm[<?= $i ?>][material]">
                <label>Material</label>
                <input type="text" value="<?= $value['material'] ?>" readonly class="form-control" name="">
            </div>
            <div class="col-md-2">
                <input type="hidden" name="selectForm[<?= $i ?>][mark]"  value="<?= $value['mark_id'] ?>">
                <label>Marka</label>
                <input type="text" value="<?= $value['mark'] ?>" readonly class="form-control" name="">
            </div>
            <div class="col-md-2">
                <label>Soni</label>
                <input type="text" value="<?= $value['quantity'] ?>" readonly class="form-control" name="">
            </div>
            <div class="col-md-2">
                <label>Qabul</label>
                <input type="number" name="selectForm[<?= $i ?>][count]" class="form-control" name="">
            </div>
            <div class="col-md-3">
                <label>Narxi</label>
                <input type="number" name="selectForm[<?= $i ?>][price]" class="form-control" name="">
            </div>
        </div>
    <?php $i++; endforeach; ?>
</form>