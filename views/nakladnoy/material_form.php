<?php if (isset($mark) and !empty($mark)): ?>
    <?php foreach ($mark as $key => $value): ?>
        <option value="<?= $value['id']; ?>">
            <?= $value['name']; ?>
        </option>
    <?php endforeach; ?>
<?php else: ?>
    <option disabled value="" selected>Bu material uchun marka mavjud emas!</option>
<?php endif; ?>


<?php
$js = <<<JS
    $("#select4").select2({
        tags: true
    });
JS;
$this->registerJs($js);
?>