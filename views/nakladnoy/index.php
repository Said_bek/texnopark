<?php
    use yii\helpers\Html;
    /* @var $this yii\web\View */
    /* @var $dataProvider yii\data\ActiveDataProvider */

    $this->title = 'Nakladnoylar';
    $this->params['breadcrumbs'][] = $this->title;

    function differenceInHours($startdate,$enddate){
        $starttimestamp = strtotime($startdate);
        $endtimestamp = strtotime($enddate);
        $difference = abs($endtimestamp - $starttimestamp)/3600;
        return $difference;
    }
?>

<div class="nakladnoy-index">
    <div class="row">
        <div class="text-left col-md-6">
            <h1 class="m-0"><?= Html::encode($this->title) ?></h1>
        </div>

        <div class="col-md-6">
            <?= Html::a("Nakladoy qo'shish", ['create'], ['class' => 'btn btn-success float-right']) ?>
        </div>
    </div>

    <table class='table table-bordered table-striped mt-20'>
        <thead>
        <tr>
            <th>#</th>
            <th>Kod</th>
            <th>Yetkazib beruvchi</th>
            <th>Sana</th>
            <th>Material soni</th>
            <th>Holati</th>
            <th>Qabul qilish</th>
            <th>Excel</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
        if (isset($model) and !empty($model)){
            $i = 1;
            foreach ($model as $key => $value) {
                echo "<tr>";
                echo "<td>".$i."</td>";
                echo "<td>".$value['code']."</td>";
                echo "<td>".$value['username']."</td>";
                echo "<td>".date('Y-m-d H:i', strtotime($value['enter_date']))."</td>";
                $priceAndSum = "
                    SELECT 
                           SUM(mh.price) as price, 
                           count(mh.nakladnoy_id) as summ 
                    FROM nakladnoy AS n
                    INNER JOIN  material_history AS mh ON n.id = mh.nakladnoy_id
                    WHERE n.id = ".$value['id'];
                $model = Yii::$app->db->createCommand($priceAndSum)->queryOne();
                echo "<td>".$model['summ']."</td>";
                echo "<td>";
                    if ($value['status'] == 0){
                        echo "<span class='badge badge-dark'>Qabul qilinmadi</span>";
                    } else if ($value['status'] == 1) {
                        echo "<span class='badge badge-warning'>Qabul qilindi</span>";
                    } else if ($value['status'] == 2) {
                        echo "<span class='badge badge-primary'>Yetkazildi</span>";
                    } else if ($value['status'] == 3) {
                        echo "<span class='badge badge-success'>Tasdiqlandi</span>";
                    }
                $id = $value['id'];
                echo "</td>";
                    if ($value['status'] == 2){
                        echo "<td><a data-id='".$id."' class='btn btn-icon btn-sm btn-success confirm_nak' data-target='#examplePositionCenter' data-toggle='modal'><i class='fa fa-check' style='color: white'></i></a></td>";
                    } else {
                        echo "<td><span class='badge badge-outline badge-danger'>Kutilmoqda</span></td>";
                    }
                echo "<td><a href='/index.php/nakladnoy/excel?id=$id' class='btn btn-outline-success btn-sm'>
                            <i class='fa fa-file-excel-o'></i>
                        </a></td>";
                echo "<td class='text-center'>"; ?>
                    <?php
                    if (differenceInHours(date('Y-m-d H:i', strtotime($value['created_date'])), date('Y-m-d H:i')) < 24){ ?>
                        <a href='<?= \yii\helpers\Url::to(['nakladnoy/update-nakladnoy', 'id' => $id]) ?>' class='update' title='Update' aria-label='Update'><span class='fa fa-pencil'></span></a>
                        <a href='<?= \yii\helpers\Url::to(['nakladnoy/delete-nakladnoy', 'id' => $id]) ?>' class='ml-3' title='Delete'><span class='fa fa-trash'></span></a>
                    <?php }
                echo "</td>";
                echo "</tr>";
                $i++;
            }
        } else {
            echo "<tr>";
            echo  "<td class='text-center' colspan='8'>Ma'lumot yo'q</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>

</div>
<style>
    .nakladnoy-index{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>

<?php
$js = <<<JS
    $(document).on('click', '.confirm_nak', function(){
        $('.modal-body').html('')
        $('.modal-footer .btn-primary').removeClass().addClass('btn btn-primary')
        var id = $(this).attr('data-id')
        $.ajax({
            url: 'save-receive-nak',
            data: {
                n: false,
                id: id
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("Tasdiqlash")
                    $('.modal-footer .btn-primary').addClass('receive_nak')
                    $('.modal-footer .btn-primary').attr('data-id', id)
                }
            }
        })
    })
    
    $(document).on('click', '.receive_nak', function(){
        var id = $(this).attr('data-id')
        var form = $(".div_form");
        $.ajax({
            url: 'save-count-nak',
            data: form.serialize() + '&id='+id + '&n=true',
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    location.reload()
                }
            }
        })
        
    })
JS;
$this->registerJs($js);
?>