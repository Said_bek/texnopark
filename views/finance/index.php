<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stanoklar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-index">
    <div class="row">
        <div class="text-left col-md-6">
            <h2 class="m-0"><?= Html::encode($this->title) ?></h2>
        </div>

        <div class="col-md-6">
            <button class="btn btn-success float-right" id="addMachine" data-target="#examplePositionCenter" data-toggle="modal" type="button">
                Stanok qo'shish
            </button>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'format' => 'html',
                'label' => "Stanok narxi (1 soat uchun)",
                'headerOptions' => ['style' => 'color: #3e8ef7 !important;'],
                'value' => function ($data) {
                    return $data->price->price;
                },
            ],
            [
                'format' => 'html',
                'label' => "Holati",
                'headerOptions' => ['style' => 'color: #3e8ef7 !important;'],
                'value' => function ($data) {
                    if ($data->status == 1)
                        $span = " <span class='badge badge-success'>Faol</span>";
                    else
                        $span = "<span class='badge badge-danger'>Faol emas</span>";
                    return $span;
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{pencil}{delete-machine}',
                'buttons' => [
                    'pencil' => function ($url, $data) {
                        return Html::a('<i class="fa fa-pencil"></i>', '#', [
                            'class' => 'update',
                            'data-target' => '#examplePositionCenter',
                            'data-toggle' => 'modal',
                            'data-update' => $data->id
                        ]);
                    },
                    'delete-machine' => function ($url, $data) {
                        $iconClass = $data->status == 1 ? 'times' : 'check';
                        return Html::a('<i class="ml-2 fa fa-'.$iconClass.'"></i>', $url);
                    }
                ],
            ],
        ],
    ]); ?>

</div>

<style>
    .finance-index{
        background-color: white;
        padding: 30px;
        -webkit-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
        box-shadow: 5px 5px 7px -4px rgba(0, 0, 0, 0.15);
    }
</style>

<?php
$js = <<<JS
    $(document).on('click', '#addMachine', function(){
        $('.modal-body').html('')
        $('.modal-footer .btn-primary').removeClass().addClass('btn btn-primary save_machine')
        $.ajax({
            url: 'add-machine',
            data: {
                m: false
            },
            dataType: 'json',
            type: 'get',
            success: function(response) {
                if (response.status == 'success') {
                    $('.modal-body').html(response.content)
                    $('.modal-header').html("Stanok qo'shish")
                    $('.modal-footer .btn-primary').addClass('save_machine')
                }
            }
        })
    })

    $(document).on('click','.save_machine',function(){
        var this_el = $(this)
        this_el.prop('disabled',true)
        let name = $('#machine_name').val()
        let price = $('#machine_price').val()

        $.ajax({
            url: 'add-machine',
            dataType: 'json',
            type: 'GET',
            data:{
                name: name,
                price: price,
                m: true
            },
            success: function (response) {
                if (response.status == 'success') {
                    window.location.reload()
                }
                if(response.status == 'error_name'){
                    $('#machine_name').css('border-color','#dd4b39')
                    $('#machine_name').siblings('.p1').html(`<small style="color: #dd4b39;">Maydon bo'sh bo'lishi mumkin emas!</small>`)
                    this_el.prop('disabled',false)
                }
                if(response.status == 'error_price'){
                    $('#machine_price').css('border-color','#dd4b39')
                    $('#machine_price').siblings('.p2').html(`<small style="color: #dd4b39;">Maydon bo'sh bo'lishi mumkin emas!</small>`)
                    this_el.prop('disabled',false)
                }
            }
        });
    })

    $(document).on('click','.update',function(){
        $('.modal-body').html('')
        $('.modal-footer .btn-primary').removeClass().addClass('btn btn-primary update-save-machine')

        let id = $(this).attr('data-update')
        $.ajax({
            url: 'update-machine',
            dataType: 'json',
            type: 'GET',
            data: {
                id: id,
                update: false
            },
            success: function (response) {
                if (response.status == 'success') {
                    $(".modal-body").html(response.content)
                    $(".modal-header").html(response.header)
                    $(".modal-footer .btn-primary").addClass("update-save-machine")
                    $('.update-save-machine').attr('data-update',id)
                }
            }
        });
    })


    $(document).on('click','.update-save-machine',function(){
        var this_el = $(this)
        this_el.prop('disabled',true)
        let name = $('#machine_name').val()
        let price = $('#machine_price').val()
        let id = $(this).attr('data-update')

        $.ajax({
            url: 'update-machine',
            dataType: 'json',
            type: 'GET',
            data:{
                name: name,
                price: price,
                id: id,
                update: true
            },
            success: function (response) {
                if (response.status == 'success') {
                    window.location.reload()
                }
                if(response.status == 'error_name'){
                    $('#machine_name').css('border-color','#dd4b39')
                    $('#machine_name').siblings('.p1').html(`<small style="color: #dd4b39;">Maydon bo'sh bo'lishi mumkin emas!</small>`)
                    this_el.prop('disabled',false)
                }
                if(response.status == 'error_price'){
                    $('#machine_price').css('border-color','#dd4b39')
                    $('#machine_price').siblings('.p2').html(`<small style="color: #dd4b39;">Maydon bo'sh bo'lishi mumkin emas!</small>`)
                    this_el.prop('disabled',false)
                }
            }
        });
    })
JS;
$this->registerJs($js);
?>